---
title: "KDE Free Qt Foundation"
hidden: true
---

## Trolltech and KDE Free Qt Foundation Announce Amended and Restated Software License Agreement

(July 23, 2004) <a href="http://qt.nokia.com/">Trolltech</a>,
developer of Qt, the leading cross-platform development framework upon which <a
href="/">KDE</a> is
based, and the <a href="../kdefreeqtfoundation">KDE
Free Qt Foundation</a> (the <em>Foundation</em>) today announced the signing
of an <a
href="../kdefreeqtfoundation#updated_agreement">Amended
and Restated Software License Agreement</a>
(the <em>Agreement</em>).

The Foundation was formed in 1998 by Trolltech and <a
href="http://ev.kde.org/">KDE e.V.</a>, which represents KDE in
certain legal and financial matters, to provide assurances to free software
developers developing with Qt should Qt development cease.

The revised Agreement continues to honour the original purposes of the
Foundation. In particular, should Trolltech ever discontinue making regular
releases of the Qt Free Edition
for any reason - including a buyout or merger of Trolltech or the liquidation
of Trolltech - the Qt Free Edition will be released under the BSD license and
optionally under one or more other Open Source Licenses designated by the
Board of the Foundation.

The amendments are aimed at modernizing the Agreement in light of
developments occurring after the original agreement was executed - including
the release of the Qt Free Edition under the <a
href="http://www.gnu.org/copyleft/gpl.html">GPL</a> and <a
href="http://doc.trolltech.com/3.0/license.html">QPL</a> in October 2000 -
and at improving the accuracy of the parties' understanding, thereby ensuring
that the Agreement continues to operate in the best interest of both parties.
The major amendments include improved precision in the definitions of the Qt
product releases, the clarification of licensing terms, and the addition of
possible termination of the Agreement under certain limited circumstances.

&quot;Third-party software development is a crucial element of KDE's
continued future success, and the restated Agreement encourages both Open
Source and proprietary software developers and enterprises to develop with
KDE and/or Qt on the X Window System by providing a practical solution should
Trolltech be unable to continue developing Qt&quot;, explained Martin Konold,
a KDE
board member on the Foundation and a key figure in the formation of the
Foundation and execution of the <a
href="../kdefreeqtfoundation#updated_agreement">original
agreement</a> in 1998. &quot;As with the release of Qt under the GPL
and QPL, we are very delighted that Trolltech has again demonstrated its
ongoing support of, and commitment to, Open Source software development, and
to the KDE project in particular.&quot;

&quot;Maintaining a close connection with the Open Source community is
essential to the success of Trolltech,&quot; said Matthias Ettrich, director
of software tools development at Trolltech. &quot;The amended Agreement
provides an excellent framework for a continued and mutually rewarding
relationship between Trolltech and the KDE Free Qt Foundation.&quot;

### About the KDE Free Qt Foundation

The KDE project, via KDE e.V., and Trolltech, the creators of Qt,
announced the formation of the <em>KDE Free Qt Foundation</em> in June 1998.
The purpose of the Foundation is to assure the availability of Qt for free
software development now and into the future. The Foundation is controlled by
a Board, which consists of two KDE e.V. representatives and two Trolltech
representatives; in many cases, the KDE e.V. representatives break a voting
tie. The governing document of the Foundation is available online (<a
href="../images/kdefreeqt5.png">Page 1</a>, <a
href="../images/kdefreeqt6.png">Page 2</a>).

For further information, please contact:

Trolltech AS
Tonje Sund
+47 21 60 48 78
press@trolltech.com

KDE
Martin Konold
+49 7071 940353
press&#064;&#x6b;&#100;&#0101;&#046;o&#x72;g
