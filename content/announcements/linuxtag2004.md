---
custom_about: true
custom_contact: true
title: "KDE: Large Presence at LinuxTag 2004"
hidden: true
---

The KDE Team today announced its plans for LinuxTag 2004, the biggest Linux
and Open Source exhibition in Europe (last year 19500 visitors and 159
exhibitors attended). LinuxTag 2004 will take place from June 23 to June 26
in Karlsruhe, Germany. 

The KDE Project's primary focus this year will cover such topics as "KDE --
The Integrative Desktop", "Kiosk/NoMachine: KDE on Enterprise Desktops and
Thin Clients ", "Accessibility", "KDE PIM / Kontact -- Groupware for Linux",
"KDevelop and Qt-Forum", "KDE EDU -- School, Science &amp; Edutainment" and
"KOffice -- Linux in the Office".

Besides staffing a KDE booth where users can see the latest KDE, ask questions
and mingle with KDE developers, KDE developers will make numerous
presentations targeted at users and developers. The conference program
features more than a dozen KDE-related talks about exciting topics like KDE
Usage/(Enterprise) Deployment, KDE &amp; Qt Development, KOffice, Thinclients,
NoMachine, Knoppix, usability and successful examples for migrations to the
KDE desktop in the government.

Many of these talks will be held by notable KDE developers, e.g. David Faure,
Fabrice Mous, Ariya Hidayat, Lucijan Busch, Ralf Nolden, Kurt Pfeifle, Daniel
Molkentin, Klas Kalass, Eva Brucherseifer and Torsten Rahn. 

See the <a href="http://www.linuxtag.org/cc/schedule.pl">LinuxTag Talk Schedule 2004</a>
for more information.

At the Office Productivity Booth Franz Schmid will present the latest
developments concerning the DTP application Scribus.

We invite everybody to meet more than 40 KDE developers, KDE enthusiasts, the
members of the <a href="http://opie.handhelds.org">Opie</a> and
<a href="http://www.qt-forum.org">Qt-Forum</a> team  and of course our mascot
Konqi at the KDE booth. 

You find the KDE project on booth D124 and the Office Productivity Booth at
A102.

For more information on LinuxTag, see <a href="http://www.linuxtag.org/">http://www.linuxtag.org</a>.
For more information on KDE, see <a href="/">http://www.kde.org</a>.
For information relating to this press release, please contact Torsten Rahn &lt;r&#97;&#104;n&#00064;k&#x64;e&#x2e;o&#x72;g&gt;.

