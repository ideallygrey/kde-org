---
qtversion: 5.15.2
date: 2024-02-10
layout: framework
libCount: 83
---


### Breeze Icons

* Update Google icon (bug 462165)

### Extra CMake Modules

* ECMUninstallTarget: port generated code away from deprecated exec_program

### KCalendarCore

* src/incidence.cpp - fix infinite looping

### KCoreAddons

* Add isProcExists func to check if /proc exists
* Determine UNIX process if "/proc" does not exist

### KDeclarative

* Show GridDelegate labels as plaintext (bug 480106)

### KHolidays #

* holiday_ie_en-gb - Add St Brigid's Day (bug 479832)

### KIconThemes

* CI: Don't require Windows test to pass

### KIO

* KDirModel: Consider invalid roots are local fs (bug 477039)
* slavebase: abort mimetype emission when the worker was terminated (bug 474909)
* KDirModel: Allow expanding network directories in file picker again (bug 479531)
* KCoreDirLister: updateDirectory: update parent folder if it is listed (bug 440712)
* copyjob: Fix implicitly skipping files when copying (bug 479082)

### KTextEditor

* Add parent widget for diff dialogs

### KWallet Framework

* Emit the walletCreated signal in the KWalletD::pamOpen function if a new wallet is created during its call

### Prison

* Enable exceptions for videoscannerworker.cpp

### Security information

The released code has been GPG-signed using the following key:
pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure <faure@kde.org>
Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
