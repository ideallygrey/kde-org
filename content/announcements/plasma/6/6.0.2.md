---
date: 2024-03-12
changelog: 6.0.1-6.0.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Fix sending window to all desktops. [Commit.](http://commits.kde.org/kwin/dbf1edcc4185819aae95fcb3d078574ce2019f67) Fixes bug [#482670](https://bugs.kde.org/482670)
+ Folder Model: Handle invalid URL in desktop file. [Commit.](http://commits.kde.org/plasma-desktop/0264726f8720d3093bd3ba10f6107197b4f90be3) Fixes bug [#482889](https://bugs.kde.org/482889)
+ Fix panels being set to floating by upgrades. [Commit.](http://commits.kde.org/plasma-workspace/7f0e8e43de4155f5fb138aae6379c3fbef5570e3)
