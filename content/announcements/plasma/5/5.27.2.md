---
date: 2023-02-28
changelog: 5.27.1-5.27.2
layout: plasma
video: false
asBugfix: true
draft: false
---

+ Discover: don't claim 3rd-party repos are part of the OS on Debian derivatives. [Commit.](http://commits.kde.org/discover/3fb783b4fdb95a326307e053c87d54c1f5489b84) 
+ Dr Konqi: add Plasma Welcome to mappings file. [Commit.](http://commits.kde.org/drkonqi/e04f013931783c7e1ee7cce5d2a74a811b76c09a)
+ Sddm: Focus something useful when switching between alternative login screens. [Commit.](http://commits.kde.org/plasma-workspace/fd67273f299f340c5e225f5f505c8559a0712066)
