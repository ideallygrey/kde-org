---
aliases:
- ../../plasma-5.20.5
changelog: 5.20.4-5.20.5
date: 2021-01-05
layout: plasma
video: true
asBugfix: true
---

+ Plasma NM: Fix password entry jumping to different networks with wifi scanning, by pausing the scan when appropriate. [Commit.](http://commits.kde.org/plasma-nm/21a410ff77049e996df5fdc35215c4b30d893ccc) 
+ Plasma PA: Read text color from proper theme. [Commit.](http://commits.kde.org/plasma-pa/099e925c879ece8f90734ea66c0878bc174c9608) 
+ Plasma Workspace: Move keyboard positioning in the keyboard itself. [Commit.](http://commits.kde.org/plasma-workspace/8c267852a56c74c14a471d266f87ac867b8276d9) Fixes bug [#427934](https://bugs.kde.org/427934)
