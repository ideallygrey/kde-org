---
aliases:
- ../../plasma-5.20.2
changelog: 5.20.1-5.20.2
date: 2020-10-27
layout: plasma
video: true
asBugfix: true
---

+ Daemon: improve consistency of the lid behaviour. [Commit.](http://commits.kde.org/kscreen/420c60adfcb95b09ab352b614fdd2dc2a66dcfa8) 
+ Fix bug: Some user profile fields won't apply unless they all have unique new values. [Commit.](http://commits.kde.org/plasma-desktop/3308dee942d1982a0aba68fc41368a284afc8e21) Fixes bug [#427348](https://bugs.kde.org/427348)
