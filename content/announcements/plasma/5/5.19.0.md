---
aliases:
- ../../plasma-5.19.0
changelog: 5.18.5-5.19.0
date: 2020-06-09
layout: plasma
title: 'Plasma 5.19: A more Polished Plasma'
video: true
---

{{% i18n_date %}}

Plasma 5.19 is out! If we gave alliterative names to Plasma releases, this one could be "Polished Plasma". The effort developers have put into squashing bugs and removing annoying papercuts has been immense.

In this release, we have prioritized making Plasma more consistent, correcting and unifying designs of widgets and desktop elements; worked on giving you more control over your desktop by adding configuration options to the System Settings; and improved usability, making Plasma and its components easier to use and an overall more pleasurable experience.

Read on to discover all the new features and improvements of Plasma 5.19…

## Plasma Desktop and Widgets

The first change you will see is the new wallpaper. The eye-catching <I>Flow</I> background designed by Sandra Smukaste brings a dash of light and color to your desktop. Of course, you can choose something different by right-clicking on your desktop and clicking on "Configure desktop..." from the pop-up menu. Notice how you can now see the name of the creators of each desktop wallpaper when you go to pick one. Another personalization detail you will probably appreciate is that there is a completely new collection of photographic avatars to choose from when setting up your user.

{{<figure src="/announcements/plasma/5/5.19.0/user-avatars.png" alt="Completely New User Avatars" caption="Completely New User Avatars" width="600px" >}}

Some of the changes are more subtle. However, the panel spacer, for example, the invisible element that helps place components on the panel, can now automatically center widgets, and Plasma 5.19 also incorporates a consistent design and header area for system tray applets as well as notifications. These are things you may not immediately notice, but contribute to making the design of the desktop subliminally more visually attractive. In a similar vein, GTK3 apps immediately apply a newly selected color scheme, GTK2 apps no longer have broken colors and we have increased the default fixed-width font size from 9 to 10, making all the text easier to read.

{{<figure src="/announcements/plasma/5/5.19.0/system-tray-applets.png" alt="Consistent System Tray Applets" caption="Consistent System Tray Applets" width="600px" >}}  

We have refreshed the look of the media playback applet in the System Tray and, related to that, you now have more control over the visibility of the volume controls. Task Manager tooltips have also been overhauled, the System monitor widgets have all been rewritten from scratch and Sticky notes get several usability improvements.

{{<figure src="/announcements/plasma/5/5.19.0/sensors.png" alt="Rewritten System Monitor Widgets" caption="Rewritten System Monitor Widgets" width="600px" >}}


## System Settings

Users that want more control over the file indexing process will appreciate the new configurable file indexing options for individual directories. You can also completely disable indexing for hidden files if you so wish. If you use Wayland, you will also appreciate the new option that lets you configure the mouse and touchpad scroll speed.

Intent on improving not only features, but also the look and feel of Plasma, the Default Applications, Online Accounts, Global Shortcuts, KWin Rules and Background Services settings pages have all been overhauled and we have added lots of small improvements to the font configuration.

{{<figure src="/announcements/plasma/5/5.19.0/redesigned-kcms.png" alt="Redesigned Settings Pages" caption="Redesigned Settings Pages" width="600px" >}}

Increasing usability throughout is one of Plasma 5.19's main objectives, so to make reaching the section you need easier, when you launch System Settings modules from within KRunner or the application launcher, the complete System Settings app launches on the page you asked for.

{{< video src-webm="/announcements/plasma/5/5.19.0/krunner.webm" caption="Full System Settings App Is Now Launching" >}}

Other small changes that make your life better are that the Display settings page now shows the aspect ratio for each available screen resolution and you now have a more granular control over the animation speed for your desktop effects.

## Info Center

Consistency is an important aspect of a well-designed desktop, and that is why the Info Center application has been redesigned with a look and feel that is consistent with the System Settings. The Info Center also gains a feature that lets you see information about your graphics hardware.

{{<figure src="/announcements/plasma/5/5.19.0/kinfocenter.png" alt="Redesigned Info Center" caption="Redesigned Info Center" width="600px" >}}

## KWin Window Manager

The KWin window manager includes subsurface clipping. This feature greatly reduces the flickering in many apps, making them less of a strain on the eyes. In similar news, the icons in titlebars have also been recolored to fit the color scheme making them much easier to see.

{{<figure src="/announcements/plasma/5/5.19.0/recoloured-icons.png" alt="Icon Recoloring in the Titlebar" caption="Icon Recoloring in the Titlebar" width="600px" >}}

A nice new feature in Wayland is that screen rotation for tablets and convertible laptops now works.

## Discover

Discover, the application that lets you add, remove and update software, has a new feature that makes removing Flatpak repositories much easier. Discover also now displays the app version. This is helpful for example when there is more than one version of the application you are looking for. You can choose the version that contains the features you need; or a version that is older, but more stable; or the one that works better with your set up.

{{<figure src="/announcements/plasma/5/5.19.0/discover.png" alt="Flatpak Repository Removal in Discover" caption="Flatpak Repository Removal in Discover" width="600px" >}}

## KSysGuard

The version of KSysGuard shipped with Plasma 5.19 supports systems with more than 12 CPU cores.
