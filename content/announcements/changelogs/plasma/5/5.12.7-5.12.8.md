---
aliases:
- /announcements/plasma-5.12.7-5.12.8-changelog
hidden: true
plasma: true
title: Plasma 5.12.8 Complete Changelog
type: fulllog
version: 5.12.8
---

### <a name='breeze' href='https://commits.kde.org/breeze'>Breeze</a>

- Fix uncentered crosshairs. <a href='https://commits.kde.org/breeze/1bc49c9551ec1a7f3db70cd303b13d826dbe2477'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17038'>D17038</a>
- Improve contrast for crosshair cursors. <a href='https://commits.kde.org/breeze/39c5b09297cc74a0dac78a3d65ff9c3a4836fa13'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400110'>#400110</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16861'>D16861</a>

### <a name='breeze-gtk' href='https://commits.kde.org/breeze-gtk'>Breeze GTK</a>

- Set the default cursor theme to breeze_cursors. <a href='https://commits.kde.org/breeze-gtk/dcacaa1d4f7e2a591de64889ae108e9c07054b1c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17187'>D17187</a>
- GTK theme treeview style typo/bug fix. <a href='https://commits.kde.org/breeze-gtk/765e5b21fb501ba11d9b909931eb9c0c86752eaf'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16331'>D16331</a>

### <a name='kdeplasma-addons' href='https://commits.kde.org/kdeplasma-addons'>Plasma Addons</a>

- [weather] Fix weather Notices tab not showing. <a href='https://commits.kde.org/kdeplasma-addons/46204a5e2a84b184d41d80ff469a5797a0dbf16e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18936'>D18936</a>
- [KonsoleProfiles applet] Fix navigating with the keyboard. <a href='https://commits.kde.org/kdeplasma-addons/91ee9cc72a490ccaf931c49229a9b8d2303b8e65'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15877'>D15877</a>

### <a name='kinfocenter' href='https://commits.kde.org/kinfocenter'>Info Center</a>

- Increase default window size so nothing gets cut off by default. <a href='https://commits.kde.org/kinfocenter/9f546137ec6b8b92434136a7f74773e207196f98'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364767'>#364767</a>

### <a name='kscreen' href='https://commits.kde.org/kscreen'>KScreen</a>

- Calculate screen scaling dynamically, so it always fits to the page. <a href='https://commits.kde.org/kscreen/ca4d74acb2aeba1a5fcddf0cfdbd095054158808'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18093'>D18093</a>
- Don't lose the widget layout after Default is pressed. <a href='https://commits.kde.org/kscreen/3e90b446c121acf3252de67247573253f5d15aee'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18121'>D18121</a>
- Redraw the slider if modes have changed. <a href='https://commits.kde.org/kscreen/32e95fad0d512d1ca57332640881fc4c21b744be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17686'>D17686</a>

### <a name='kscreenlocker' href='https://commits.kde.org/kscreenlocker'>KScreenlocker</a>

- Force software rendering when greeter crashed. <a href='https://commits.kde.org/kscreenlocker/e1d676c46e5060f06176cdc050b9edc23306812b'>Commit.</a>

### <a name='kwin' href='https://commits.kde.org/kwin'>KWin</a>

- [effects/startupfeedback] Fix shader. <a href='https://commits.kde.org/kwin/bc5978b75944895db2c49c2ed0e032527024bef4'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18757'>D18757</a>
- Save the correct value of noborder property. <a href='https://commits.kde.org/kwin/64115d4de231a100adf1af7cddb9bd21604851b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/403948'>#403948</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18756'>D18756</a>
- Fix flickering with Qt 5.12. <a href='https://commits.kde.org/kwin/5d63b9c05bbe0c6545b3eeea98d95b40f800fb55'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18366'>D18366</a>
- [effects/presentwindows] Avoid potential freeze during fill-gaps. <a href='https://commits.kde.org/kwin/4348cd56834cb17da5aa9d95d16ddc27bf39e0e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/364709'>#364709</a>. Fixes bug <a href='https://bugs.kde.org/380865'>#380865</a>. Fixes bug <a href='https://bugs.kde.org/368811'>#368811</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16278'>D16278</a>
- [effects/desktopgrid] Specify screen projection matrix when drawing moving window. <a href='https://commits.kde.org/kwin/408ed80604bb52870469a4f76704c224e15c02aa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361371'>#361371</a>. Fixes bug <a href='https://bugs.kde.org/364509'>#364509</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16430'>D16430</a>
- [kcmkwin/ruleswidget] Disable "Detect" button when countdown is running. <a href='https://commits.kde.org/kwin/980e390743c153966dd364f4a0ebaadd8d1b03a5'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399644'>#399644</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16124'>D16124</a>

### <a name='libkscreen' href='https://commits.kde.org/libkscreen'>libkscreen</a>

- Notify also if modes have changed. <a href='https://commits.kde.org/libkscreen/5422a4a8b120139b41febe3bb6a4ef01806ca509'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17685'>D17685</a>

### <a name='plasma-desktop' href='https://commits.kde.org/plasma-desktop'>Plasma Desktop</a>

- [Kickoff] Return Kickoff to Favorites page after running a search. <a href='https://commits.kde.org/plasma-desktop/82b904bce54b971f1c74133fcfba23cdc2b914b3'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18848'>D18848</a>
- Use Dialog's visibility directly rather than relying on the mainItem's. <a href='https://commits.kde.org/plasma-desktop/67d8ac416ff6933b082b84ec6f30c2c4e90a0500'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18844'>D18844</a>
- Fix new file creation leading to dupe items on a fresh view. <a href='https://commits.kde.org/plasma-desktop/4a3fbf9116f588f6f2dedfd9481ec7298528dafd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401023'>#401023</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18182'>D18182</a>
- Check icon positions after move. <a href='https://commits.kde.org/plasma-desktop/e2aa89898db3bfe05f0af1a1ab064542f095620f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/402574'>#402574</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17809'>D17809</a>
- Defer initial positions apply until listing is complete. <a href='https://commits.kde.org/plasma-desktop/aaebb51077aef6c5a5b974a38958e23366e357f2'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/354802'>#354802</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18598'>D18598</a>
- Fixed incorrect tooltip colors applied to GTK2 applications leading to unreadable text. <a href='https://commits.kde.org/plasma-desktop/c6bab929ac252c053f46d1ddd07d9cc421db22bd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355540'>#355540</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18482'>D18482</a>
- [KRDB] Write correct tooltip colors into gtkrc in kcminit. <a href='https://commits.kde.org/plasma-desktop/c394dc46fb73aaaf9c4a11a2596e69c0c474299d'>Commit.</a> See bug <a href='https://bugs.kde.org/355540'>#355540</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18482'>D18482</a>
- [Date & Time KCM] Fix clock display with fractional scale factor. <a href='https://commits.kde.org/plasma-desktop/985acbb7465739627b6c8603442b6fd5c396d684'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/396936'>#396936</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18340'>D18340</a>
- Add an X-DocPath link to the Activities KCM. <a href='https://commits.kde.org/plasma-desktop/4110ed27c1265ae76c468eddf0f116c1694f54be'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18145'>D18145</a>
- [Activities KCM] Fix patch to not delete default activity. <a href='https://commits.kde.org/plasma-desktop/ae8483efa40f4d47b3143405f0757a7edc126984'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397887'>#397887</a>. Phabricator Code review <a href='https://phabricator.kde.org/D18159'>D18159</a>
- [Activities KCM] Disable delete button when there's only one activity. <a href='https://commits.kde.org/plasma-desktop/eafb35076fa3e88951712a7f5bed2a33ccb7b4e0'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397887'>#397887</a>
- [Activities KCM] vertically center the buttons. <a href='https://commits.kde.org/plasma-desktop/9cdfe25910cbbfbef09364ce3adba53bcc292853'>Commit.</a>
- Make accessibility warning dialog usable again. <a href='https://commits.kde.org/plasma-desktop/611fc3079b243eb0e76230a07c8515523e69c797'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17557'>D17557</a>
- Round label width. <a href='https://commits.kde.org/plasma-desktop/3526334e82342dad221410cf3e8f839ed671bd7e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17365'>D17365</a>
- [Folder View] implement a minimum width for icon view to ensure that labels are never rendered useless. <a href='https://commits.kde.org/plasma-desktop/0b654afd88844c5a0ab8eb9a4f639ab52eac6d57'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/379432'>#379432</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16901'>D16901</a>
- Fix group popup dialog. <a href='https://commits.kde.org/plasma-desktop/ed34cc5f181e61d1fc98872866c5d7300a90af86'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/401508'>#401508</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17219'>D17219</a>
- [Folder View] improve label contrast against challenging backgrounds. <a href='https://commits.kde.org/plasma-desktop/10278e79f11677bd59f7d554eb8e18e580686082'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/361228'>#361228</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16968'>D16968</a>
- [Componentchooser KCM] Make default browser app choice work even when combobox is not used. <a href='https://commits.kde.org/plasma-desktop/26fb5ec20af60682561c7a9678506199966ef2c1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/350663'>#350663</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17181'>D17181</a>
- Fix dismissing the Dashboard by clicking inbetween multi-grid categories. <a href='https://commits.kde.org/plasma-desktop/e12e3cad1d7ee8c2f95cecd5939a648b508a408e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400720'>#400720</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17005'>D17005</a>
- Revert unintended line. <a href='https://commits.kde.org/plasma-desktop/454347370883c7c9b4e8cb29a6cdcffea9553326'>Commit.</a>
- Compress calls to `updateSize`. <a href='https://commits.kde.org/plasma-desktop/ab26ebb18b74d8def8e653dc94516fa7da935a5c'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/400364'>#400364</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17006'>D17006</a>
- Remove. <a href='https://commits.kde.org/plasma-desktop/0e1aa9e046c20dbc75a33eac368b1f0554ac0368'>Commit.</a>
- Focus handling fixes. <a href='https://commits.kde.org/plasma-desktop/03b17ac5ec3e04aebe07e69d9240e035fa743c2e'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399566'>#399566</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16106'>D16106</a>
- [Kicker] Rename id column to itemColumn. <a href='https://commits.kde.org/plasma-desktop/004d838c2718ed743d8a7c746cb605331d1b46c6'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16316'>D16316</a>
- Add accessibility information to desktop icons. <a href='https://commits.kde.org/plasma-desktop/498c42fed65df76ca457955bab18a252d63ca409'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16309'>D16309</a>
- Fix initial focus. <a href='https://commits.kde.org/plasma-desktop/13ff4a319309b05fe50d4fd8da7878c1e0fe0d66'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399185'>#399185</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15856'>D15856</a>
- [Folder View] Don't spawn multiple stat jobs for the same folder. <a href='https://commits.kde.org/plasma-desktop/7e4a522cec25cf4945645d8379e8145226cc85b8'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D14077'>D14077</a>
- Fix drop between shared views. <a href='https://commits.kde.org/plasma-desktop/1fe0175b66607d1e8299554aa0d54eb9664169d1'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D13612'>D13612</a>

### <a name='plasma-pa' href='https://commits.kde.org/plasma-pa'>Plasma Audio Volume Control</a>

- Fix connecting to PulseAudio with Qt 5.12 beta 3. <a href='https://commits.kde.org/plasma-pa/3b3b8c3d60e48db47d7b86e61f351bac03fd57b7'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16443'>D16443</a>

### <a name='plasma-sdk' href='https://commits.kde.org/plasma-sdk'>Plasma SDK</a>

- [plasmoidviewer] Pick up KQuickAddons::QtQuickSettings. <a href='https://commits.kde.org/plasma-sdk/ffa0cc8d956fc89989f428cfd791ef464a39398c'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18626'>D18626</a>
- [cuttlefish] Bind StandardKey.Quit (Ctrl+Q) to exit the app. <a href='https://commits.kde.org/plasma-sdk/47e4fa8e5366eb8b2103b979bd4257ac1d51b548'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16521'>D16521</a>
- [cuttlefish] Auto-focus on search textfield when app opens. <a href='https://commits.kde.org/plasma-sdk/bdf7f24499e4b1c09f56febe6980f2aec9425207'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D16516'>D16516</a>

### <a name='plasma-vault' href='https://commits.kde.org/plasma-vault'>plasma-vault</a>

- Fix use of QRegularExpressionMatch. <a href='https://commits.kde.org/plasma-vault/5d3face3c1bbe5a8a40927048b6f1c3280b26399'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17359'>D17359</a>

### <a name='plasma-workspace' href='https://commits.kde.org/plasma-workspace'>Plasma Workspace</a>

- [weather] envcan: Fix typo in lowercase icon lookup string. <a href='https://commits.kde.org/plasma-workspace/58e6cc6e119f7b149d623058cc50a5ad247a8c5f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19176'>D19176</a>
- Don't show entries with NoDisplay=true with the applauncher containmentaction. <a href='https://commits.kde.org/plasma-workspace/7816978b51204b82471359a8c0960a13710d489f'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D19157'>D19157</a>
- [weather dataengine] bbc,envcan,noaa: fix day/night calculation for observe. <a href='https://commits.kde.org/plasma-workspace/03e13b10d877733528d75f20a3f1d706088f7b9b'>Commit.</a>
- [weather dataengine] envcan: fix forecast icons to match "ice pellets". <a href='https://commits.kde.org/plasma-workspace/28d0af6791a91eeba7c76721926d18d88bf0b99f'>Commit.</a>
- Weather dataengine] noaa: another forecast string found in use. <a href='https://commits.kde.org/plasma-workspace/ed7c5796ebdc57082fee51dd65befbc6484c520c'>Commit.</a>
- [kio_applications] Fix last dir item being shown twice. <a href='https://commits.kde.org/plasma-workspace/3431ea6dd1d26419e4c579c8c242c29095220edd'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18641'>D18641</a>
- [plasmawindowed] Pick up KQuickAddons::QtQuickSettings. <a href='https://commits.kde.org/plasma-workspace/ddde180ae922267e410ff1f1d3b0a6adc8e801df'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18625'>D18625</a>
- Weather dataengine] noaa: support more "Slight Chance \*" forecast strings. <a href='https://commits.kde.org/plasma-workspace/182ef20af6fbc3da48a8f43b83832621dc24ef72'>Commit.</a>
- StatusItemNotifier: fix overlays by name with icons by name. <a href='https://commits.kde.org/plasma-workspace/65f3a9610f44896c007d1a78a7d955503a1081ef'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17983'>D17983</a>
- [weather dataengine] noaa: add another forecast string found in use. <a href='https://commits.kde.org/plasma-workspace/91633c6cefbe9c1465f9a7bd875da042f8391c4e'>Commit.</a>
- [weather dataengine] bbc: use night icons for forecast "Tonight". <a href='https://commits.kde.org/plasma-workspace/458fdeac4bd976fd66044f71a202c0bf2bd8e82a'>Commit.</a>
- Useful error output when shell loading is aborted due to kactivitymanagerd not being activatable. <a href='https://commits.kde.org/plasma-workspace/6d16583e479b90062fdb586a5440131e69432197'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18493'>D18493</a>
- [weather dataengine] envcan: fix typo in forecast string "Mainly sunny". <a href='https://commits.kde.org/plasma-workspace/2097b614904c78ad1622bf3e77380a2d4db56d00'>Commit.</a>
- [weather dataengine] envcan: support also "Partly cloudy" forecast. <a href='https://commits.kde.org/plasma-workspace/0ab3d161c69e64642add3eb4a7faca70f46fc249'>Commit.</a>
- [weather dataengine] bbc: adapt to changed strings for same day in forecast. <a href='https://commits.kde.org/plasma-workspace/f0134f6441d1d8b356c62b6e537241c1ae983090'>Commit.</a>
- [weather dataengine] noaa: add more forecast strings found in use. <a href='https://commits.kde.org/plasma-workspace/419097542fef18b285927202e3a1fe2fd004d2b9'>Commit.</a>
- [weather dataengine] envcan: support also "Mainly cloudy" forecast. <a href='https://commits.kde.org/plasma-workspace/3a38a571061d0854914547d648cdcfc36459318d'>Commit.</a>
- [weather dataengine] noaa: use https over http. <a href='https://commits.kde.org/plasma-workspace/a9c537dbda0c6fb4db11422f2ab00397a0073b09'>Commit.</a>
- [weather dataengine] noaa: fix unknown icon for Flurries. <a href='https://commits.kde.org/plasma-workspace/075629b96e4fc8c613c19db7cd468560ce4a9ff4'>Commit.</a>
- [kuiserver] Debug--. <a href='https://commits.kde.org/plasma-workspace/f02759d028a56919b86128816c63bca0630ef792'>Commit.</a>
- [kuiserver] Avoid double warning on terminated jobs. <a href='https://commits.kde.org/plasma-workspace/c4dcba898a9711a165ab5d839730e44560a48055'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D17711'>D17711</a>
- Set error if a kjob host disappears from kuiserver with active jobs. <a href='https://commits.kde.org/plasma-workspace/2beb1a0ad23177f7dc2e5ee622bed3a70f671278'>Commit.</a> See bug <a href='https://bugs.kde.org/352761'>#352761</a>. Phabricator Code review <a href='https://phabricator.kde.org/D17171'>D17171</a>
- Only use wl-shell for the ksmserver greeters on Qt < 5.12. <a href='https://commits.kde.org/plasma-workspace/790f5bf48f2b6b8c23e2dafc8f0071066215d85d'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/399918'>#399918</a>. Phabricator Code review <a href='https://phabricator.kde.org/D16381'>D16381</a>
- Plasmashell freezes when trying to get free space info from mounted remote filesystem after losing connection to it. <a href='https://commits.kde.org/plasma-workspace/be3b80e78017cc6668f9227529ad429150c27faa'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/397537'>#397537</a>. Phabricator Code review <a href='https://phabricator.kde.org/D14895'>D14895</a>. See bug <a href='https://bugs.kde.org/399945'>#399945</a>
- KRunner: remove no longer existant and unused column from SQL query. <a href='https://commits.kde.org/plasma-workspace/99fa6ccc57c5038ffb16d2e999893d55dc91f5b1'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/398305'>#398305</a>. Phabricator Code review <a href='https://phabricator.kde.org/D15305'>D15305</a>
- Fallback to in-process prompt if logout prompt fails. <a href='https://commits.kde.org/plasma-workspace/c2ea0afb559687690cd72ab7c39fd42e5bc8ed32'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D15869'>D15869</a>
- Klipper: Do not insert secret data into history. <a href='https://commits.kde.org/plasma-workspace/6f9721765017dbd4c6dba2131c544bf3552d1592'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D12539'>D12539</a>

### <a name='sddm-kcm' href='https://commits.kde.org/sddm-kcm'>SDDM KCM</a>

- Fix autologin session loading. <a href='https://commits.kde.org/sddm-kcm/634a2dd1bef5dd8434db95a391300f68f30fd14e'>Commit.</a> Phabricator Code review <a href='https://phabricator.kde.org/D18765'>D18765</a>