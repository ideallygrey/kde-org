---
aliases:
- ../../fulllog_applications-16.12.3
hidden: true
title: KDE Applications 16.12.3 Full Log Page
type: fulllog
version: 16.12.3
---

<h3><a name='akonadi' href='https://cgit.kde.org/akonadi.git'>akonadi</a> <a href='#akonadi' onclick='toggle("ulakonadi", this)'>[Show]</a></h3>
<ul id='ulakonadi' style='display: none'>
<li>Fix ServerManager going into Broken state when called before going to the event loop. <a href='http://commits.kde.org/akonadi/f42e54c0c390beac9d118fe5cf18c15dc371789c'>Commit.</a> </li>
<li>Fix crash when Connection is closed while ItemRetriever is running. <a href='http://commits.kde.org/akonadi/1593c17da9cbdc1483541e1e7d7630f3c91f8208'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/374734'>#374734</a></li>
<li>DataStream: throw exception if device is null. <a href='http://commits.kde.org/akonadi/5219f770b998aad16a3d51bbd4f4c7ceb8b143c9'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376385'>#376385</a></li>
<li>Don't export internal classes when not building autotests. <a href='http://commits.kde.org/akonadi/3557b70de817f60982f1d398bd31d8ebb66601f2'>Commit.</a> </li>
<li>Remove unused included cpp file from autotest. <a href='http://commits.kde.org/akonadi/2e658b98faf9d889b5f611436ef0bb228d372e0d'>Commit.</a> </li>
<li>Fix ItemRetriever in case of concurrent requests for the same item(s). <a href='http://commits.kde.org/akonadi/d42f94701f38c26cf3439727b50ee73cf0d09bee'>Commit.</a> </li>
<li>No need to generate the header twice, we can use the one from src/server here. <a href='http://commits.kde.org/akonadi/859d99b28c8fc429f47483a304d0cfe74de76eda'>Commit.</a> </li>
<li>Revert unwanted changes in previous commit. <a href='http://commits.kde.org/akonadi/59159c6fe9755122db2cbadc5265c62b0b75f514'>Commit.</a> </li>
<li>Implement "TODO: Qt 5: Use QDir::removeRecursively". <a href='http://commits.kde.org/akonadi/0b1caefae7410a1d021c8874c9696a69531effbd'>Commit.</a> </li>
<li>Fix runtime warning at the end of itemretrievertest. <a href='http://commits.kde.org/akonadi/d8534b286105f0a7b25e77051bfccd1456f44cc4'>Commit.</a> </li>
<li>Show collection id if collection is valid. <a href='http://commits.kde.org/akonadi/47afd2c0ca7dec789d93899045ca946b38bbade4'>Commit.</a> </li>
<li>Add more debug. <a href='http://commits.kde.org/akonadi/3af0b2650e7b790030a02cfec797684da9d0e673'>Commit.</a> </li>
</ul>
<h3><a name='ark' href='https://cgit.kde.org/ark.git'>ark</a> <a href='#ark' onclick='toggle("ulark", this)'>[Show]</a></h3>
<ul id='ulark' style='display: none'>
<li>Fix archivemodel columns with singlefile archives. <a href='http://commits.kde.org/ark/c1a72a8e053a859946a4b96effdc9193f7afe10b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376225'>#376225</a></li>
<li>Fix cross-thread object creation. <a href='http://commits.kde.org/ark/0db57f3f1981370177b7f089744a7f14d59907f5'>Commit.</a> </li>
</ul>
<h3><a name='cantor' href='https://cgit.kde.org/cantor.git'>cantor</a> <a href='#cantor' onclick='toggle("ulcantor", this)'>[Show]</a></h3>
<ul id='ulcantor' style='display: none'>
<li>[julia] Fix build with -fno-operator-names. <a href='http://commits.kde.org/cantor/45322d9f58f50df3d4d5755d4199e579f6fd8646'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129942'>#129942</a></li>
<li>Remove modern C++ use to fix compile with current KDE policy. <a href='http://commits.kde.org/cantor/4b8ef6bed62daced90c7826985650c2a813d2996'>Commit.</a> </li>
</ul>
<h3><a name='filelight' href='https://cgit.kde.org/filelight.git'>filelight</a> <a href='#filelight' onclick='toggle("ulfilelight", this)'>[Show]</a></h3>
<ul id='ulfilelight' style='display: none'>
<li>Ensure all folders created for ignored paths have a trailing slash. <a href='http://commits.kde.org/filelight/b06ec65e0c79d8eea8e44d4367364fd572c3dd19'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129953'>#129953</a></li>
<li>Make "Do not scan across file system boundaries" work properly again. <a href='http://commits.kde.org/filelight/ddcc6efdfccf3e7c9defd4a3de6f40f61f9e95fb'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/339037'>#339037</a>. Code review <a href='https://git.reviewboard.kde.org/r/129952'>#129952</a></li>
</ul>
<h3><a name='gwenview' href='https://cgit.kde.org/gwenview.git'>gwenview</a> <a href='#gwenview' onclick='toggle("ulgwenview", this)'>[Show]</a></h3>
<ul id='ulgwenview' style='display: none'>
<li>Fix wrong action name for setDefaultShortcut(). <a href='http://commits.kde.org/gwenview/1182058adaa07758584ace0aad06331b8e8cea8b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/355313'>#355313</a></li>
</ul>
<h3><a name='kalarm' href='https://cgit.kde.org/kalarm.git'>kalarm</a> <a href='#kalarm' onclick='toggle("ulkalarm", this)'>[Show]</a></h3>
<ul id='ulkalarm' style='display: none'>
<li>Bug 376209: Make command options --edit-new-* work. <a href='http://commits.kde.org/kalarm/7143b74632e48cd77c1b15a41b95d7fc9b798bd6'>Commit.</a> </li>
<li>Update version number. <a href='http://commits.kde.org/kalarm/fed6b750ea15a7c7054008d84b99066cba7afaea'>Commit.</a> </li>
<li>Bug 374520: Fix not showing main window if re-activated. <a href='http://commits.kde.org/kalarm/c5897d0eabf515a82a62429a726819cb60e255ae'>Commit.</a> </li>
</ul>
<h3><a name='kalzium' href='https://cgit.kde.org/kalzium.git'>kalzium</a> <a href='#kalzium' onclick='toggle("ulkalzium", this)'>[Show]</a></h3>
<ul id='ulkalzium' style='display: none'>
<li>Fix build with extra-cmake-modules > 5.30. <a href='http://commits.kde.org/kalzium/f233d458959548ab371e3faeca7313f746625afc'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129873'>#129873</a></li>
</ul>
<h3><a name='kate' href='https://cgit.kde.org/kate.git'>kate</a> <a href='#kate' onclick='toggle("ulkate", this)'>[Show]</a></h3>
<ul id='ulkate' style='display: none'>
<li>Add shortcut (Ctrl+Alt+f) for Search and Replace. <a href='http://commits.kde.org/kate/26308a744ee094aff19617ee890f0f56086459e8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/368377'>#368377</a></li>
</ul>
<h3><a name='kcachegrind' href='https://cgit.kde.org/kcachegrind.git'>kcachegrind</a> <a href='#kcachegrind' onclick='toggle("ulkcachegrind", this)'>[Show]</a></h3>
<ul id='ulkcachegrind' style='display: none'>
<li>Fix Slot connections for recently added menu items. <a href='http://commits.kde.org/kcachegrind/223af46d1786b87295294eb75a858cd9810c5cde'>Commit.</a> </li>
<li>Backport "Improve format detection". <a href='http://commits.kde.org/kcachegrind/5e9afaa80cde7425946d7686bb3e0783354c889b'>Commit.</a> </li>
<li>Fix detection of callgrind files for very long commands. <a href='http://commits.kde.org/kcachegrind/2a1173382a18bf86e95248afae83b475f1179771'>Commit.</a> </li>
</ul>
<h3><a name='kcalc' href='https://cgit.kde.org/kcalc.git'>kcalc</a> <a href='#kcalc' onclick='toggle("ulkcalc", this)'>[Show]</a></h3>
<ul id='ulkcalc' style='display: none'>
<li>Also reset percent mode when clearing state. <a href='http://commits.kde.org/kcalc/5338befe329357a65f5eb333f921f07ce25de1cd'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376655'>#376655</a></li>
</ul>
<h3><a name='kdebugsettings' href='https://cgit.kde.org/kdebugsettings.git'>kdebugsettings</a> <a href='#kdebugsettings' onclick='toggle("ulkdebugsettings", this)'>[Show]</a></h3>
<ul id='ulkdebugsettings' style='display: none'>
<li>Support loading qtlogging.ini from QLibraryInfo::DataPath too. <a href='http://commits.kde.org/kdebugsettings/acffcd986c9cf91e266e8701eaa997a18edebc6c'>Commit.</a> </li>
</ul>
<h3><a name='kdelibs' href='https://cgit.kde.org/kdelibs.git'>kdelibs</a> <a href='#kdelibs' onclick='toggle("ulkdelibs", this)'>[Show]</a></h3>
<ul id='ulkdelibs' style='display: none'>
<li>Sanitize URLs before passing them to FindProxyForURL. <a href='http://commits.kde.org/kdelibs/1804c2fde7bf4e432c6cf5bb8cce5701c7010559'>Commit.</a> </li>
</ul>
<h3><a name='kdenlive' href='https://cgit.kde.org/kdenlive.git'>kdenlive</a> <a href='#kdenlive' onclick='toggle("ulkdenlive", this)'>[Show]</a></h3>
<ul id='ulkdenlive' style='display: none'>
<li>Fix crash & corruption on dragging multiple clips in timeline, fix thread warning on monitor refresh. <a href='http://commits.kde.org/kdenlive/c17302f7eacbf08bb6a2c77c5a5e90d657d1b926'>Commit.</a> </li>
<li>Avoid possible profile corruption with xml producer. <a href='http://commits.kde.org/kdenlive/463e9f845318c6ffa83be97ed81da18af84f75aa'>Commit.</a> See bug <a href='https://bugs.kde.org/371189'>#371189</a></li>
<li>Avoid relying on xml to clone a clip. <a href='http://commits.kde.org/kdenlive/dc80cd7352e01fd7e4b29aa3953f8f577b702a86'>Commit.</a> See bug <a href='https://bugs.kde.org/377255'>#377255</a></li>
<li>Src/dvdwizard/dvdwizardmenu.cpp: do not show "grid" in output. <a href='http://commits.kde.org/kdenlive/dbc6793d3fe04089fbebfba5f8d2ecf8de614783'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377256'>#377256</a></li>
<li>Src/dvdwizard/dvdwizard.cpp: fix file loading in slotLoad. <a href='http://commits.kde.org/kdenlive/76ceba16400109e991a43684f75ab91c76106749'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377254'>#377254</a></li>
<li>Fix Render Widget's file dialog not working correctly. <a href='http://commits.kde.org/kdenlive/0f1f490cfc66566fa12421b5103f8f29d62c86e6'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/371685'>#371685</a></li>
<li>Fix render job duration when past midnight. <a href='http://commits.kde.org/kdenlive/28816933a37a4799d8cade1b04c80bdde417911a'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/372370'>#372370</a></li>
<li>Fix Bin Effect reset. <a href='http://commits.kde.org/kdenlive/d85742fcb1b62dcf1b0f4e03532072615499fca8'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376494'>#376494</a></li>
<li>Fix unnecessary refresh of tools when selecting titler item. <a href='http://commits.kde.org/kdenlive/a6ac89e1ad406c74d5169080cf9277ef685fb832'>Commit.</a> </li>
<li>Fix fadeouts re-appearing on clip cut+resize. <a href='http://commits.kde.org/kdenlive/a5490e5609d3676ec6cf12d9013d4e6854347171'>Commit.</a> </li>
</ul>
<h3><a name='kdepim-runtime' href='https://cgit.kde.org/kdepim-runtime.git'>kdepim-runtime</a> <a href='#kdepim-runtime' onclick='toggle("ulkdepim-runtime", this)'>[Show]</a></h3>
<ul id='ulkdepim-runtime' style='display: none'>
<li>Preserve icaldir resource/collection display name. <a href='http://commits.kde.org/kdepim-runtime/c551ad2f4dc3a829241aec05eb343de020fee67f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376379'>#376379</a></li>
</ul>
<h3><a name='kholidays' href='https://cgit.kde.org/kholidays.git'>kholidays</a> <a href='#kholidays' onclick='toggle("ulkholidays", this)'>[Show]</a></h3>
<ul id='ulkholidays' style='display: none'>
<li>Holiday_ua_uk - updated. <a href='http://commits.kde.org/kholidays/c88b89db49874407ba4b7cf0266208fb64cb6b33'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/359708'>#359708</a></li>
<li>CMakeLists.txt - tiny cleaning. <a href='http://commits.kde.org/kholidays/4594308464a3ee8386c6e85e643dc7dac052e250'>Commit.</a> </li>
<li>Holiday_hk_en-gb, holiday_hk_zh-cn - update Hong Kong holidays. <a href='http://commits.kde.org/kholidays/88dcc058172f8f529a01d10593940de75f0b33c7'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/375530'>#375530</a></li>
<li>Add a new holiday for Serbia. <a href='http://commits.kde.org/kholidays/10b2b6fab1b08745c2429456dddf6f2b35ee7753'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129668'>#129668</a></li>
<li>Add nullptr. <a href='http://commits.kde.org/kholidays/98b7d1bc5ad4e033e23a29446741678bf886d53a'>Commit.</a> </li>
<li>Add missing Q_DECL_OVERRIDE. <a href='http://commits.kde.org/kholidays/b0ea611585f8ff3a1ab7d2def4366c7ff2db9caa'>Commit.</a> </li>
<li>Add ECMCoverageOption. <a href='http://commits.kde.org/kholidays/e771cbd56ac7f9a27946d0328d80523d1e8c6159'>Commit.</a> </li>
</ul>
<h3><a name='kig' href='https://cgit.kde.org/kig.git'>kig</a> <a href='#kig' onclick='toggle("ulkig", this)'>[Show]</a></h3>
<ul id='ulkig' style='display: none'>
<li>Install the syntax file in the right place. <a href='http://commits.kde.org/kig/7c99631baeff6533ff515d60c5260c933d5d9045'>Commit.</a> </li>
</ul>
<h3><a name='kio-extras' href='https://cgit.kde.org/kio-extras.git'>kio-extras</a> <a href='#kio-extras' onclick='toggle("ulkio-extras", this)'>[Show]</a></h3>
<ul id='ulkio-extras' style='display: none'>
<li>Fix kio_sftp hanging when server doesn't support statvfs. <a href='http://commits.kde.org/kio-extras/7386dfcf96a58e91a7ae63a3e2a96a4c8b1d5a49'>Commit.</a> </li>
<li>Kio_mtp: add write permissions to root storage folder. <a href='http://commits.kde.org/kio-extras/6a828b172937a17f56d90a23daf2c7ed77d8ebc7'>Commit.</a> See bug <a href='https://bugs.kde.org/366795'>#366795</a></li>
<li>Fix install files. <a href='http://commits.kde.org/kio-extras/6014a6ef0f3f17b40a32d6c81e0a9456e2cc72dc'>Commit.</a> </li>
<li>Enable html thumbnail. <a href='http://commits.kde.org/kio-extras/1572393a727374fa2098719fa2acfb707d4b6306'>Commit.</a> </li>
<li>Thumbnails should be a clean image representation, remove frame. <a href='http://commits.kde.org/kio-extras/db7f8d0c945880b0fbb5ea9ad8ed6817d6ba0340'>Commit.</a> Code review <a href='https://git.reviewboard.kde.org/r/129921'>#129921</a></li>
</ul>
<h3><a name='kmail' href='https://cgit.kde.org/kmail.git'>kmail</a> <a href='#kmail' onclick='toggle("ulkmail", this)'>[Show]</a></h3>
<ul id='ulkmail' style='display: none'>
<li>Use isEmpty() directly. <a href='http://commits.kde.org/kmail/5e2e5c7a9f9c9e3ffab4c9d482d1e2a012462dbe'>Commit.</a> </li>
</ul>
<h3><a name='konqueror' href='https://cgit.kde.org/konqueror.git'>konqueror</a> <a href='#konqueror' onclick='toggle("ulkonqueror", this)'>[Show]</a></h3>
<ul id='ulkonqueror' style='display: none'>
<li>Rename define, to match kdepim. <a href='http://commits.kde.org/konqueror/943008d4895e4dfebc77aa4b5ace67b0e704256b'>Commit.</a> </li>
</ul>
<h3><a name='kopete' href='https://cgit.kde.org/kopete.git'>kopete</a> <a href='#kopete' onclick='toggle("ulkopete", this)'>[Show]</a></h3>
<ul id='ulkopete' style='display: none'>
<li>Prepare for 16.12.3. <a href='http://commits.kde.org/kopete/703bccecf843725d32d4fe8bca2b153a2c3d0c47'>Commit.</a> </li>
<li>Fix CVE 2017-5593 (User Impersonation Vulnerability) in jabber protocol. <a href='http://commits.kde.org/kopete/6243764c4fd0985320d4a10b48051cc418d584ad'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376348'>#376348</a></li>
</ul>
<h3><a name='kpimtextedit' href='https://cgit.kde.org/kpimtextedit.git'>kpimtextedit</a> <a href='#kpimtextedit' onclick='toggle("ulkpimtextedit", this)'>[Show]</a></h3>
<ul id='ulkpimtextedit' style='display: none'>
<li>Add isEmpty method. We don't want to rely on toPlainText().isEmpty(). <a href='http://commits.kde.org/kpimtextedit/b6a8cc3c6f190b258433c45b9d414121ea73a795'>Commit.</a> </li>
</ul>
<h3><a name='ktnef' href='https://cgit.kde.org/ktnef.git'>ktnef</a> <a href='#ktnef' onclick='toggle("ulktnef", this)'>[Show]</a></h3>
<ul id='ulktnef' style='display: none'>
<li>Fix Directory Traversal problem in ktnef. <a href='http://commits.kde.org/ktnef/4ff38aa15487d69021aacad4b078500f77fb4ae8'>Commit.</a> </li>
<li>Fixes for some fuzzed files. <a href='http://commits.kde.org/ktnef/a04ebbb2a2585ee111eaae6ea5b91c0dd7c909fb'>Commit.</a> </li>
</ul>
<h3><a name='libkcddb' href='https://cgit.kde.org/libkcddb.git'>libkcddb</a> <a href='#libkcddb' onclick='toggle("ullibkcddb", this)'>[Show]</a></h3>
<ul id='ullibkcddb' style='display: none'>
<li>Update test. <a href='http://commits.kde.org/libkcddb/6394f89d94a66e2bd3bac594570ef274b20eac6e'>Commit.</a> </li>
</ul>
<h3><a name='libkdepim' href='https://cgit.kde.org/libkdepim.git'>libkdepim</a> <a href='#libkdepim' onclick='toggle("ullibkdepim", this)'>[Show]</a></h3>
<ul id='ullibkdepim' style='display: none'>
<li>Answer the "Why?" question in the code. <a href='http://commits.kde.org/libkdepim/074d6837b9a84ac0d9310c4716bb4fc2bec5ae42'>Commit.</a> </li>
</ul>
<h3><a name='mailcommon' href='https://cgit.kde.org/mailcommon.git'>mailcommon</a> <a href='#mailcommon' onclick='toggle("ulmailcommon", this)'>[Show]</a></h3>
<ul id='ulmailcommon' style='display: none'>
<li>Add export macro for private classes with unittest. <a href='http://commits.kde.org/mailcommon/73d97fdab253d0976cf52a01b6c4b5d07feacd9a'>Commit.</a> </li>
</ul>
<h3><a name='messagelib' href='https://cgit.kde.org/messagelib.git'>messagelib</a> <a href='#messagelib' onclick='toggle("ulmessagelib", this)'>[Show]</a></h3>
<ul id='ulmessagelib' style='display: none'>
<li>Fix Bug 377247 - kMail 2 does not properly escape header. <a href='http://commits.kde.org/messagelib/3b0126cd9d716091f53b26cd0f03e9ced624126b'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377247'>#377247</a></li>
<li>Use correct variable for date direction. <a href='http://commits.kde.org/messagelib/ae9e8384060ff9c140878dae5c71800bdbeb97ef'>Commit.</a> </li>
<li>Finish removing the user agent from sent emails. <a href='http://commits.kde.org/messagelib/1f4f40492226644b940208196220085720c211d8'>Commit.</a> </li>
<li>Rename method as requested by David. <a href='http://commits.kde.org/messagelib/5328082f79997e2abe82d55d15e10a8a7f48ad08'>Commit.</a> </li>
<li>Keep "disable emoticon" state even if we switch message. <a href='http://commits.kde.org/messagelib/6ed0d76e519eec68f520e0d6d3e7770ca6232ba1'>Commit.</a> </li>
</ul>
<h3><a name='okular' href='https://cgit.kde.org/okular.git'>okular</a> <a href='#okular' onclick='toggle("ulokular", this)'>[Show]</a></h3>
<ul id='ulokular' style='display: none'>
<li>Djvu: Initialize to white if rendering fails. <a href='http://commits.kde.org/okular/0419812ad7b86417bfda3e165957f33419e9fa4f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377202'>#377202</a></li>
<li>Indent++. <a href='http://commits.kde.org/okular/7da5adce57abc2055561cde067b92c64700d1e3e'>Commit.</a> </li>
<li>Accumulate Control+Wheel Deltas until they reach QWheelEvent::DefaultDeltasPerStep. <a href='http://commits.kde.org/okular/7a50ce0edfc9be8bd23441e52a4f3a0c60f7e60f'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/377128'>#377128</a></li>
<li>Fix crash on CHM files that use "plain English" LCID. <a href='http://commits.kde.org/okular/6874317c8ac7606adca5f8f73ad12653f898ca28'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376936'>#376936</a></li>
<li>Account for non local non relative video file urls. <a href='http://commits.kde.org/okular/ee7e3737f4d2031e9bac9f5a25ac0b31c2f5e496'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/376574'>#376574</a></li>
<li>Extend viewport update area when updating or clearing the a selection rectangle. <a href='http://commits.kde.org/okular/6f3970dbe48fb6b75c611ea68994e57a98791a47'>Commit.</a> Fixes bug <a href='https://bugs.kde.org/266537'>#266537</a></li>
</ul>
<h3><a name='pimcommon' href='https://cgit.kde.org/pimcommon.git'>pimcommon</a> <a href='#pimcommon' onclick='toggle("ulpimcommon", this)'>[Show]</a></h3>
<ul id='ulpimcommon' style='display: none'>
<li>Fix regexp. <a href='http://commits.kde.org/pimcommon/7b9931193c0306d1f716e1e35a3a087e09c711c6'>Commit.</a> </li>
</ul>
<h3><a name='print-manager' href='https://cgit.kde.org/print-manager.git'>print-manager</a> <a href='#print-manager' onclick='toggle("ulprint-manager", this)'>[Show]</a></h3>
<ul id='ulprint-manager' style='display: none'>
<li>Remove not needed PopupApplet definition from the plasmoid. <a href='http://commits.kde.org/print-manager/49c9202bbc74cecce04818b1842ffa0936f0fe63'>Commit.</a> </li>
</ul>
<h3><a name='step' href='https://cgit.kde.org/step.git'>step</a> <a href='#step' onclick='toggle("ulstep", this)'>[Show]</a></h3>
<ul id='ulstep' style='display: none'>
<li>Correctly load the Qt-native translations. <a href='http://commits.kde.org/step/09e3268eaa2b6765b95757228dc71decd147cffb'>Commit.</a> </li>
</ul>
<h3><a name='umbrello' href='https://cgit.kde.org/umbrello.git'>umbrello</a> <a href='#umbrello' onclick='toggle("ulumbrello", this)'>[Show]</a></h3>
<ul id='ulumbrello' style='display: none'>
<li>Fix crash by disconnecting slots in UMLApp::~UMLApp(). <a href='http://commits.kde.org/umbrello/fbe7331061e9c08ebf11e128d7ece5d1750c2464'>Commit.</a> See bug <a href='https://bugs.kde.org/376939'>#376939</a></li>
</ul>