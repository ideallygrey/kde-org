---
aliases:
- ../../fulllog_releases-23.08.3
title: KDE Gear 23.08.3 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="akonadi" title="akonadi" link="https://commits.kde.org/akonadi" >}}
+ Only keep mysql log from the last session. [Commit](http://commits.kde.org/akonadi/897d583942e7acb2bce5b65b56a6c0c19260d6f9). Fixes bug [#456983](https://bugs.kde.org/456983).
{{< /details >}}
{{< details id="akonadi-search" title="akonadi-search" link="https://commits.kde.org/akonadi-search" >}}
+ Htmltotext needs a QGuiApplication, because QTextHtmlParser does. [Commit](http://commits.kde.org/akonadi-search/cbccd911b8676ec8f17ea03d725891cab7a264c5).
{{< /details >}}
{{< details id="ark" title="ark" link="https://commits.kde.org/ark" >}}
+ Fix opening of AppImage files. [Commit](http://commits.kde.org/ark/347c6f79e71529d89587d691bc6eca096e22bc2a).
+ Fix checks using old ISO mimetype. [Commit](http://commits.kde.org/ark/dac14f552fb147bf4c9f759611cdb3b544a500b9).
+ Don't hardcode bzip2 mimetype in tests. [Commit](http://commits.kde.org/ark/785640b090b51e0067dbfc6c7944a478429204a2).
+ Fix support for bzip2 format with shared-mime-info 2.3. [Commit](http://commits.kde.org/ark/9bcbcb056c43abef88540c54f25bc6c1a78c7c0e).
+ Explicitly support new application/vnd.efi.iso mimetype. [Commit](http://commits.kde.org/ark/62d94c62f2fd2052be91dfe565e35a6d43c7d381).
{{< /details >}}
{{< details id="audiotube" title="audiotube" link="https://commits.kde.org/audiotube" >}}
+ Report QFuture results even on failure. [Commit](http://commits.kde.org/audiotube/11ec69862b1a33715cc9cd05476ab36734e6cce4).
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/dolphin/4dff8a2d4544ebc14fbfa8901ba2c080da5ee6d6).
+ Update to reflect move of KUserFeedback to Frameworks. [Commit](http://commits.kde.org/dolphin/e4096ec6c8e98a6f5628b24e6cb4d36e671eeccf).
+ Process correct model when applying service menu changes. [Commit](http://commits.kde.org/dolphin/4202fbdeca991f1fb043ecdecbc00f67b24e8e2e). Fixes bug [#475547](https://bugs.kde.org/475547).
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/gwenview/93801a541cbb9af13d0fb17af95f6ae6fc43fb0d).
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Reset AttendeeTableModel when settings attendees. [Commit](http://commits.kde.org/incidenceeditor/9e739ee45ade1f6b43cc5bc67f91e7b723d7dddb). Fixes bug [#428888](https://bugs.kde.org/428888).
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Exclude Poppler's fallback fonts from the Itinerary APK. [Commit](http://commits.kde.org/itinerary/8a60d48e22b6beb925fd8cf885a2e7f16fa8113f).
+ Use include:project for all templates. [Commit](http://commits.kde.org/itinerary/aaa50c834b97c69c6da7d3dada46dce11eb008d8).
+ Use Kirigami Addons from the kf5 branch. [Commit](http://commits.kde.org/itinerary/256c1ac323bee38ea8a681b1b1b5d90e72eace9c).
+ Update Android asset exclusion list. [Commit](http://commits.kde.org/itinerary/52ef4ed513ce44d6224f7b0226652ab3af35f6dc).
{{< /details >}}
{{< details id="kalgebra" title="kalgebra" link="https://commits.kde.org/kalgebra" >}}
+ Adapt to plasma-framework moving to Plasma. [Commit](http://commits.kde.org/kalgebra/49a7fb2d9c91a6d3f053764a49f8b9b4486147e1).
{{< /details >}}
{{< details id="kalzium" title="kalzium" link="https://commits.kde.org/kalzium" >}}
+ Fix build with Avogadro 1.98. [Commit](http://commits.kde.org/kalzium/15a4bea23504d3b24dffa5c0647cd7f1ba9cdddc).
{{< /details >}}
{{< details id="kapptemplate" title="kapptemplate" link="https://commits.kde.org/kapptemplate" >}}
+ Templates: drop accidentally script-added release info from appdata files. [Commit](http://commits.kde.org/kapptemplate/177ce72a1d8ae78577502455ca8eecf06fec8012).
{{< /details >}}
{{< details id="kasts" title="kasts" link="https://commits.kde.org/kasts" >}}
+ Fix streaming button showing up instead of download button. [Commit](http://commits.kde.org/kasts/0286d64d248184154b29bc865507c1eaa3716a54). Fixes bug [#476144](https://bugs.kde.org/476144).
+ Remove network connectivity check. [Commit](http://commits.kde.org/kasts/ea5d01043d01a8d5ecdcf81ef30d3313361c8ad4). See bug [#475400](https://bugs.kde.org/475400).
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/kate/65eabb27b1bf3ecdacf2070ad7fd6410318025cd).
+ Update to reflect move of KUserFeedback to Frameworks. [Commit](http://commits.kde.org/kate/ddeeb03060e6a71ab9c3561b2a71bb3520817b81).
+ Fix crash on dropping file in project. [Commit](http://commits.kde.org/kate/6b2165f2dda73fd7fed9e5586956fc9d403663cd). Fixes bug [#476016](https://bugs.kde.org/476016).
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix timeremap. [Commit](http://commits.kde.org/kdenlive/7f89dc52c356550bfb419c5e9d1e388a3f0fc4b8).
+ Fix replace clip keeping audio index from previous clip, sometimes breaking audio. [Commit](http://commits.kde.org/kdenlive/aa68d9e2e5dc3c2f16edc7865a69acc9fa3ecb18). See bug [#476612](https://bugs.kde.org/476612).
+ Create sequence from selection: ensure we have enough audio tracks for AV groups. [Commit](http://commits.kde.org/kdenlive/bf6c6299cca9190ce404091bb86330ac36bdb063).
+ Fix timeline duration incorrect after create sequence from timeline selection. [Commit](http://commits.kde.org/kdenlive/28f67c08274b762dfcff746e59d0a851f4d165e2).
+ Fix project duration not updating when moving the last clip of a track to another non last position. [Commit](http://commits.kde.org/kdenlive/19c63f04fb50c81717f36142ee21f09e3d359425). See bug [#476493](https://bugs.kde.org/476493).
+ Don't lose subtitle styling when switching to another sequence. [Commit](http://commits.kde.org/kdenlive/528e88c9b083ac644338b7cbbcbcc06a4172d14d). Fixes bug [#476544](https://bugs.kde.org/476544).
+ Fix crash dropping url to Library. [Commit](http://commits.kde.org/kdenlive/e1bcabe42f33a4e8e2de9e67a62eab1e5024a38e).
+ When dropping multiple files in project bin, improve import speed by not checking if every file is on a remote drive. [Commit](http://commits.kde.org/kdenlive/7d71f50332b65d35de2e10f436f78ee36b293491).
+ Fix titler shadow incorrectly pasted on selection. [Commit](http://commits.kde.org/kdenlive/72f45eb94496486d8fa90a9540a9e5554f72e596). Fixes bug [#476393](https://bugs.kde.org/476393).
+ Fix pasted effects not adjusted to track length. [Commit](http://commits.kde.org/kdenlive/01acd5fcadd238feba836e7d7579a150a517312a).
+ Fix timeline preview ignored in temporary data dialog. [Commit](http://commits.kde.org/kdenlive/e5463c1646652f1b75120aaf0db55153c2ba3553). Fixes bug [#475980](https://bugs.kde.org/475980).
+ Speech to text: fix whisper install aborting after 30secs. [Commit](http://commits.kde.org/kdenlive/68d45a57a774b8b09dd3b6f2084435df229af09e).
+ Don't try to generate proxy clips for audio with clipart. [Commit](http://commits.kde.org/kdenlive/277e8e09ae4ddef9948073079e5f737b431decfe).
+ Clip loading: switch to Mlt::Producer probe() instead of fetching frame. [Commit](http://commits.kde.org/kdenlive/707f139f5d869ec659045c0d534f90a762d4021c).
+ Multiple fixes for time remap losing keyframes. [Commit](http://commits.kde.org/kdenlive/84f7373df491d7cf4a38330a70a9cb74e9d0b860).
+ Add png with alpha render profile. [Commit](http://commits.kde.org/kdenlive/3fb09a9c368ec25ee1e72245550766d45f8c81e1).
+ Fix Mix not correctly deleted on group track move. [Commit](http://commits.kde.org/kdenlive/e4a4f3c5d5f567b1f2a0dbc15d57dc82f39654a1).
+ Fix rendering with alpha. [Commit](http://commits.kde.org/kdenlive/b9bf00fe63786b25179fb403954d4cf66478373c).
+ Rotoscoping: don't auto add a second kfr at cursor pos when creating the initial shape, don't auto add keyframes until there are 2 keyframes created. [Commit](http://commits.kde.org/kdenlive/2f0326e48ab2f0a3b82baaa685c69e6e5774c165).
+ Fix keyframe param not correctly enabled when selecting a clip. [Commit](http://commits.kde.org/kdenlive/2ec04d4aaeed4c76869e77ed7ecdbeefbcfbc86e).
+ Fix smooth keyframe path sometimes incorrectly drawn on monitor. [Commit](http://commits.kde.org/kdenlive/3e81be35d2acf4a5fda557eefce714c524f0a3cb).
+ Properly adjust timeline clips on sequence resize. [Commit](http://commits.kde.org/kdenlive/f4ab1a556add5e25a181ae62d852c214cea042cf).
+ Remove unused debug stuff. [Commit](http://commits.kde.org/kdenlive/4bcf1a6ac433ece524ba90901b70b4a895bc443c).
+ Fix project duration not correctly updated on hide / show track. [Commit](http://commits.kde.org/kdenlive/84cc82268c4dc579b3d398af64b2261a881daca3).
+ Fix resize clip with mix test. [Commit](http://commits.kde.org/kdenlive/e338f811e4429e260c9d1799f08cc1fe61f81244).
+ Fix resize clip start to frame 0 of timeline not correctly working in some zoom levels,. [Commit](http://commits.kde.org/kdenlive/8424aa13a7977f0ecfc53bfa0891f15a736f78a9).
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Fix show configure dialogbox. [Commit](http://commits.kde.org/kdepim-addons/76a3cd760b47b6ff51448b98a45f948d1e1535c1).
{{< /details >}}
{{< details id="kfourinline" title="kfourinline" link="https://commits.kde.org/kfourinline" >}}
+ Render graphics hidpi aware. [Commit](http://commits.kde.org/kfourinline/46ecc6a6f5dc1fd893e2e9eba772bdeee22c9611).
+ Fix config dialog apply/ok not doing anything. [Commit](http://commits.kde.org/kfourinline/a9281f5f7bbfcf54301492df466aafe4d03f7fc2). Fixes bug [#475891](https://bugs.kde.org/475891).
{{< /details >}}
{{< details id="kgpg" title="kgpg" link="https://commits.kde.org/kgpg" >}}
+ Use KLazyLocalizedString to init static lokalized string. [Commit](http://commits.kde.org/kgpg/eb9ef206cdd4850782f513da8afa70bb411a334c). Fixes bug [#475232](https://bugs.kde.org/475232).
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/kio-extras/98805c23cc72dc69610af686b2fa00a1b25de2a3).
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Specify an encoding for libxml2 when reading from byte array data as well. [Commit](http://commits.kde.org/kitinerary/acb2935298b55a459a1ba6b2cf863d041b2ffa9a).
+ Work around broken date/time values in booking.com mails without times. [Commit](http://commits.kde.org/kitinerary/d164a52219b12e5d26896c3a78d7627493383826).
+ Make Uic9183Header externally usable. [Commit](http://commits.kde.org/kitinerary/c4a3bf74a23ad1aea07076c9306622fb3966e2a1).
+ Adapt DB online import parser to also handle DB Next API replies. [Commit](http://commits.kde.org/kitinerary/993d92f570f97e183c4d335ad3297f45d9e557ff).
+ Adapt online ticket dumping tool to DB Next API changes. [Commit](http://commits.kde.org/kitinerary/96ab638f190edb4850d0c2de70753e2d0c9a147a).
+ Fix booking.com address country extraction. [Commit](http://commits.kde.org/kitinerary/a24356feb12e7f3f609530e10432b305b2444fe7).
+ Handle multi-line airport names in Qatar Airways tickets. [Commit](http://commits.kde.org/kitinerary/d1a52e530794b21fb8503c9968809c1db59d91b5).
+ Display non-ascii chars in PKP tickets. [Commit](http://commits.kde.org/kitinerary/bef4c3e39d24fd1d1b3875fc789553150dbe0875).
+ Add no-q event booking extractor. [Commit](http://commits.kde.org/kitinerary/21e0476d17381c84dcf15c350189cf86785d2410).
+ Sanity-check terminal names to deal with weird Iberia PDFs. [Commit](http://commits.kde.org/kitinerary/bcd66f798fc47b563fc98d1be5ea68cb7b4accde).
+ Handle Iberia pkpass boarding passes and PDFs without a boarding group. [Commit](http://commits.kde.org/kitinerary/b3a24a566d57396216eb1c9c6e5c2b558842deea).
+ Fix extracting Thalys tickets with a program membership used. [Commit](http://commits.kde.org/kitinerary/023b09a6e8d9211423f53a2974425be15c74ff21).
+ Fix extraction from multi-line Pretix PDF tickets. [Commit](http://commits.kde.org/kitinerary/3433295a5fb91e1996092731b5b88f5c3686040c).
+ Handle Pretix pkpass files with non-formatted date/time values. [Commit](http://commits.kde.org/kitinerary/4ca024dbf053880eb082c480a8fd72298316ffeb).
+ Handle Dutch language European Sleeper seat reservations. [Commit](http://commits.kde.org/kitinerary/9e76fc6b516922e49d1b256a6a4566cf2943820b).
+ Handle Dutch language variants of European Sleeper tickets. [Commit](http://commits.kde.org/kitinerary/1bf53031a8ff4c34b979bd255855f6c496992d01).
+ Small optimizations for the static build. [Commit](http://commits.kde.org/kitinerary/e3377addc92ace2182e2c16a87f60fa48944a06f).
+ Update train station data from Wikidata. [Commit](http://commits.kde.org/kitinerary/6add11a65bd06ab92306726a4be63e887a2cdad9).
+ Fix train station country SPARQL query. [Commit](http://commits.kde.org/kitinerary/670f03c812712c3c5af79369259e8c5154eb62e1).
{{< /details >}}
{{< details id="kleopatra" title="kleopatra" link="https://commits.kde.org/kleopatra" >}}
+ Fix bug 475485: Handbook: typo in OpenPGP link to Wikipedia. [Commit](http://commits.kde.org/kleopatra/acb2e544bae3ac0d803e027d5b39451ff656641d). Fixes bug [#475485](https://bugs.kde.org/475485).
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Fix signed integer overflow due to uninitialized vars warning. [Commit](http://commits.kde.org/konsole/76814cfa3a75f6050d1eae6f09993f7297911901).
+ Account for CJK wide chars in copyLineToStream. [Commit](http://commits.kde.org/konsole/2e69d229ef65ea81f3bdbd67198d75a1f940173b).
+ Add test case for block selection of CJK chars. [Commit](http://commits.kde.org/konsole/e194f00955030ab1c9c9cd961b5c7b5f860e4c21).
+ Don't select half a CJK character. [Commit](http://commits.kde.org/konsole/6b93b9995c76696f94eaec57e4dc420bc71e8980). Fixes bug [#474055](https://bugs.kde.org/474055).
+ Don't try to free terminalPart. [Commit](http://commits.kde.org/konsole/3049fe0f5a9ba6b706a680c1980cc8329d77938a).
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Fix crash when forwarding incidence via context menu. [Commit](http://commits.kde.org/korganizer/e28baf6ff1290edbc8e5771400fe87c81dbc8fea). Fixes bug [#474144](https://bugs.kde.org/474144).
+ Repair searching in korganizer. [Commit](http://commits.kde.org/korganizer/cdbccd911da7a4c092cde62ac6c3dd3d981b57de).
{{< /details >}}
{{< details id="kpkpass" title="kpkpass" link="https://commits.kde.org/kpkpass" >}}
+ Make the message catalog UTF-8 encoding detection slightly more broad. [Commit](http://commits.kde.org/kpkpass/f5dbba7d3bd435ade775feca4b27c7bec8f2ab14).
{{< /details >}}
{{< details id="kpublictransport" title="kpublictransport" link="https://commits.kde.org/kpublictransport" >}}
+ Correctly detect past canceled stops in DB ICE onboard journey data. [Commit](http://commits.kde.org/kpublictransport/943860edba0e7c686f9d201d09b79cce115b45d9).
+ Fix asymmetric location type comparison. [Commit](http://commits.kde.org/kpublictransport/e1ce0d510920b6b7936701c110ea20be47330f40).
+ Handle canceled stops in DB ICE onboard API journey data. [Commit](http://commits.kde.org/kpublictransport/079d958bbcc652698ae879a832ac5e1acb3c62ca).
{{< /details >}}
{{< details id="krdc" title="krdc" link="https://commits.kde.org/krdc" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/krdc/a0199ac4f308de0bfb83a6a2875929fcaeea7338).
{{< /details >}}
{{< details id="kubrick" title="kubrick" link="https://commits.kde.org/kubrick" >}}
+ Fix mouse input handling with HiDpi. [Commit](http://commits.kde.org/kubrick/691986d97ed15836afd4de26829c49a1c5d3df64).
{{< /details >}}
{{< details id="kweather" title="kweather" link="https://commits.kde.org/kweather" >}}
+ Adapt to plasma-framework moving to Plasma. [Commit](http://commits.kde.org/kweather/1e4f6bef1f36a0ab1ab0ec4b1d063be410e80edb).
{{< /details >}}
{{< details id="libkdepim" title="libkdepim" link="https://commits.kde.org/libkdepim" >}}
+ Align to top. [Commit](http://commits.kde.org/libkdepim/0ea63f86a5f1badc3fe9e0871ca142d5d87315de).
{{< /details >}}
{{< details id="libkmahjongg" title="libkmahjongg" link="https://commits.kde.org/libkmahjongg" >}}
+ Tile sets: hide design helper lines around tile "faces". [Commit](http://commits.kde.org/libkmahjongg/93797a3f3780679c3b5ed8585ab005fa9aa5b383).
+ Background selection preview: render hidpi aware. [Commit](http://commits.kde.org/libkmahjongg/c31cf9a6bb6b983ed71022bb288096a498a25f89).
+ Background: render pixmap hidpi aware. [Commit](http://commits.kde.org/libkmahjongg/b9b2d6870b47b7ceaef658f029b48cca6cdfa6d0).
+ Tileset selection preview: render hidpi aware. [Commit](http://commits.kde.org/libkmahjongg/ec25239f4b0f527dc445bfe103f6c917c442f989).
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Fix bug 476456: No scrollbar in simple editing mode. [Commit](http://commits.kde.org/libksieve/8111ad54030f6d1a38d9c340fb0bbb5d96e691d0). Fixes bug [#476456](https://bugs.kde.org/476456).
+ Add scrollarea. [Commit](http://commits.kde.org/libksieve/39ab9f6a7954a4914c52f82e8403d6e56803f3ec). See bug [#476456](https://bugs.kde.org/476456).
+ Rename variables. [Commit](http://commits.kde.org/libksieve/6f97962185e82749f377cb817b02984a4a41f0d1).
+ Fix update button when we click on clear. [Commit](http://commits.kde.org/libksieve/c7ddb1221a5edba82f5dd6561717397040937f76).
{{< /details >}}
{{< details id="lskat" title="lskat" link="https://commits.kde.org/lskat" >}}
+ Render pixmaps HiDpi-aware. [Commit](http://commits.kde.org/lskat/42c2ce8a30deac0db77b4dcd666bae9294e20af1).
{{< /details >}}
{{< details id="merkuro" title="merkuro" link="https://commits.kde.org/merkuro" >}}
+ Ensure we mark element as selected in contact list. [Commit](http://commits.kde.org/merkuro/8abf72ba270f44a5fffa92a11452ebb31e11d909).
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/okular/427408708fb3ca306b039a3322d5d1d4de0b909d).
{{< /details >}}
{{< details id="sweeper" title="sweeper" link="https://commits.kde.org/sweeper" >}}
+ Reflect move of KActivities out of Frameworks to Plasma. [Commit](http://commits.kde.org/sweeper/c41a8e0832fda9d37b0089df9e65c71b2260a2c2).
{{< /details >}}
{{< details id="tokodon" title="tokodon" link="https://commits.kde.org/tokodon" >}}
+ Fix account switcher not working in certain sections. [Commit](http://commits.kde.org/tokodon/53ee15030c340f53f287fbb377210c3fc925634c). Fixes bug [#475897](https://bugs.kde.org/475897).
+ Remove _L1 from cherry-picking error. [Commit](http://commits.kde.org/tokodon/0c1402538b726fea36fb38daee9ace9ba953326e).
+ Fix some weird attachment issues if they don't exist locally. [Commit](http://commits.kde.org/tokodon/2ab3d16f31d2b809d958af5c990eeebba7685e16).
{{< /details >}}
