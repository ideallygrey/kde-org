---
aliases:
- ../../fulllog_releases-22.03.90
title: KDE Gear 22.03.90 Full Log Page
type: fulllog
gear: true
hidden: true
---
{{< details id="bomber" title="bomber" link="https://commits.kde.org/bomber" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/bomber/12834e90ba9232cc9d75b970f81e010a6914960f) 
{{< /details >}}
{{< details id="bovo" title="bovo" link="https://commits.kde.org/bovo" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/bovo/6c8142100ffea5b0e5606902445dc86fe082fc24) 
{{< /details >}}
{{< details id="dolphin" title="dolphin" link="https://commits.kde.org/dolphin" >}}
+ Improve details mode ctrl-press rubberband creation. [Commit.](http://commits.kde.org/dolphin/2de8f4c0fb2141f68400327a4f87eefa71d81e3c) Fixes bug [#452181](https://bugs.kde.org/452181)
+ Revert "KStandardItemListWidget: handle RtL drawing properly". [Commit.](http://commits.kde.org/dolphin/a4f9974daf1e533c39f67129b85193ad4d47e5ad) Fixes bug [#451704](https://bugs.kde.org/451704). Fixes bug [#451341](https://bugs.kde.org/451341)
+ Fix tooltip closing while mouse moves over item. [Commit.](http://commits.kde.org/dolphin/29383426d66feb255034a3e160ae48b6273773d1) 
+ Don't crash when mountpoint is not found. [Commit.](http://commits.kde.org/dolphin/1041b52c7a9002d682246052a587e9a17357afa1) Fixes bug [#452100](https://bugs.kde.org/452100)
+ [ViewProperties] Apply better default roles for special views. [Commit.](http://commits.kde.org/dolphin/466fa20f7faf26ef2df2453da1622e0805564140) Fixes bug [#400969](https://bugs.kde.org/400969). Fixes bug [#340393](https://bugs.kde.org/340393). Fixes bug [#186376](https://bugs.kde.org/186376)
+ [ViewProperties] Consider existing roles in convertAdditionalInfo. [Commit.](http://commits.kde.org/dolphin/e304aa79b42bc7382376aef855e3e4366bb345b3) 
+ Port to target-based ecm_add_app_icon. [Commit.](http://commits.kde.org/dolphin/1cd00946c411f525d25b288a93e276a25e9a49b7) 
{{< /details >}}
{{< details id="elisa" title="elisa" link="https://commits.kde.org/elisa" >}}
+ Fix volume and duration slider mouse wheel events. [Commit.](http://commits.kde.org/elisa/611c0bf86ee3a2b1a0408c8fb7ec6caac2b8f3e1) Fixes bug [#450984](https://bugs.kde.org/450984)
+ Add SI units to bit rate and sample rate. [Commit.](http://commits.kde.org/elisa/be01acf1ca870cab159d0fc6e8fa1bafcd4deb6a) Fixes bug [#447897](https://bugs.kde.org/447897)
{{< /details >}}
{{< details id="eventviews" title="eventviews" link="https://commits.kde.org/eventviews" >}}
+ Force the todo completion progress bar to be rendered horizontally. [Commit.](http://commits.kde.org/eventviews/f463775a6d7144165f68f1d9d58b8ae20dc08602) 
{{< /details >}}
{{< details id="falkon" title="falkon" link="https://commits.kde.org/falkon" >}}
+ Enable WebRTC screen capturer in WebEngine. [Commit.](http://commits.kde.org/falkon/1ef7a6547a7b871bba83dd700773024412849559) Fixes bug [#450839](https://bugs.kde.org/450839)
{{< /details >}}
{{< details id="filelight" title="filelight" link="https://commits.kde.org/filelight" >}}
+ Make free/used in the summary view translatable. [Commit.](http://commits.kde.org/filelight/94792b1797981857ae028ff03b713cd96d65bfd4) 
+ Do not show silly mounts in sandboxed envs. [Commit.](http://commits.kde.org/filelight/e30f05c2a27a62a9eb8641fa594a331077eb4d28) Fixes bug [#450835](https://bugs.kde.org/450835)
{{< /details >}}
{{< details id="gwenview" title="gwenview" link="https://commits.kde.org/gwenview" >}}
+ Revert "Add global scope left/right arrow key shortcuts for the view". [Commit.](http://commits.kde.org/gwenview/bc0b3a2a694c54c2ddfd86e19f60a6800915b3b6) See bug [#451952](https://bugs.kde.org/451952)
{{< /details >}}
{{< details id="incidenceeditor" title="incidenceeditor" link="https://commits.kde.org/incidenceeditor" >}}
+ Fix keyboard focus-change order. [Commit.](http://commits.kde.org/incidenceeditor/4c561994aef353b82a7032e5b0111ceb42e9ab32) Fixes bug [#331543](https://bugs.kde.org/331543)
+ Fix crash when creating all-day to-dos. [Commit.](http://commits.kde.org/incidenceeditor/5a02a1c1129c19e9f0436d361a862b31df752b90) 
+ Preserve datetimes of incidences created from templates. [Commit.](http://commits.kde.org/incidenceeditor/ba964b3b52028423b5434058490f8a5062c18072) Fixes bug [#332048](https://bugs.kde.org/332048)
+ Test more time zone special cases. [Commit.](http://commits.kde.org/incidenceeditor/5a1ab6d6838b7cf11c2c48505b4961217fa63533) 
+ Test start/end validation with timezones. [Commit.](http://commits.kde.org/incidenceeditor/83e04fe0567c6c6bc32daa028dc37bfc186190e1) 
+ Synchronize "floating" status of date-times. [Commit.](http://commits.kde.org/incidenceeditor/b14b742af789ffbf039dae57634f3650f3ef0bfd) 
+ New todos and journals do not float by default. [Commit.](http://commits.kde.org/incidenceeditor/21effd9e9f29da281641094bad1785b36b888aa1) 
+ Do not change the timeSpec of system dateTimes. [Commit.](http://commits.kde.org/incidenceeditor/7003299beb4cde26c44af546136bb7c68086c031) 
{{< /details >}}
{{< details id="itinerary" title="itinerary" link="https://commits.kde.org/itinerary" >}}
+ Fix country combo box being too narrow. [Commit.](http://commits.kde.org/itinerary/dc2d60e5906079a0ba9250a877bb09715c4fdf94) 
{{< /details >}}
{{< details id="k3b" title="k3b" link="https://commits.kde.org/k3b" >}}
+ Add missing KF5Auth dep and link. [Commit.](http://commits.kde.org/k3b/ba355bd1adc1df29124d516de0ea3748cd0b7b6e) 
{{< /details >}}
{{< details id="kalarm" title="kalarm" link="https://commits.kde.org/kalarm" >}}
+ Set default resources created on first run as standard calendars. [Commit.](http://commits.kde.org/kalarm/9e141111ab32983e407e41b6ebbaffa8465e70d7) 
+ When a resource is re-enabled, ensure its alarms are displayed. [Commit.](http://commits.kde.org/kalarm/99737933d5bc7ee243965feba8f5964ba59b36f4) 
+ Allow build without KAuth header files having been installed. [Commit.](http://commits.kde.org/kalarm/3d72300141405861d2f34acff19ee0e957f66ba4) 
+ Use typedef for email IDs. [Commit.](http://commits.kde.org/kalarm/6d678cee8cb3b88f9cb3feb2cec947f428570ac4) 
+ Remove unused dependency. [Commit.](http://commits.kde.org/kalarm/be700e8301c2b20e5c5387d1c5b67770787fedcd) 
{{< /details >}}
{{< details id="kapman" title="kapman" link="https://commits.kde.org/kapman" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kapman/874e78360e3b63782927858a270eefb3ca27064f) 
{{< /details >}}
{{< details id="kate" title="kate" link="https://commits.kde.org/kate" >}}
+ Fix possible infinite formatting calls. [Commit.](http://commits.kde.org/kate/e834835afb7b3ad58c19abc0bb0c0bf0f18d9633) Fixes bug [#452047](https://bugs.kde.org/452047)
+ Replace KateColorSchemeChooser with using KColorSchemeManager directly. [Commit.](http://commits.kde.org/kate/ea46115f0bcd25b3f5586376a4eb2b0caef8508f) 
+ Build: Append to textEdit directly for better performance. [Commit.](http://commits.kde.org/kate/c3f24d49496fa0970fadfad40fe853a3800f1f77) Fixes bug [#442739](https://bugs.kde.org/442739)
+ Snippets addon: comment out debug output. [Commit.](http://commits.kde.org/kate/477a1874ea3150de2a50e286c5a8f82a9b043c82) 
+ Ignore blame errors. [Commit.](http://commits.kde.org/kate/dc8369f75684bddd56ce2473e00258cb8684245f) Fixes bug [#451699](https://bugs.kde.org/451699)
+ Remove default shortcut for query-run action. [Commit.](http://commits.kde.org/kate/35692a6f2124609b88a9c57c925f9927d2b43536) 
+ Git widget: all columns should focus. [Commit.](http://commits.kde.org/kate/9db52a90e5f1f1a08ce0d11b298bd00caad35fd9) 
+ Ensure project loaded on start is activate. [Commit.](http://commits.kde.org/kate/e96b52061dbf8ea7e7e7e4f01b94ed2a7b14b48a) 
+ Cleanup git version detection code. [Commit.](http://commits.kde.org/kate/a06235c972bb176fa401ea18ee0908074713fdbb) 
+ Fix triplicated documents when occurs git conflict. [Commit.](http://commits.kde.org/kate/8362c6c226cda8e0638be3868d19fd6c07af32f8) 
+ Store projects in session as json lines. [Commit.](http://commits.kde.org/kate/85b367aa60f04934c3ee53d73bcbb478571b9fc5) 
{{< /details >}}
{{< details id="kblackbox" title="kblackbox" link="https://commits.kde.org/kblackbox" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kblackbox/811f66bde3a32532721ba0e01d7495a547630fef) 
{{< /details >}}
{{< details id="kblocks" title="kblocks" link="https://commits.kde.org/kblocks" >}}
+ Enable highdpi scaling. [Commit.](http://commits.kde.org/kblocks/e45775777c6baba2f212f573faecf61ce15eeef0) 
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kblocks/8331fca7d33b39c8129167ca6673a23f7063ac0b) 
{{< /details >}}
{{< details id="kbreakout" title="kbreakout" link="https://commits.kde.org/kbreakout" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kbreakout/a26c2cf1ee684d6ac385738602d25c7496887160) 
{{< /details >}}
{{< details id="kcharselect" title="kcharselect" link="https://commits.kde.org/kcharselect" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kcharselect/bad3eff0d2a4677f64e8bd6fe5b58bc845121cb4) Fixes bug [#451752](https://bugs.kde.org/451752)
{{< /details >}}
{{< details id="kcolorchooser" title="kcolorchooser" link="https://commits.kde.org/kcolorchooser" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kcolorchooser/0a584271020c4f4b7af600d227e01063ea8c38b3) Fixes bug [#451751](https://bugs.kde.org/451751)
{{< /details >}}
{{< details id="kdeconnect-kde" title="kdeconnect-kde" link="https://commits.kde.org/kdeconnect-kde" >}}
+ Enable highdpi for all executables. [Commit.](http://commits.kde.org/kdeconnect-kde/bafec85d06b5c80110acccc1552799f223486e0e) 
{{< /details >}}
{{< details id="kdenetwork-filesharing" title="kdenetwork-filesharing" link="https://commits.kde.org/kdenetwork-filesharing" >}}
+ Add missing KF5Auth dependency. [Commit.](http://commits.kde.org/kdenetwork-filesharing/26f463f163b23b8cf53f55f84fed2cab7a6c411a) 
+ Fix opening of samba KInfoCenter KCM. [Commit.](http://commits.kde.org/kdenetwork-filesharing/762748a59094a9a0e268d9132943589ad14f7b59) Fixes bug [#451091](https://bugs.kde.org/451091)
{{< /details >}}
{{< details id="kdenlive" title="kdenlive" link="https://commits.kde.org/kdenlive" >}}
+ Fix monitor image size with non integer screen scaling. [Commit.](http://commits.kde.org/kdenlive/b729c5c7f73bfd1463e30c3a8534a1597e6c4ae3) 
+ Fix app focus lost on Windows when exiting monitor fullscreen. [Commit.](http://commits.kde.org/kdenlive/7e0067a7e552c15330b69338d75f7d6f453883eb) 
+ Switch from QQuickView to QQuickWidget - fixes broken playback on Mac OS. [Commit.](http://commits.kde.org/kdenlive/a4682ad91d2a2e8f42ef6a0a06504848e140b841) 
+ Fix several cases of timeline losing focus. [Commit.](http://commits.kde.org/kdenlive/b6731517b73091f26010f0307040252b3f462061) 
+ Correctly update "apply" button on monitor change. [Commit.](http://commits.kde.org/kdenlive/62cf0613fec973b962d4812077be11352871d3a9) 
+ Make monitor detection more robust for fullscreen mode. [Commit.](http://commits.kde.org/kdenlive/5406eba019d6805e6654a16b3387ea44a288bff5) 
+ Fix resetting effect does not clear timeline keyframe view, resulting in possible crash. [Commit.](http://commits.kde.org/kdenlive/24f760e4fd1cd3ab66023d5038665e166a6b8a1c) 
+ Don't propose rtaudio backend if not available. [Commit.](http://commits.kde.org/kdenlive/ef92d8cc67046e1192d94d09ac61ffd00004cf59) 
+ Fix layout warning. [Commit.](http://commits.kde.org/kdenlive/786083774541dba535e403525b7c7ee4fe939f33) 
+ Fix play zone seeking to first frame of timeline. [Commit.](http://commits.kde.org/kdenlive/22dc6a7e5c1b650e609196ca2477437bc41afe34) 
+ Fix import keyframes importing outside clip out. [Commit.](http://commits.kde.org/kdenlive/803fcbee0626e4d55fffc5b1288d48a0b1c3b00e) 
+ Remove confusing "autorotate" checkbox in transcode to edit friendly. [Commit.](http://commits.kde.org/kdenlive/001007324369af3c6c5d814578576972bded3b11) 
+ Code quality fixes. [Commit.](http://commits.kde.org/kdenlive/d901abe7a994b93b1e588d769fc597e9c68d4a11) 
+ Fix fullscreen monitor selection doesn't work on Windows. [Commit.](http://commits.kde.org/kdenlive/79eade13a8af3c8c92e88dad10e45492cab8b8eb) 
+ Fix possible crash on exit. [Commit.](http://commits.kde.org/kdenlive/ea7791f54408b274c5efe872f91cdebafa15c1b1) 
+ Don't query producer length on each frame. [Commit.](http://commits.kde.org/kdenlive/145bf5100f44a8ef5f98a7339d5286ba262f57c7) 
+ Fix speed not saved in custom render profiles. [Commit.](http://commits.kde.org/kdenlive/3d9a1a572e983ced5eb2300fd7ca1a9be38d08fd) 
+ Code quality fixes. [Commit.](http://commits.kde.org/kdenlive/52b16a742f44e42661ffbfeb99a508a83cfc0372) 
+ Ensure we use the breeze widget style on first run in Mac. [Commit.](http://commits.kde.org/kdenlive/185e9b9d1c3ef84b5a6ab4f3dfdc422b65d9bdda) 
+ Make progress bar for effects more visible (for ex. in motion tracker). [Commit.](http://commits.kde.org/kdenlive/cde34ac0db2408bb4bc22e68d446933ff8791816) 
+ Fix project duration not updated on bin clip deletion. [Commit.](http://commits.kde.org/kdenlive/17e85bcfb74cc56680bd307650a9fe7261d59426) 
+ Fix timeline focus issues on drag & drop, fix mouse position and project duration in timeline toolbar not consistently updated. [Commit.](http://commits.kde.org/kdenlive/3be2b2867b24dab8aa4518fd315109d134db6958) 
+ Fix no speech end time in analysed speech. [Commit.](http://commits.kde.org/kdenlive/18a625dca0ca8330fcec86833f29cd3ca76b9b2c) 
+ Ignore audio files album art. [Commit.](http://commits.kde.org/kdenlive/9ec8f4f4579acd87b4921cf7972c0b9a3b380432) 
+ Fix typo (missing space). [Commit.](http://commits.kde.org/kdenlive/d6d061f277354e1c795ddc4bc668fc818295d7a8) 
+ Fix last silence analysis in speech to text, small drawing fixes. [Commit.](http://commits.kde.org/kdenlive/f44eab174a96adeec5bd0a37709a94ddce349ecc) 
+ Fix creating guides from project notes. [Commit.](http://commits.kde.org/kdenlive/eef8f05a732f1ebb91f81770498bef3e0e9ebd4c) 
+ Fix line feed lost on project notes paste. [Commit.](http://commits.kde.org/kdenlive/154812c8b574afcf908231488903f0e3f68d0f73) 
+ Add invert param to luma mix to allow reversing direction of transition. [Commit.](http://commits.kde.org/kdenlive/1e891803b9885571aed27b66c6602bf119785ac5) 
+ Only save bin thumbnail on project save to avoid displaying incorrect thumb after unsaved project change. [Commit.](http://commits.kde.org/kdenlive/78c6a435a9868b00fec6af820163099cf7065115) 
+ Fix freeze on add clip recently introduced. [Commit.](http://commits.kde.org/kdenlive/ffba99db04fb7907c056f279e105f7fc29492b12) 
+ Fix thumbnail cache bug causing incorrect thumbs to sometimes display after saving project. [Commit.](http://commits.kde.org/kdenlive/57152173d8b773944e0f00d278cf06a6e62597ae) 
+ Speech to text: cleaner html output for project files, fix work selection not really working. [Commit.](http://commits.kde.org/kdenlive/2933fe30134a0e9407c1e17e970bcf0242cba47c) 
+ Fix foxus issue on effect drop. [Commit.](http://commits.kde.org/kdenlive/7a2334cebcbb46f20de52bbc6d9bc8dded94b421) 
+ Smaller drag image in bin, also show it in icon view mode. [Commit.](http://commits.kde.org/kdenlive/0274866f2b1c23fa8cc6d4e63ab7210f9e65b28c) 
+ Fix startup warning. [Commit.](http://commits.kde.org/kdenlive/990e53e87b6676bbd3007c2ab535045212e82bde) 
+ Fix timeline focus issues on drag and drop. [Commit.](http://commits.kde.org/kdenlive/603b0d6876be112fa3028c4258dc9d3711a62dbf) 
+ Fix timeline scrolling below zero and timeline drag/drop bug. [Commit.](http://commits.kde.org/kdenlive/6b47852a90ae9a6fe622462097950c64af025bbd) 
+ [Renderer] Fix wrongly inverted logic to hide "Generate Script" button. [Commit.](http://commits.kde.org/kdenlive/68ad7435c3317558c73f7e4139c8882c12838d0b) 
+ Fixes for saving and editing render presets. [Commit.](http://commits.kde.org/kdenlive/7bc34df40f6baf9fbd05d2073359f40cbe34eb17) 
+ [Render Presets] GOP and B-Frames params: enable only if it makes sense. [Commit.](http://commits.kde.org/kdenlive/8a714260e1f5d2390f06a7d57914fc976c3d1aa9) 
+ Fix possible crash in bin when selecting a clip. [Commit.](http://commits.kde.org/kdenlive/ada77e207f37a597d4d398cde9445d63ef29b1cf) 
+ [Renderer] Reset DAR to 1:1 on resolution override to avoid errors. [Commit.](http://commits.kde.org/kdenlive/aa121f9d52c1098d6c12f03f393b2e3ab5d2337f) 
+ [Render Presets] Combo Box for PAR to prevent render errors. [Commit.](http://commits.kde.org/kdenlive/9905e26c397ccf3f9304023aad55f47b3f23fde7) 
+ Fix rubberband selection on scroll, and don't overlap track headers, fix move clip + mouse wheel. [Commit.](http://commits.kde.org/kdenlive/70378af1e5fc3979daa725b5a8972b593653c014) Fixes bug [#417209](https://bugs.kde.org/417209)
+ Fix razor tool misbehaviour on subtitle track. [Commit.](http://commits.kde.org/kdenlive/e7c10c2985b003b3d176182933ddea9ec881aebf) 
+ Fix keymap info on subtitle track. [Commit.](http://commits.kde.org/kdenlive/d93edff9c9d4af4b3663dafe2df4c58dce133390) 
+ Fix timecode rounding for fps like 23.98. [Commit.](http://commits.kde.org/kdenlive/4b7f91db359fdb493df7a49ebc94684694192ab9) Fixes bug [#435213](https://bugs.kde.org/435213)
+ Fix double warning and duplicate folder on manage cache data deletion. [Commit.](http://commits.kde.org/kdenlive/73e3994f7265337fdb596148aad8f7a975786727) Fixes bug [#434754](https://bugs.kde.org/434754)
+ Keep focus on clip monitor after editing marker. [Commit.](http://commits.kde.org/kdenlive/0dee8d47d7f410035d86e04ac011d9dfb516a26d) Fixes bug [#433595](https://bugs.kde.org/433595)
+ Right click on a timeline guide seeks to its position and shows context menu. [Commit.](http://commits.kde.org/kdenlive/6f288c8e28596796c1b3a53391f259a64b7dee8d) Fixes bug [#441014](https://bugs.kde.org/441014)
+ Fix editing title/color clips does not invalidate timeline preview. [Commit.](http://commits.kde.org/kdenlive/11b0c242b4a87c542470a5011716d7221233b21a) Fixes bug [#437427](https://bugs.kde.org/437427)
+ Titler: remember and restore last used text alignment. [Commit.](http://commits.kde.org/kdenlive/43f4e060a36df83c9dc8b59e6a80f821eee18210) See bug [#413572](https://bugs.kde.org/413572)
+ Bin: tree view: hide audio/video icons for audio or video only clips. icon view: hide audio/video icons depending on zoom level, fix tag color covering thumb on drop. [Commit.](http://commits.kde.org/kdenlive/37dfd901697ceb790a0e670ca07cc0e0325c189c) 
+ [Render Widget] Prettify by removing some frames. [Commit.](http://commits.kde.org/kdenlive/539b48ab06ceffdd35d21a2e8ad6e6b14d5cd856) 
+ [Render Presets] Always disable "Scanning" properly if "Progressive". [Commit.](http://commits.kde.org/kdenlive/fbb6ebf806e453618d7419d2a61bbc74838cc741) 
+ [Renderer] Fix preset gets deleted if saving is canceled. [Commit.](http://commits.kde.org/kdenlive/a3b8301dd05063208a4bb37783d76462150db458) 
+ Fix selection in bin icon view. [Commit.](http://commits.kde.org/kdenlive/a0827f2d20274c636ad25c21dc218c1af64aa459) 
+ Fix previous commit (inverted logic). [Commit.](http://commits.kde.org/kdenlive/ae8f2a06a367a3cfacdb0a5452f18e0dcc9de452) 
+ Fix extract frame using proxy clips. [Commit.](http://commits.kde.org/kdenlive/29a172a257387b1e5827f98c70bc19420ea3810e) 
+ Fix "Clip" menu not properly update. [Commit.](http://commits.kde.org/kdenlive/e39840ec6925189114facb044b60ae0955ac2f65) 
+ Fix regression "Extract Audio" always hidden. [Commit.](http://commits.kde.org/kdenlive/60316635521d8f43890709f855d2fd877c316ff7) 
+ [Render Widget] Fix threads param not updated. [Commit.](http://commits.kde.org/kdenlive/230d8ca58b55f227ee5124acef703009a076b563) 
+ Make audio/video usage icons more visible. [Commit.](http://commits.kde.org/kdenlive/67ad873256b2b28adb7537b36b9630e046a95efd) 
+ Spot remover, start with a small zone, not full screen. [Commit.](http://commits.kde.org/kdenlive/c623db8e7afcf41c8171855b94292010625088aa) 
+ Fix crash and corruption (disappearing effect) when dropping a clip close to 0 in timeline. [Commit.](http://commits.kde.org/kdenlive/a73d42f5309a8527072e29541033e02a4753ec95) 
+ Fix open clip in bin when multiple bins and in icon view. [Commit.](http://commits.kde.org/kdenlive/33634f32a11cf1abaab806944868e02955c1f4e6) 
+ Multiple fixes for bin. [Commit.](http://commits.kde.org/kdenlive/9ce629db1b473f95e72668d29648a725869acfee) 
{{< /details >}}
{{< details id="kdepim-addons" title="kdepim-addons" link="https://commits.kde.org/kdepim-addons" >}}
+ Update color when theme changed. [Commit.](http://commits.kde.org/kdepim-addons/af3bb5f914790eee6249657480ce13931bd20d43) 
+ Extract HTML parts based on decoded text, not raw data. [Commit.](http://commits.kde.org/kdepim-addons/9492fa31b7cd76277e44945592db6ab6837d2cc3) 
+ Add clear button. [Commit.](http://commits.kde.org/kdepim-addons/021708b9d40362cacda367dbd4e228a9fa639a8f) 
{{< /details >}}
{{< details id="kdepim-runtime" title="kdepim-runtime" link="https://commits.kde.org/kdepim-runtime" >}}
+ Fix open kmail when we click on button. [Commit.](http://commits.kde.org/kdepim-runtime/a4bdb16f187c3b177a4f3bc56c9fbb5206d21d69) 
{{< /details >}}
{{< details id="kdiamond" title="kdiamond" link="https://commits.kde.org/kdiamond" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kdiamond/c66e3f7405a1d31e294f94c09dc249d3f01368f6) 
{{< /details >}}
{{< details id="kfourinline" title="kfourinline" link="https://commits.kde.org/kfourinline" >}}
+ Fix undo sometimes making the AI play on wrong turn. [Commit.](http://commits.kde.org/kfourinline/b2a3b9dd48a2a1f86d7fcb107c535cc8672fb08d) Fixes bug [#351815](https://bugs.kde.org/351815)
{{< /details >}}
{{< details id="kget" title="kget" link="https://commits.kde.org/kget" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kget/9522d3091c083b2211d47b59bcf0abc0832cac89) 
+ Fix sqlite detection. [Commit.](http://commits.kde.org/kget/0c0a590784db83d2cc382d1ada10596a51b07110) 
{{< /details >}}
{{< details id="killbots" title="killbots" link="https://commits.kde.org/killbots" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/killbots/307cf5a1f50b1499c5bb2b4546e240363d5230e0) 
{{< /details >}}
{{< details id="kio-extras" title="kio-extras" link="https://commits.kde.org/kio-extras" >}}
+ Sftp: guard sftp_close it's not safe to call with nullptrs. [Commit.](http://commits.kde.org/kio-extras/ac49e5def4f6a22c868bd34e154cc980248c4419) Fixes bug [#447527](https://bugs.kde.org/447527)
{{< /details >}}
{{< details id="kiriki" title="kiriki" link="https://commits.kde.org/kiriki" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kiriki/eb6f6eb649440255117305666660221b49a2a1a5) 
{{< /details >}}
{{< details id="kiten" title="kiten" link="https://commits.kde.org/kiten" >}}
+ Enable highdpi scaling and pixmaps. [Commit.](http://commits.kde.org/kiten/d1e16dd37e00f971380031192b8a01ef297d01b0) 
{{< /details >}}
{{< details id="kitinerary" title="kitinerary" link="https://commits.kde.org/kitinerary" >}}
+ Add basic extractor script for Peach Aviation. [Commit.](http://commits.kde.org/kitinerary/2e5f734d5890a646764863b387b2f17fe64ba5bb) 
+ Fix extracting Renfe train names from newer PDF tickets. [Commit.](http://commits.kde.org/kitinerary/d18dae5ee73715c8fd4d78be4d3e2088726eb5d9) 
+ Properly merge Renfe tickets with and without the commuter option. [Commit.](http://commits.kde.org/kitinerary/5b179138cb2687a6b9cfbeff143c9c29e781a3ed) 
+ Support extracting Renfe pkpass files. [Commit.](http://commits.kde.org/kitinerary/3c6df9873d8e21ea0298e5aa000b1101ba108178) Fixes bug [#451974](https://bugs.kde.org/451974)
{{< /details >}}
{{< details id="klickety" title="klickety" link="https://commits.kde.org/klickety" >}}
+ Revert "org.kde.ksame.appdata: Update help url to avoid redirection". [Commit.](http://commits.kde.org/klickety/0bff7e327b1b453283e278ec24508d4918c3ae2e) 
{{< /details >}}
{{< details id="klines" title="klines" link="https://commits.kde.org/klines" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/klines/d219df554bb36c5ad2babf37effc6ad45485c07b) 
{{< /details >}}
{{< details id="kmag" title="kmag" link="https://commits.kde.org/kmag" >}}
+ Revert "Fix dependancy". [Commit.](http://commits.kde.org/kmag/a9f4c2a5da295f1168635f3a364bfbf3f9832a0b) 
{{< /details >}}
{{< details id="kmail" title="kmail" link="https://commits.kde.org/kmail" >}}
+ Update color when we change theme. [Commit.](http://commits.kde.org/kmail/199dfee455fd41303fc208648e7c631b30295729) 
+ Fix build on FreeBSD by linking to Akonadi. [Commit.](http://commits.kde.org/kmail/02d60acdb11f9ba9076328db09abd99fc5a96dae) 
{{< /details >}}
{{< details id="kmix" title="kmix" link="https://commits.kde.org/kmix" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kmix/a7b36dac7315343c6f5795005aceafc09a812061) Fixes bug [#451756](https://bugs.kde.org/451756)
{{< /details >}}
{{< details id="knavalbattle" title="knavalbattle" link="https://commits.kde.org/knavalbattle" >}}
+ Add missing KCoreAddons dep. [Commit.](http://commits.kde.org/knavalbattle/63c07ef82dabaa44f91d36921576abcce664d052) 
{{< /details >}}
{{< details id="knetwalk" title="knetwalk" link="https://commits.kde.org/knetwalk" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/knetwalk/94e788eb544000283aa332b5d334e93a12cac8f2) 
{{< /details >}}
{{< details id="knotes" title="knotes" link="https://commits.kde.org/knotes" >}}
+ Update color when theme changed. [Commit.](http://commits.kde.org/knotes/fe123f3f5ecabef299ec278ff844877a876e0cd2) 
{{< /details >}}
{{< details id="kollision" title="kollision" link="https://commits.kde.org/kollision" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kollision/56fd9fdc1e6331a4eb09d1a73b5238ef771e0bbd) 
{{< /details >}}
{{< details id="konqueror" title="konqueror" link="https://commits.kde.org/konqueror" >}}
+ Always enable WebRTCPipeWireCapture. [Commit.](http://commits.kde.org/konqueror/8794f7cbcabd336b57118b27a69b49f6ede3d61e) 
+ Append to environment variable instead of overwriting it. [Commit.](http://commits.kde.org/konqueror/208e7eec129b48e2ff93b1e3bf0e74e89fd7e11d) 
+ Enable WebRTCPipeWireCapturer feature for QtWebEngine. [Commit.](http://commits.kde.org/konqueror/93a1b098cad3e83afeba467d8528c19edfe000b5) Fixes bug [#450840](https://bugs.kde.org/450840)
{{< /details >}}
{{< details id="konsole" title="konsole" link="https://commits.kde.org/konsole" >}}
+ Remove block of code that should not be there, and should do nothing. [Commit.](http://commits.kde.org/konsole/2f5ce7ed919abb0ba9f5bcf5cfecbc7ab385d1df) 
+ Fix XTSMGRAPHICS (report sixel number of color registers and image size). [Commit.](http://commits.kde.org/konsole/7b919ec0ca52bb7a64f34ba4a3d7f3b730a0d898) 
+ Scroll when a drawn sixel extends beyond bottom of line. [Commit.](http://commits.kde.org/konsole/336e310fab88646ae32a58614592b48258d9d6a4) 
+ Add missed two parameters in call to addPlacement from iterm2 inline image escape sequence. [Commit.](http://commits.kde.org/konsole/70aecfe9283d0205337633be111088ab46623144) 
+ When an escape sequence is longer than the buffer, keep the last two chars. [Commit.](http://commits.kde.org/konsole/96a18a9b7f651e27da99f8b483d84fdb9a1a1e62) 
+ Fix problems discovered by coverity (and more):. [Commit.](http://commits.kde.org/konsole/ae24b4e7ea18b9f0cb64a008ca93b2d5a4d5a410) 
+ Add `DoNotMoveCursor` option to iterm2 inline image protocol. [Commit.](http://commits.kde.org/konsole/8c9014bbe6ea09ce33108c5a4bff147132275bfb) 
+ Add support for `OSC 4` and `OSC 104`. [Commit.](http://commits.kde.org/konsole/a2029280eaca0051d59f10204b03159de6399af5) 
+ Add support for `CSI 16 t` - report character cell size in pixels. [Commit.](http://commits.kde.org/konsole/891bc75ced1400008019e9f405c6afefe4dee17d) 
{{< /details >}}
{{< details id="konversation" title="konversation" link="https://commits.kde.org/konversation" >}}
+ Task manager badge: ignore events from channels with disabled notifications. [Commit.](http://commits.kde.org/konversation/e0a176f8ad58f8c6019df6395c4a0901fa8a7d92) Fixes bug [#451814](https://bugs.kde.org/451814)
{{< /details >}}
{{< details id="korganizer" title="korganizer" link="https://commits.kde.org/korganizer" >}}
+ Trigger the reminder daemon fallback start code also in Kontact. [Commit.](http://commits.kde.org/korganizer/c644a1ba331f6e7ce04af3142759c9e761fd814c) 
+ Today button moves Month view to today's month. [Commit.](http://commits.kde.org/korganizer/c8809c2dc0be2c48eedd956620a8c48e483f11b9) Fixes bug [#333066](https://bugs.kde.org/333066)
+ Fix mem leak. [Commit.](http://commits.kde.org/korganizer/2ac201a43f35c6f6835ecc18e875539ba75a21c9) 
+ Remove unused include. [Commit.](http://commits.kde.org/korganizer/130f7f9eddcf0e7e1183ec855c280df212d550d3) 
+ Save the agenda and month views' "icons to use" settings. [Commit.](http://commits.kde.org/korganizer/23fb3b25a44ecffdad17dd44ddbac42fc0ec650f) Fixes bug [#449473](https://bugs.kde.org/449473)
{{< /details >}}
{{< details id="kosmindoormap" title="kosmindoormap" link="https://commits.kde.org/kosmindoormap" >}}
+ Only merge ways on synthetic nodes. [Commit.](http://commits.kde.org/kosmindoormap/2ffd5b27899dc6cdb8df0ea33daa7fb526539ae3) 
+ Fix geometry reassembly when we get both lines and areas for the same way. [Commit.](http://commits.kde.org/kosmindoormap/93784bec51f5281db93b7f93855fec4a50869ca9) 
{{< /details >}}
{{< details id="kpimtextedit" title="kpimtextedit" link="https://commits.kde.org/kpimtextedit" >}}
+ Remove not necessary margin. [Commit.](http://commits.kde.org/kpimtextedit/feb3175330103b549d77674b5a2d6efe314d1efa) 
+ Make sure to update link color when theme changed. [Commit.](http://commits.kde.org/kpimtextedit/07ed743cc8f953ac805bf3257263a35883e82bcb) 
+ Fix compile against qt6. [Commit.](http://commits.kde.org/kpimtextedit/b53b906f3d32cf3626bd4b67b50052398153498f) 
+ Fix color when we change theme. [Commit.](http://commits.kde.org/kpimtextedit/f1faada821989c94e38417092affcbf8f1ad9a1b) 
{{< /details >}}
{{< details id="krfb" title="krfb" link="https://commits.kde.org/krfb" >}}
+ Make KF5Wayland optional behind existing DISABLE_PIPEWIRE. [Commit.](http://commits.kde.org/krfb/4b7985db1b7ba0ef4fa0c5e2a664f0531026d0be) 
{{< /details >}}
{{< details id="kruler" title="kruler" link="https://commits.kde.org/kruler" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kruler/e589bd85866dd7c4db050b9b25cdca9f31493ebd) Fixes bug [#451754](https://bugs.kde.org/451754)
{{< /details >}}
{{< details id="kshisen" title="kshisen" link="https://commits.kde.org/kshisen" >}}
+ Add missing find_package for KCoreAddons. [Commit.](http://commits.kde.org/kshisen/7d20a5e5db803335890e97c36a491b34ae19efc5) 
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kshisen/f082a03135631972ec8b2ec257e11b31cae7641c) 
{{< /details >}}
{{< details id="kspaceduel" title="kspaceduel" link="https://commits.kde.org/kspaceduel" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kspaceduel/b0991bde00ed93beb4a60cea1d486fcabde46303) 
{{< /details >}}
{{< details id="kteatime" title="kteatime" link="https://commits.kde.org/kteatime" >}}
+ Add missing KCoreAddons dep and link. [Commit.](http://commits.kde.org/kteatime/20d34c64cb1b8e0b5a650654c137cbe832272ba8) 
{{< /details >}}
{{< details id="kwalletmanager" title="kwalletmanager" link="https://commits.kde.org/kwalletmanager" >}}
+ Kwalletmanager: do not show window on activation if not already shown. [Commit.](http://commits.kde.org/kwalletmanager/e1391cb3ab7eaf929989e7954180daff3b9c7566) Fixes bug [#451881](https://bugs.kde.org/451881)
{{< /details >}}
{{< details id="kwave" title="kwave" link="https://commits.kde.org/kwave" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/kwave/45d40afe8443eae3e30ce4df2790bd9ae8b0d765) 
{{< /details >}}
{{< details id="libkdegames" title="libkdegames" link="https://commits.kde.org/libkdegames" >}}
+ KgThemeProvider: Smooth previews. [Commit.](http://commits.kde.org/libkdegames/0aab524add6dbb0beaa402a3f2609a9dcd9166b8) 
+ KGameThemeSelector: Fix preview in HiDPI mode. [Commit.](http://commits.kde.org/libkdegames/f4e27ac16ab4acbe25b56a03bc1b152c8c02c1ab) 
+ KGamePopupItem: Fix icon and text alignment in HiDPI mode. [Commit.](http://commits.kde.org/libkdegames/7b9e6f0331cfa3627a3792d7ff8bbda1c3475897) 
+ KGamePopupItem: Use fixed offset from canvas border. [Commit.](http://commits.kde.org/libkdegames/5b7bd359dc967a727a90e3f05706fa306b7f9ab2) 
+ KGamePopupItem: Fix incorrect positioning relative to canvas. [Commit.](http://commits.kde.org/libkdegames/5adee9b53eb7f36807a9845c8aa4afd9466c2fa9) 
{{< /details >}}
{{< details id="libkeduvocdocument" title="libkeduvocdocument" link="https://commits.kde.org/libkeduvocdocument" >}}
+ Fix replacing QString::left() with QString::at(). [Commit.](http://commits.kde.org/libkeduvocdocument/3a34565b55a5980dbabab69be1cbfe1786a2007c) 
{{< /details >}}
{{< details id="libkgapi" title="libkgapi" link="https://commits.kde.org/libkgapi" >}}
+ Increase minimum KF5 version to actual minimum KF5 version. [Commit.](http://commits.kde.org/libkgapi/09aabd427ad46e4a5f1abe6e601ccc81edcbd9e3) 
{{< /details >}}
{{< details id="libksane" title="libksane" link="https://commits.kde.org/libksane" >}}
+ Improve hiding single option entry lists. [Commit.](http://commits.kde.org/libksane/78dd1ff68c8b99e635f4f387972e2de5045e3fa1) 
{{< /details >}}
{{< details id="libksieve" title="libksieve" link="https://commits.kde.org/libksieve" >}}
+ Add parent. [Commit.](http://commits.kde.org/libksieve/5be10919521995972f26494d73d8b47692ac648d) 
{{< /details >}}
{{< details id="lskat" title="lskat" link="https://commits.kde.org/lskat" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/lskat/5b44e06d73efc70b224e50ed24db5d7153b0951c) 
{{< /details >}}
{{< details id="mailcommon" title="mailcommon" link="https://commits.kde.org/mailcommon" >}}
+ Update color when theme changed here too. [Commit.](http://commits.kde.org/mailcommon/fa4897d0766a3f8b7573612a89ca6ce4af44ade0) 
+ Update color when theme changed. [Commit.](http://commits.kde.org/mailcommon/6973f532e938fdd9af7503206f951df102924735) 
{{< /details >}}
{{< details id="messagelib" title="messagelib" link="https://commits.kde.org/messagelib" >}}
+ Export QCH targets after they are created. [Commit.](http://commits.kde.org/messagelib/fba4329464e9e96e3dce304cb9cf87aae7de6e0e) 
+ Fix color when we change color theme. [Commit.](http://commits.kde.org/messagelib/6873b50fb671b4c183782a63fc64fda103c8bafb) 
+ Fix background color. [Commit.](http://commits.kde.org/messagelib/7fabdc2592b180a084d09b4dce840a6bda0819af) 
+ Fix update color when theme changed. [Commit.](http://commits.kde.org/messagelib/ca878fa5f405f3dfe552c4807d730767628e55a3) 
+ Debug--. [Commit.](http://commits.kde.org/messagelib/a39d7386d245260781f31253993db375b578a3fc) 
+ Update color when theme changed. [Commit.](http://commits.kde.org/messagelib/d5a0cebfdf24c4ef47f8a0a9d70fef7d00fdc19e) 
+ Add missing KCoreAddons dep and link. [Commit.](http://commits.kde.org/messagelib/fd84f4bde828e8a04c77e3fd1f08e7d78a8c708b) 
+ Readd support for showing emoticon. [Commit.](http://commits.kde.org/messagelib/97a7139ac69924a52d1f3c79d2bbf41e27bc6775) 
+ Add missing Q_REQUIRED_RESULT. [Commit.](http://commits.kde.org/messagelib/6f332a0e29cea5495f0131ca8110b7e48a2d2c4b) 
+ Update number of emails even if it's maximum value. [Commit.](http://commits.kde.org/messagelib/3f963917a305904267f4f4476a4e4a1cb90051e4) 
{{< /details >}}
{{< details id="okular" title="okular" link="https://commits.kde.org/okular" >}}
+ Markdown: Fix images with special chars in URLs not loaded. [Commit.](http://commits.kde.org/okular/5339b7eef1826b2eb35f345627a16d03eb65baeb) 
+ Try to fix randomly failing jenkins. [Commit.](http://commits.kde.org/okular/140b37c340aa7d2f27d05b34c820d9c83e335a30) 
+ Don't create a KIconLoader instance. [Commit.](http://commits.kde.org/okular/adc01be2639706c9425766f49093282ea8c9eb84) 
+ Fix opening CHM files on Windows. [Commit.](http://commits.kde.org/okular/dbcda67ab986a46815c154d49d414b4d2cbdcb61) Fixes bug [#451985](https://bugs.kde.org/451985)
+ Simplify `QPainter::drawRoundedRect` usage. [Commit.](http://commits.kde.org/okular/2ad9e6f0b322d0e5f3603a5d686bd718b479da28) 
+ Bring corner radius of popup messages in line with widget style. [Commit.](http://commits.kde.org/okular/1d30b03e39a50537144e2a6b75c3455b96b55c55) 
+ Restore properly rounded corners of popup messages. [Commit.](http://commits.kde.org/okular/667e73325ab83e7292712fd9938b22aff379b57c) 
{{< /details >}}
{{< details id="pim-sieve-editor" title="pim-sieve-editor" link="https://commits.kde.org/pim-sieve-editor" >}}
+ Fix react when color scheme changed. [Commit.](http://commits.kde.org/pim-sieve-editor/ea1d0d43e15c07c33f3b21eaeb7776bd147749fb) 
+ Add missing KCoreAddons dep and link. [Commit.](http://commits.kde.org/pim-sieve-editor/8f7027ab238feafd395d7be18b48cc79c716fa2b) 
{{< /details >}}
{{< details id="print-manager" title="print-manager" link="https://commits.kde.org/print-manager" >}}
+ Revert "Port to PlasmaExtras version of Highlight". [Commit.](http://commits.kde.org/print-manager/1abb32b79632b00685e8997ac07863f23b3e8748) Fixes bug [#451713](https://bugs.kde.org/451713)
{{< /details >}}
{{< details id="rocs" title="rocs" link="https://commits.kde.org/rocs" >}}
+ Add missing KCoreAddons link. [Commit.](http://commits.kde.org/rocs/f37c13758f0035085d4b4492521852a7580287ee) 
{{< /details >}}
{{< details id="signon-kwallet-extension" title="signon-kwallet-extension" link="https://commits.kde.org/signon-kwallet-extension" >}}
+ Add CI. [Commit.](http://commits.kde.org/signon-kwallet-extension/d69dcb64480f08a6f8fcb59f6b1bcbe4bda0a292) 
+ Add INSTALL_BROKEN_SIGNON_EXTENSION cmake option. [Commit.](http://commits.kde.org/signon-kwallet-extension/d844e9bc0dd27f856aa039b6fc211bc83974a3ce) 
{{< /details >}}
{{< details id="skanpage" title="skanpage" link="https://commits.kde.org/skanpage" >}}
+ Fix page manipulation of active page. [Commit.](http://commits.kde.org/skanpage/38ba68e3557ac9b83948b5980c0f5b73efb11f89) 
{{< /details >}}
{{< details id="umbrello" title="umbrello" link="https://commits.kde.org/umbrello" >}}
+ Fix "Pressing OK in General Properties of Initial or End activity changes its type/shape". [Commit.](http://commits.kde.org/umbrello/5a9b74243d2e59ab5b8ebf4ed12457cd282a3d79) Fixes bug [#451735](https://bugs.kde.org/451735)
{{< /details >}}
