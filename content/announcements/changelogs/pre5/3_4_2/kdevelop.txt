2005-05-24 12:11 +0000 [r417719]  adrian

	* parts/snippet/snippetitem.h: fix compile with current gcc

2005-06-20 09:31 +0000 [r427306]  coolo

	* vcs/compat/svn/Makefile.am: go away

2005-06-27 15:18 +0000 [r429401]  binner

	* configure.in.in, kdevelop.lsm: 3.4.2 preparations

2005-06-29 08:05 +0000 [r429868]  rdale

	* languages/ruby/debugger/rdbparser.cpp,
	  languages/ruby/debugger/variablewidget.cpp,
	  languages/ruby/debugger/rdbcontroller.cpp: * Promoted some
	  debugger fixes to the release branch * Constants are no longer
	  shown in the debugger as there are too many in the Qt/KDE ruby
	  bindings and it slows the debugger down and clutters the variable
	  tree

