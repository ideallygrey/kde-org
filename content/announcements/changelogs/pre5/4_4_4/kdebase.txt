------------------------------------------------------------------------
r1120719 | mueller | 2010-04-30 07:52:27 +1200 (Fri, 30 Apr 2010) | 2 lines

bump version

------------------------------------------------------------------------
r1120898 | scripty | 2010-04-30 13:56:42 +1200 (Fri, 30 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1121083 | trueg | 2010-04-30 22:00:05 +1200 (Fri, 30 Apr 2010) | 1 line

Backport: Properly encode URLs in the filter strings. This fixes an issue with spaces (and other special chars) in file names.
------------------------------------------------------------------------
r1121161 | nlecureuil | 2010-05-01 03:58:03 +1200 (Sat, 01 May 2010) | 4 lines

Add QCoreApplication , this fixes encoding issues
Big part from dfaure


------------------------------------------------------------------------
r1121274 | scripty | 2010-05-01 13:50:21 +1200 (Sat, 01 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1122046 | scripty | 2010-05-03 13:55:40 +1200 (Mon, 03 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1122210 | nlecureuil | 2010-05-03 21:37:58 +1200 (Mon, 03 May 2010) | 3 lines

Backport commit 1122208
CCBUG: 235984

------------------------------------------------------------------------
r1122353 | dfaure | 2010-05-04 05:05:12 +1200 (Tue, 04 May 2010) | 5 lines

Don't create broken proxy urls like ftp://:8080 from empty lineedits.
Based on investigation by Ingomar Wesp - thanks!
Fixed for: 4.4.4
BUG: 195085

------------------------------------------------------------------------
r1122355 | markuss | 2010-05-04 05:10:56 +1200 (Tue, 04 May 2010) | 1 line

Fix missing icon
------------------------------------------------------------------------
r1122762 | ppenz | 2010-05-05 04:18:02 +1200 (Wed, 05 May 2010) | 3 lines

Backport SVN commit 1121567: Don't show svn:ignores as versioned files. Thanks to Felix Möller for the patch!

CCBUG: 227373
------------------------------------------------------------------------
r1122916 | scripty | 2010-05-05 13:55:07 +1200 (Wed, 05 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1123232 | johnflux | 2010-05-06 05:17:52 +1200 (Thu, 06 May 2010) | 6 lines

Fix crash - we could call endMoveRows when we hadn't called beginMoveRows

This was the result of many many bug reports.  I only found the reason because of more strict assertions in Qt 4.7.  Thank you Qt!

BUG:227475
BUG:230488
------------------------------------------------------------------------
r1123233 | johnflux | 2010-05-06 05:18:10 +1200 (Thu, 06 May 2010) | 1 line

Expand init correctly
------------------------------------------------------------------------
r1123234 | johnflux | 2010-05-06 05:18:26 +1200 (Thu, 06 May 2010) | 1 line

Change from using layoutAboutToBeChanged  to using the new beginMoveRows API in qt 4.6
------------------------------------------------------------------------
r1123257 | aseigo | 2010-05-06 06:16:07 +1200 (Thu, 06 May 2010) | 3 lines

stop the timer; patch by Stefan Br?\195?\188ns
BUG:230740

------------------------------------------------------------------------
r1123262 | aseigo | 2010-05-06 06:25:02 +1200 (Thu, 06 May 2010) | 3 lines

drop the cached shadow on theme change as well, patch by David Benjamin
CCBUG:233643

------------------------------------------------------------------------
r1123665 | mart | 2010-05-07 03:12:43 +1200 (Fri, 07 May 2010) | 4 lines

since trunk doesn't behave this way don't activate the systray also in
branch (whie still doing popups)
BUG:207629

------------------------------------------------------------------------
r1123885 | trueg | 2010-05-07 20:19:40 +1200 (Fri, 07 May 2010) | 1 line

Backport: properly initialize member variables to prevent crashes before being initialized.
------------------------------------------------------------------------
r1124349 | aseigo | 2010-05-09 10:44:29 +1200 (Sun, 09 May 2010) | 3 lines

an optimization in the constraints event broke this; fix it
CCBUG:236874

------------------------------------------------------------------------
r1124366 | mmrozowski | 2010-05-09 12:06:40 +1200 (Sun, 09 May 2010) | 2 lines

Backport r1124361
CCBUG: 224281
------------------------------------------------------------------------
r1124384 | scripty | 2010-05-09 13:41:20 +1200 (Sun, 09 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1124532 | trueg | 2010-05-09 21:36:51 +1200 (Sun, 09 May 2010) | 1 line

Backport: do not follow nie:url using Nepomuk::Resource.
------------------------------------------------------------------------
r1124546 | ossi | 2010-05-09 22:45:26 +1200 (Sun, 09 May 2010) | 7 lines

append additional x server args after the display name

solaris' server is a bit picky about argument order.

BUG: 236170


------------------------------------------------------------------------
r1124605 | ossi | 2010-05-10 04:32:29 +1200 (Mon, 10 May 2010) | 9 lines

fix merge

sorry for the fuckup. no idea how patch managed to do *that*, but
evidently i didn't pay attention to the result. :}

CCBUG: 236170
CCMAIL: zander@kde.org


------------------------------------------------------------------------
r1124663 | lueck | 2010-05-10 07:38:58 +1200 (Mon, 10 May 2010) | 4 lines

backport from trunk:Fix next and previous page functions in khelpcenter, patch by David Benjamin
CCMAIL:davidben@mit.edu
CCBUG:211545
CCBUG:BUG:137107
------------------------------------------------------------------------
r1124665 | lueck | 2010-05-10 07:40:21 +1200 (Mon, 10 May 2010) | 2 lines

backport from trunk:Use the BrowserExtension to navigate between pages, patch by David Benjamin
CCMAIL:davidben@mit.edu
------------------------------------------------------------------------
r1124667 | lueck | 2010-05-10 07:41:50 +1200 (Mon, 10 May 2010) | 3 lines

backport from trunk:Schedule TOC builds as part of a setOpen, patch by David Benjamin
CCMAIL:davidben@mit.edu
CCBUG:179427
------------------------------------------------------------------------
r1124747 | scripty | 2010-05-10 13:51:10 +1200 (Mon, 10 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1125038 | hpereiradacosta | 2010-05-11 03:15:02 +1200 (Tue, 11 May 2010) | 5 lines

Backport r1125026
Use _helper.viewFocusBrush() in place of kcolorscheme(...) every time it is possible, for optimization 
purposes. Thanks to Allan Sandfeld Jensen for providing the original patch.
CCBUG: 226719

------------------------------------------------------------------------
r1125603 | dfaure | 2010-05-12 08:08:11 +1200 (Wed, 12 May 2010) | 3 lines

Fix location bar getting focus when opening konqueror with a url (or more) as cmdline argument.
Regression reported by Maksim, and coming from a seemingly innocent change in r1120142.

------------------------------------------------------------------------
r1125643 | hpereiradacosta | 2010-05-12 09:34:26 +1200 (Wed, 12 May 2010) | 4 lines

Backport r1106745
Added protection to disable animation when target is destroyed.
CCBUG: 216215

------------------------------------------------------------------------
r1125950 | mart | 2010-05-13 07:16:58 +1200 (Thu, 13 May 2010) | 2 lines

backport fix to 237234

------------------------------------------------------------------------
r1126207 | mart | 2010-05-13 21:50:16 +1200 (Thu, 13 May 2010) | 3 lines

backport fix to 237218
BUG:237218

------------------------------------------------------------------------
r1126343 | fredrik | 2010-05-14 07:13:22 +1200 (Fri, 14 May 2010) | 5 lines

Backport r1126342:

Fix a crash in configAccepted() when changing the preview settings
when the applet is on the panel.

------------------------------------------------------------------------
r1126347 | fredrik | 2010-05-14 07:39:15 +1200 (Fri, 14 May 2010) | 5 lines

Backport r1126346:

If a containment action is assigned to the left mouse button,
give it precedence over elastic-band selections.

------------------------------------------------------------------------
r1126467 | scripty | 2010-05-14 14:03:13 +1200 (Fri, 14 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1126656 | lunakl | 2010-05-15 02:57:55 +1200 (Sat, 15 May 2010) | 4 lines

Check whether utmp USER_PROCESS entries are still valid
the same way coreutils/procps do in who/w (bnc#600812).


------------------------------------------------------------------------
r1127437 | hpereiradacosta | 2010-05-17 03:32:05 +1200 (Mon, 17 May 2010) | 6 lines

Backport r1127429
Check QX11Info::depth() to decide whether alphachannel can be use to paint widgets like 
QMenu, ComboBox containers, detached toolbars and detached 
dockpanels.
CCBUG: 237772

------------------------------------------------------------------------
r1127537 | rkcosta | 2010-05-17 09:40:53 +1200 (Mon, 17 May 2010) | 5 lines

Backport r1127536.

Fix compilation on systems without utmpx (and thus no ut_pid).
CCMAIL: l.lunak@kde.org

------------------------------------------------------------------------
r1127555 | hpereiradacosta | 2010-05-17 10:34:47 +1200 (Mon, 17 May 2010) | 3 lines

Backport r1127549
 do not draw menu frame+background for embedded menus

------------------------------------------------------------------------
r1127584 | scripty | 2010-05-17 14:16:54 +1200 (Mon, 17 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1127877 | jacopods | 2010-05-18 09:31:35 +1200 (Tue, 18 May 2010) | 3 lines

backport r1127874
CCBUG: 230151

------------------------------------------------------------------------
r1127879 | anschneider | 2010-05-18 09:52:41 +1200 (Tue, 18 May 2010) | 2 lines

kio_sftp: Try to restore the owner and group if we overwrite a file.

------------------------------------------------------------------------
r1127913 | rysin | 2010-05-18 13:56:58 +1200 (Tue, 18 May 2010) | 1 line

BUG: 209247
------------------------------------------------------------------------
r1127922 | rysin | 2010-05-18 14:36:57 +1200 (Tue, 18 May 2010) | 3 lines

Fixed in KDE 4.4 branch as well
BUG: 196190

------------------------------------------------------------------------
r1127923 | scripty | 2010-05-18 14:38:51 +1200 (Tue, 18 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1128193 | lunakl | 2010-05-19 04:15:08 +1200 (Wed, 19 May 2010) | 3 lines

backport 1128192


------------------------------------------------------------------------
r1128233 | mfuchs | 2010-05-19 06:41:15 +1200 (Wed, 19 May 2010) | 4 lines

Backport r1128227
When deleting an XDG path in the path KCM it is set to $HOME as suggested in the XDG-user-dirs spec.
This fixes some issues e.g. KGlobalSettings::downloadPath() pointing to "/".

------------------------------------------------------------------------
r1128319 | afiestas | 2010-05-19 10:45:19 +1200 (Wed, 19 May 2010) | 2 lines

Backported fix that fix the "positioning" of the QuickSand interface.

------------------------------------------------------------------------
r1128322 | afiestas | 2010-05-19 10:49:00 +1200 (Wed, 19 May 2010) | 2 lines

Unhardcoded the label color

------------------------------------------------------------------------
r1128330 | afiestas | 2010-05-19 11:06:19 +1200 (Wed, 19 May 2010) | 2 lines

Backported manually, added Esc shortcut to QuickSand interface (Esc == hide the interface)

------------------------------------------------------------------------
r1128341 | hpereiradacosta | 2010-05-19 12:26:00 +1200 (Wed, 19 May 2010) | 6 lines

backport r1128339
announce to kstyle that comboboxes and spinboxes can be painted frameless. This increases the 
available edition rect.

CCBUG: 237770

------------------------------------------------------------------------
r1128350 | scripty | 2010-05-19 13:50:49 +1200 (Wed, 19 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1128371 | johnflux | 2010-05-19 18:23:14 +1200 (Wed, 19 May 2010) | 3 lines

Fix mem leak

Backport from mlaurent's commit in master
------------------------------------------------------------------------
r1128405 | dfaure | 2010-05-19 20:28:55 +1200 (Wed, 19 May 2010) | 2 lines

Fix runtime warning "Object::disconnect: Unexpected null parameter"

------------------------------------------------------------------------
r1128622 | ppenz | 2010-05-20 07:35:50 +1200 (Thu, 20 May 2010) | 3 lines

Backport of SVN commit 1128615: Don't change the default view properties, if the view mode is changed on a remote folder.

CCBUG: 234852
------------------------------------------------------------------------
r1129019 | scripty | 2010-05-21 13:48:08 +1200 (Fri, 21 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1129208 | hpereiradacosta | 2010-05-22 04:03:32 +1200 (Sat, 22 May 2010) | 6 lines

Backport r1129205
Allow "disabled" "busy" progressBars to be animated.
Also, reviewed the logic to start busy scrollbar animations
This is based on (but not identical to) the patch provided by Jonathan Liu.
CCBUG: 238360

------------------------------------------------------------------------
r1129216 | jacopods | 2010-05-22 04:49:50 +1200 (Sat, 22 May 2010) | 3 lines

Backport r1129215
CCBUG:226710

------------------------------------------------------------------------
r1129352 | scripty | 2010-05-22 13:54:20 +1200 (Sat, 22 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1129395 | afiestas | 2010-05-22 20:14:57 +1200 (Sat, 22 May 2010) | 2 lines

Backport to take care of taskoriented interface here

------------------------------------------------------------------------
r1129408 | afiestas | 2010-05-22 21:35:12 +1200 (Sat, 22 May 2010) | 2 lines

Fix like 9 crashes of at least 3 different kind

------------------------------------------------------------------------
r1129410 | afiestas | 2010-05-22 21:43:30 +1200 (Sat, 22 May 2010) | 2 lines

Uuups! removed qDebug

------------------------------------------------------------------------
r1129411 | afiestas | 2010-05-22 21:47:49 +1200 (Sat, 22 May 2010) | 2 lines

YAQDM (Yet another qDebug message), sorry for that is my first time using svnbackport >.<!

------------------------------------------------------------------------
r1129726 | trueg | 2010-05-24 02:44:16 +1200 (Mon, 24 May 2010) | 1 line

Backport: Also update the filename property when moving a file resource.
------------------------------------------------------------------------
r1130300 | scripty | 2010-05-25 14:33:50 +1200 (Tue, 25 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1130362 | nlecureuil | 2010-05-25 20:05:22 +1200 (Tue, 25 May 2010) | 3 lines

Fix build
CCMAIL: Alex Fiestas <alex@eyeos.org>

------------------------------------------------------------------------
r1130817 | freininghaus | 2010-05-27 01:35:43 +1200 (Thu, 27 May 2010) | 11 lines

Backport the bug fix part of r1124982 to the 4.4 branch.

This prevents crashes when Dolphin reads a dolphinrc file that has
been written by a different Dolphin version which has more or less
columns. Crashes could occur if the "column positions" list in
dolphinrc has less entries than the running Dolphin version expects or
if it has more, and a "column position" is outside the valid range.

Will be in 4.4.4.


------------------------------------------------------------------------
r1130851 | trueg | 2010-05-27 04:11:14 +1200 (Thu, 27 May 2010) | 1 line

Backport: crash fix on logout
------------------------------------------------------------------------
r1130862 | jacopods | 2010-05-27 04:34:52 +1200 (Thu, 27 May 2010) | 3 lines

Cleaner fix 
CCBUG:226710

------------------------------------------------------------------------
r1131022 | scripty | 2010-05-27 14:05:40 +1200 (Thu, 27 May 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1131231 | freininghaus | 2010-05-28 06:21:23 +1200 (Fri, 28 May 2010) | 7 lines

When pasting or dropping items in the view in Dolphin or the
DolphinPart (running in Konqueror), do not only select the new items,
but also clear the previous selection.

CCBUG: 223905
FIXED-IN: 4.4.4

------------------------------------------------------------------------
r1131526 | mueller | 2010-05-28 22:16:45 +1200 (Fri, 28 May 2010) | 2 lines

KDE 4.4.4

------------------------------------------------------------------------
