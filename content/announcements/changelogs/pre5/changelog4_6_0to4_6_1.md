---
aliases:
- ../changelog4_6_0to4_6_1
hidden: true
title: KDE 4.6.1 Changelog
---

<h2>Changes in KDE 4.6.1</h2>
    <h3 id="kdelibs"><a name="kdelibs">kdelibs</a><span class="allsvnchanges"> [ <a href="4_6_1/kdelibs.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="khtml">khtml</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix crash">Containing block and absolutely positioned element in @media print page triggers an application crash when attempting to print. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=264985">264985</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/0126c4bf738229fedb1a5a1c05abf565b3131dce">0126c4b</a>. </li>
      </ul>
      </div>
      <h4><a name="kdeui">kdeui</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Update the search of a KListWidgetSearchLine if items are added or modified. Fixes a problem in the "Edit Toolbars" dialog: after an action was dragged an dropped from one side to the other, the filter was reset. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=265709">265709</a>.  See Git commit <a href="http://commits.kde.org/kdelibs/abc9c4c67cf5a25d0f279beedc81727cca154cad">abc9c4c</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdebase"><a name="kdebase">kdebase</a><span class="allsvnchanges"> [ <a href="4_6_1/kdebase.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="konsole">konsole</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix issue where the context menu popup no longer appeared after closing a tab.  A couple of close methods have been disabled until they can be fixed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=185466">185466</a>.  See Git commit <a href="http://commits.kde.org/konsole/18dd5bd012dae86b118c7b7a132866c0c7781297">18dd5bd</a>. </li>
        <li class="bugfix normal">After a 'clear and reset' make sure the prompt reappears. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=242110">242110</a>.  See Git commit <a href="http://commits.kde.org/konsole/82778e87bc75e07d006e3de70ec0951ff445a56c">82778e8</a>. </li>
      </ul>
      </div>
      <h4><a name="dolphin">dolphin</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Don't show the pointing-hand cursor when double-click is enabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=264796">264796</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/09d30e3590b27e6955512d4726e1b188430765f8">09d30e3</a>. </li>
        <li class="bugfix normal">Show the pointing-hand cursor when the selection-toggle is disabled. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=264791">264791</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/8c1cb5b79a6a90726112fbd00d3aab37d2090504">8c1cb5b</a>. </li>
        <li class="bugfix normal">The filter-panel should be disabled if the current folder is not indexed at all. Also when triggering a "Find" the filter-panel should stay invisible per default when the current folder is not indexed. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=264969">264969</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/d54313ecd8a4e9ec1bbe11b8264ea20114f02714">d54313e</a>. </li>
        <li class="bugfix normal">Fix regression that creating a sub-folder is not possible when using the context-menu above a folder. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=265714">265714</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/6bb8f5ba17be70237972f805e54b3d6491bb6574">6bb8f5b</a>. </li>
        <li class="bugfix normal">If only one file is selected, pressing RETURN should behave similar like triggering the item with the mouse. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=250475">250475</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/ec05ba261fffa6d9a4d301e5ee64d3922a6d020d">ec05ba2</a>. </li>
        <li class="bugfix normal">Open folder in a new tab when a middle-click is done in the column-view. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=265859">265859</a>.  See Git commit <a href="http://commits.kde.org/kde-baseapps/5791fc023f702c145696deb9ecc76a78104e7389">5791fc0</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdenetwork"><a name="kdenetwork">kdenetwork</a><span class="allsvnchanges"> [ <a href="4_6_1/kdenetwork.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="kopete">kopete</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Fix "last seen" property when an account goes offline. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=266580">266580</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1221356&amp;view=rev">1221356</a>. </li>
        <li class="bugfix normal">Fix sending and receiving Yahoo! webcams. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=244135">244135</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1217895&amp;view=rev">1217895</a>. </li>
        <li class="bugfix normal">Do not receive duplicate messages after login fails in the Yahoo! protocol. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=206159">206159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1217959&amp;view=rev">1217959</a>. </li>
        <li class="bugfix normal">Force all accounts to go online when Status -&gt; Online menu is clicked. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=234000">234000</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1219502&amp;view=rev">1219502</a>. </li>
        <li class="bugfix normal">Enable contact-gone-offline custom notification. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=260892">260892</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1219499&amp;view=rev">1219499</a>. </li>
        <li class="bugfix normal">Hide InfoEventIconLabel when server messages count reaches zero. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=263181">263181</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1219497&amp;view=rev">1219497</a>. </li>
        <li class="bugfix normal">Use user-offline.png as overlay icon to make it more visible when user is offline. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=256667">256667</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1219492&amp;view=rev">1219492</a>. </li>
        <li class="bugfix normal">Make Oscar/Icq use Kopete's events instead of systems' for error notification. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=265811">265811</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1219462&amp;view=rev">1219462</a>. </li>
        <li class="bugfix normal">Sorts accounts before adding them to the status bar. See SVN commit <a href="http://websvn.kde.org/?rev=1218860&amp;view=rev">1218860</a>. </li>
        <li class="bugfix normal">Show contact tooltips even when main window has no focus. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=266559">266559</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1221522&amp;view=rev">1221522</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdegraphics"><a name="kdegraphics">kdegraphics</a><span class="allsvnchanges"> [ <a href="4_6_1/kdegraphics.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://okular.kde.org/" name="okular">Okular</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Properly encode the document URL in the properties dialog. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=263778">263778</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1216334&amp;view=rev">1216334</a>. </li>
        <li class="bugfix ">Accept slight pixel movements for detecting mouse clicks (it should help e.g. with tablets). Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=263314">263314</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1216391&amp;view=rev">1216391</a>. </li>
        <li class="bugfix ">Make --page work with the --unique instance already running. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=254917">254917</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1218022&amp;view=rev">1218022</a>. </li>
        <li class="bugfix ">Fix glitches in page outline drap shadow rendering. See SVN commit <a href="http://websvn.kde.org/?rev=1219653&amp;view=rev">1219653</a>. </li>
        <li class="bugfix crash">Do not crash when any TOC item points to a page which does not exist. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=266133">266133</a>.  See SVN commits <a href="http://websvn.kde.org/?rev=1220023&amp;view=rev">1220023</a> and <a href="http://websvn.kde.org/?rev=1220027&amp;view=rev">1220027</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeutils"><a name="kdeutils">kdeutils</a><span class="allsvnchanges"> [ <a href="4_6_1/kdeutils.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a href="http://utils.kde.org/projects/ark" name="ark">Ark</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Extract renamed files in tar archives. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=266159">266159</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1220439&amp;view=rev">1220439</a>. </li>
      </ul>
      </div>
      <h4><a href="http://utils.kde.org/projects/kgpg" name="kgpg">KGpg</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Make sure the decrypted text in the editor always ends with a line break. See SVN commit <a href="http://websvn.kde.org/?rev=1222284&amp;view=rev">1222284</a>. </li>
        <li class="bugfix normal">Fix the width of the key count status bar item in translated environments. See SVN commit <a href="http://websvn.kde.org/?rev=1222285&amp;view=rev">1222285</a>. </li>
        <li class="bugfix crash">Fix crash when reading signatures gives no result. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=265339">265339</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1222286&amp;view=rev">1222286</a>. </li>
        <li class="bugfix normal">Fix hang in key generation. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=263805">263805</a>.  See SVN commit <a href="http://websvn.kde.org/?rev=1222431&amp;view=rev">1222431</a>. </li>
      </ul>
      </div>
    </div>
    <h3 id="kdeworkspace"><a name="kdeworkspace">kdeworkspace</a><span class="allsvnchanges"> [ <a href="4_6_1/kdeworkspace.txt">all SVN changes</a> ]</span></h3>    
    <div class="module" style="padding-left: 20px;">
      <h4><a name="plasma workspaces">Plasma workspaces</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix normal">Enable UPS battery monitoring in battery plasmoid. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=231195">231195</a>.  See Git commits <a href="http://commits.kde.org/kde-workspace/c5721519a0ccf8a528bdb31f051509db00e98e0c">c572151</a> and <a href="http://commits.kde.org/kde-workspace/09492b4dd8c83056125275cf558e00ad7c4a1e0f">09492b4</a>. </li>
      </ul>
      </div>
      <h4><a name="powerdevil">Powerdevil</a></h4>
      <div class="product" style="padding-left: 20px;">
      <em>Bugfixes:</em>
      <ul>
        <li class="bugfix ">Fix problem with increase brightness key not working sometimes. Fixes bug <a href="http://bugs.kde.org/show_bug.cgi?id=264534">264534</a>.  See Git commit <a href="http://commits.kde.org/kde-workspace/57a927ff69f5b1673e441c87ac59550f23ac3594">57a927f</a>. </li>
      </ul>
      </div>
    </div>