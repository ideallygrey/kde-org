2008-09-26 08:18 +0000 [r864955-864954]  cgilles <cgilles@localhost>:

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (removed): bye bye
	  libkdcraw based on dcraw!

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (added): like KDE
	  4.1.2 is tagged, now we will set likdcraw based on libraw...

2008-09-26 08:50 +0000 [r864961-864959]  cgilles <cgilles@localhost>:

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (removed): remove
	  this version accordingly with Pinotree

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (added): revert
	  libkdcraw with code from KDE 4.1.2

2008-09-28 20:34 +0000 [r865743-865742]  gateau <gateau@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/lib/document/loadingdocumentimpl.cpp:
	  Backported r865649 Do not leak mJpegContent when we do not load
	  the full image. CCBUG:169383

	* branches/KDE/4.1/kdegraphics/gwenview/app/main.cpp: Bumped
	  version number.

2008-09-30 18:50 +0000 [r866384]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (removed): delete
	  libkdcraw

2008-09-30 18:54 +0000 [r866386]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdegraphics/libs/libkdcraw (added): restore the
	  libkdcraw from KDE 4.1.1

2008-10-01 13:13 +0000 [r866615]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/ui/pageview.cpp: backport:
	  when no extension is set, assume png (fix the previous check)
	  reported by Fathi Boudra

2008-10-02 11:33 +0000 [r866938]  mueller <mueller@localhost>:

	* branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/kexiv2image.cpp,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/NEWS,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/CMakeLists.txt,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/CMakeLists.txt,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/README,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/kexiv2.h,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/kexiv2gps.cpp,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/kexiv2private.cpp,
	  branches/KDE/4.1/kdegraphics/libs/libkexiv2/libkexiv2/kexiv2.cpp:
	  revert libkexiv so bump breakage as well

2008-10-03 06:14 +0000 [r867213]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/generators/comicbook/libokularGenerator_comicbook.desktop,
	  branches/KDE/4.1/kdegraphics/gwenview/app/slideshow.desktop,
	  branches/KDE/4.1/kdegraphics/okular/generators/kimgio/libokularGenerator_kimgio.desktop,
	  branches/KDE/4.1/kdegraphics/okular/generators/chm/libokularGenerator_chmlib.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-04 19:35 +0000 [r867873-867872]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/core/document_p.h: backport:
	  do not forget to init m_maxAllocatedTextPages

	* branches/KDE/4.1/kdegraphics/okular/core/document.cpp: backport:
	  always deal with memory measures as ulonglong, this way systems
	  with >4 GB of memory work for real now

2008-10-11 13:48 +0000 [r869969]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/ui/findbar.cpp: Backport:
	  select all the text when opening the find bar. CCBUG: 172585

2008-10-11 22:58 +0000 [r870165]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/ui/pagepainter.cpp: consider
	  annotBoundary as boundary *without* cropping to the requested
	  limits this should fix drawing problems with note, stamp, and
	  geometric annotations CCBUG: 160396

2008-10-12 16:29 +0000 [r870503]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/shell/shell.cpp,
	  branches/KDE/4.1/kdegraphics/okular/shell/shell.h: Remove the
	  "old" way of configuring the toolbars and just use the XMLGUI
	  facilities. This makes the toolbar editing safer. CCBUGS: 168528,
	  171186

2008-10-12 16:37 +0000 [r870511]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/ui/pageview.cpp,
	  branches/KDE/4.1/kdegraphics/okular/ui/pageview.h: Switch from a
	  KSelectAction to a KActionMenu+KAction's, so the actions of the
	  "View Mode" submenu can have custom shortcuts (and placed in
	  toolbars, etc). CCBUG: 172661

2008-10-17 06:08 +0000 [r872394]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/generators/kimgio/libokularGenerator_kimgio.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-17 08:25 +0000 [r872439-872438]  gateau <gateau@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/lib/croptool.cpp: Avoid
	  crash if context menu was opened. BUG:171873 Backported
	  https://svn.kde.org/home/kde/trunk/KDE/kdegraphics@871979

	* branches/KDE/4.1/kdegraphics/gwenview/lib/thumbnailview/thumbnailview.cpp:
	  When removing current image, select the next one if possible,
	  otherwise fallback to the previous one. BUG:172008 BUG:172009

2008-10-17 19:02 +0000 [r872665]  gateau <gateau@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/part/gvpart.cpp: Support
	  reload in KPart. BUG:169114

2008-10-17 20:20 +0000 [r872681]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/ui/pageview.cpp: oups,
	  accidental typo

2008-10-19 09:56 +0000 [r873278]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/core/textpage.cpp: Backport:
	  do not ignore (white)spaces in the search query when searching
	  within the text of a page. Will be in KDE 4.1.3. CCBUG: 172244

2008-10-20 22:23 +0000 [r874182]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/lib/jpegcontent.cpp,
	  branches/KDE/4.1/kdegraphics/gwenview/lib/CMakeLists.txt: small
	  fix until I find a better one

2008-10-21 06:51 +0000 [r874286]  scripty <scripty@localhost>:

	* branches/KDE/4.1/kdegraphics/kgamma/kcmkgamma/kgamma.desktop:
	  SVN_SILENT made messages (.desktop file)

2008-10-24 20:15 +0000 [r875560]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/okular/generators/djvu/generator_djvu.cpp:
	  backport: be sure to close the file before sending it for
	  printing

2008-10-25 00:15 +0000 [r875637]  sberger <sberger@localhost>:

	* branches/KDE/4.1/kdegraphics/kolourpaint/tools/selection/text/kpToolText_KeyboardEvents.cpp:
	  Use QKeyEvent::text() to see if a keypress generate text, fixes
	  bug #156122

2008-10-26 19:52 +0000 [r876218]  pino <pino@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/lib/imageview.cpp: compile
	  when qreal is float and not double

2008-10-27 12:13 +0000 [r876403]  sengels <sengels@localhost>:

	* branches/KDE/4.1/kdegraphics/gwenview/lib/jpegcontent.cpp,
	  branches/KDE/4.1/kdegraphics/gwenview/lib/thumbnailloadjob.cpp:
	  adapt to newer exiv2 version as done in libkexiv2 do not use that
	  exiv2 thumbnails on windows

