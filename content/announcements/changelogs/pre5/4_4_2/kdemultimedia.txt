------------------------------------------------------------------------
r1096679 | esken | 2010-02-28 00:20:44 +1300 (Sun, 28 Feb 2010) | 2 lines

Bump version number to 3.6 (removing "-alpha2" prefix).

------------------------------------------------------------------------
r1096684 | esken | 2010-02-28 00:32:35 +1300 (Sun, 28 Feb 2010) | 5 lines

Fix "disappearing channels" issue.
Possibly a workaround for users with defective profiles will
need tobe developed.
CCBUGS: 228669

------------------------------------------------------------------------
r1098397 | lueck | 2010-03-04 05:54:24 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1098640 | pino | 2010-03-04 13:50:37 +1300 (Thu, 04 Mar 2010) | 2 lines

link to solid, as it is used

------------------------------------------------------------------------
r1098669 | scripty | 2010-03-04 15:17:22 +1300 (Thu, 04 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1098839 | pino | 2010-03-05 02:17:51 +1300 (Fri, 05 Mar 2010) | 2 lines

need to link to phonon as well

------------------------------------------------------------------------
r1099732 | scripty | 2010-03-06 15:25:10 +1300 (Sat, 06 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1100330 | lueck | 2010-03-07 22:41:40 +1300 (Sun, 07 Mar 2010) | 1 line

fixed errors found while translating
------------------------------------------------------------------------
r1101304 | esken | 2010-03-10 10:27:25 +1300 (Wed, 10 Mar 2010) | 3 lines

Fix compile error in mixer_oss4.cpp 
CCBUGS: 229910

------------------------------------------------------------------------
r1106103 | mpyne | 2010-03-22 10:01:00 +1300 (Mon, 22 Mar 2010) | 3 lines

Backport null pointer check to KDE 4.4.2. Merci trés bien to aseigo for noting in trunk.
Trunk commit was r1106034

------------------------------------------------------------------------
