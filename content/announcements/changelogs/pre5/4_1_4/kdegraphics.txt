------------------------------------------------------------------------
r877985 | pino | 2008-10-30 21:46:15 +0000 (Thu, 30 Oct 2008) | 5 lines

bump version

Dirk, can you please backport it to the KDE 4.1.3 tag?
CCMAIL: mueller@kde.org

------------------------------------------------------------------------
r881005 | scripty | 2008-11-07 07:36:25 +0000 (Fri, 07 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r882010 | pino | 2008-11-09 14:59:29 +0000 (Sun, 09 Nov 2008) | 4 lines

Backport: hack around the fact that CBR or CBZ documents can ahve the wrong extension.

CCBUG: 174701

------------------------------------------------------------------------
r882132 | pino | 2008-11-09 21:40:08 +0000 (Sun, 09 Nov 2008) | 3 lines

backport:
recent files: do not add '-' (aka stdin) when reading a document from it

------------------------------------------------------------------------
r885277 | pino | 2008-11-16 22:33:37 +0000 (Sun, 16 Nov 2008) | 2 lines

backport: calculate PS paper size more correctly when passed to poppler

------------------------------------------------------------------------
r888260 | msoeken | 2008-11-24 07:56:02 +0000 (Mon, 24 Nov 2008) | 5 lines

Backport r882995

CCMAIL: rich@kde.org
CCBUG: 166826

------------------------------------------------------------------------
r890789 | pino | 2008-11-30 09:55:04 +0000 (Sun, 30 Nov 2008) | 7 lines

Backport of SVN commit 890775 by bhards:

Implement scale-down for images that would not otherwise fit
on the selected printed page format.

CCBUG:174447

------------------------------------------------------------------------
r890790 | pino | 2008-11-30 09:57:50 +0000 (Sun, 30 Nov 2008) | 6 lines

Backport:
Add VIM-like navigation (HJKL) in the content view.
Patch by Frederik Gladhorn, thanks!

CCBUG: 174647

------------------------------------------------------------------------
r890792 | pino | 2008-11-30 10:01:50 +0000 (Sun, 30 Nov 2008) | 2 lines

backport: reset the line edit palette when we get results

------------------------------------------------------------------------
r890801 | pino | 2008-11-30 11:04:03 +0000 (Sun, 30 Nov 2008) | 3 lines

backport:
When starting an incremental search, do not consider the start page "done" if there was a match in it.

------------------------------------------------------------------------
r890804 | pino | 2008-11-30 11:33:41 +0000 (Sun, 30 Nov 2008) | 4 lines

backport of SVN commit 890803 by pino:

add more checks against invalid viewports

------------------------------------------------------------------------
r891479 | scripty | 2008-12-02 08:12:28 +0000 (Tue, 02 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r894124 | aacid | 2008-12-07 23:37:18 +0000 (Sun, 07 Dec 2008) | 3 lines

Backport r894123 
correctly restore m_showMenuBarAction status on startup

------------------------------------------------------------------------
r894428 | aacid | 2008-12-08 16:10:34 +0000 (Mon, 08 Dec 2008) | 5 lines

Backport r894427
Try to fix again the inifite scroolbar loop
It's still to be improved because it gives you a uncentered page, but better a bit ugly than unusable
This fix will be in KDE 4.1.4

------------------------------------------------------------------------
r895235 | pino | 2008-12-10 12:33:19 +0000 (Wed, 10 Dec 2008) | 2 lines

bump versions for kde 4.1.4

------------------------------------------------------------------------
r896113 | pino | 2008-12-12 14:47:15 +0000 (Fri, 12 Dec 2008) | 3 lines

backport:
check whether the loading really suceeeded

------------------------------------------------------------------------
r896221 | pino | 2008-12-12 21:46:13 +0000 (Fri, 12 Dec 2008) | 4 lines

backport:
add the compressed versions of eps
CCBUG: 177516

------------------------------------------------------------------------
r896270 | jlayt | 2008-12-13 00:32:02 +0000 (Sat, 13 Dec 2008) | 14 lines

Backport from 4.2 of revision 895905.

Fixes to FilePrinter for Qt 4.4:

1) detectCupsService() finally works because QTCPSocket finally works.
   Assumes Cups is on localhost:631 which is a compromise as Qt won't
   even tell us if it is using Cups, let alone where Cups is.

2) Duplex printing obeys user selected option from dialog rather than 
   just guessing.

3) If custom page margins set then respect these


------------------------------------------------------------------------
r897005 | jlayt | 2008-12-14 23:56:37 +0000 (Sun, 14 Dec 2008) | 2 lines

Backport from 4.2 of commit 897000 to fix Print to File for PDF, DVI and DJVU. 

------------------------------------------------------------------------
r898293 | pino | 2008-12-17 21:48:12 +0000 (Wed, 17 Dec 2008) | 3 lines

Backport: when moving annotations, consider just the rotation of their pages, and not also the page orientation.
CCBUG: 177598

------------------------------------------------------------------------
r898355 | pino | 2008-12-18 01:22:04 +0000 (Thu, 18 Dec 2008) | 3 lines

backport: fix logic error
CCBUG: 178028

------------------------------------------------------------------------
r898743 | aacid | 2008-12-18 22:12:40 +0000 (Thu, 18 Dec 2008) | 3 lines

Backport r898742 okular/trunk/KDE/kdegraphics/okular/shell/ (shell.cpp shell.h):
Update m_showMenuBarAction on the proper time

------------------------------------------------------------------------
r899137 | pino | 2008-12-19 23:06:33 +0000 (Fri, 19 Dec 2008) | 3 lines

Backport: scan recursively for images in CBZ archives.
CCBUG: 178029

------------------------------------------------------------------------
r899240 | pino | 2008-12-20 12:26:40 +0000 (Sat, 20 Dec 2008) | 4 lines

backport:
- open the temporary file name only after any option is set to it (fix crash when asking to print to pdf)
- close the file before sending it for printing, so we can be sure it is written as a whole

------------------------------------------------------------------------
r904612 | pino | 2009-01-02 16:22:46 +0000 (Fri, 02 Jan 2009) | 6 lines

Backport SVN commit 903998 by bhards:

Protect against potential broken manifest files.

Fixes CID:4679 and CID:4680.

------------------------------------------------------------------------
r906555 | habacker | 2009-01-06 12:32:50 +0000 (Tue, 06 Jan 2009) | 1 line

backported win32 application icons from trunk
------------------------------------------------------------------------
r906566 | habacker | 2009-01-06 12:39:55 +0000 (Tue, 06 Jan 2009) | 1 line

fixed wrong image name
------------------------------------------------------------------------
r906632 | mueller | 2009-01-06 13:51:09 +0000 (Tue, 06 Jan 2009) | 3 lines

Parse error.  Expected a newline, got identifier with text
"kde4_add_executable"

------------------------------------------------------------------------
