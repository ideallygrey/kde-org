------------------------------------------------------------------------
r878038 | scripty | 2008-10-31 07:18:08 +0000 (Fri, 31 Oct 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r879948 | xvello | 2008-11-04 12:26:07 +0000 (Tue, 04 Nov 2008) | 2 lines

backport of r879942 : "added precisions on required version of libmusicbrainz"

------------------------------------------------------------------------
r883425 | ilic | 2008-11-12 21:36:34 +0000 (Wed, 12 Nov 2008) | 1 line

i18n fixes (bport: 883421)
------------------------------------------------------------------------
r884077 | scripty | 2008-11-14 07:30:52 +0000 (Fri, 14 Nov 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r895178 | scripty | 2008-12-10 08:20:29 +0000 (Wed, 10 Dec 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r904769 | mpyne | 2009-01-03 01:22:38 +0000 (Sat, 03 Jan 2009) | 5 lines

Backport fix for bug 176956 (JuK crashes during internet tag guessing) to KDE 4.1.  Again, not sure
if the feature actually works but crash-- is good at least.

BUG:176956

------------------------------------------------------------------------
r904770 | mpyne | 2009-01-03 01:24:57 +0000 (Sat, 03 Jan 2009) | 1 line

Bump version of Juk in KDE 4.1 branch in case another release is made.
------------------------------------------------------------------------
