2006-02-03 19:21 +0000 [r505385]  jriddell

	* branches/KDE/3.5/kdegames/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdegames
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdegames

2006-02-12 12:53 +0000 [r508616]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/boardwidget.h,
	  branches/KDE/3.5/kdegames/kmahjongg/Preview.cpp,
	  branches/KDE/3.5/kdegames/kmahjongg/kmahjongg.cpp,
	  branches/KDE/3.5/kdegames/kmahjongg/Preview.h,
	  branches/KDE/3.5/kdegames/kmahjongg/version.h: Allow reading of
	  pictures in ~/.kde/share/apps/kmahjongg/pics so people don't need
	  to be root to add themes, etc BUGS: 121807

2006-02-12 12:59 +0000 [r508617]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/Preview.cpp: forgot to remove
	  that debug

2006-02-14 22:29 +0000 [r509514]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/Preview.cpp: that names is a
	  bad idea as we then use the index to access the non sorted
	  pathlist

2006-02-23 21:25 +0000 [r512887]  lueck

	* branches/KDE/3.5/kdegames/doc/kenolaba/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kwin4/index.docbook,
	  branches/KDE/3.5/kdegames/doc/klickety/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kmahjongg/index.docbook,
	  branches/KDE/3.5/kdegames/doc/ksirtet/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kfouleggs/index.docbook,
	  branches/KDE/3.5/kdegames/doc/kmines/index.docbook,
	  branches/KDE/3.5/kdegames/doc/atlantik/index.docbook: updated
	  kdegames docbooks for KDE 3.5.2 CCMAIL:kde-doc-english@kde.org

2006-02-25 13:04 +0000 [r513418]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/Preview.cpp: Fix theme
	  loading as reported on second part of 121807 BUGS: 121807

2006-03-16 18:53 +0000 [r519279]  aacid

	* branches/KDE/3.5/kdegames/kmahjongg/version.h: update version as
	  i fixed a bug

2006-03-17 21:34 +0000 [r519783]  coolo

	* branches/KDE/3.5/kdegames/kdegames.lsm: tagging 3.5.2

