2006-02-03 19:21 +0000 [r505381]  jriddell

	* branches/KDE/3.5/kdetoys/debian (removed): Remove debian
	  directory, now at
	  http://svn.debian.org/wsvn/pkg-kde/trunk/packages/kdetoys
	  svn://svn.debian.org/pkg-kde/trunk/packages/kdetoys

2006-03-13 15:03 +0000 [r518256]  coolo

	* branches/KDE/3.5/kdetoys/kweather/kweather.cpp: init on reload

2006-03-17 21:34 +0000 [r519790]  coolo

	* branches/KDE/3.5/kdetoys/kdetoys.lsm: tagging 3.5.2

