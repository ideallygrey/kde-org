---
aliases:
- ../announce-2.2-beta1
custom_about: true
custom_contact: true
date: '2001-07-04'
description: KDE 2.2 Beta1 is a user and developer preview of KDE 2.2 incorporating
  major improvements, enhancements and fixes over KDE 2.1.2. "KDE 2.2beta1 completely
  integrates the XFree anti-aliased font extensions and can provide a fully anti-aliased
  font-enabled desktop."
title: KDE 2.2-beta1 Release Announcement
---

FOR IMMEDIATE RELEASE

<H3 ALIGN="center">New KDE Beta Released for Linux Desktop</H3>

KDE Ships Beta of Leading Desktop with Advanced Web Browser, Anti-Aliased Font Capabilities for Linux and Other UNIXes

The <a href="/">KDE
Project</a> today announced the release of KDE 2.2beta1,
a powerful and easy-to-use Internet-enabled desktop for Linux. KDE
features <a href="http://konqueror.kde.org/">Konqueror</a>, a
state-of-the-art web browser, as an integrated
component of its user-friendly desktop environment, as well as
<a href="http://www.kdevelop.org/">KDevelop</a>,
an advanced IDE, as a central component of KDE's powerful
development environment. KDE 2.2beta1 completely integrates the
<a href="http://www.xfree.org/4.1.0/fonts5.html#30">XFree anti-aliased
font extensions</a> and can provide
a fully anti-aliased font-enabled desktop.

The primary goals of this release, which follows two months after the release
of KDE 2.1.2, are to provide a preview of KDE 2.2 and to involve
users and developers who wish to request/implement missing features or
identify problems. Code development is currently focused on stabilizing
KDE 2.2, scheduled for final release later this quarter.
Despite all the improvements, KDE 2.2 will be binary compatible with KDE 2.0.

Major changes to KDE since the last stable release are
<a href="#changelog">enumerated below</a>. In addition, a
<a href="/announcements/changelogs/changelog2_1to2_2">more
thorough list of changes</a> since the KDE 2.1.1 release,
as well as <a href="http://www.kde.com/Information/Fonts/index">information
on using anti-aliased fonts</a> with KDE, are available at the KDE
<a href="/">website</a>.

KDE 2.2beta1 and all its components are available for free under
Open Source licenses from the KDE
<a href="http://ftp.kde.org/unstable/2.2beta1/">server</a>
and its <a href="/mirrors">mirrors</a> (including
many <a href="#Package_Locations">precompiled packages</a>) and can
also be obtained on <a href="http://www.kde.org/cdrom.html">CD-ROM</a>.
As a result of the dedicated efforts of hundreds of translators,
KDE 2.2beta1 is available in
<a href="http://i18n.kde.org/teams/distributed.html">38 languages and
dialects</a>. KDE 2.2beta1 ships with the core KDE
libraries, the core desktop environment (including Konqueror), developer
packages (including KDevelop), as well
as the over 100 applications from the other
standard base KDE packages (addons, administration, artwork, bindings, games,
graphics, multimedia, SDK, network, PIM and utilities).

<a id="changelog"></a>#### Incremental Changelog

The following are the major improvements, enhancements and fixes since the
KDE 2.1 release earlier this year:

<ul>
<li>KDE has added a new plugin-based printing framework, which features:
<ul>
  <li>support for <a href="http://cups.sourceforge.net/">CUPS</a>,
      lpr and rlpr, though support for other printing systems can be
      easily added;</li>
  <li>a Control Center module for managing printers
      (add/remove/enable/disable/configure), print servers (currently
      only CUPS), and print jobs (cancel/hold/move);</li>
  <li>a configurable filter mechanism (for setting number of pages per
      sheet, etc.);</li>
  <li>a print job viewer for the KDE panel's system tray; and</li>
  <li>support for configurable "pseudo-printers", such as fax machines,
      email, etc.;</li>
</ul>
<br>

<li>Konqueror, the KDE file manager and web browser, sports a number of
    new features:</li>
<ul>
  <li>HTML and JavaScript handling has been improved and made faster;</li>
  <li>Ability to stop animated images;</li>
  <li>New file previews, including PDF, PostScript, and sound files;</li>
  <li>New "Send File" and "Send Link" options in the <TT>File</TT>menu;</li>
  <li>Added a number of new plugins:</li>
  <ul>
    <li>A web archiver for downloading and saving an entire web page, including
        images, in an archive for offline viewing;</li>
    <li><a href="http://babelfish.altavista.com/">Babelfish</a>
        translation of web pages;</li>
    <li>A directory filter for displaying only specified mimetypes (such as
        images);</li>
    <li>A quick User Agent changer to get Konqueror to work with websites
        that discriminate based on the browser you are using;</li>
    <li>An HTML validator using <a href="http://www.w3c.org/">W3C</a> to
        validate the XML/HTML of a webpage (useful for web developers);
        and</li>
    <li>A DOM tree-viewer for viewing the DOM structure of a web page (useful
        for web developers);</li>
  </ul>
<br>

  <li>New configuration for user-defined CSS stylesheets;</li>
  <li>Saving toolbar layout in the profile;</li>
  <li>A new "Most Often Visited" URL in the <TT>Go</TT> menu; and</li>
  <li>Many other enhancements, usability improvements and bug fixes.</li>
</ul>
<br>

<li>KDevelop, the KDE IDE, offers a number of new features:</li>
<ul>
  <li>Enhanced user interface with an MDI structure, which supports multiple
      views of the same file;</li>
  <li>Added new templates for implementing a KDE/Qt style library and Control
      Center modules;</li>
  <li>Updated the kde-common/admin copy (admin.tar.gz); and</li>
  <li>Extended the user manual to reflect the new GUI layout and added
      a chapter for using Qt Designer with KDevelop projects;</li>
</ul>
<br>

<li>KMail, the KDE mail client, has a number of improvements:</li>
<ul>
  <li>Added support for IMAP mail servers;</li>
  <li>Added support for SSL and TSL for POP3 mail servers;</li>
  <li>Added configuration of SASL and APOP authentication;</li>
  <li>Made mail-sending non-blocking;</li>
  <li>Improved performance for very large folders;</li>
  <li>Added message scoring;</li>
  <li>Improved the filter dialog and implemented automatic filter
      creation;</li>
  <li>Implemented quoting only selected parts of an email on a reply;</li>
  <li>Implemented forwarding emails as attachments; and</li>
  <li>Added support for multiple PGP (encryption) identities;</li>
</ul>
<br>

<li>New Control Center modules:</li>
<ul>
  <li>Listing USB information (attached devices);</li>
  <li>Configuring window manager decoration;</li>
  <li>Configuring application startup notification;</li>
  <li>Configuring user-defined CSS stylesheets;</li>
  <li>Configuring automatic audio-CD ripping (MP3, Ogg Vorbis); and</li>
  <li>Configuring key bindings;</li>
</ul>
<br>

<li>Added Kandy, a synchronization tool for mobile phones and the KDE address
    book, and improved KPilot address book synchronization;</li>
<li>KOrganizer, the KDE personal organizer, has a number of improvements:</li>
<ul>
  <li>Added a "What's Next" view;</li>
  <li>Added a journal feature;</li>
  <li>Switched to using the industry-standard iCalendar as the default file
      format;</li>
  <li>Added remote calendar support; and</li>
  <li>Added ability to send events using KMail, the KDE mail client;</li>
</ul>
<br>

<li>Noatun, the KDE multimedia player, sports a number of new features:</li>
<ul>
  <li>Improved the plugin architecture and added a number of new plugins:</li>
  <ul>
    <li>An Alarm plugin for playing music at a specified time;</li>
    <li>A Blurscope plugin which creates an SDL-based blurred monoscope;</li>
    <li>A Luckytag plugin for guessing titles based on filenames;</li>
    <li>A Noatun Madness plugin, which moves the Noatun window in sync with
        the music being played;</li>
    <li>A Synaescope plugin, based on
       <a href="http://yoyo.cc.monash.edu.au/~pfh/synaesthesia.html">Synaesthesia</a>,
       which provides an impressive SDL-based visualization; and</li>
    <li>A Tyler plugin, which is similar to
        <a href="http://www.xmms.org/">XMMS</a>'s
        <a href="http://www.xmms.org/plugins_vis.html">Infinity</a>;</li>
  </ul>
<br>

  <li>Added support for pre-amplification; and</li>
  <li>Added support for hardware mixers;</li>
</ul>
<br>

<li>Added a Personalization wizard (KPersonalizer) to configure the desktop
    settings easily;</li>
<li>Added <a href="http://www.rhrk.uni-kl.de/~gebauerc/kdict/">KDict</a>,
    a powerful graphical dictionary client;</li>
<li>Added KDE-wide scanning support with the application Kooka;</li>
<li>Replaced the default editor KWrite with the more advanced editor Kate,
    which provides split views and basic project management;</li>
<li>The window manager now supports Xinerama (multi-headed displays);</li>
<li>Improved the file dialog, including mimetype-based file previews;</li>
<li>Improved the configurability of the KDE panel;</li>
<li>Added IPv6 and socks support to the core libraries;</li>
<li>Improved application startup:
<ul>
  <li>applications are now placed on the desktop from which they were
      launched; and</li>
  <li>startup notification can be configured with a new Control Center module,
      with options including a busy cursor next to the application's icon;</li>
</ul>
<br>

<li>Improved icons and added new 64x64 icons;</li>
<li>New window manager decoration styles (quartz, IceWM themes, MWM, Web);</li>
<li>Improved the help system, which is now XML-based;</li>
<li>Added support for the Meta and AltGr keys for shortcuts; and</li>
<li>Made many other usability improvements.</li>
</ul>
<br>

For a much more complete list, please read the
<a href="/announcements/changelogs/changelog2_1to2_2">official
ChangeLog</a>.

#### Downloading and Compiling KDE

<a id="Source_Code"></a><em>Source Packages</em>.
The source packages for KDE 2.2beta1 are available for free download at
<a href="http://ftp.kde.org/unstable/2.2beta1/src/">http://ftp.kde.org/unstable/2.2beta1/src/</a>
or in the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>.

<a id="Source_Code-Library_Requirements"></a><em>Library Requirements.</em>
KDE 2.2beta1 requires at least qt-x11-2.2.4, which is available from
<a href="http://www.trolltech.com/">Trolltech</a> at
<a href="ftp://ftp.trolltech.com/qt/source/">ftp://ftp.trolltech.com/qt/source/</a>
under the name <a href="ftp://ftp.trolltech.com/qt/source/qt-x11-2.2.4.tar.gz">qt-x11-2.2.4.tar.gz</a>,
although
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.1.tar.gz">qt-2.3.1</a>
is recommended (for anti-aliased fonts,
<a href="ftp://ftp.trolltech.com/pub/qt/source/qt-x11-2.3.0.tar.gz">qt-2.3.0</a>
and <a href="ftp://ftp.xfree86.org/pub/XFree86/4.0.3/">XFree 4.0.3</a> or
newer is required).
KDE 2.2beta1 will not work with versions of Qt older than 2.2.4.

<em>Compiler Requirements</em>. Please note that
<a href="http://gcc.gnu.org/gcc-3.0/gcc-3.0.html">gcc 3.0</a> is not
recommended for compilation of KDE 2.2beta1. Several known miscompilations
of production C++ code (such as virtual inheritance, which is used in <a href="http://www.arts-project.org/">aRts</a>) occur with this compiler.
The problems are mostly known and the KDE team is working with the gcc team
to fix them.

<em>Further Instructions</em>.
For further instructions on compiling and installing KDE, please consult
the <a href="http://www.kde.org/install-source.html">installation
instructions</a> and, if you encounter problems, the
<a href="http://www.kde.org/compilationfaq.html">compilation FAQ</a>.

#### Installing Binary Packages

<em>Binary Packages</em>.
Some distributors choose to provide binary packages of KDE for certain
versions of their distribution. Some of these binary packages for
KDE 2.2beta1 will be available for free download under
<a href="http://ftp.kde.org/unstable/2.2beta1/">http://ftp.kde.org/unstable/2.2beta1/</a>
or under the equivalent directory at one of the many KDE ftp server
<a href="/mirrors">mirrors</a>. Please note that the
KDE team is not responsible for these packages as they are provided by third
parties -- typically, but not always, the distributor of the relevant
distribution (if you cannot find a binary package for your distribution,
please read the <a href="http://dot.kde.org/986933826/">KDE Binary Package
Policy</a>).

<em>Library Requirements</em>.
The library requirements for a particular binary package vary with the
system on which the package was compiled. Please bear in mind that
some binary packages may require a newer version of Qt and/or KDE
than was included with the particular version of a distribution for
which the binary package is listed below (e.g., LinuxDistro 8.0 may have
shipped with qt-2.2.3 but the packages below may require qt-2.3.x). For
general library requirements for KDE, please see the text at
<a href="#Source_Code-Library_Requirements">Source Code - Library
Requirements</a> above.

<a id="Package_Locations"><em>Package Locations</em></a>.
At the time of this release, pre-compiled packages are available for:

<ul>
  <li><a href="http://www.debian.org/">Debian GNU/Linux</a> (package "kde"):  <a href="ftp://ftp.debian.org/">ftp.debian.org</a>:  sid (devel) (see also <a href="http://http.us.debian.org/debian/pool/main/k/">here</a>)</li>
  <li><a href="http://www.linux-mandrake.com/en/">Linux-Mandrake</a>
  <ul>
    <li>8.0:  <a href="http://ftp.kde.org/unstable/2.2beta1/Mandrake/8.0/i586/">Intel i586</a></li>
    <li>7.2:  <a href="http://ftp.kde.org/unstable/2.2beta1/Mandrake/7.2/i586/">Intel i586</a></li>
  </ul>
<br>

  <li><a href="http://www.suse.com/">SuSE Linux</a> (<a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/README">README</a>):
  <ul>
    <li>7.2:  <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.2/">Intel i386</a> and <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/ia64/7.1/">HP/Intel IA-64</a>; please also check the <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</a> directory for common (language) files</li>
    <li>7.1:  <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.1/">Intel i386</a>, <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.1/">PowerPC</a>, <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/sparc/7.1/">Sun Sparc</a> and <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/axp/7.1/">Alpha</a>; please also check the <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</a> directory for common (language) files</li>
    <li>7.0:  <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/7.0/">Intel i386</a>, <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/ppc/7.0/">PowerPC</a> and <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/s390/7.0/">IBM S390</a>; please also check the <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</a> directory for common (language) files</li>
    <li>6.4:  <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/i386/6.4/">Intel i386</a>; please also check the <a href="http://ftp.kde.org/unstable/2.2beta1/SuSE/noarch/">noarch</a> directory for common (language) files</li>
  </ul>
<br>

  <li>Tru64 Systems:  <a href="http://ftp.kde.org/unstable/2.2beta1/Tru64/">4.0e,f,g, or 5.x</a> (<a href="http://ftp.kde.org/unstable/2.2beta1/Tru64/README.Tru64">README.Tru64</a>)</li>
</ul>
<br>

Please check the servers periodically for pre-compiled packages for other
distributions. More binary packages will become available over the
coming days and weeks. In particular,
<a href="http://www.redhat.com/">RedHat Linux</a> packages should be
available shortly.

#### About KDE

KDE is an independent, collaborative project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop environment
employing a component-based, network-transparent architecture.
KDE is working proof of the power of the Open Source "Bazaar-style" software
development model to create first-rate technologies on par with
and superior to even the most complex commercial software.

Please visit the KDE family of web sites for the
<a href="http://www.kde.org/documentation/faq/index.html">KDE FAQ</a>,
<a href="/screenshots/kde2shots">screenshots</a>,
<a href="http://www.koffice.org/">KOffice information</a>,
<a href="http://developer.kde.org/documentation/kde2arch.html">developer
information</a> and
a developer's
<a href="http://webcvs.kde.org/cgi-bin/cvsweb.cgi/~checkout~/kdelibs/KDE2PORTING.html">KDE 1 - KDE 2 porting guide</a>.
Much more information about KDE is available from KDE's
<a href="http://www.kde.org/whatiskde/">web site</a>.

<hr /><FONT SIZE=2>
<em>Trademarks Notices.</em>
KDE and K Desktop Environment are trademarks of KDE e.V.
Linux is a registered trademark of Linus Torvalds.
Unix is a registered trademark of The Open Group.
PostScript is a registered trademark of Adobe Systems Incorporated.
Trolltech and Qt are trademarks of Trolltech AS.
All other trademarks and copyrights referred to in this announcement are the property of their respective owners.</font>
<BR>
<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](mailto:pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr valign="top"><td>
Europe (French and English):
</td><td >
David Faure<br>

[faure@kde.org](mailto:faure@kde.org)<br>
(44) 1225 837409

</td></tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>mailto:
    
  [konold@kde.org](mailto:konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
