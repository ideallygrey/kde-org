---
title: KDE for Gamers
description: Enjoy your games on Plasma and Linux
images:
- /for/gamers/thumbnail.png
sassFiles:
- /scss/for/gamers.scss
video: game-video.mp4
other:
 - name: Lutris
   description:  A video game preservation platform aiming to keep your video game collection up and running for years to come.
   link: https://lutris.net/
   img: https://lutris.net/static/images/logo.png
 - name: RetroArch
   description: RetroArch is a frontend for emulators, game engines and media players.
   link: https://www.retroarch.com/
   img: retroarch-logo.jpeg
 - name: Godot
   description: The open source game engine you've been waiting for.
   link: https://godotengine.org/
   img: godot-logo.svg
 - name: Astra
   description: An unofficial Final Fantasy XIV launcher using KDE technologies
   link: https://xiv.zone/astra/
   img: zone.xiv.astra.svg
 - name: Minetest
   description: An open source voxel game engine.
   link: https://www.minetest.net/
   img: minetest.svg
 - name: 0Ad
   description: A free, open-source game of ancient warfare
   link: https://play0ad.com
   img: oad.png
 - name: Heroic Games Launcher
   description: Heroic is an Open Source GOG and Epic games launcher for Linux and the SteamDeck!
   link: https://heroicgameslauncher.com/
   img: heroic.png
 - name: BAR - Beyond All Reason
   link: https://www.beyondallreason.info/
   description: Real-Time Strategy game
   img: bar.svg
game_ids: ['org.kde.granatier', 'org.kde.kapman', 'org.kde.kmines', 'org.kde.knights', 'org.kde.kpat', 'org.kde.kmahjongg']
---

{{< section class="text-center pb-5 pt-2" >}}

## KDE Plasma on the Steam Deck

Play your entire gaming library with this portable console that has the flexibility of a PC. The Steam Deck runs KDE Plasma optimized to play the latest AAA games and your favorite emulators. You can even connect a screen, keyboard, and mouse for a full Plasma-based desktop PC experience.

![Steam deck showing Plasma desktop](/content/hardware/steam-deck-large.png)

Learn how KDE contributed to the Steam Deck success, by watching the [following talk](https://tube.kockatoo.org/w/vovzT9bWHFHjBecxPr1V2X).

{{< for/app-links learn="https://store.steampowered.com/steamdeck" centered="true">}}

{{< /section >}}

{{< section class="proton py-5" >}}

## Proton and ProtonDB

Proton is a new tool released by Valve Software and integrated with Steam Play. Proton makes playing Windows games on Linux as simple as hitting the Play button within Steam. Under the hood, Proton comprises several popular technologies, including [Wine](https://www.winehq.org/) and [DXVK](https://github.com/doitsujin/dxvk) among others that gamers would otherwise have to install and maintain themselves.

[ProtonDB](https://www.protondb.com/) is a crowdsourced Linux game compatibility reports database containing reports about more than twenty thousands games.

<div class="row">
  <div class="col-6 col-md-3 mb-4 text-left"><span class="d-inline-block" role="text">More than<br aria-hidden="true"><span class="h1">3 000</span> <span class="h2">games</span><br aria-hidden="true">steam deck verified</span></div>
  <div class="col-6 col-md-3 mb-4 text-left"><span class="d-inline-block" role="text">More than<br aria-hidden="true"><span class="h1">10 000</span> <span class="h2">games</span><br aria-hidden="true">playable</span></div>
</div>


{{< for/app-links learn="https://www.protondb.com/" dark="true" >}}

{{< /section >}}

{{< section class="text-small text-center ">}}

## Bottles

Not developed by KDE but, still very helpful, Bottles is a free and open source software that uses environments to help you easily manage and run Windows apps on Linux. You can have immediate access to the most popular game stores (e.g. Epic Games Store, EA Launcher, Battle.net etc.) and then play your favorite games, as if you were on Windows.

![](https://usebottles.com/uploads/bottle-creation.png)

{{< for/app-links learn="https://usebottles.com/" centered="true" download="https://usebottles.com/download/" fund="https://usebottles.com/funding/" >}}

{{< /section >}}

{{< section class="clasic py-5 text-center text-small" >}}

## Explore our collection of classic games

Missing playing Solitaire? Not anymore, thanks to [KPatience](https://apps.kde.org/kpat/)
and the rest of the KDE Games. These games don't have DLC, ads, or trackers and are not pay to win. Let's enjoy games as they were meant to be.

![Screenshot of kpatience showing cards on a table](https://cdn.kde.org/screenshots/kpatience/kpatience.png)

{{< for/games learn="See all games" >}}

{{< /section >}}

{{< section class="text-small text-center ">}}

## Plasma for gamers

{{< /section >}}

{{< feature-container img="plasma-modable.png" img-class="w-100" >}}

### Moddable

Plasma is completely moddable with various extension points. Create new styles, change the color scheme of your apps, write a custom task switcher, make your own Plasma Widgets and write complex wallpaper plugins or compositor effects.

{{< for/app-links learn="https://develop.kde.org/docs/plasma/" >}}

{{< /feature-container >}}

{{< feature-container img="https://cdn.kde.org/screenshots/plasma-systemmonitor/overview.png" reverse="true" img-class="true" >}}

### Lightweight

Plasma is light on your computer resources, giving more power for your games. Monitor your system memory and CPU usage, with Plasma System Monitor.

You can even extend Plasma System Monitor to monitor your [solar panels](https://github.com/kbroulik/qalphacloud) if you want.

{{< for/app-links learn="https://develop.kde.org/docs/plasma/" >}}

{{< /feature-container >}}

{{< section class="text-small" >}}

## More open-source applications for gamers

Here are some other applications from other open-source organizations that
might interest you:

{{< link-boxes >}}

{{< /section >}}

{{< section class="clasic py-5 text-center text-small" >}}

## Get your new hardware with Plasma pre-installed

Check out some of the devices available from our partners, from portable gaming consoles to powerful PCs:

{{< for/app-links learn="https://kde.org/hardware/" dark="true" centered="true" >}}

![](/content/hardware/tuxedo-laptop.png)

![](https://kde.slimbook.es/assets/img/duo-ok.png)

![](https://kfocus.org/img/hosted/kfocus-systems-med-80p.webp)

{{< /section >}}

{{< container >}}

This site has no affiliation with Valve Software. All game images and logos are property of their respective owners.

{{< /container >}}

