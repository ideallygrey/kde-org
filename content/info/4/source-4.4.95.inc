<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top"><th align="left">Location</th><th align="left">Size</th><th align="left">SHA1&nbsp;Sum</th></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeaccessibility-4.4.95.tar.bz2">kdeaccessibility-4.4.95</a></td><td align="right">5,2MB</td><td><tt>9c77dcdf813370effce686e5ec2d66bafac84d62</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeadmin-4.4.95.tar.bz2">kdeadmin-4.4.95</a></td><td align="right">1,4MB</td><td><tt>b9f49b5a71eb2fa624df44bfdc7b1b1221f06583</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeartwork-4.4.95.tar.bz2">kdeartwork-4.4.95</a></td><td align="right">98MB</td><td><tt>8858ada3de11d7f32984c8567008c2ebc63c433d</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdebase-4.4.95.tar.bz2">kdebase-4.4.95</a></td><td align="right">2,5MB</td><td><tt>a0e923d9727cb6450c0a8908d61b15baf9f15d15</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdebase-runtime-4.4.95.tar.bz2">kdebase-runtime-4.4.95</a></td><td align="right">5,5MB</td><td><tt>d4a5b83ef6d2a61801029a656f56e681fae2243a</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdebase-workspace-4.4.95.tar.bz2">kdebase-workspace-4.4.95</a></td><td align="right">63MB</td><td><tt>2f38dfa0d925147af8f60c0d00eda54513c405ab</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdebindings-4.4.95.tar.bz2">kdebindings-4.4.95</a></td><td align="right">6,1MB</td><td><tt>4e92f4e693943012e2456c1b9ce7f4b683671fb2</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeedu-4.4.95.tar.bz2">kdeedu-4.4.95</a></td><td align="right">62MB</td><td><tt>a4918bd7517abe08e55f241c3cc65f75f4afb904</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdegames-4.4.95.tar.bz2">kdegames-4.4.95</a></td><td align="right">56MB</td><td><tt>00851bda1d3aece56842d41dc74cf559776eee12</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdegraphics-4.4.95.tar.bz2">kdegraphics-4.4.95</a></td><td align="right">4,5MB</td><td><tt>93b5a9d30e00a190ea2fba6c3e151721cc9c0eae</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdelibs-4.4.95.tar.bz2">kdelibs-4.4.95</a></td><td align="right">14MB</td><td><tt>21d31e2ac4062f763191ba54b1e452d541ff0ca6</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdemultimedia-4.4.95.tar.bz2">kdemultimedia-4.4.95</a></td><td align="right">1,6MB</td><td><tt>c116d87a515f4289f90aec35faba0a34c173653c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdenetwork-4.4.95.tar.bz2">kdenetwork-4.4.95</a></td><td align="right">7,8MB</td><td><tt>2374f6c85dabe19c4d17cb037da59f8e7f499dca</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdepimlibs-4.4.95.tar.bz2">kdepimlibs-4.4.95</a></td><td align="right">2,6MB</td><td><tt>59a90368e0a0d925b42069ccf9d3610aa18277cb</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeplasma-addons-4.4.95.tar.bz2">kdeplasma-addons-4.4.95</a></td><td align="right">1,7MB</td><td><tt>9d3bcd41243d630c5285e9c6524f3687844cd0ab</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdesdk-4.4.95.tar.bz2">kdesdk-4.4.95</a></td><td align="right">5,6MB</td><td><tt>0da4e63e7eaa5d48505fdf4744ef95c014393229</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdetoys-4.4.95.tar.bz2">kdetoys-4.4.95</a></td><td align="right">396KB</td><td><tt>c523778de4257e37a64159222ac31553b13ec452</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdeutils-4.4.95.tar.bz2">kdeutils-4.4.95</a></td><td align="right">3,6MB</td><td><tt>06febcbea4021354b70bcc0465e8223fa501892c</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/kdewebdev-4.4.95.tar.bz2">kdewebdev-4.4.95</a></td><td align="right">2,1MB</td><td><tt>42857d4f51f27f6f8ed38705a2c3c9eec0548159</tt></td></tr>
<tr valign="top"><td><a href="http://download.kde.org/unstable/4.4.95/src/oxygen-icons-4.4.95.tar.bz2">oxygen-icons-4.4.95</a></td><td align="right">133MB</td><td><tt>3d2677d3307fa76e146e19c120872d22be4269c7</tt></td></tr>
</table>
