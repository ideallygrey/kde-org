-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

KDE Security Advisory: resLISa / LISa Vulnerabilities
Original Release Date: 2002-11-11
URL: http://www.kde.org/info/security/advisory-20021111-2.txt

0. References

        iDEFENSE Security Advisory 11.11.02
	(http://www.idefense.com/advisory/11.11.02.txt).

1. Systems affected:

        All KDE 2 releases from KDE 2.1 and all KDE 3 releases (up to
        3.0.4 and 3.1rc3).

2. Overview:
        
        The kdenetwork module of KDE contains a LAN browsing implementation
	known as LISa, which is used to identify CIFS and other servers on
        the local network.  LISa consists of two main modules, "lisa", a
        network daemon, and "reslisa", a restricted version of the lisa
        daemon.  LISa can be accessed in KDE using the URL type "lan://",
        and resLISa using the URL type "rlan://".
        
        LISA will obtain information on the local network by looking for an
        existing LISA server on other local hosts, and if there is one,
        retrieves the list of servers from it.  If there is no other LISA
        server, it will scan the network and create as server list.

        The browser daemon 'lisa' is typically configured to start as a
        system service at system boot time.

        resLISa is a restricted version of LISa which uses a configuration
        file to identify hosts on the network rather than scanning for
        them.  resLISa is typically installed SUID root and started by a user
        to browse the confitured network servers.  However, it does not
        directly communicate with servers on the network.

3. Impact:

        The resLISa daemon contains a buffer overflow vulnerability which
        potentially enables any local user to obtain access to a raw socket
        if 'reslisa' is installed SUID root.  This vulnerability was
        discovered by the iDEFENSE security team and Texonet.
	
        The lisa daemon contains a buffer overflow vulnerability which
        potentially enables any local user, as well any any remote attacker
        on the LAN who is able to gain control of the LISa port (7741 by
        default), to obtain root privileges.
        
        In addition, a remote attacker potentially may be able to gain
        access to a victim's account by using an "lan://" URL in an HTML
        page or via another KDE application.  These vulnerabilities were
        discovered by Olaf Kirch at SuSE Linux AG.
        
4. Solution:
        
        The vulnerabilities have been fixed in KDE 3.0.5 and patches
        are available for those using KDE 3.0.4.  We recommend either
        upgrading to KDE 3.0.5, applying the patches or disabling the
        resLISa and LISa services.

        The resLISa vulnerability can be disabled by unsetting the SUID bit
        on resLISa.  Typically this is accomplished by executing the command:

          chmod a-s `which reslisa`
        
        Note that this will prevent users from using the resLISa service.

        The first LISa vulnerability can be disabled by disabling the LISa
        service.  Typically this is accomplished by executing the commands:
        
          /etc/init.d/lisa stop
          rm /etc/init.d/lisa `which lisa`

        or

          rpm -e kdenetwork-lisa

        However, the appropriate commands depend on your vendor's OS and how
        the various components of kdenetwork were packaged.

        The second LISa vulnerability can be disabled by deleting any
        lan.protocol and rlan.protocol files on the system and restarting
        the active KDE sessions.  The files are usually installed in
        [kdeprefix]/share/services/lan.protocol and
        [kdeprefix]/share/services/rlan.protocol  ([kdeprefix] is typically
        /opt/kde3 or /usr), but copies may exist elsewhere, such as in
        users' [kdehome]/share/services directory ([kdehome] is typically
        the .kde directory in a user's home directory).

        kdenetwork-3.0.5 can be downloaded from
        http://download.kde.org/stable/3.0.5/src/ :

         504032bceeef0dfa9ff02aed0faf795d   kdenetwork-3.0.5.tar.bz2

        Some vendors are building binary packages of kdenetwork-3.0.5.
        Please check your vendors website and the KDE 3.0.5 information page
        (/info/1-2-3/3.0.5) periodically for availability.


5. Patch:

	Patches are available for KDE 3.0.4 from the KDE FTP server
        (ftp://ftp.kde.org/pub/kde/security_patches/):

        5b2334c689ae9412475f6b653a107401  post-3.0.4-kdenetwork-lanbrowsing.diff
-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.0.6 (GNU/Linux)
Comment: For info see http://www.gnupg.org

iD8DBQE90Nk6lPwzJhSeGawRAs8FAJ9ugwFSkvBI8Wa0bKB+uAHEB4rFxwCeIQc9
IqFSYfX6boeptNgueNyjP6E=
=+8EN
-----END PGP SIGNATURE-----
