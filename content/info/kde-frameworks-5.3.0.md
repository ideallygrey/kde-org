---
version: "5.3.0"
title: "KDE Frameworks 5.3.0 Source Info and Download"
type: info/frameworks
bugs:
  - kwindowsystem v5.3.0 crashes on X11. Update to <a href="http://download.kde.org/stable/frameworks">kwindowsystem v5.3.1</a>
  - plasma-framework v5.3.0 is incompatible with Plasma 5.0. Update to <a href="http://download.kde.org/stable/frameworks">plasma-framework v5.3.1</a>
patch_level:
- 5.3.1
---

