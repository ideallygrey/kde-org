---
version: "5.21.5"
title: "KDE Plasma 5.21.5, bugfix Release"
type: info/plasma5
signer: Bhushan Shah
signing_fingerprint: 0AAC775BB6437A8D9AF7A3ACFE0784117FBCE11D
draft: false
---

This is a bugfix release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.21.5 announcement](/announcements/plasma/5/5.21.5).
