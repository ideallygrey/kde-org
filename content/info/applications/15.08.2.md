---
title: "KDE Applications 15.08.2 Info Page"
announcement: /announcements/announce-applications-15.08.2
layout: applications
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
