---
version: "5.27.80"
title: "KDE Plasma 6, Alpha Release"
type: info/plasma6
signer: Jonathan Esk-Riddell
signing_fingerprint: E0A3EB202F8E57528E13E72FD7574483BB57B18D
draft: false
---

This is a alpha release of KDE Plasma, featuring Plasma Desktop and
other essential software for your computer. Details in the
[Plasma 5.27.80 announcement](/announcements/kdes-6th-megarelease-alpha/).
