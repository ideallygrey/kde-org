    <table border="0" cellpadding="4" cellspacing="0">
      <tr valign="top">
        <th align="left">Location</th>

        <th align="left">Size</th>

        <th align="left">MD5&nbsp;Sum</th>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/arts-1.1.5.tar.bz2">arts-1.1.5</a></td>

        <td align="right">969KB</td>

        <td><tt>1d34348e805715559fcd0d978272f031</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdeaddons-3.1.5.tar.bz2">kdeaddons-3.1.5</a></td>

        <td align="right">1.1MB</td>

        <td><tt>789f4ff2037a9ce1ae0219f7e2fc4db6</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdeadmin-3.1.5.tar.bz2">kdeadmin-3.1.5</a></td>

        <td align="right">1.5MB</td>

        <td><tt>abaa80f34c3b4cbeac18c598cbfe79de</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdeartwork-3.1.5.tar.bz2">kdeartwork-3.1.5</a></td>

        <td align="right">14MB</td>

        <td><tt>3d58cd50865714e8b489802d3c5968b6</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdebase-3.1.5.tar.bz2">kdebase-3.1.5</a></td>

        <td align="right">15MB</td>

        <td><tt>88c94a1a6b3381c67164e1051b2e070e</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdebindings-3.1.5.tar.bz2">kdebindings-3.1.5</a></td>

        <td align="right">6.0MB</td>

        <td><tt>8bfe1598db8112eab9f2d09af57a4237</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdeedu-3.1.5.tar.bz2">kdeedu-3.1.5</a></td>

        <td align="right">20MB</td>

        <td><tt>093b3ca0ad0401935671611189c4e461</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdegames-3.1.5.tar.bz2">kdegames-3.1.5</a></td>

        <td align="right">8.2MB</td>

        <td><tt>6611d73c37dbd648f556e1370ba00031</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdegraphics-3.1.5.tar.bz2">kdegraphics-3.1.5</a></td>

        <td align="right">4.4MB</td>

        <td><tt>ba16a4a71381d57928325bc55ce7fe4c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kde-i18n-3.1.5.tar.bz2">kde-i18n-3.1.5</a></td>

        <td align="right">143MB</td>

        <td><tt>cca8b1090a8c0e30dcf18d752bae8482</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdelibs-3.1.5.tar.bz2">kdelibs-3.1.5</a></td>

        <td align="right">11MB</td>

        <td><tt>6e33c0f7c124e77d807da0ddb537b369</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdemultimedia-3.1.5.tar.bz2">kdemultimedia-3.1.5</a></td>

        <td align="right">5.7MB</td>

        <td><tt>f588d3aeb6f25387dee74befa096de0b</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdenetwork-3.1.5.tar.bz2">kdenetwork-3.1.5</a></td>

        <td align="right">4.8MB</td>

        <td><tt>d202929df2d5d88dca187c3d09f6ab44</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdepim-3.1.5.tar.bz2">kdepim-3.1.5</a></td>

        <td align="right">3.2MB</td>

        <td><tt>a3bc4986bd6e2a635f9c0ca67e2c8bb9</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdesdk-3.1.5.tar.bz2">kdesdk-3.1.5</a></td>

        <td align="right">2.1MB</td>

        <td><tt>7b7bdc08b774c98e99ce185e0612dd77</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdetoys-3.1.5.tar.bz2">kdetoys-3.1.5</a></td>

        <td align="right">1.8MB</td>

        <td><tt>ef9ad9c79647a8c72aa72285810c50d8</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/kdeutils-3.1.5.tar.bz2">kdeutils-3.1.5</a></td>

        <td align="right">1.4MB</td>

        <td><tt>a9e8c413f332448b547d12d15cf5707c</tt></td>
      </tr>

      <tr valign="top">
        <td><a href="http://download.kde.org/stable/3.1.5/src/quanta-3.1.5.tar.bz2">quanta-3.1.5</a></td>

        <td align="right">2.8MB</td>

        <td><tt>4998f3159bf9f5986a6b86ba7a9b2be7</tt></td>
      </tr>
    </table>
