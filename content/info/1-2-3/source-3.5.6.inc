<table border="0" cellpadding="4" cellspacing="0">
<tr valign="top">
  <th align="left">Location</th>
  <th align="left">Size</th>
  <th align="left">MD5&nbsp;Sum</th>
</tr>
<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/arts-1.5.6.tar.bz2">arts-1.5.6</a></td>
   <td align="right">944kB</td>
   <td><tt>e986393a5827499bbad04a00b797add0</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeaccessibility-3.5.6.tar.bz2">kdeaccessibility-3.5.6</a></td>
   <td align="right">8.2MB</td>
   <td><tt>03d3c9f4d8c2fd12b7d0e020e11cd88e</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeaddons-3.5.6.tar.bz2">kdeaddons-3.5.6</a></td>
   <td align="right">1.5MB</td>
   <td><tt>96d6d2a76da2a5232b3b46318456a5bc</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeadmin-3.5.6.tar.bz2">kdeadmin-3.5.6</a></td>
   <td align="right">2.0MB</td>
   <td><tt>13654a93e83b7c8fd2ccce3aceb2d535</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeartwork-3.5.6.tar.bz2">kdeartwork-3.5.6</a></td>
   <td align="right">15MB</td>
   <td><tt>4c817eab517fba30fce8f3b40a6f019d</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdebase-3.5.6.tar.bz2">kdebase-3.5.6</a></td>
   <td align="right">23MB</td>
   <td><tt>a53f589f58012e655a52220a6a151019</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdebindings-3.5.6.tar.bz2">kdebindings-3.5.6</a></td>
   <td align="right">5.1MB</td>
   <td><tt>d26b5f54f062b765a949d66657c2ab3c</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeedu-3.5.6.tar.bz2">kdeedu-3.5.6</a></td>
   <td align="right">28MB</td>
   <td><tt>6017317b133d973e7fc8a279a81f37a1</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdegames-3.5.6.tar.bz2">kdegames-3.5.6</a></td>
   <td align="right">10MB</td>
   <td><tt>d6cdf7d9235896670d5299e9e91665e2</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdegraphics-3.5.6.tar.bz2">kdegraphics-3.5.6</a></td>
   <td align="right">6.9MB</td>
   <td><tt>79a1ffb7ae89bede1410411a30be3210</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdelibs-3.5.6.tar.bz2">kdelibs-3.5.6</a></td>
   <td align="right">14MB</td>
   <td><tt>e4d137879a66e92b895b3de5413a61d8</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdemultimedia-3.5.6.tar.bz2">kdemultimedia-3.5.6</a></td>
   <td align="right">6.0MB</td>
   <td><tt>57c50bfcb0147324a1af02ebcc103376</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdenetwork-3.5.6.tar.bz2">kdenetwork-3.5.6</a></td>
   <td align="right">8.8MB</td>
   <td><tt>0f428cccc4ea16aa53c427530874c591</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdepim-3.5.6.tar.bz2">kdepim-3.5.6</a></td>
   <td align="right">13MB</td>
   <td><tt>e37e6173fe9fd7f242c9502a4ae1d7de</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdesdk-3.5.6.tar.bz2">kdesdk-3.5.6</a></td>
   <td align="right">4.8MB</td>
   <td><tt>1462e1a884fdaa070ed493c10a336728</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdetoys-3.5.6.tar.bz2">kdetoys-3.5.6</a></td>
   <td align="right">3.0MB</td>
   <td><tt>7d4f1a33e5379f789fcbf17b9e503bfd</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdeutils-3.5.6.tar.bz2">kdeutils-3.5.6</a></td>
   <td align="right">2.8MB</td>
   <td><tt>e0ea2c15ccf2bd3d8be5f2bf57cfe14a</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdevelop-3.3.6.tar.bz2">kdevelop-3.3.6</a></td>
   <td align="right">7.7MB</td>
   <td><tt>0de7c7d82c176456f2adff48981f5d40</tt></td>
</tr>

<tr valign="top">
   <td><a href="http://download.kde.org/stable/3.5.6/src/kdewebdev-3.5.6.tar.bz2">kdewebdev-3.5.6</a></td>
   <td align="right">5.7MB</td>
   <td><tt>fa1fc2d7c81465c7e1762014a892ced3</tt></td>
</tr>

</table>
