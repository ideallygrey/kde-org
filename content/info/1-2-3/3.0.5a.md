---
title : "KDE 3.0.5a Info Page"
unmaintained: true
publishDate: 2002-12-21 00:01:00
---


KDE 3.0.5a was released on December 21th, 2002.
Read the <a href="/announcements/announce-3.0.5a">official announcement</a>.

## FAQ

See the <a href="https://userbase.kde.org/Asking_Questions">KDE FAQ</a> for any specific
questions you may have.  Questions about Konqueror should be directed
<a href="http://konqueror.kde.org/faq/">to the Konqueror FAQ</a> and sound related
questions are answered in the
<a href="http://www.arts-project.org/doc/handbook/faq.html">FAQ of the aRts Project</a>

<h2>Download and Installation</h2>

<u>Source code</u>

{{< readfile "/content/info/1-2-3/source-3.0.5a.inc" >}}


<p>
  The translation package has been split into individual language
  packages so you can
  <a href="http://download.kde.org/stable/3.0.5a/src/kde-i18n/">download</a> only the
  translations you need.
</p>

<u>Binary packages</u>

<p>
Binary packages can be found under
<a href="http://download.kde.org/stable/3.0.5a/">http://download.kde.org/stable/3.0.5a/</a>
or in the equivalent directory at one of the KDE
<a href="/mirrors/ftp.php">FTP mirrors</a>.
</p>
<p>
The current list of available binary packages:
</p>

{{< readfile "/content/info/1-2-3/binary-3.0.5a.inc" >}}

<p>
  Several users have contributed packages for this release. You can find them
  in the <a href="http://download.kde.org/stable/3.0.5a/contrib/">contrib subdir</a>
  of the KDE 3.0.5a download area.
</p>

<p>
Additional binary packages might become available in the coming weeks,
as well as updates to the current packages.
</p>

<h2>Updates</h2>

<h2>Security Issues</h2>

<p>Please report possible problems to <a href="&#x6d;a&#x69;&#108;&#00116;o:secu&#114;i&#116;&#0121;&#64;kd&#x65;&#00046;&#x6f;rg">secu&#114;&#x69;&#x74;&#00121;&#x40;k&#100;e&#46;o&#x72;g</a>.</p>

<ul>
<li>
Several problems with KDE's use of Ghostscript where discovered that allow the execution of
arbitrary commands contained in PostScript (PS) or PDF files with the privileges of the victim.
Read the <a href="/info/security/advisory-20030409-1.txt">detailed advisory</a>. KDE 3.0.5b has been
released to address this problem.
It is strongly recommended to update at least kdelibs, kdebase and kdegraphics to
<a href="../3.0.5b">KDE 3.0.5b</a>
</li>
<li>
A HTTP authentication credentials leak via the a "Referrer" was discovered by George Staikos
in Konqueror. If the HTTP authentication credentials were part of the URL they would be possibly sent
in the referer header to a 3rd party web site.
Read the <a href="/info/security/advisory-20030729-1.txt">detailed advisory</a>. KDE 3.1.3 and newer
are not vulnerable.
</li>
</ul>

<h2><a name="bugs">Bugs</a></h2>

<p>This is a list of grave bugs and common pitfalls
surfacing after the release date:</p>

<ul>
<li>currently none known.</li>
</ul>

<h2>Developer Info</h2>

If you need help porting your application to KDE 3.x see the <a
href="http://websvn.kde.org/*checkout*/branches/KDE/3.5/kdelibs/KDE3PORTING.html">
porting guide</a> or subscribe to the
<a href="http://mail.kde.org/mailman/listinfo/kde-devel">KDE Devel Mailinglist</a>
to ask specific questions about porting your applications.

<p>There is also info on the <a
href="http://developer.kde.org/documentation/library/kdeqt/kde3arch/index.html">architecture</a>
and the <a href="http://developer.kde.org/documentation/library/3.0-api/classref/index.html">
programming interface of KDE 3.0</a>.
</p>
