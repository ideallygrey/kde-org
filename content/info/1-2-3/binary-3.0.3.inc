<ul>
<!--   CONECTIVA LINUX -->
<li><a href="http://www.conectiva.com/">Conectiva Linux</a>:
  (<a href="http://download.kde.org/stable/3.0.3/Conectiva/README">README</a>
  /
  <a href="http://download.kde.org/stable/3.0.3/Conectiva/LEIAME">LEIAME</a>)
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.3/Conectiva/conectiva/RPMS.kde/">Intel
      i386</a>
    </li>
  </ul>
  <p />
</li>

<!--  DEBIAN -->
<li><a href="http://www.debian.org">Debian</a>:
    <ul type="disc">
      <li>
         Debian unstable (sid):
         <a href="http://download.kde.org/stable/3.0.3/Debian/">Intel i386</a>
      </li>
    </ul>
</li>

<!--   FREEBSD -->
<!--
  <a href="http://www.freebsd.org/">FreeBSD</a>:  
  <a href="http://download.kde.org/stable/3.0.3/FreeBSD/">4.5-STABLE</a>
  <p />
-->

<!--   MAC OS X (FINK PROJECT) -->
<!--
<li>
  <a href="http://www.apple.com/macosx/">Mac OS</a> /
  <a href="http://www.opendarwin.org/">OpenDarwin</a>
  (downloads via the <a href="http://fink.sourceforge.net/">Fink</a> project):
  <ul type="disc">
    <li>
      <a href="http://fink.sourceforge.net/news/kde.php">X</a>
    </li>
  </ul>
  <p />
</li>
-->

<!--   MANDRAKE LINUX -->
<li>
  <a href="http://www.linux-mandrake.com/">Mandrake Linux</a>:
  <ul type="disc">
    <li>
      8.2:
      <a href="http://download.kde.org/stable/3.0.3/Mandrake/8.2/">Intel
      i586</a>
    </li>
    <li>
      8.1:
      <a href="http://download.kde.org/stable/3.0.3/Mandrake/8.1/">Intel
      i586</a>
    </li>
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.3/Mandrake/8.0/">Intel
      i586</a>
    </li>
  </ul>
  Please see also the
  <a href="http://download.kde.org/stable/3.0.3/Mandrake/noarch/">noarch</a>
  directory for i18n packages.
  <p />
</li>

<!--   REDHAT LINUX -->
<li>
  <a href="http://www.redhat.com/">Red Hat</a>:
  <ul type="disc">
    <li>
      7.3: <a href="http://download.kde.org/stable/3.0.3/RedHat/7.3/i386/">Intel i386</a>, 
           <a href="http://download.kde.org/stable/3.0.3/RedHat/7.3/ia64/">IA 64</a>,
           <a href="http://download.kde.org/stable/3.0.3/RedHat/7.3/alpha/">Alpha</a>,
           <a href="http://download.kde.org/stable/3.0.3/RedHat/7.3/noarch/">noarch</a>
    </li>
    <li>
      limbo: <a href="http://download.kde.org/stable/3.0.3/RedHat/limbo/i386/">Intel i386</a>,
             <a href="http://download.kde.org/stable/3.0.3/RedHat/limbo/ia64/">IA 64</a>,
             <a href="http://download.kde.org/stable/3.0.3/RedHat/limbo/alpha/">Alpha</a>,
             <a href="http://download.kde.org/stable/3.0.3/RedHat/limbo/noarch/">noarch</a>
    </li>
  </ul>
</li>

<li>
  <a href="http://www.redhat.com/">Red Hat</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      7.3: <a href="http://download.kde.org/stable/3.0.3/contrib/RedHat/7.3/">Intel i386</a>
    </li>
  </ul>
</li>

<!-- SLACKWARE LINUX -->
<li>
  <a href="http://www.slackware.org/">Slackware</a> (Unofficial contribution):
   <ul type="disc">
     <li>
       8.1: <a href="http://download.kde.org/stable/3.0.3/contrib/Slackware/8.1/">Intel i386</a>
     </li>
   </ul>
</li>

<!--
  <a href="http://download.kde.org/stable/3.0.3/Slackware/">8.0</a>
  <p />
</li>
-->

<!-- SOLARIS -->
<li>
  <a href="http://www.sun.com/solaris/">SUN Solaris</a> (Unofficial contribution):
  <ul type="disc">
    <li>
      8.0: <a href="http://download.kde.org/stable/3.0.3/contrib/Solaris/8.0/x86/">Intel x86</a>
    </li>
  </ul>
</li>

<!--   SUSE LINUX -->
<li>
  <a href="http://www.suse.com/">SuSE Linux</a>
<!--
  (<a href="http://download.kde.org/stable/3.0.3/SuSE/README">README</a>,
   <a href="http://download.kde.org/stable/3.0.3/SuSE/SORRY">SORRY</a>)-->:
  <ul type="disc">
    <li>
      8.0:
      <a href="http://download.kde.org/stable/3.0.3/SuSE/i386/8.0/">Intel
      i386</a>
    </li>
    <li>
      7.3:
      <a href="http://download.kde.org/stable/3.0.3/SuSE/i386/7.3/">Intel
      i386</a> and
      <a href="http://download.kde.org/stable/3.0.3/SuSE/ppc/7.3/">IBM
      PowerPC</a>
    </li>
    <li>
      7.2:
      <a href="http://download.kde.org/stable/3.0.3/SuSE/i386/7.2/">Intel
      i386</a>
    </li>
  </ul>
  Please see also the
  <a href="http://download.kde.org/stable/3.0.3/SuSE/noarch/">noarch</a>
  directory for i18n packages.
  <p />
</li>

<!-- TURBO LINUX -->
<li>
    <a href="http://www.turbolinux.com/">Turbolinux</a>
<!--    (<a href="http://download.kde.org/stable/3.0.3/Turbo/README">README</a>)-->:
    <ul>
      <li>
        <a href="http://download.kde.org/stable/3.0.3/Turbo/noarch/">Language
        packages</a> (all versions and architectures)</li>
      <li>
       7.0:
       <a href="http://download.kde.org/stable/3.0.3/Turbo/7/i586/">Intel i586</a>
      </li>
      <li>
       8.0: 
       <a href="http://download.kde.org/stable/3.0.3/Turbo/8/i586/">Intel i586</a>
      </li>
    </ul>
</li>

<!--   TRU64 -->
<li>
  <a href="http://www.tru64unix.compaq.com/">Tru64</a>
  (<a href="http://download.kde.org/stable/3.0.3/contrib/Tru64/README.Tru64">README</a>):
  <ul type="disc">
    <li>
      4.0d, e, f and g and 5.x:
      <a href="http://download.kde.org/stable/3.0.3/contrib/Tru64/">Compaq Alpha</a>
    </li>
  </ul>
  <p />
</li>
<!--   YELLOWDOG LINUX -->
<!--
  <a href="http://www.yellowdoglinux.com/">YellowDog</a>:
  <a href="http://download.kde.org/stable/3.0.3/YellowDog">2.2</a>
-->
</ul>
