---
aliases:
- ../announce-applications-18.08.0
changelog: true
date: 2018-08-16
description: KDE levererar KDE-program 18.08.0
layout: application
title: KDE levererar KDE-program 18.08.0
version: 18.08.0
---
16:e augusti, 2018. KDE-program 18.08.0 är nu utgivna.

Vi arbetar kontinuerligt på att förbättra programvaran som ingår i vår KDE-programserie, och vi hoppas att du finner alla nya förbättringar och felrättningar användbara.

### Vad är nytt i KDE-program 18.08

#### System

{{<figure src="/announcements/applications/18.08.0/dolphin1808-settings.png" width="600px" >}}

<a href='https://www.kde.org/applications/system/dolphin/'>Dolphin</a>, KDE:s kraftfulla filhanterare, har fått diverse livskvalitetsförbättringar:

- Inställningsdialogrutan har moderniserats så att den följer våra konstruktionsriktlinjer bättre och är intuitivare.
- Diverse minnesläckor som skulle kunna göra datorn långsammare har eliminerats.
- Menyalternative 'Skapa ny' är inte längre tillgängliga när papperskorgen visas.
- Programmet anpassar sig nu bättre till skärmar med hög upplösning.
- Den sammanhangsberoende menyn inkluderar mer användbara alternativ, som låter dig sortera och ändra visningsläget direkt.
- Sortering enligt ändringstid är nu 12 gånger snabbare. Dessutom kan du nu starta Dolphin igen om du är inloggad med systemadministratörens användarkonto. Stöd för att ändra filer ägda av systemadministratören när Dolphin körs som en normal användare är fortfarande pågående arbete.

{{<figure src="/announcements/applications/18.08.0/konsole1808-find.png" width="600px" >}}

Flera förbättringar av <a href='https://www.kde.org/applications/system/konsole/'>Terminal</a>, KDE:s terminalemulator, är tillgängliga:

- Den grafiska sökkomponenten visas nu ovanpå fönstret utan att störa arbetsflödet.
- Stöd för fler undantagssekvenser  (DECSCUSR & XTerm alternativt rullningsläge) har lagts till.
- Du kan nu också tilldela vilket eller vilka tecken som helst som tangent för en genväg.

#### Grafik

{{<figure src="/announcements/applications/18.08.0/gwenview1808.png" width="600px" >}}

18.08 är en huvudutgåva av <a href='https://www.kde.org/applications/graphics/gwenview/'>Gwenview</a>, KDE:s bildvisare och organisatör. Under de senaste månaderna har bidragsgivare arbetat med en uppsjö olika förbättringar. Nämnvärda är:

- Statusraden i Gwenview har nu en bildräknare och visar totalt antal bilder.
- Det är nu möjligt att sortera enligt betyg och i fallande ordning. Sortering enligt datum separerar nu kataloger och arkiv, och har rättats i vissa fall.
- Stöd för drag och släpp har förbättrats för att tillåta att filer och kataloger dras till visningsläget för att visa dem, samt att dra visade objekt till externa program.
- Att klistra in kopierade bilder från Gwenview fungerar nu också för program som bara acceptera obehandlad bilddata, men inte en filsökväg. Kopiering av ändrade bilder stöds nu också.
- Dialogrutan för storleksändring av bilder har setts över för bättre användbarhet och för att lägga till ett alternativ för att ändra storlek på bilder baserat på procentvärden.
- Storleksreglaget och hårkorsmarkören i verktyget för reduktion av röda ögon har rättats.
- Val av genomskinlig bakgrund har nu ett alternativ för 'Ingen' och kan också ställas in för SVG:er.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-resize.png" width="600px" >}}

Bildzoomning har blivit bekvämare.

- Zoomning genom att rulla eller klicka samt panorering är också aktiverade när verktygen för beskärning och reduktion av röda ögon är aktiva.
- Att klicka med mittenknappen växlar återigen mellan anpassad zoomning och 100 % zoomning.
- Tillägg av skift och mittenklick samt Skift+F snabbtangenter för att växla till fyllzoomning.
- Ctrl-klick zoomar nu snabbare och tillförlitligare.
- Gwenview zoomar nu till markörens nuvarande plats för zoomåtgärderna zooma in/ut, fyll och 100 % när musen och snabbtangenterna används.

Bildjämförelseläget har fått flera förbättringar:

- Rätta storlek och justering av markeringsfärgläggning.
- Rätta att SVG:er överlappar markeringsfärgläggningen.
- För små SVG-bilder, matchar markeringsfärgläggningen bildstorleken.

{{<figure src="/announcements/applications/18.08.0/gwenview1808-sort.png" width="600px" >}}

Ett antal små förbättringar introducerades för att göra arbetsflödet ännu mer njutbart:

- Förbättrade toningsövergångarna mellan bilder av varierande storlek och genomskinlighet.
- Rättade synlighet för ikoner i vissa flytande knappar när ett ljust färgschema används.
- När en bild sparas med ett nytt namn, går inte visningen till en orelaterad bild efteråt.
- När delningsknappen klickas och kipi-insticksprogram inte är installerade, ber Gwenview användaren att installera dem. Efter installation, visas de omedelbart.
- Sidoraden förhindrar att den döljs av misstag vid storleksändring och kommer ihåg sin bredd.

#### Kontor

{{<figure src="/announcements/applications/18.08.0/kontact1808.png" width="600px" >}}

<a href='https://www.kde.org/applications/internet/kmail/'>Kmail</a>, KDE:s kraftfulla e-postprogram, erbjuder några förbättringar i gränssnittet för extrahering av resdata. Det stöder nu UIC 918.3 och SNCF streckkoder för tågbiljetter, och Wikidata-driven uppslagning av tågstationer. Stöd för resplaner för flera resande har lagts till, och Kmail har nu integrerats med KDE:s reseplaneringsprogram.

<a href='https://userbase.kde.org/Akonadi'>Akonadi</a>, ramverket för personlig informationshantering, är nu snabbare tack vare underrättelsenyttolaster och erbjuder stöd för XOAUTH i SMTP, vilket tillåter inbyggd behörighetskontroll med Gmail.

#### Utbildning

<a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, KDE:s gränssnitt för matematisk programvara, sparar nu panelernas status (\"Variabler\", \"Hjälp\", etc.) separat för varje session. Julia-sessioner går mycket fortare att skapa.

Användarupplevelsen av <a href='https://www.kde.org/applications/education/kalgebra/'>Kalgebra</a>, vår grafiska räknare, har signifikant förbättrats för pekenheter.

#### Verktyg

{{<figure src="/announcements/applications/18.08.0/spectacle1808.png" width="600px" >}}

Bidragsgivarna till <a href='https://www.kde.org/applications/graphics/spectacle/'>Spectacle</a>, KDE:s mångsidiga skärmbildsverktyg, fokuserade på att förbättra funktionen för rektangulärt område:

- I rektangulärt områdesläge finns nu ett förstoringsglas för att hjälpa till att rita en rektangel som är bildpunktsriktig.
- Det går nu att flytta och ändra storlek på markeringsrektangeln genom att använda tangentbordet.
- Användargränssnittet följer användarens färgschema, och presentationen av hjälptext har förbättrats.

För att göra det lättare att dela skärmbilder med andra, kopieras automatiskt länkar till delade bilder till klippbordet. Skärmbilder kan nu automatiskt sparas i användarspecificerade underkataloger.

<a href='https://userbase.kde.org/Kamoso'>Kamoso</a>, vår webbkamerainspelare, har uppdaterats för att undvika krascher med nyare versioner av GStreamer.

### Felutplåning

Mer än 120 fel har rättats i program, inklusive Kontact-sviten, Ark, Cantor, Dolphin, Gwenview, Kate, Konsole, Okular, Spectacle, Umbrello med flera.

### Fullständig ändringslogg
