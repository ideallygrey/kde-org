---
aliases:
- ../announce-applications-18.08.1
changelog: true
date: 2018-09-06
description: KDE levererar KDE-program 18.08.1
layout: application
title: KDE levererar KDE-program 18.08.1
version: 18.08.1
---
6:e september, 2018. Idag ger KDE ut den första stabilitetsuppdateringen av <a href='../18.08.0'>KDE-program 18.08</a>. Utgåvan innehåller bara felrättningar och översättningsuppdateringar, och är därmed en säker och behaglig uppdatering för alla.

Fler än ett dussin registrerade felrättningar omfattar förbättringar av bland annat Kontact, Cantor, Gwenview, Okular och Umbrello.

Förbättringar omfattar:

- Komponenten KIO-MTP kraschar inte längre när enheten redan används av ett annat program.
- Skicka brev i Kmail använder nu lösenordet när det anges via lösenordsförfrågan
- Okular kommer nu ihåg sidoradens läge efter att PDF-dokument har sparats
