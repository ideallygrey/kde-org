---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Nya ikoner för Mime-typer.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Använd riktig inställningspost i villkor för automatisk start
- Rätta sorterad infogning (också känt som infogning liknande flat_map) (fel 367991)
- Lägg till saknad stäng miljö, som påpekades av Loïc Yhuel (fel 353783)
- Transaktioner skapades inte ⇒ försök inte avbryta dem
- Rätta saknad tilldelning m_env = nullptr
- Gör t.ex. Baloo::Query trådsäker
- På 64-bitars system tillåter nu Baloo &gt; 5 GiB indexlagring (fel 364475)
- Tillåt ctime/mtime == 0 (fel 355238)
- Hantera skadad indexdatabas för baloo_file, försök återskapa databasen eller avbryt om det misslyckas

### BluezQt

- Rätta krasch vid försök att lägga till enhet till okänd adapter (fel 364416)

### Breeze-ikoner

- Nya ikoner för Mime-typer
- Uppdatera vissa ikoner för kstars (fel 364981)
- Fel stil actions/24/format-border-set (fel 368980)
- Lägg till ikon för Wayland-program
- Lägg till ikon för Xorg-program (fel 368813)
- Återställ distribute-randomize, view-calendar + inför rättningen av transform igen (fel 367082)
- Ändra folder-documents från en fil till flera filer ifall mer än en fil är inkluderad i en katalog (fel 368224)

### Extra CMake-moduler

- Säkerställ att vi inte lägger till test för appstream två gånger

### KActivities

- Sortera aktiviteter i cachen alfabetiskt enligt namn (fel 362774)

### KDE Doxygen-verktyg

- Många ändringar av den övergripande layouten i de skapade dokumentationen av programmeringsgränssnittet
- Rätta sökväg för taggar, beroende på om biblioteket ingår i en grupp eller inte
- Sökning: Rätta href för bibliotek som inte ingår i en grupp

### KArchive

- Rätta minnesläcka för KTar:s KCompressionDevice
- KArchive: Rätta minnesläcka när en post med samma namn redan finns
- Rätta minnesläcka i KZip när tomma kataloger hanteras
- K7Zip: Rätta minnesläckor vid fel
- Rätta minnesläcka detekterad av ASAN när open() misslyckas för underliggande enhet
- Ta bort felaktig typkonvertering till KFilterDev, detekterad av ASAN

### KCodecs

- Lägg till saknade exportmakron för klasserna Decoder och Encoder

### KConfig

- Rätta minnesläcka i SignalsTestNoSingletonDpointer, hittad av ASAN

### KCoreAddons

- Registrera QPair&lt;QString,QString&gt; som metatyp i KJobTrackerInterface
- Konvertera inte som webbadress, en webbadress som har ett dubbelt citationstecken
- Kompileringsrättningar för Windows
- Rätta mycket gammalt fel när vi tar bort mellanslag i webbadress såsom "exempel &lt;&lt;webbadress&gt; &lt;webbadress&gt;&gt;"

### KCrash

- CMake alternativet KCRASH_CORE_PATTERN_RAISE att skicka vidare till kärnan
- Ändra normal loggnivå från varning till information

### Stöd för KDELibs 4

- Städning. Installera inte deklarationsfiler som pekar på icke existerande deklarationsfiler och ta också bort dessa filer
- Använd riktigare std::remove_pointer, också tillgänglig i C++ 11

### KDocTools

- Rätta 'checkXML5 skriver ut genererad HTML på standardutmatningen för giltiga docbooks' (fel 369415)
- Rätta fel som gjorde att det inte var möjligt att köra inbyggda verktyg i paket med korskompilerad kdoctools
- Ställ in mål för korskompilering som kör kdoctools från andra paket
- Lägg till stöd för korskompilering av docbookl10nhelper
- Lägg till stöd för korskompilering av meinproc5
- Konvertera checkxml5 till ett körbart Qt-program för korsplattformsstöd

### KFileMetaData

- Förbättra extrahering av epub, färre segmenteringsfel (fel 361727)
- Gör ODF-indexering mer felsäker, kontrollera om filerna finns dör (och överhuvudtaget är filer) (meta.xml + content.xml)

### KIO

- Rätta I/O-slavar som bara använder TLS 1.0
- Rätta söndrigt binärprogramgränssnitt i KIO
- KFileItemActions: Lägg till addPluginActionsTo(QMenu *)
- Visa bara kopieringsknappar efter checksumman har beräknats
- Lägg till saknad återmatning när en checksumma beräknas (fel 368520)
- Rätta att KFileItem::overlays returnerar tomma strängvärden
- Rätta start av terminal .desktop-filer med Terminal
- Klassificera nfs4 monteringar som troligtvis långsam, som nfs/cifs/...
- KNewFileMenu: visa genväg för åtgärden Ny katalog (fel 366075)

### KItemViews

- Använd standardimplementeringen av moveCursor i listvyläge

### KNewStuff

- Lägg till KAuthorized kontroller för att tillåta att ghns inaktiveras i kdeglobals (fel 368240)

### Paketet Framework

- Skapa inte appstream-filer för komponenter som inte finns i RDN
- Gör så att kpackage_install_package fungerar med KDE_INSTALL_DIRS_NO_DEPRECATED
- Ta bort oanvänd variabel KPACKAGE_DATA_INSTALL_DIR

### KParts

- Rätta att webbadresser med ett avslutande snedstreck alltid antas vara kataloger

### KPeople

- Rätta ASAN-byggning (duplicates.cpp använder KPeople::AbstractContact som finns i KF5PeopleBackend)

### KPty

- Använd ECM-sökväg för att hitta binärfilen utempter, mer tillförlitlig än enkelt CMake-prefix
- Anropa hjälpprogrammet utempter manuellt (fel 364779)

### KTextEditor

- XML-filer: Ta bort hårdkodad färg för värden
- XML: Ta bort hårdkodad färg för värden
- XML-schemadefinition: Ändra 'version' till en xs:integer
- Färgläggningsdefinitionsfiler: Avrunda version uppåt till nästa heltal
- Stöd bara lagring av flera tecken i {xxx} för att undvika regressioner
- Stöd ersättning med reguljära uttryck med lagringar &gt; 9, t.ex. 111 (fel 365124)
- Rätta återgivning av tecken som fortsätter till nästa rad, t.ex. understreck skärs inte bort av några teckensnitt eller teckenstorlekar (fel 335079)
- Rätta krasch: Säkerställ att den visade markören är giltig efter textvikning (fel 367466)
- KateNormalInputMode måste köra om ingångsmetoderna i SearchBar
- Försök att "rätta" återgivning av understreck och liknande (fel 335079)
- Visa bara knappen "Visa skillnad" om 'diff' är installerad
- Använd meddelandekomponent utan fast läge för externt modifierade filunderrättelser (fel 353712)
- Rätta regression: testNormal fungerade bara på grund av testkörning på en gång
- Dela upp indenteringstest i separata körningar
- Stöd åtgärden "Expandera toppnivånoder" igen (fel 335590)
- Rätta krasch när topp- eller bottenmeddelanden visas flera gånger
- Rätta radslutsinställningar för lägesrader (fel 365705)
- Färglägg .nix-filer som bash, en gissning kan inte skada (fel 365006)

### Ramverket KWallet

- Kontrollera om plånboken är aktiverad i Wallet::isOpen(namn) (fel 358260)
- Lägg till saknad boost-deklarationsfil
- Ta bort duplicerad sökning för KF5DocTools

### Kwayland

- [server] Skicka inte tangentsläpp för tangenter som inte tryckts ner och inga dubbla tangenttryckningar (fel 366625)
- [server] När klippbordets markering ersätts måste föregående datakälla avbrytas (fel 368391)
- Lägg till stöd för in/ut-händelser för Surface
- [klient] Följ alla skapade utgångar och lägg till statisk hämtningsmetod

### KXmlRpcClient

- Konvertera kategorier till org.kde.pim.*

### NetworkManagerQt

- Vi måste ställa in tillståndet under initiering
- Ersätt alla blockerande anrop vid initiering med bara ett blockerande anrop
- Använd standardgränssnitt o.f.DBus.Properties för signalen PropertiesChanged med NM 1.4.0+ (fel 367938)

### Oxygen-ikoner

- Ta bort ogiltig katalog från index.theme
- Introducera test av dubbletter från breeze-icons
- Konvertera alla dubbletter av ikoner till symboliska länkar

### Plasma ramverk

- Stör utmatning för tidsspårare
- [ToolButtonStyle] Rätta menypil
- i18n: Hantera strängar i kdevtemplate-filer
- i18n: Granska strängarna i kdevtemplate-filer
- Lägg till removeMenuItem i PlasmaComponents.ContextMenu
- Uppdatera ktorrent-ikon (fel 369302)
- [WindowThumbnail] Kasta bildpunktsavbildning vid avbildningshändelser
- Inkludera inte kdeglobals när en cacheinställning hanteras
- Rätta Plasma::knownLanguages
- Ändra storlek på vyn precis efter 
- Undvik skapa en KPluginInfo från en instans av KPluginMetaData
- Aktiviteten som kör måste ha någon indikering
- Linjer för aktivitetsrad enligt RR 128802, Marco gav klartecken
- [AppletQuickItem] Hoppar ur snurra när en layout hittats

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
