---
aliases:
- ../../kde-frameworks-5.0
- ./5.0
customIntro: true
date: '2014-07-07'
description: KDE levererar första betaversion av Ramverk 5.
layout: framework
qtversion: 5.2
title: Första utgåvan av KDE Ramverk 5
---
7:e juli 2014. KDE:s gemenskap är stolt över att tillkännage en KDE:s Ramverk 5. Ramverk 5 är nästa generation av KDE:s bibliotek, modulariserade och optimerade för enkel integrering i Qt-program. Ramverken erbjuder ett brett utbud av allmänt användbar funktionalitet i referentgranskade och väl utprovade bibliotek med gynnsamma licensvillkor. Det finns över 50 olika ramverk, som ingår i den här utgåvan, och erbjuder lösningar som omfattar hårdvaruintegrering, stöd för filformat, ytterligare grafiska komponenter, ritfunktioner, stavningskontroll med mera. Många av ramverken fungerar på olika plattformar och har få eller inga extra beroenden, vilket gör det enkelt att bygga dem och lägga till i vilket Qt-program som helst.

KDE:s Ramverk representerar ett bemödande att omarbeta de kraftfulla biblioteken i KDE 4 plattformen till en uppsättning fristående plattformsoberoende bibliotek som kommer att vara lätt tillgängliga för alla Qt-utvecklare för att förenkla, accelerera och reducera kostnaden för utveckling med Qt. De enskilda ramverken är plattformsoberoende, väl dokumenterade och utprovade, och deras användning är välkänd för Qt-utvecklare genom att följa stilen och standarder bestämda av Qt-projektet. Ramverk utvecklas enligt KDE:s beprövade styrningsmodell med en förutsägbar utgivningstidplan, en klar och tillverkarneutral bidragsprocess, öppen styrning och flexibel licens (LGPL).

Ramverken har en klar beroendestruktur, uppdelad i kategorier och lager. Kategorierna gäller körtidsberoenden:

- <strong>Funktionella</strong> element har inga körtidsberoenden.
- <strong>Integrering</strong> betecknar kod som kan kräva körtidsberoenden för integrering beroende på vad operativsystemet eller plattformen erbjuder.
- <strong>Lösningar</strong> kräver körtidsberoenden.

<strong>Lager</strong> gäller kompileringsberoenden på andra ramverk. Lager 1 ramverk har inga beroende inom ramverken och behöver bara Qt och andra relevanta bibliotek. Lager 2 ramverk kan bara bero på lager 1. Lager 3 ramverk kan bero på andra lager 3 ramverk samt på lager 2 och lager 1.

Övergången från plattform till ramverk har pågått i över tre år, styrd av de bästa tekniska bidragsgivarna inom KDE. Ta reda på mer om Ramverk 5 <a href='http://dot.kde.org/2013/09/25/frameworks-5'>i den här artikeln från förra året</a>.

## Ändringar

För närvarande finns över 50 ramverk tillgängliga. Bläddra i hela mängden <a href='http://api.kde.org/frameworks-api/frameworks5-apidocs/'>i dokumentationen av programmeringsgränssnittet på nätet</a>. Nedan följer några intryck av vissa av de funktioner som ramverken erbjuder för Qt-programutvecklare.

<strong>KArchive</strong> erbjuder stöd för många populära komprimeringskodare i ett självständigt, funktionsrikt och lättanvänt bibliotek för filarkivering och uppackning. Skicka bara filer till det: Det finns inget behov av att återuppfinna en arkiveringsfunktion i ett Qt-baserat program.

<strong>ThreadWeaver</strong> erbjuder ett högnivå-programmeringsgränssnitt för att hantera trådar genom användning av jobb- och köbaserade gränssnitt. Det möjliggör enkel schemaläggning av trådkörning genom att ange beroenden mellan trådarna och köra dem medan dessa beroenden uppfylls, vilket kraftigt förenklar användning av flera trådar.

<strong>KConfig</strong> är ett ramverk för att hantera lagring och hämtning av konfigurationsinställningar. Det tillhandahåller ett grupporienterat programmeringsgränssnitt. Det fungerar med INI-filer och XDG-kompatibla kaskadkataloger. Det skapar kod baserat på XML-filer.

<strong>Solid</strong> erbjuder detektering av hårdvara och kan informera ett program om lagringsenheter och volymer, processor, batteristatus, strömhantering, nätverksstatus och gränssnitt, samt Blåtand. För krypterade partitioner, ström och nätverk krävs att demoner kör.

<strong>KI18n</strong> lägger till Gettext-stöd i program, vilket gör det enklare att integrera arbetsflödet för översättning av Qt-program med den allmänna infrastrukturen för översättning i många projekt.
