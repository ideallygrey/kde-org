---
aliases:
- ../../kde-frameworks-5.61.0
date: 2019-08-10
layout: framework
libCount: 70
---
### Baloo

- Länka med KIOCore istället för KIOWidgets i I/O-slavar
- [IndexCleaner] ignorera icke-existerande poster inne i inställning

### BluezQt

- Rätta krasch på grund av att q-pekaren aldrig initieras
- Inkludera inte bluezqt_dbustypes.h från installerade deklarationsfiler

### Breeze-ikoner

- Lägg till "user-others" ikon (fel 407782)
- Gör "edit-none" en symbolisk länk till "dialog-cancel"
- Ta bort redundanta och svartvita versioner av applications-internet
- Lägg till view-pages-* ikoner, som behövs i Okular val av sidlayout (fel 409082)
- Använd medurs pilar för ikonerna _refresh_ och update-* (fel 409914)

### Extra CMake-moduler

- android: Tillåt att överskrida ANDROID_ARCH och ANDROID_ARCH_ABI som miljövariabler
- Underrätta användare när KDE_INSTALL_USE_QT_SYS_PATHS inte används om prefix.sh
- Tillhandahåll ett vettigare förval för CMAKE_INSTALL_PREFIX
- Gör förvald byggtyp till "Debug" när en utcheckning från git kompileras

### KActivitiesStats

- Lägg till Date term i KActivities Stats för att filtrera på resursens händelsedatum

### KActivities

- Förenkla kod för föregående-/nästa aktivitet i kactivities-cli

### KDE Doxygen-verktyg

- Rätta kontroll av kataloger för metainfo.yaml med icke-ascii tecken med Python 2.7
- Logga felaktiga sökvägsnamn (via repr()) istället för att krascha helt och hållet
- generera lista över datafiler i farten

### KArchive

- KTar::openArchive: Ingen assert om en fil har två rotkataloger
- KZip::openArchive: Ingen assert när felaktiga filer öppnas

### KCMUtils

- anpassa till ändringar av användargränssnittet i KPageView

### KConfig

- Säkerhet: ta bort för stöd $(...) i inställningsnycklar med [$e] markör
- Inkludera definition för klass använd i sidhuvud

### KCoreAddons

- Lägg till funktionen KFileUtils::suggestName för att föreslå ett unikt filnamn

### KDeclarative

- Scrollview - Fyll inte överliggande objekt med vyn (fel 407643)
- introducera FallbackTapHandler
- KRun QML-proxy: rätta förvirring mellan sökväg/webbadress
- Kalenderhändelser: tillåt insticksprogram att visa händelseinformation

### KDED

- kded5 skrivbordsfil: använd giltig typ (Service) för att undertrycka varning från kservice

### Stöd för KDELibs 4

- Designer plugin: använd "KF5" konsekvent i gruppnamn och texter
- Tillkännagör inte användning av KPassivePopup

### KDesignerPlugin

- exponera ny KBusyIndicatorWidget
- Ta bort generering av designer insticksprogram för KF5WebKit

### KDE WebKit

- Använd förhandsgranskning av ECMAddQtDesignerPlugin istället för KF5DesignerPlugin
- Lägg till alternativ för att bygga Qt Designer insticksprogram (BUILD_DESIGNERPLUGIN, förval ON)

### KFileMetaData

- Se till att mobipocket extrahering är aktuell, med behåll den inaktiverad

### KHolidays

- Lägg till ersättningsdagar för helgdagar i Ryssland för 2019-2020
- Uppdatera helger i Ryssland

### KIconThemes

- Återställ "Kontrollera om group &lt; LastGroup, eftersom KIconEffect ändå inte hanterar UserGroup"

### KIO

- Avråd från suggestName till förmån för den i KCoreAddons
- Rätta fel att inte kunna gå in i kataloger på vissa FTP-servrar med turkisk landsinställning (fel 409740)

### Kirigami

- Gör om Kirigami.AboutPage
- Använd Units.toolTipDelay konsekvent istället för hårdkodade värden
- ändra storlek på kortinnehåll riktigt när kortstorleken är begränsad
- dölj krusning när vi inte vill att objekt ska vara klickbara
- gör så att greppet följer lådans godtyckliga höjd
- [SwipeListItem] Ta hänsyn till rullningslisters synlighet och formfaktor för grepp och åtgärder på plats
- Ta bort skalning av ikonstorleksenhet för isMobile
- alltid visa bakåtknapp för lager &gt; 1
- dölj åtgärder med undermenyer från fler menyer
- förvald position för ActionToolBar till Header
- stort z att inte visas under dialogrutor
- använd ogenomskinlighet för att dölja knappar som inte får plats
- lägg bara till distans när den fyller bredden
- fullständigt bakåtkompatibel med showNavigationButtons som bool
- mer granularitet för globalToolBar.showNavigationButtons

### KItemModels

- David Faure är nu underhållsansvarig för KItemModels
- KConcatenateRowsProxyModel: lägg till anmärkning att Qt 5.13 tillhandahåller QConcatenateTablesProxyModel

### Ramverket KPackage

- Erbjud metadata.json när paketets metadata begärs
- PackageLoader: Använd rätt räckvidd för KCompressionDevice

### KPeople

- declarative: uppdatera åtgärdslista med personändringar
- declarative: krascha inte när programmeringsgränssnittet används felaktigt
- personsmodel: Lägg till telefonnummer

### KService

- Exponera X-KDE-Wayland-Interfaces
- Rätta bygge av KService på Android
- KService: ta bort felaktigt koncept med global sycoca-databas
- Ta bort verkligt farlig borttagningskod med kbuildsycoca5 --global
- Rätta oändlig rekursion och asserts när sycoca-databasen inte är oläsbar av användare (t.ex. ägd av systemadministratören)
- Avråd från KDBusServiceStarter. All användning i kdepim är nu borta, DBus-aktivering är en bättre lösning
- Tillåt att KAutostart skapas med en absolut sökväg

### KTextEditor

- Spara och läs in sidmarginaler
- Låt inte behörighetskontroll vara beständig
- Ändra avbildning av förvald genväg "Byt indataläge" så den inte är i konflikt med konsolepart (fel 409978)
- Gör så att nyckelordskompletteringsmodellen normalt returnerar HideListIfAutomaticInvocation
- Minimap: Ta inte tag i left-mouse-button-click för upp- och nerknappar
- tillåt upp till 1024 färgläggningsintervall istället för att inte färglägga raden alls om den gränsen har uppnåtts
- rätta vikning av rader med slutposition på kolumn 0 av en rad (fel 405197)
- Lägg till alternativ för att också behandla vissa tecken som "automatisk parenteser" när vi har en markering
- Lägg till en åtgärd för att infoga en icke-indenterad nyrad (fel 314395)
- Lägg till inställning för att aktivera/inaktivera dra och släppt för text (normalt på)

### KUnitConversion

- Lägg till binära dataenheter (bitar, kilobyte, kibibyte ... yottabyte)

### Ramverket KWallet

- Flytta kwalletd initiering tidigare (fel 410020)
- Ta bort kde4 konverteringsmodul fullständigt (fel 400462)

### Kwayland

- Använd wayland-protocols

### KWidgetsAddons

- introducera konceptet med sidhuvud och sidfot för kpageview
- [Upptagetindikering] Matcha varaktighet med QQC2-desktop-style versionen
- Lägg till en varningsdialog med en hopfällbar detaljsektion
- ny klass KBusyIndicatorWidget som liknar BusyIndicator i QtQuick

### KWindowSystem

- [platforms/xcb] Använd XRES-ändelse för att få verkligt fönster-PID (fel 384837)
- Konvertera KXMessages från QWidget

### KXMLGUI

- Lägg till expanderande distanser som ett anpassningsalternativ för verktygsrader
- Använd svartvita åtgärdsikoner för KAboutData knappar
- Ta bort anslutningen visibilityChanged till förmån för befintligt eventFilter

### ModemManagerQt

- Tillåt uppdatering av tidsgräns för DBus för varje gränssnitt

### NetworkManagerQt

- enhet: inkludera reapplyConnection() i gränssnittet

### Plasma ramverk

- [ToolButtonStyle] Använd samma färggrupp för håll-över tillstånd
- Hantera färgfil i falsk plasma temainstallation
- Installera plasma-tema i lokal XDG_DATA_DIR för ikontest
- Verkställ ändring av varaktighet för upptagetindikering i D22646 för QQC2-stilen
- Kompilera paketets strukturinsticksprogram i förväntad underkatalog
- Ändra färgläggning till ButtonFocus
- Rätta körning av dialognativetest utan att installera
- Sök efter den andra plasmoidens insticksprogram
- [Upptagetindikering] Matcha varaktighet med QQC2-desktop-style versionen
- Lägg till saknade komponenter i org.kde.plasma.components 3.0
- Uppdatera ikonerna refresh och restart för att reflektera nya versioner av breeze-icons (fel 409914)
- itemMouse är inte definierat i plasma.components 3.0
- använd clearItems när ett miniprogram blir borttaget
- Rätta krasch om switchSize justeras vid initial tilldelning
- förbättra lagring i cache för insticksprogram

### Syfte

- Phabricator: öppna automatiskt en ny jämförelse i webbläsaren
- Rätta extrahering. Programfix av Victor Ryzhykh

### QQC2StyleBridge

- Rätta felaktigt skydd som förhindrar att förse skjutreglage med negative värden
- Gör upptagetindikeringens rotationshastighet långsammare
- Rätta "Typfel" när ett TextField med focus: true  skapas
- [ComboBox] Ställ in stängningspolicy till stäng vid klick utanför istället för bara utanför överliggande objekt (fel 408950)
- [SpinBox] Tilldela renderType (fel 409888)

### Solid

- Säkerställ att solid-gränssnitt är återanropningsbart

### Syntaxfärgläggning

- TypeScript: rätta nyckelord i villkorsuttryck
- Rätta generering och testsökvägar för CMake
- Lägg till stöd för ytterligare QML-nyckelord som inte ingår i Javascript
- Uppdatera cmake-färgläggning

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
