---
aliases:
- ../../kde-frameworks-5.50.0
date: 2018-09-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Lägg till stöd för föreslaget tillägg av taggar i OCS 1.7

### Baloo

- Rättade felstavning i utmatning av indexstorlek (fel 397843)
- Ta bort källa inte mål när en webbadress just blivit oindexerbar
- [tags_kio] Förenkla matchning av filnamnssökvägsförfrågan genom att använda en fångstgrupp
- Återställ "Hoppa över att placera nya icke indexerade filer i kö och ta omedelbart bort dem från indexet."
- [tags_kio] Förenkla while-snurra för filsökvägsuppslagning

### Breeze-ikoner

- Lägg till projektfilikon för LabPlot
- ScalableTest, lägg till "skalbar" plasma-browser-integration (fel 393999)

### Extra CMake-moduler

- Bindningar: Kontrollera om bindningar kan genereras för en specifik version av Python
- Bindningar: Gör generatorn framåtkompatibel med Python 3
- Inaktivera förändring av QT_PLUGIN_PATH av ECM när tester körs
- Bindningar: Lägg till stöd för scoped enums (fel 397154)
- Gör det möjligt för ECM att detektera po-filer vid konfigureringstillfälle

### Integrering med ramverk

- [KStyle] Använd dialog-question för frågeikon

### KArchive

- Hantera icke-ASCII kodningar av filnamn i tar-arkiv (fel 266141)
- KCompressionDevice: Anropa inte write efter WriteError (fel 397545)
- Lägg till saknade Q_OBJECT makron för QIODevice delklasser
- KCompressionDevice: Skicka vidare fel från QIODevice::close() (fel 397545)
- Rätta huvudsida för bzip

### KCMUtils

- Använd anpassad QScrollArea med storlekstips som inte är begränsat av teckenstorlek (fel 389585)

### KConfig

- Ta bort varning om gammal kioskfunktion som inte längre gäller
- Ange systemets standardgenväg Ctrl+0 för åtgärden "Verklig storlek"

### KCoreAddons

- Ta inte bort mellanslag mellan två webbadresser när raden börjar med " (fel i kmail)
- KPluginLoader: Använd '/' också på Windows, libraryPaths() returnerar sökvägar med '/'
- KPluginMetaData: Konvertera tom sträng till tom stränglista

### KDeclarative

- Återställ "Säkerställ att vi alltid skriver i gränssnittets rotsammanhang"
- Anslut egenskap till "delegat" (fel 397367)
- [KCM GridDelegate] Använd bara lagereffekt för OpenGL-gränssnitt (fel 397220)

### KDocTools

- Lägg till förkortningen ASCII i general.entities
- Lägg till JSON i general.entities
- Låt meinproc5 vara mångordigare i automatisk test för 'install'
- Använd absoluta sökvägar för att hitta installerade filer i testen kdoctools_install

### KFileMetaData

- Lägg till alias Property::Language i uppräkningstyp för felskrivningen Property::Langauge

### KHolidays

- Implementera riktig beräkningsalgoritm för dagjämning och solstånd (fel 396750)
- src/CMakeLists.txt - Installera deklarationsfiler på ramverkssätt

### KI18n

- Gör assert vid försök att använda KCatalog utan en QCoreApplication
- Konvertera ki18n från QtScript till QtQml
- Kontrollera också byggkatalogen för po/

### KIconThemes

- Ställ in breeze som reservikontema

### KIO

- [KSambaShareData] Acceptera mellanslag i ACL värddatornamn
- [KFileItemListProperties] Använd mostLocalUrl för möjligheter
- [KMountPoint] Kontrollera också "smb-share" om det är en SMB-montering (fel 344146)
- [KMountPoint] Lös upp gvfsd-monteringar (fel 344146)
- [KMountPoint] Ta bort rester av supermontering
- [KMountPoint] Ta bort stöd för AIX och Windows CE
- Visa monterad filsystemtyp och monterad från fält i egenskapsdialogruta (fel 220976)
- kdirlistertest går inte fel vid random
- [KUrlComboBox] Rätta konverteringsfel för KIcon
- Konvertera KPATH_SEPARATOR "fix" till QDir::listSeparator, tillagd i Qt 5.6
- Rättar minnesläcka i KUrlComboBox::setUrl
- [KFileItem] Läs inte katalogkommentar för långsamma monteringar
- Använd QDir::canonicalPath istället
- Ignorera NTFS dold flagga för rotvolym (fel 392913)
- Ge dialogrutan "iogiltigt katalognamn" knappen Avbryt
- KPropertiesDialog: Byt till etikett i setFileNameReadOnly(true)
- Förbättra ordval när en katalog med ett ogiltigt namn inte kunde skapas
- Använd lämplig ikon för knappen Avbryt som frågar efter ett nytt namn
- Gör skrivskyddade filsystem valbara
- Använd stora inledande bokstäver för vissa knappetiketter
- Använd KLineEdit för katalognamn om katalogen har skrivåtkomst, använd annars QLabel
- KCookieJar: Rätta felaktig tidszonkonvertering

### Kirigami

- Stöd fillWidth för objekt
- Skydda mot extern borttagning av sidor
- Visa alltid rubrik när vi är i hopdraget läge
- Rätta beteende hos showContentWhenCollapsed
- Rätta hål i menyerna med stilen Material
- Standardåtgärdsmeny för sidan sammanhangsmeny
- Begär explicit QtQuick från Qt 5.7 för att dra nytta av Connections.enabled
- Använd fönsterfärg istället för ett bakgrundsobjekt
- Säkerställ att lådan också stängs när en ny skjuts
- Exportera separatorvisible till den globala verktygsraden
- Rätta generering av Kirigami QRC statiskt insticksprogram
- Rätta inbyggt LTO statiskt läge
- Säkerställ att egenskapen drawerOpen synkroniseras riktigt (fel 394867)
- drawer: Visa den grafiska innehållskomponenten vid dragning
- Tillåt att grc-tillgångar används i åtgärdsikoner
- ld för gammal gcc (fel 395156)

### KItemViews

- Avråd från användning av KFilterProxySearchLine

### KNewStuff

- Lagra providerId

### KNotification

- Stöd libcanberra för ljudunderrättelse

### KService

- KBuildSycoca: Behandla alltid programmens skrivbordsfiler

### KTextEditor

- Omvandla uppräkningstypen Kate::ScriptType till en uppräkningsklass
- Rätta felhantering för QFileDevice och KCompressedDevice
- InlineNotes: Skriv inte ut anteckningar på plats
- Ta bort QSaveFile till förmån för vanlig gammal spara fil
- InlineNotes: Använd skärmglobala koordinater överallt
- InlineNote: Initialisera position med Cursor::invalid()
- InlineNote: Pimpl anteckning på plats utan allokeringar
- Lägg till gränssnitt för anteckning på plats
- Visa bara textförhandsgranskning om huvudfönstret är aktivt (fel 392396)
- Rätta krasch när den grafiska komponenten TextPreview döljs (fel 397266)
- Sammanfoga ssh://git.kde.org/ktexteditor
- Förbättra återgivning av ikonkant för skärmar med hög upplösning
- Förbättra vim färgtema (fel 361127)
- Sök: Lägg till provisorisk lösning för saknade ikoner i Gnome ikontema
- Rätta överskrivning av _ eller bokstäver som j på sista raden (fel 390665)
- Utöka skript-programmeringsgränssnitt för att tillåta att kommandon körs
- Indenteringsskript för R
- Rätta krasch när n ersätts runt tomma rader (fel 381080)
- Ta bort färgläggning av nerladdningsdialogruta
- Inget behov att göra nytt eller ta bort hash vid varje doHighlight, att rensa det är bra nog
- Försäkra att vi kan hantera ogiltiga egenskapsindex som kan inträffa vid kvarblivna efter HL-byte för ett dokument
- Låt smart pekare hantera borttagning av objekt, mindre manuella saker att göra
- Ta bort avbildning för att slå upp ytterligare HL-egenskaper
- KTextEditor använder ramverket KSyntaxHighlighting för allt
- Använd teckenkodningar som definitionerna tillhandahåller
- Sammanfoga grenen 'master' med syntax-highlighting
- Text utan fetstil återges inte längre med teckenvikt tunn men (fel 393861)
- Använd foldingEnabled
- Ta bort EncodedCharaterInsertionPolicy
- Utskrift: Respektera sidfotens teckensnitt, rätta sidfotens vertikala position, gör skiljelinjen för sidhuvud och sidfot visuellt ljusare
- Sammanfoga grenen 'master' med syntax-highlighting
- Låt ramverket för syntaxfärgläggning hantera all definitionshantering nu när definitionen None finns i arkivet
- Kompletteringskomponent: Rätta minimal storlek för rubriksektion
- Rättning: Rulla visningsrader istället för riktiga rader för rullning med hjul och tryckplatta (fel 256561)
- Ta bort syntaxtest, det testas nu i själva ramverket för syntaxfärgläggning
- Inställning av KTextEditor är nu programlokal igen, den gamla globala inställningen importeras vid första användning
- Använd KSyntaxHighlighting::CommentPosition istället för KateHighlighting::CSLPos
- Använd isWordWrapDelimiter() från KSyntaxHighlighting
- Byt namn på isDelimiter() till isWordDelimiter()
- Implementera mer uppslagningssaker via länken format -&gt; definition
- Nu får vi alltid giltiga format
- Snyggare sätt att hämta egenskapsnamn
- Rätta indenteringstest för Python, säkrare åtkomst till egenskapspåsar
- Lägg till rätta definitionsprefix igen
- Sammanfoga grenen 'syntax-highlighting' från git://anongit.kde.org/ktexteditor med syntax-highlighting
- Försök att få tillbaka listor som behövs för att göra inställningen per schema
- Använd KSyntaxHighlighting::Definition::isDelimiter()
- make kan bryta bit mer som i ordkod
- Ingen länkad lista utan orsak
- Städa upp egenskapsinitiering
- Rätta ordning av format, kom ihåg definition i färgläggningspåse
- Hantera ogiltiga format och format med längden noll
- Ta bort fler gamla implementeringsdelar, rätta några åtkomster att använda formatsakerna
- Rätta indenteringsbaserad vikning
- Ta bort exponeringen av sammanhangsstacken i doHighlight + rätta ctxChanged
- Börja lagra vikningssaker
- Riv ut färgläggningshjälpare, behövs inte längre
- Ta bort behov av contextNum, lägg till markeringen FIXME-SYNTAX för saker som måste rättas på ett riktigt sätt
- Anpassa till ändringar i includedDefinitions, ta bort contextForLocation, man behöver antingen bara nyckelord för plats eller stavningskontroll för plats, kan implementeras senare
- Ta bort fler saker för syntaxfärgläggning som inte längre används
- Rätta till m_additionalData och avbildningen av det lite grand, ska fungera för egenskaper, inte för sammanhang
- Skapa initiala egenskaper, fortfarande utan riktiga egenskapsvärden, bara en lista över någonting
- Anropa färgläggningen
- Härled från abstrakt färgläggning, ange definition

### Ramverket KWallet

- Flytta exempel från teknikbasen till eget arkiv

### Kwayland

- Synkronisera metoder för inställning, skicka och uppdatera
- Lägg till serienummer och EISA-id till gränssnittet OutputDevice
- Korrektion av färgkurvor för utdataenhet
- Rätta minneshantering i WaylandOutputManagement
- Isolera alla tester inne i WaylandOutputManagement
- Bråkdelsskalning i OutputManagement

### KWidgetsAddons

- Skapa ett första exempel av användning av KMessageBox
- Rätta två fel i KMessageWidget
- [KMessageBox] Anropa stil för en ikon
- Lägg till provisorisk lösning för etiketter med radbrytning (fel 396450)

### KXMLGUI

- Gör så att Konqi ser bra ut på skärmar med hög upplösning
- Lägg till saknade parenteser

### NetworkManagerQt

- Kräv NetworkManager 1.4.0 eller senare
- manager: Lägg till stöd för att läsa och skriva egenskapen GlobalDnsConfiguration
- Tillåt verkligen att uppdateringshastighet ställs in för enhetsstatistik

### Plasma ramverk

- Provisorisk lösning för fel med inbyggd återgivning och ogenomskinlighet för text i TextField (fel 396813)
- [Icon Item] Bevaka ändring av KIconLoader ikon när QIcon används (fel 397109)
- [Icon Item] Använd ItemEnabledHasChanged
- Bli av med användning av QWeakPointer som avråds från
- Rätta stilmall för 22-22-system-suspend (fel 397441)
- Förbättra grafiska komponenters borttagnings- och anpassningstext

### Solid

- solid/udisks2: Lägg till stöd för kategoriserad loggning
- [Windows Device] Visa bara enhetsbeteckning om det finns någon
- Tvinga återutvärdering av predikat om gränssnitt tas bort (fel 394348)

### Sonnet

- hunspell: Återställ byggning med hunspell &lt;=v1.5.0
- Inkludera hunspell deklarationsfiler som systeminkluderingar

### Syndikering

Ny modul

### Syntaxfärgläggning

- Färglägg 20000 rader per testfall
- Gör färgläggningsmätningar mer reproducerbara, vi vill hur som helst mäta körningen med t.ex. perf från utsidan
- Finjustera uppslagning av KeywordList och undvik allokeringar för implicit fångstgrupp
- Ta bort fångst av Int, aldrig implementerad
- Deterministisk iteration av tester för bättre resultatjämförelse
- Hantera nästlade inkluderingsegenskaper
- Uppdatera Modula-2 färgläggning (fel 397801)
- Beräkna egenskapsformat för sammanhang och regler i förväg
- Undvik kontroll av ordavgränsare vid början på nyckelord (fel 397719)
- Lägg till syntaxfärgläggning för SELinux-kärnans policyspråk
- Dölj bestCandidate, kan vara statisk funktion inne i fil
- Lägg till några förbättringar i kate-syntax-highlighter för användning i skript
- Lägg till := som en giltig del av en identifierare
- Användn vår egen indata för prestandamätning
- Försök rätta problem med radslut vid jämförelse av resultat
- Försök med trivial jämförelseutmatning för Windows
- Lägg till defData igen för giltig tillståndskontroll
- Minska StateData-rymd med mer än 50 % och halva antalet nödvändiga minnesallokeringar
- Förbättra prestanda av Rule::isWordDelimiter och KeywordListRule::doMatch
- Förbättra hantering av hoppa över position, tillåt att hela raden hoppas över vid ingen träff
- Kontrollera lista över jokertecken för filändelser
- Fler asterisk-hl, jag försökte med några asteriskinställningar, de är bara ini-stil, använd .conf som ini-ändelse
- Rätta färgläggning för #ifdef _xxx (fel 397766)
- Rätta jokertecken i filer
- Ändring av licens till MIT för KSyntaxHighlighting utförd
- Javascript: Lägg till binärfiler, rätta oktala värden, förbättra undantag och tillåt identifierare som inte är ASCII (fel 393633)
- Tillåt att uppslagningar av QStandardPaths stängs av
- Tillåt att syntaxfiler installeras istället för att de finns i en resurs
- Hantera egenskaper för kontextbyten för själva kontexterna
- Byt från statiskt bibliotek till objektbibliotek med rätt pic-inställning, ska fungera för delad och statisk byggning
- Undvik all heap-allokering för standardkonstruerad Format() som används som "ogiltig"
- Ta hänsyn till cmake-variabel för statiskt eller dynamiskt bibliotek, som t.ex. karchive
- Ändring av licens till MIT, https://phabricator.kde.org/T9455
- Ta bort gammalt skript add_license, behövs inte längre
- Rätta inkluderingsdefinitioner, hantera definitionsändring i kontextbyte (fel 397659)
- SQL: Diverse förbättringar och rättning av detektering av if/case/loop/end med SQL (Oracle)
- Rätta referensfiler
- SCSS: Uppdatera syntax. CSS: Rätta färgläggning av Operator och Selector Tag
- debchangelog: Lägg till Bookworm
- Ändring av Dockerfile licens till MIT-licens
- Ta bort inställningsdel som inte längre stöds av stavningskontroll som alltid bara hade ett läge som vi nu hårdkodar
- Lägg till stöd för syntaxfärgläggning av Stan
- Lägg till bakåtindentering
- Optimera många syntaxfärgläggningsfiler och rätta tecknet '/' i SQL
- Modelines: Lägg till  byte-order-mark och mindre rättningar
- Ändring av modelines.xml licens till MIT-licens (fel 198540)
- Lägg till QVector&lt;QPair&lt;QChar, QString&gt;&gt; Definition::characterEncodings() const
- Lägg till bool Definition::foldingEnabled() const
- Lägg till färgläggning "None" i arkivet som förval
- Uppdatera syntaxstöd för språket Logtalk
- Lägg till Autodesk EAGLE sch och brd filformat till XML-kategorin
- C# färgläggning: Föredra indentering med C-stil
- AppArmor: Uppdatera syntax och diverse förbättringar och rättningar
- Java: Lägg till binärfiler och hex-float, och stöd understreck i tal (fel 386391)
- Upprensning: Indentering flyttades från allmänna till språksektionen
- Definition: Exponera kommandomarkörer
- Lägg till färgläggning av Javascript React
- YAML: Rätta nycklar, lägg till tal och andra förbättringar (fel 389636)
- Lägg till bool Definition::isWordWrapDelimiter(QChar)
- Definition: Byt namn på isDelimiter() till isWordDelimiter()
- Notera idéer om förbättringar av KF6-programmeringsgränssnitt från konvertering av KTE
- Tillhandahåll också ett giltigt format för tomma rader
- Make-definition::isDelimiter() fungerar också för ogiltiga definitioner
- Definition: Exponera bool isDelimiter() const
- Sortera returnerade format i Definition::formats() enligt id

### Säkerhetsinformation

Den utgivna koden GPG-signerad genom att använda följande nyckel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primärt nyckelfingeravtryck: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Det går att diskutera och dela med sig av idéer om den här utgåvan via kommentarssektionen i <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artikeln på Dot</a>.
