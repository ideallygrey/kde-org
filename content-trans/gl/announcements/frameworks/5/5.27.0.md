---
aliases:
- ../../kde-frameworks-5.27.0
date: 2016-10-08
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
New mimetypes icons.

{{<figure src="/announcements/frameworks/5/5.27.0/kf-5.27-mimetypes-icons.png" >}}

### Baloo

- Usar a entrada correcta de configuración na condición de inicio automático
- Corrixir a inserción ordenada (tamén chamada inserción estilo flat_map) (fallo 367991)
- Engadir un «env» de peche que faltaba, como indicou Loïc Yhuel (fallo 353783)
- Cando non se cree unha transacción non intentar interrompelas
- corrixir unha asignación m_env = nullptr que faltaba
- Facer compatíbeis con fíos cousas como Baloo::Query
- En sistemas de 64 bits agora Baloo permite un almacenamento de índice de máis de 5 GB (fallo 364475)
- Permitir ctime/mtime == 0 (fallo 355238)
- Xestionar a corrupción da base de datos de índice de baloo_file, intentar crear de novo a base de datos ou interromper se iso falla

### BluezQt

- Corrixir a quebra ao intentar engadir un dispositivo a un adaptador descoñecido (fallo 364416)

### Iconas de Breeze

- Novas iconas de tipos MIME
- Actualizar algunhas iconas de KStars (fallo 364981)
- actions/24/format-border-set de estilo incorrecto (fallo 368980)
- Engadir a icona de aplicación en wayland
- Engadir unha icona de aplicación para xorg (fallo 368813)
- Reverter distribute-randomize e view-calendar e aplicar de novo a corrección de transformación (fallo 367082)
- Cambiar folder-documents de un ficheiro a varios porque nun cartafol inclúese máis dun ficheiro (fallo 368224)

### Módulos adicionais de CMake

- Asegurarse de que non engadimos a proba de AppStream dúas veces

### KActivities

- Ordenar as actividades alfabeticamente por nome na caché (fallo 362774)

### Ferramentas de Doxygen de KDE

- Moitos cambios na disposición xeral da documentación xerada da API
- Ruta de etiquetas correcta dependendo de se a biblioteca forma parte dun grupo ou non
- Busca: corrixir o href de bibliotecas que non forman parte dun grupo

### KArchive

- Corrixir unha fuga de memoria co KCompressionDevice de KTar
- KArchive: corrixir unha fuga de memoria cando xa existe unha entrada co mesmo nome
- Corrixir unha fuga de memoria en KZip ao xestionar directorios baleiros
- K7Zip: Corrixir fugas de memoria cando se producen erros
- Corrixir unha fuga de memoria detectada por ASAN cando open() falla no dispositivo correspondente
- Retirar unha reinterpretación como KFilterDev, detectada por ASAN

### KCodecs

- Engadir macros de exportación que faltaban nas clases Decoder e Encoder

### KConfig

- Corrixir unha fuga de memoria en SignalsTestNoSingletonDpointer, atopada por ASAN

### KCoreAddons

- Rexistrar QPair&lt;QString,QString&gt; como metatipo en KJobTrackerInterface
- Non converter como URL un URL que ten comiñas duplas
- Corrección de compilación de Windows
- Corrixir un fallo moi vello ao retirar espazo dun URL como «foo &lt;&lt;url&gt; &lt;url&gt;&gt;»

### KCrash

- Opción KCRASH_CORE_PATTERN_RAISE de CMake para pasar ao núcleo
- Cambiar o nivel de rexistro predeterminado de aviso a información

### Compatibilidade coa versión 4 de KDELibs

- Limpeza. Non instalar inclusións que apuntan a inclusións inexistentes e ademais retirar eses ficheiros
- Usar std::remove_pointer, que é máis correcto e está dispoñíbel con c++11

### KDocTools

- Corrixir «checkXML5 imprime o HTML xerado á saída estándar para docbooks válidos» (fallo 369415)
- Corrixir o fallo de non poder executar as ferramentas nativas do paquete ao usar un kdostools froito de compilación cruzada
- Configurar os destinos de compilación cruzada executando kdoctools desde outros paquetes
- Engadir a posibilidade de compilación cruzada de docbookl10nhelper
- Engadir a posibilidade de compilación cruzada de meinproc5
- Converter checkxml5 nun executábel del Qt por compatibilidade con varias plataformas

### KFileMetaData

- Mellorar o extractor de epub, menos fallos de segmento (fallo 361727)
- Facer que o indexador de ODF sexa máis resistente a erros, comprobar se os ficheiros existen (e son ficheiros) (meta.xml + content.xml)

### KIO

- Corrixir que os escravos de KIO só usasen tls1.0
- Corrixir unha rotura da ABI en KIO
- KFileItemActions: engadir addPluginActionsTo(QMenu *)
- Mostrar os botóns de copia só tras calcular a suma de comprobación
- Engadir información que faltaba ao calcular unha suma de comprobación (fallo 368520)
- Corrixir que KFileItem::overlays envíe valores de cadea baleira
- Corrixir o inicio de ficheiros .desktop de terminal con Konsole
- Clasificar as montaxes de nfs4 como probablySlow (probabelmente lenta), como nfs, cifs, etc.
- KNewFileMenu: mostrar o atallo da acción de «Novo cartafol» (fallo 366075)

### KItemViews

- No modo de vista de lista, usar a lóxica predeterminada de moveCursor

### KNewStuff

- Engadir comprobacións de KAuthorized para permitir desactivar ghns en kdeglobals (fallo 368240)

### Infraestrutura de paquetes

- Non xerar ficheiros de AppStream para compoñentes que non están en rdn
- Facer que kpackage_install_package funcione con KDE_INSTALL_DIRS_NO_DEPRECATED
- Retirar a variábel non usada KPACKAGE_DATA_INSTALL_DIR

### KParts

- Corrixir que se asuma que os URL que rematan en barra inclinada son directorios

### KPeople

- Corrixir a construción de ASAN (duplicates.cpp usa KPeople::AbstractContact que está en KF5PeopleBackend)

### KPty

- Usar a tira de ECM para atopar o binario utempter, máis fiábel que un simple prefixo de CMake
- Chamar o executábel de asistente de utempter manualmente (fallo 364779)

### KTextEditor

- Ficheiros XML: Retirar os valores de cor definidos a man
- XML: Retirar os valores de cor definidos a man
- Definición de esquema de XML: Converter «version» nun xs:integer
- Ficheiros de definición de realce: redondear a versión cara arriba, ao seguinte enteiro
- só permitir capturas de múltiples caracteres en {xxx} para evitar regresións
- Support regular expressions replaces with captures &gt; 9, e.g. 111 (bug 365124)
- Corrixir a renderización de caracteres que acadan a seguinte liña, por exemplo os subliñados xa non se cortan con algunhas fontes e tamaños de letra (fallo 335079)
- Corrixir unha quebra: asegurarse de que o cursor da pantalla é válido tras pregar texto (fallo 367466)
- KateNormalInputMode necesita executar de novo os métodos de entrada de SearchBar
- intentar «aquelar» a renderización de subliñado e cousas parecidas (fallo 335079)
- Só mostrar o botón «Ver a diferenza» se «diff» está instalado
- Usar un trebello de mensaxe non modal para notificacións de ficheiros modificados externamente (fallo 353712)
- corrixir unha regresión: testNormal só funcionou por executar a proba dunha vez
- dividir a proba de sangrado en execucións separadas
- Permitir de novo a acción «Despregar os nodos raíz» (fallo 335590)
- Corrixir a quebra ao mostrar mensaxes superiores ou inferiores varias veces
- corrixir a opción de fin de liña nas liñas de modo (fallo 365705)
- realzar ficheiros .nix como Bash, supoño que mal non fai (fallo 365006)

### Infraestrutura de KWallet

- Comprobar se KWallet está activado en Wallet::isOpen(name) (fallo 358260)
- Engadir unha cabeceira de Boost que faltaba
- Retirar a busca duplicada de KF5DocTools

### KWayland

- [servidor] Non enviar cando se deixe de premer a tecla no caso de teclas non premidas ou non hai presión dupla de teclas (fallo 366625)
- [servidor] Ao substituír a selección do portapapeis hai que cancelar a fonte de datos anterior (fallo 368391)
- Engadir compatibilidade con eventos de entrar e saír de Surface
- [cliente] Facer un seguimento de todas as Output creadas e engadir o método estático get

### KXmlRpcClient

- Converter as categorías en org.kde.pim.*

### NetworkManagerQt

- Necesitamos definir o estado durante a inicialización
- Substituír todas as chamadas bloqueantes de inicialización cunha única chamada bloqueante
- Usar a interface estándar o.f.DBus.Properties para o sinal PropertiesChanged de NM ≥ 1.4.0 (fallo 367938)

### Iconas de Oxygen

- Retirar un directorio incorrecto de index.theme
- Introducir a proba de duplicados de breeze-icons
- Converter todas as iconas duplicadas en ligazóns simbólicas

### Infraestrutura de Plasma

- Mellorar a saída do cronómetro
- [ToolButtonStyle] Corrixir a frecha do menú
- i18n: xestionar cadeas en ficheiros kdevtemplate
- i18n: revisar cadeas en ficheiros kdevtemplate
- Engadir removeMenuItem a PlasmaComponents.ContextMenu
- actualizar a icona de KTorrent (fallo 369302)
- [WindowThumbnail] Descartar mapas de píxeles en eventos de mapa
- Non incluír kdeglobals ante unha configuración de caché
- Corrixir Plasma::knownLanguages
- cambiar o tamaño da vista tras definir o contedor
- Evitar crear un KPluginInfo dunha instancia de KPluginMetaData
- as tarefas en execución deben ter algunha indicación
- as liñas de barra de tarefas segundo marco en RR 128802 déixano listo para publicar
- [AppletQuickItem] Saír do buque tras atopar unha disposición

### Información de seguranza

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Pode comentar e compartir ideas sobre esta versión na sección de comentarios do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
