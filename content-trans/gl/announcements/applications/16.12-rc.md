---
aliases:
- ../announce-applications-16.12-rc
date: 2016-12-02
description: KDE Ships Applications 16.12 Release Candidate.
layout: application
release: applications-16.11.90
title: KDE publica a candidata a versión final da versión 16.12 das aplicacións de
  KDE
---
2 de decembro de 2016. Hoxe KDE publicou a candidata a versión final da nova versión das súas aplicacións. Coa desautorización temporal de dependencias e funcionalidades novas, agora o equipo de KDE centrase en solucionar fallos e pulir funcionalidades.

Check the <a href='https://community.kde.org/Applications/16.12_Release_Notes'>community release notes</a> for information on new tarballs, tarballs that are now KF5 based and known issues. A more complete announcement will be available for the final release

Hai que probar ben a versión 16.12 das aplicacións de KDE para manter e mellorar a calidade e a experiencia de usuario. Os usuarios reais son críticos para manter unha alta calidade en KDE, porque os desenvolvedores simplemente non poden probar todas as configuracións posíbeis. Contamos con vostede para axudarnos a atopar calquera fallo canto antes para poder solucionalo antes da versión final. Considere unirse ao equipo instalando a candidata a versión final <a href='https://bugs.kde.org/'>e informando de calquera fallo</a>.
