---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE stelt KDE Applicaties 18.12.1 beschikbaar.
layout: application
major_version: '18.12'
title: KDE stelt KDE Applicaties 18.12.1 beschikbaar
version: 18.12.1
---
{{% i18n_date %}}

Vandaag heeft KDE de eerste stabiele update vrijgegeven voor <a href='../18.12.0'>KDE Applicaties 18.12</a> Deze uitgave bevat alleen reparaties van bugs en bijgewerkte vertalingen, die een veilige en plezierige update voor iedereen levert.

Ongeveer 20 aangegeven reparaties van bugs, die verbeteringen bevatten aan Kontact, Cantor, Dolphin, Juk, Kdenlive, Konsole, Okular, naast andere.

Verbeteringen bevatten:

- Akregator werkt nu met WebEngine uit Qt 5.11 of nieuwer
- Kolommen sorteren in de muziekspeler JuK is gerepareerd
- Konsole geeft tekens in vakjes opnieuw juist weer
