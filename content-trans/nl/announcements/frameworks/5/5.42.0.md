---
aliases:
- ../../kde-frameworks-5.42.0
date: 2018-01-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Algemene wijzigingen

- Reparaties voor cmake 3.10+ AUTOMOC waarschuwingen
- Meer breed gebruik van gecategoriseerde logging, om debuguitvoer standaard uit te zetten (gebruik kdebugsetting om het opnieuw in te schakelen)

### Baloo

- balooctl-status: proces alle argumenten
- Meerdere woordtag queries repareren
- Voorwaarden voor hernoemen vereenvoudigen
- Onjuiste UDSEntry schermnaam repareren

### Breeze pictogrammen

- Pictogramnaam "geen-weer" -&gt; "weer-niet-beschikbaar" (bug 379875)
- Vivaldi pictogram verwijderen omdat het oorspronkelijke app-pictogram perfect past in breeze (bug 383517)
- Enige ontbrekende MIME's toevoegen
- Pictogram Document-verzonden in Breeze pictogrammen toegevoegd (bug 388048)
- pictogram albumartiest bijwerken
- labplot-editlayout ondersteuning toevoegen
- duplicaten verwijderen en donker thema bijwerken
- ondersteuning voor gnumeric breeze-pictogrammen toegevoegd

### Extra CMake-modules

- readelf gebruiken om projectafhankelijkheden te vinden
- Introductie van INSTALL_PREFIX_SCRIPT om gemakkelijk prefixes op te zetten

### KActivities

- crash in kactivities vermijden als er geen dbus-connectie beschikbaar is

### KConfig

- API-documenten: leg uit hoe KWindowConfig te gebruiken vanuit een dialoogconstructeur
- Markeer KDesktopFile::sortOrder() als verouderd
- Het resultaat van KDesktopFile::sortOrder() repareren

### KCoreAddons

- Breid CMAKE_AUTOMOC_MACRO_NAMES uit ook voor eigen bouwsels
- Laat licentiesleutels overeenkomen door spdx

### KDBusAddons

- Detectie van absoluut pad repareren voor cmake 3.5 op Linux
- cmake-functie toevoegen aan 'kdbusaddons_generate_dbus_service_file'

### KDeclarative

- Qml-besturing voor aanmaken van kcm

### KDED

- cmake-functie gebruiken in 'kdbusaddons_generate_dbus_service_file' uit kdbusaddons om dbus-service-bestand te genereren
- Gebruikte eigenschap toevoegen aan servicebestandsdefinitie

### Ondersteuning van KDELibs 4

- Informeer de gebruiker als de module niet geregistreerd kan worden met org.kde.kded5 en beëindig met een fout
- Reparatie Mingw32 compileren

### KDocTools

- entity voor Michael Pyne toevoegen
- entities voor Martin Koller aan contributor.entities toevoegen
- debian-entry repareren
- entity Debian aan general.entities toevoegen
- entity kbackup toevoegen, het was geïmporteerd
- entity latex toevoegen, we hebben al 7 entities in verschillende index.docbooks in kf5

### KEmoticons

- Schema (file://) toevoegen. Het is nodig wanneer we het gebruiken in qml en we alles toevoegen

### KFileMetaData

- extractor gebaseerd op QtMultimedia verwijderen
- Controleer op Linux in plaats van TagLib en vermijd het bouwen van de usermetadatawritertest op Windows
- Herstel # 6c9111a9 totdat het mogelijk is met succes te bouwen en te koppelen zonder TagLib
- Afhankelijkheid van taglib verwijderen, veroorzaakt door een achtergelaten include

### KHTML

- Sta eindelijk uitschakelen van debuguitvoer toe door gecategoriseerde logging te gebruiken

### KI18n

- behandel ts-pmap-compile niet als uitvoerbaar
- Geheugenlek in KuitStaticData repareren
- KI18n: een aantal dubbel opzoeken repareren

### KInit

- Onmogelijk bereikbare code verwijderen

### KIO

- Datums in cookies op de juiste manier ontleden bij werken in een niet-Engelse taalcode (bug 387254)
- [kcoredirlister] aanmaken subpad repareren
- Status van prullenbak in iconNameForUrl weergeven
- Signalen van QComboBox doorsturen in plaats van signalen van regelbewerking van QComboBox 
- Websneltoetsen gerepareerd die hun bestandspad tonen in plaats van hun door de mens te lezen naam
- TransferJob: reparatie voor get geval de readChannelFinished al is uitgestuurd voordat start is called() (bug 386246)
- Crash repareren, waarschijnlijk sinds Qt 5.10? (bug 386364)
- KUriFilter: geef geen fout terug bij niet-bestaande bestanden
- Aanmaken van paden repareren
- Een kfile-dialoog implementeren waaraan we aangepaste widgets toe kunnen voegen
- Ga na dat qWaitForWindowActive niet mislukt
- KUriFilter: overzetten weg van KServiceTypeTrader
- API dox: klassenamen in titels van voorbeeldschermafdrukken gebruiken
- API dox: ook rekening houden met KIOWIDGETS-exportmacro's in aanmaken van QCH
- behandeling van KCookieAdvice::AcceptForSession repareren (bug 386325)
- 'GroupHiddenRole' voor KPlacesModel aangemaakt
- socketfouttekenreeks doorgeven aan KTcpSocket
- Herschrijf en verwijder gedupliceerde code in kfileplacesview
- Stuur 'groupHiddenChanged'-signaal uit
- Herschrijven van de verborgen/getoonde animatiegebruik binnen KFilePlacesView
- De gebruiker kan nu een gehele groep plaatsen uit KFilePlacesView verbergen (bug 300247)
- Groepen plaatsen verbergen geïmplementeerd in KFilePlacesModel (bug 300247)
- [KOpenWithDialog] redundante aanmaak van KLineEdit verwijderen
- Ondersteuning voor ongedaan maken toevoegen aan BatchRenameJob
- BatchRenameJob toevoegen aan KIO
- doxygen codeblock die niet eindigt met endcode repareren

### Kirigami

- de flickable interactive behouden
- evenals juiste prefix voor pictogrammen
- afmetingen van formulieren van grootte wijzigen repareren
- wheelScrollLines lezen uit kdeglobals indien deze bestaat
- een prefix voor kirigami bestanden toevoegen om conflicten te vermijden
- enige statische linking reparaties
- plasma stijlen verplaatsen naar plasma-framework
- Enkele quotes gebruiken voor tekens overeen laten komen + QLatin1Char
- Formulierindeling

### KJobWidgets

- QWindow API voor KJobWidgets:: decoratiemakers beiden

### KNewStuff

- Verzoekcachegrootte beperken
- Dezelfde interne versie vereisen als u aan het bouwen bent
- Globale variabelen beschermen om gebruikt te worden na vrijgeven

### KNotification

- [KStatusNotifierItem] widgetpositie niet herstellen ("restore") bij zijn eerste tonen
- Posities van oudere systeemvakpictogrammen gebruiken voor minimaliseer/herstelacties
- Behandel posities van LMB-kliks op oudere systeemvakpictogrammen
- maak van het contextmenu geen venster
- Toelichtend commentaar toevoegen
- Lazy-instanciate en lazy-load KNotification plug-ins

### KPackage-framework

- de runtimecache bij installatie ongeldig maken
- experimentele ondersteuning voor rcc-bestanden laden in kpackage
- compileren tegen Qt 5.7
- Indexering van pakketten opschonen en runtime-caching toevoegen
- nieuwe methode KPackage::fileUrl()

### KRunner

- [RunnerManager] niet rommelen met ThreadWeaver thread-count

### KTextEditor

- Jokerteken overeenkomsten voor mode-regels repareren
- Een regressie veroorzaakt door wijzigen van gedrag van backspace-toets
- overzetten naar niet verouderde API zoals al gebruikt op andere plaatsen (bug 386823)
- Ontbrekende include voor std::array toegevoegd
- MessageInterface: CenterInView als extra positie toevoegen
- QStringList initializer list opschonen

### KWallet Framework

- Juiste service-executable-pad gebruiken voor installeren van kwalletd-dbus-service onder Win32

### KWayland

- Inconsistentie in naamgeving repareren
- Interface aanmaken voor het doorgeven van serverdecoratiepalettes
- Doe expliciet include std::bind functies
- [server] een methode IdleInterface::simulateUserActivity toevoegen
- Regressie veroorzaakt door achterwaartse ondersteuning van compatibiliteit in gegevensbron
- Ondersteuning voor versie 3 van beheerdersinterface voor gegevensapparaten (bug 386993)
- Koppel geëxporteerde/geïmporteerde objecten aan de test
- Fout in WaylandSurface::testDisconnect repareren
- Expliciet AppMenu-protocol toevoegen
- Uitsluiten van gegenereerd bestand uit automoc-feature repareren

### KWidgetsAddons

- Crash in setMainWindow op wayland repareren

### KXMLGUI

- API dox: laat doxygen sessie gerelateerde macro's &amp; functies opnieuw dekken
- Sneltoetsbewerkingsslot op vernietiging van widget ontbinden (bug 387307)

### NetworkManagerQt

- 802-11-x: ondersteuning voor PWD EAP methode

### Plasma Framework

- [Air thema] voortgangsgrafiek aan taakbalk toevoegen (bug 368215)
- Sjablonen: * instray uit licentiekoppen verwijderen
- packageurlinterceptor als noop als mogelijk maken
- Draai "Renderer niet neerhalen en ander werk dat bezig is wanneer Svg::setImagePath wordt aangeroepen met hetzelfde argument"
- kirigami plasma-stijlen naar hier verplaatsen
- Verdwijnende schuifbalken op mobiel
- KPackage exemplaar tussen PluginLoader en Applet opnieuw gebruiken
- [AppletQuickItem] Stel QtQuick Controls 1 stijl eenmalig in per engine
- Geen vensterpictogram instellen in Plasma::Dialog
- [RTL] - de geselecteerde tekst voor RnL juist uitlijnen (bug 387415)
- Schaalfactor initialiseren naar de laatst ingestelde schaalfactor op elk exemplaar
- Draai "Schaalfactor initialiseren naar de laatst ingestelde schaalfactor op elk exemplaar" terug
- Niet bijwerken wanneer de onderliggende FrameSvg is geblokkeerd voor opnieuw tekenen
- Schaalfactor initialiseren naar de laatst ingestelde schaalfactor op elk exemplaar
- Verplaats als controle is binnen #ifdef
- [FrameSvgItem] geen onnodige nodes aanmaken
- Renderer niet neerhalen en ander werk dat bezig is wanneer Svg::setImagePath wordt aangeroepen met hetzelfde argument

### Prison

- Zoek ook naar qrencode met debug achtervoegsel

### QQC2StyleBridge

- vereenvoudig en probeer geen muisgebeurtenis te blokkeren
- indien geen wheel.pixelDelta, globale wielschuiflijnen gebruiken
- tabbalken op bureaublad hebben verschillende breedtes voor elk tabblad
- zorg voor een hint voor grootte die niet-nul is

### Sonnet

- Exporteer geen interne uitvoerbare programma's voor hulp 
- Sonnet: verkeerde taal voor suggesties in gemengde taal teksten repareren
- Oude en gebroken workaround verwijderen
- Veroorzaak geen circulaire koppeling op Windows

### Accentuering van syntaxis

- Indexeringsaccentueerder: waarschuwing over contextswitch fallthroughContext="#stay"
- Indexeringsaccentueerder: waarschuwen bij lege attributen
- Accentueringsindexeerder: fouten inschakelen
- Accentueringsindexeerder: rapporteer ongebruikte itemDatas en ontbrekende itemDatas
- Prolog, RelaxNG, RMarkDown: Accenteringsproblemen repareren
- Haml: ongeldige en ongebruikte itemDatas repareren
- ObjectiveC++: Dubbele commentaarcontexten verwijderen
- Diff, ObjectiveC++: opschoningen en accentueringsbestanden repareren
- XML (Debug): onjuiste DetectChar regel repareren
- Accentueringsindexeerder: cross-hl controle op context ondersteunen
- Terugdraaien: GNUMacros naar gcc.xml opnieuw toevoegen, gebruikt door isocpp.xml
- email.xml: *.email aan de extensies toevoegen
- Accentueringsindexeerder: controleer op oneindige lussen
- Indexeringsaccentueerder: controleer op lege contextnamen en reguliere expressies
- Referentie naar niet-bestaande lijsten met sleutelwoorden repareren
- Eenvoudige gevallen van gedupliceerde contexten repareren
- Gedupliceerde itemDatas repareren
- DetectChar en Detect2Chars regels repareren
- Accentueringsindexeerder: controleer lijsten met sleutelwoorden
- Indexeringsaccentueerder: waarschuwen bij gedupliceerde context
- Accentueringsindexeerder: controleer op duplicaten van itemDatas
- Accentueringsindexeerder: controleer DetectChar en Detect2Chars
- Valideer dat voor alle attributen een itemData bestaat

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

U kunt discussiëren en ideeën delen over deze uitgave in de section voor commentaar van <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>het artikel in the dot</a>.
