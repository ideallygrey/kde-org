---
aliases:
- ../../kde-frameworks-5.68.0
date: 2020-03-07
layout: framework
libCount: 70
---
### Baloo

- [UnindexedFileIndexer] tijdcontroles voor nieuwe bestanden corrigeren
- [ModifiedFileIndexer] uitvoeren van BasicIndexingJob niet doen indien niet vereist
- IndexerConfig synchroniseren bij beëindigen (bug 417127)
- [FileIndexScheduler] evaluatie van indexerState na onderbreken/hervatten afdwingen

### BluezQt

- Fouten repareren in de commit van QRegularExpression

### Breeze pictogrammen

- Netwerk-draadloos-hotspot-pictogram toevoegen
- Telegrampaneelpictogrammen verplaatsen naar categorie status
- [breeze-icons] pictogrammen in systeemvak van telegram-bureaublad toevoegen (bug 417583)
- [breeze-icons] nieuw 48px telegram-pictogram
- Rss-pictogrammen in actie toevoegen
- 48px telegrampictogrammen verwijderen
- Snelle reparatie om er zeker van te zijn dat validatie niet gedaan wordt parallel aan generatie
- Nieuw yakuake-logo/pictogram
- Inconsistenties en duplicaten repareren in network-bedraad/draadloos-pictogrammen
- Oude tekstkleurwaarden voor osd-* pictogrammen repareren
- Alleen geïnstalleerde gegenereerde pictogrammen installeren als ze gegenereerd waren
- Alle paden van escape voorzien om zeker te zijn dat het CI-systeem werkt
- -e op het generatorscript zetten zodat het op de juiste manier fouten opvangt
- build: het bouwen repareren waar installatie-prefix niet door de gebruiker is te schrijven
- snelle reparatie van nieuwe 24px-generator om bash te gebruiken in plaats van sh
- Ook symbolische koppelingen voor compatibiliteit van 24@2x automatisch genereren
- 24px monochrome pictogrammen automatisch genereren
- Pictogrammen toevoegen die alleen in actions/24 naar actions/22 waren
- Documentschaal instellen naar 1.0 voor alle actions/22 pictogrammen
- Nieuw <code>smiley-add</code> pictogrammen toevoegen
- Shapes en shape-kies pictogrammen consistent maken met andere -shape pictogrammen
- Smiley-shape consistent maken met andere -shape pictogrammen
- Bloem-shape en hexagon-shape pictogrammen consistent maken met andere -shape pictogrammen
- &lt;use/&gt; vervangen door &lt;path/&gt; in muondiscover.svg
- Statuspictogrammen toevoegen: gegevensfout, gegevenswaarschuwing, gegevensinformatie
- Pictogram voor org.kde.Ikona toevoegen
- vvave-pictogram toevoegen
- puremaps pictogram toevoegen
- Het uiterlijk van alle pictogrammen die 🚫 bevatten gelijk trekken (geen teken)
- Nieuw pictogram voor KTimeTracker (bug 410708)
- Pictogrammen voor KTrip en KDE Itinerary optimaliseren
- Reis-familie pictogrammen bijwerken

### Extra CMake-modules

- NDK r20 en Qt 5.14 ondersteunen
- QM-bestanden laden uit bezittingen: URL's op Android
- ecm_qt_install_logging_categories &amp; ecm_qt_export_logging_category toevoegen
- ECMGeneratePriFile: gebroken opheffen voor gebruik met LIB_NAME geen doelnaam
- ECMGeneratePriFile: statische configuraties repareren

### Frameworkintegratie

- [KStyle] de kleur instellen van KMessageWidgets naar de juiste uit het huidige kleurenschema

### KActivities

- Probleem repareren bij vinden van de include-mappen van Boost
- Blootgestelde DBus-methoden gebruiken om activiteiten in CLI om te schakelen

### KAuth

- [KAuth] ondersteuning toevoegen voor actiedetails in Polkit1-backend
- [policy-gen] de code repareren om de juiste vanggroep te gebruiken
- Policykit-backend laten vallen
- [polkit-1] opzoeken van Polkit1Backend-actie bestaat vereenvoudigen
- [polkit-1een foutstatus teruggeven in actionStatus als er een fout is
- KAuthAction::isValid op aanvraag berekenen

### KBookmarks

- Acties hernoemen om ze consistent te maken

### KCalendarCore

- Zichtbaarheidscache bijwerken wanneer zichtbaarheid van notebook is gewijzigd

### KCMUtils

- activeModule controleren alvorens deze te gebruiken (bug 417396)

### KConfig

- [KConfigGui] styleName-lettertype-eigenschap wissen voor Regular lettertypenstijlen (bug 378523)
- Codegeneratie repareren voor items met min/max (bug 418146)
- KConfigSkeletonItem : instellen van een KconfigGroup toestaan om items in geneste groepen te lezen en te schrijven
- is&lt;PropertyName&gt;Immutable gegenereerde eigenschap repareren
- setNotifyFunction toevoegen aan KPropertySkeletonItem
- Een is&lt;PropertyName&gt;Immutable toevoegen aan weten als een eigenschap niet is te wijzigen

### KConfigWidgets

- "Redisplay" in "Refresh" wijzigen

### KCoreAddons

- tip toevoegen dat QIcon gebruikt kan worden als een programmalogo

### KDBusAddons

- KDBusConnectionPool afkeuren

### KDeclarative

- Vangstsignaal blootstellen aan KeySequenceItem
- Grootte van de header in GridViewKCM repareren (bug 417347)
- Afgeleide klasse van ManagedConfigModule toestaan om expliciet KCoreConfigSkeleton te registreren
- Gebruik van KPropertySkeletonItem toestaan in ManagedConfigModule

### KDED

- Een optie --replace aan kded5 toevoegen

### KDE GUI-addons

- [UrlHandler] openen van de online documenten voor KCM modulen behandelen
- [KColorUtils] tintenreeks van getHcy() wijzigen naar [0.0, 1.0)

### KHolidays

- Japanse vakantiedagen bijgewerkt
- holiday_jp_ja - spelling voor National Foundation Day repareren (bug 417498)

### KI18n

- Qt 5.14 op Android ondersteunen

### KInit

- kwrapper/kshell klauncher5 laten afsplitsen indien nodig

### KIO

- [KFileFilterCombo] ongeldig QMimeType niet toevoegen aan MIME-filter (bug 417355)
- [src/kcms/*] foreach (afgekeurd) vervangen door reeks/index gebaseerd for
- KIO::iconNameForUrl(): het geval van een bestand/map onder trash:/ behandelen
- [krun] implementatie van runService en runApplication delen
- [krun] ondersteuning van KToolInvocation uit KRun::runService laten vallen
- KDirModel verbeteren om tonen van '+' te vermijden als er geen submappen zijn
- Konsole uitvoeren op Wayland repareren (bug 408497)
- KIO::iconNameForUrl: zoeken naar kde-protocolpictogrammen repareren (bug 417069)
- Hoofdletters maken voor "basic link"-item corrigeren
- "AutoSkip" wijzigen naar "Skip All" (bug 416964)
- Geheugenlek repareren in KUrlNavigatorPlacesSelector::updateMenu
- bestand ioslave: kopiëren stoppen zodra de ioslave is afgebroken
- [KOpenWithDialog] het resultaat automatisch selecteren als het modelfilter slechts één overeenkomst heeft (bug 400725)

### Kirigami

- tekstballon tonen met volledige URL voor URL-knop met overschreven tekst
- Terugtrekwerbalken op pagina's met schuifbalken ook voor voettekst laten verschijnen
- Gedrag van PrivateActionToolButton met showText vs IconOnly repareren
- ActionToolBar/PrivateActionToolButton in combinatie met QQC2-actie repareren
- Geactiveerd menu-item altijd in reeks plaatsen
- Bewaak gebeurtenis taalwijziging en stuur die naar the QML-engine
- Qt 5.14 op Android ondersteunen
- overlay-vellen niet onder paginakop hebben
- terugval gebruiken wanneer laden van pictogram mislukt
- Ontbrekende koppelingen naar paginapool van broncodebestanden
- Pictogram: weergeven van afbeelding: url's op hoge DPI (bug 417647)
- Niet crashen wanneer breedte of hoogte van pictogram 0 is (bug 417844)
- Marges in OverlaySheet repareren
- [examples/simplechatapp] isMenu altijd op true instellen
- [RFC] grootte van Level 1 headings verminderen en linker opvulling op paginatitels vergroten
- tips over grootte op de juiste manier synchroniseren met statusmachine (bug 417351)
- Ondersteuning voor statische platformthemaplug-ins toevoegen
- headerParent juist uitlijnen wanneer er een schuifbalk is
- Berekenen van tabbladbalkbreedte repareren
- PagePoolAction aan QRC-bestand toevoegen
- werkbalkstijl toestaan op mobiel
- De api-documenten zo maken dat Kirigami niet alleen een toolkit voor een mobiel is

### KItemModels

- KRearrangeColumnsProxyModel: toekennen tijdelijk uitschakelen vanwege bug in QTreeView
- KRearrangeColumnsProxyModel: in setSourceColumns() resetten
- Plasma's SortFilterProxyModel verplaatsen in QML-plug-in van KItemModel

### KJS

- De functies voor evaluatie van timeoutbeheer in publieke API exposeren

### KNewStuff

- Klikken op thumb-only afgevaardigde repareren (bug 418368)
- Schuiven op de EntryDetails pagina repareren (bug 418191)
- CommentsModel niet dubbel verwijderen (bug 417802)
- Ook de qtquick-plug-in in het geïnstalleerde categorieënbestand dekken
- De juiste vertalingencatalogus gebruiken om vertalingen te tonen
- De sluitentitel van KNSQuick Dialog en basis indeling repareren (bug 414682)

### KNotification

- kstatusnotifieritem beschikbaar maken zonder dbus
- Actienummering in Android aanpassen om te werken zoals in KNotifications
- Kai-Uwe als de onderhouder van knotifications opschrijven
- Html strippen als de server het niet ondersteunt
- [android] defaultActivated uitzenden wanneer op de melding wordt getikt

### KPeople

- pri-bestandsgeneratie repareren

### KQuickCharts

- Geen fouten afdrukken over ongeldige rollen wanneer roleName niet is ingesteld
- Offscreen platform gebruiken voor testen op Windows
- Download van glsl-validator verwijderen uit validatiescript
- Validatiefout repareren in schaduwmaker in lijngrafiek
- Schaduwmaker van linechart-coreprofiel bijwerken om op niveau van compatibiliteit te komen
- Commentaar toevoegen over controleren van grenzen
- LineChart: ondersteuning voor controle op min/max y-grenzen toevoegen
- Functie sdf_rectangle toevoegen aan sdf-bibliotheek
- [linechart] tegen delen door 0 waken
- Lijngrafieken: Het aantal punten per segment verkleinen
- Geen punten verliezen aan het einde van een lijngrafiek

### Kross

- Qt5::UiTools is niet optioneel in deze module

### KService

- Nieuw afvraagmechanisme voor toepassingen: KApplicationTrader

### KTextEditor

- Een optie toevoegen aan dynamisch afbreken in woorden
- KateModeMenuList: de schuifbalk niet laten overlappen

### KWayland

- Toepassingenmenu dbus paden naar interface org_kde_plasma_window toevoegen
- Registry: de callback op globalsync niet vernietigen
- [surface] bufferoffset repareren bij buffers plakken aan oppervlakken

### KWidgetsAddons

- [KMessageWidget] de stijl toestaan ons palet te wijzigen
- [KMessageWidget] Het tekenen met QPainter in plaats van stijlsheet gebruiken
- Kopgrootte van niveau 1 een beetje verkleinen

### ModemManagerQt

- qmake pri bestandsgeneratie &amp; installatie laten vallen, nu gebroken

### NetworkManagerQt

- SAE in securityTypeFromConnectionSetting ondersteunen
- qmake pri bestandsgeneratie &amp; installatie laten vallen, nu gebroken

### Oxygen-pictogrammen

- Data-error/waarschuwing/informatie ook in groottes 32,46,64,128 ondersteunen
- "plugins" actie-item toevoegen, om met Breeze pictogrammen overeen te komen
- Statuspictogrammen toevoegen: gegevensfout, gegevenswaarschuwing, gegevensinformatie

### Plasma Framework

- Knoppen: toestaan om pictogrammen op te schalen
- Probeer het kleurenschema van het huidige thema toe te passen op QIcons (bug 417780)
- Dialog: verbinding verbreken van QWindow signalen in destructor
- Geheugenlek in ConfigView en dialoog repareren
- tips voor indelingsgrootte voor labels op knoppen repareren
- ga na dat tips voor grootte gehele getallen zijn en ven
- icon.width/height ondersteunen (bug 417514)
- hard gecodeerde kleuren verwijderen
- Construct NullEngine met KPluginMetaData() (bug 417548)
- Kopgrootte van niveau 1 een beetje verkleinen
- Tekstballon pictogram/afbeelding verticaal centreren
- beeldeigenschap voor knoppen ondersteunen
- Niet waarschuwen voor ongeldige plug-in metagegevens (bug 412464)
- Tekstballonnen hebben altijd een normale kleurgroep
- [Tests] Zorg dat radiobutton3.qml PC3 gebruikt
- Code optimaliseren bij los;laten van bestanden in het bureaublad (bug 415917)

### Prison

- Pri-bestand repareren om niet te mislukken met "CamelCase includes"
- Pri-bestand repareren om qmake-naam van QtGui als afhankelijkheid te hebben

### Omschrijving

- Nextcloud-plug-in herschrijven
- Ondersteuning van twitter afbreken

### QQC2StyleBridge

- ScrollView: Schuifbalkhoogte als opvulling van onderkant gebruiken, niet de breedte

### Solid

- Geïnverteerde logica repareren in IOKitStorage::isRemovable

### Sonnet

- Segfault bij verlaten repareren

### Accentuering van syntaxis

- Onvoldoende geheugen repareren vanwege te grote stapel met context
- Algemeen element voor bijwerken voor CartoCSS syntaxisaccentuering
- Syntaxisaccentuering voor Java-eigenschappen toevoegen
- TypeScript: privé velden en importeren/exporteren van alleen typen toevoegen en enige reparaties
- FreeCAD FCMacro extensie toevoegen aan de definitie voor python-accentuering
- Bijwerkelementen voor CMake 3.17
- C++: constinit keyword en std::format syntaxis voor tekenreeksen. Verbetering formaat van printf
- RPM spec: verschillende verbeteringen
- Accentueren van Makefile: namen van variabelen in "else" condities repareren (bug 417379)
- Syntaxisaccentuering voor Solidity toevoegen
- Kleine verbeteringen in enige XML-bestanden
- Accentueren van Makefile: substituties toevoegen (bug 416685)

### Beveiligingsinformatie

De vrijgegeven code is ondertekend met GPG met de volgende sleutel: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Vingerafdruk van primaire sleutel: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
