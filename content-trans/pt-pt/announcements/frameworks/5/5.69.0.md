---
aliases:
- ../../kde-frameworks-5.69.0
date: 2020-04-05
layout: framework
libCount: 70
---
### Baloo

- [SearchStore] Uso de registos com categorias
- [QueryParser] Correcção de problemas na detecção das aspas finais
- [EngineQuery] Oferta de reimplementação do toString(Term) para o QTest
- [EngineQuery] Remoção do número de posição não usado, alargamento dos testes
- [SearchStore] Impedimento de linhas longas e encadeamento de funções
- [baloosearch] Saída antecipada se a pasta indicada não for válida
- [MTimeDB] Consolidação do código de tratamento de intervalos de tempo
- [AdvancedQueryParser] Teste se as frases entre aspas são passadas correctamente
- [Term] Passagem de reimplementação do toString(Term) para o QTest
- [ResultIterator] Remoção de declaração posterior não-usada do SearchStore
- [QueryTest] Mudança e extensão do caso de testes de frases para estar orientado aos dados
- [Inotify] Início do temporizador de expiração do MoveFrom para o máximo de uma vez por lote do 'inotify'
- [UnindexedFileIndexer] Só marcar o ficheiro para indexação do conteúdo quando for necessário
- [Inotify] Invocação do QFile::decode apenas num único local
- [QML] Verificação correcta do cancelamento do registo
- [FileIndexScheduler] Actualização do progresso da indexação de conteúdo com maior frequência
- [FileIndexerConfig] Substituição da configuração do par QString,bool por uma classe dedicada
- [QML] Definição do tempo restante no monitor de forma mais fiável
- [TimeEstimator] Correcção do tamanho do lote, remoção da referência da configuração
- [FileIndexScheduler] Emissão da alteração ao estado LowPowerIdle
- [Debug] Melhoria da legibilidade do formato de depuração do 'positioninfo'
- [Debug] Correcção do resultado do *::toTestMap(), eliminação de não-erros
- [WriteTransactionTest] Teste de remoção apenas das posições
- [WriteTransaction] Extensão do caso de teste da posição
- [WriteTransaction] Correcção do aumento do 'm_pendingOperations' duas vezes na substituição
- [FileIndexScheduler] Limpeza do tratamento do 'firstRun'
- [StorageDevices] Correcção da ordem da ligação e inicialização da notificação
- [Config] Remoção/descontinuação do 'disableInitialUpdate'

### Ícones do Brisa

- Correcção de ligações simbólicas inválidas
- Mover a dobragem do canto para o superior-direito nos ícones a 24
- Mudança do 'find-location' para mostrar uma lupa num mapa para ser diferente do 'mark-location' (erro 407061)
- Adição de ícones do LibreOffice a 16px
- Correcção da configuração quando o 'xmllint' não está presente
- Correcção da associação da folha de estilo em 8 ícones
- Correcção de algumas cores da folha de estilo em 2 ficheiros de ícones
- Correcção das ligações simbólicas para tamanhos de ícones incorrectos
- Adição do 'input-dialpad' e 'call-voicemail'
- Adição do ícone 'buho'
- Adição do ícone do Calindori para o novo estilo do 'pm'
- Adição do ícone 'nota'
- [ícones-brisa] correcção da sombra em alguns ícones de utilizadores (applets/128)
- Adição do 'call-incoming/missed/outgoing'
- Tratamento do 'sed' do Busybox para ser igual ao da GNU
- Adição do 'transmission-tray-icon'
- Melhoria do alinhamento dos pixels e das margens dos ícones da bandeja para o 'keepassxc'
- Reversão do "[ícones-brisa] Adição dos ícones da bandeja do 'telegram-desktop'"
- Adição de ícones pequenos para o KeePassXC
- [ícones-brisa] adição de ícones da bandeja para o TeamViewer
- Adição do 'edit-reset'
- Mudança do estilo do 'document-revert' para ser mais como o 'edit-undo'
- Ícones para as categorias de emojis
- Adição de ícone da bandeja do Flameshot

### KAuth

- Correcção do requisito do espaço de nomes do tipo

### KBookmarks

- Separação do KBookmarksMenu do KActionCollection

### KCalendarCore

- Correcção de rotina de retorno para o carregamento do vCalendar no caso de erro de carregamento do iCalendar

### KCMUtils

- atendimento do 'passiveNotificationRequested'
- mudança alternativa para que o 'applicationitem' nunca se dimensione a si próprio

### KConfig

- [KConfigGui] Verificação da espessura do texto ao limpar a propriedade 'styleName'
- KconfigXT: Adição de um atributo 'value' às opções do campo Enum

### KCoreAddons

- kdirwatch: correcção de um estoiro introduzido recentemente (erro 419428)
- KPluginMetaData: tratamento de tipo MIME inválido no 'supportsMimeType'

### KCrash

- Mudança da definição do 'setErrorMessage' para fora do 'ifdef' de Linux
- Possibilidade de fornecimento de uma mensagem de erro a partir da aplicação (erro 375913)

### KDBusAddons

- Verificação do ficheiro correcto para a detecção do ambiente isolado)

### KDeclarative

- Introdução de API para notificações passivas
- [GridDelegate dos Controlos KCM] Uso do <code>ShadowedRectangle</code>
- [kcmcontrols] Respeito da visibilidade do cabeçalho/rodapé

### KDocTools

- Usar o negrito/itálico a 100% para os títulos do 'sect4' e negrito a 100% para os títulos do 'sect5' (erro 419256)
- Actualização da lista de entidades Italianas
- Uso do estilo do 'informaltable' igual ao do 'table' (erro 418696)

### KIdleTime

- Correcção de recursividade infinita no 'plugin' do xscreensaver

### KImageFormats

- Migração do 'plugin' de HDR do sscanf() para uma QRegularExpression. Correcção no FreeBSD

### KIO

- Nova classe KIO::CommandLauncherJob no KIOGui para substituir o KRun::runCommand
- Nova classe KIO::ApplicationLauncherJob no KIOGui para substituir o KRun::run
- IOSlave 'file' : uso de uma melhor definição para a chamada de sistema 'sendfile' (erro 402276)
- FileWidgets: Ignorar os eventos Return do KDirOperator (erro 412737)
- [DirectorySizeJob] Correcção da contagem de sub-pastas ao resolver as ligações simbólicas para pastas
- Marcação das montagens do KIOFuse como potencialmente lentas
- kio_file: respeito do KIO::StatResolveSymlink para o UDS_DEVICE_ID e o UDS_INODE
- [KNewFileMenu] Adição da extensão ao nome do ficheiro proposto (erro 61669)
- [KOpenWithDialog] Adição do nome genérico dos ficheiros .desktop como uma dica (erro 109016)
- KDirModel: implementação da apresentação de um nó de topo para o URL pedido
- Registo de aplicações criadas como 'cgroups' independentes
- Adição do prefixo "Stat aos elementos do Enum StatDetails
- Windows: Adição do suporte para a data de criação do ficheiro
- KAbstractFileItemActionPlugin: Adição de aspas em falta no exemplo de código
- Evitar a consulta dupla e codificação temporária em hexadecimal para os atributos do NTFS
- KMountPoint: ignorar o 'swap'
- Atribuição de um ícone aos submenus de acções
- [DesktopExecParser] Abrir os URL's {ssh,telnet,rlogin}:// com o 'ktelnetservice' (erro 418258)
- Correcção do código de saída do 'kioexec' quando o executável não existe (e o --tempfiles está activo)
- [KPasswdServer] Substituição do 'foreach' por 'for' baseado em 'range'/'index'
- KProcessRunner do KRun: terminar a notificação de arranque também em caso de erro
- [http_cache_cleaner] substituição da utilização do 'foreach' por QDir::removeRecursively()
- [StatJob] Uso de uma QFlag para indicar os detalhes devolvidos pelo StatJob

### Kirigami

- Correcção de emergência do D28468 para corrigir referências a variáveis inválidas
- eliminação do "incubator"
- desactivar o efeito intermitente da roda do rato por completo para fora
- Adição do suporte para a propriedade 'initializer' para o PagePool
- Remodelação do OverlaySheet
- Adição dos itens ShadowedImage e ShadowedTexture
- [controls/formlayout] Não tentar reiniciar o 'implicitWidth'
- Adição de sugestões de introdução de dados úteis para o campo de senhas por omissão
- [FormLayout] Configuração do intervalo do temporizador de compressão para 0
- [UrlButton] Desactivar quando não tiver nenhum URL
- simplificação do dimensionamento do cabeçalho (erro 419124)
- Remoção do cabeçalho de exportação a partir da instalação estática
- Correcção da página 'Acerca' com o Qt 5.15
- Correcção de locais errados no kirigami.qrc.in
- Adição da duração de animação "veryLongDuration"
- correcção de notificações com várias linhas
- não depender de a janela estar activa para o temporizador
- Suporte para Notificações Passivas múltiplas e empilhadas
- Correcção da activação do contorno para o ShadowedRectangle na criação do item
- verificação da existência da janela
- Adição de tipos em falta no 'qrc'
- Correcção da verificação indefinida no modo de menu global (erro 417956)
- Substituição por um rectângulo simples ao usar o desenho por 'software'
- Correcção da mistura do alfa e por pré-multiplicação
- [FormLayout] Propagação do FormData.enabled também para o texto
- Adição de um item ShadowedRectangle
- Propriedade 'alwaysVisibleActions'
- não criar instâncias quando a aplicação estiver a sair
- Não emitir mudanças da paleta se esta de facto não tiver sido modificada

### KItemModels

- [KSortFilterProxyModel QML] Tornar o 'invalidateFilter' público

### KNewStuff

- Correcção da disposição do DownloadItemsSheet (erro 419535)
- [Janela do QtQuick] Migração para UrlBUtton e esconder quando não existir nenhum URL
- Mudança para a utilização do ShadowedRectangle do Kirigami
- Correcção da actualização de cenários sem ligação de transferência seleccionada de forma explícita (erro 417510)

### KNotification

- Nova classe KNotificationJobUiDelegate

### KNotifyConfig

- Uso da libcanberra como forma principal de antever o som (erro 418975)

### KParts

- Nova classe PartLoader como substituta do KMimeTypeTrader para os componentes

### KService

- KAutostart: Adição de método estático para verificar a condição inicial
- KServiceAction: armazenamento do serviço-pai
- Leitura adequada da lista de textos X-Flatpak-RenamedFrom dos ficheiros .desktop

### KTextEditor

- Mudanças para compilação com o Qt 5.15
- correcção de estoiro na dobragem de linhas individuais (erro 417890)
- [Modo VIM ] Adição dos comandos g&lt;up&gt; g&lt;down&gt; (erro 418486)
- Adição do MarkInterfaceV2, para um s/QPixmap/QIcon/g dos símbolos das marcações
- Desenho do 'inlineNotes' após desenhar a marcação de mudança de linha

### KWayland

- [xdgoutput] Só enviar o nome e descrição iniciais se estiverem definidos
- Adição da versão 2 do XdgOutputV1
- Difusão do menu da aplicação para os recursos quando os registar
- Disponibilidade de uma implementação para a interface em 'tablet'
- [servidor] Não assumir nada sobre a ordem dos pedidos do 'damage_buffer' e de associação
- Passagem de um FD dedicado a cada teclado para o mapa de teclas do 'xkb' (erro 381674)
- [servidor] Introdução do SurfaceInterface::boundingRect()

### KWidgetsAddons

- Nova classe KFontChooserDialog (baseada no KFontDialog do KDELibs4Support)
- [KCharSelect] Não simplificar caracteres individuais na pesquisa (erro 418461)
- Se lermos os itens, será necessário limpá-lo primeiro. Caso contrário irá ver
- Actualização do kcharselect-data para o Unicode 13.0

### KWindowSystem

- Correcção da incompatibilidade do EWMH com o NET::{OnScreenDisplay,CriticalNotification}
- KWindowSystem: descontinuação do KStartupInfoData::launchedBy, por não ser usado
- Exposição do menu da aplicação através do KWindowInfo

### Plataforma do Plasma

- Adição de um elemento Page
- [pc3/busyindicator] Esconder quando não estiver em execução
- Actualização do 'window-pin', adição de mais tamanhos, e remoção do 'edit-delete' redundante
- Criação de um novo elemento TopArea com o SVG em widgets/toparea
- Adição do SVG de cabeçalho do plasmóide
- Fazer com que a propriedade 'highlighted' para o botão arredondado

### Prisão

- Também expor o tamanho mínimo verdadeiro para o QML
- Adição de um novo conjunto de funções de tamanhos de códigos de barras
- Simplificação do tratamento do tamanho mínimo
- Passagem da lógica de escala das imagens do código de barras para o AbstractBarcode
- Adição de uma API para verificar se um código de barras tem uma ou duas dimensões

### QQC2StyleBridge

- [Dialog] Uso do <code>ShadowedRectangle</code>
- Correcção do dimensionamento do CheckBox e RadioButton (erro 418447)
- Uso do <code>ShadowedRectangle</code>

### Solid

- [FsTab] Garantia de unicidade para todos os tipos de sistemas de ficheiros
- Samba: Garantia de distinção de montagens que montam a mesma origem (erro 418906)
- ferramenta de 'hardware': definição da sintaxe através do argumento 'syntax'

### Sonnet

- Correcção da detecção automática do Sonnet que falha nas línguas Índicas
- Criação do ConfigView, uma ConfigWidget não gerida

### Realce de Sintaxe

- LaTeX: correcção de parêntesis matemáticos nas legendas opcionais (erro 418979)
- Adição da sintaxe do Inno Setup, incluindo a programação incorporada em Pascal
- Lua: adição do # como separador adicional para activar a completação automática com <code>#algo</code>
- C: remoção do ' como separador de algarismos
- adição de algum comentário sobre as questões de posição a ignorar
- optimização da correspondência de expressões regulares dinâmica (erro 418778)
- correcção das regras de expressões regulares mal marcadas como dinâmicas
- extensão do indexador para detectar as expressões regulares 'dynamic=true' que não têm itens de substituição para adaptar
- Adição do realce do Overpass QL
- Agda: palavras-chave actualizadas para a versão 2.6.0 e correcção das vírgulas flutuantes

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
