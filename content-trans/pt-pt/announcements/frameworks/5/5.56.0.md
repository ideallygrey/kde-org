---
aliases:
- ../../kde-frameworks-5.56.0
date: 2019-03-09
layout: framework
libCount: 70
---
### Baloo

- Substituição de diversos Q_ASSERT's com verificações adequadas
- Verificação do tamanho do texto para evitar um estoiro num URL "tags:/"
- [tags_kio] Correcção da marcação do ficheiro local, verificando apenas as barras duplas nos URL's 'tag'
- Fixação do Intervalo de Actualização do Tempo Restante
- Correcção de regressão para a correspondência de pastas explicitamente incluídas
- Limpeza de elementos idempotentes da tabela de associações de tipos MIME
- [baloo/KInotify] Notificar se a pasta foi movida de um local não vigiado (erro 342224)
- Tratamento de pastas que correspondem correctamente a sub-textos de pastas incluídas/excluídas
- [balooctl] Normalização dos locais de inclusão/exclusão antes de os usar na configuração
- Optimização do operador de atribuição por cópia do Baloo::File, correcção do Baloo::File::load(url)
- Uso do conteúdo para determinar o tipo MIME (erro 403902)
- [Extracção] Exclusão dos dados encriptados em GPG da indexação (erro 386791)
- [balooctl] Interrupção de facto de um comando mal-formatado em vez de apenas avisar
- [balooctl] Adição de ajuda em falta para o "config set", normalização de texto
- Substituição do 'isDirHidden' recursivo por um iterativo, possibilidade de argumento 'const'
- Garantia que só são adicionadas as pastas ao monitor do 'inotify'

### Ícones do Brisa

- Adição do ícone 'code-oss'
- [ícones-brisa] Adição dos ícones das câmaras de vídeo
- [ícones-brisa] Uso de novos ícones para suspender, hibernar e mudar de utilizador no tema de ícones Brisa
- Adição de versões em 16 px e 22 px do ícone 'gamepad' às pastas 'devices/'
- Tornar consistentes os textos das dicas do tema Brisa
- Adição de ícones da bateria
- Mudar os ícones "visibility" e "hint" para "view-visible" e "view-hidden"
- [ícones-brisa] Adição de ícones de cartões SD e 'memory stick' menores e monocromáticos (erro 404231)
- Adição de ícones de dispositivos para drones
- Mudança dos ícones de tipos MIME de código/inclusão de C/C++ para um estilo com círculos/linhas
- Correcção de sombras em falta nos ícone de tipos MIME de ficheiros de inclusão em C/C++ (erro 401793)
- remoção de ícone monocromático para preferências do tipo de letra
- Melhoria no ícone de selecção de tipos de letra
- Uso de um novo ícone num estilo de campainha para todos os utilizadores do 'preferences-desktop-notification' (erro 404094)
- [ícones-brisa] Adição de versões a 16px do 'gnumeric-font.svg' e ligação simbólica do 'gnumeric-font.svg' ao 'font.svg'
- Adição de um ícone 'preferences-system-users' que aponta o ícone do 'yast-users'
- Adição do ícone 'edit-none'

### Módulos Extra do CMake

- Correcção do 'releaseme checkout' quando este está incluído numa sub-pasta
- Novo módulo de pesquisa para o Canberra
- Actualização dos ficheiros de ferramentas do Android para a realidade
- Adição de verificação na compilação do FindEGL

### KActivities

- Uso da ordenação natural do ActivityModel (erro 404149)

### KArchive

- Guarda para a invocação do KCompressionDevice::open sem nenhuma infra-estrutura disponível (erro 404240)

### KAuth

- Indicação às pessoas que deveriam estar a usar o KF5::AuthCore
- Compilação do nosso próprio utilitário com o AuthCore e não com o Auth
- Introdução do KF5AuthCore

### KBookmarks

- Substituição da dependência do KIconThemes com um utilização equivalente do QIcon

### KCMUtils

- Uso do nome do KCM no cabeçalho do KCM
- Adição de 'ifndef KCONFIGWIDGETS_NO_KAUTH' em falta
- Adaptação às alterações no 'kconfigwidgets'
- Sincronização do preenchimento do módulo de QML para reflectir as páginas de configuração do sistema

### KCodecs

- Correcção do CVE-2013-0779
- QuotedPrintableDecoder::decode: devolver 'false' em caso de erro em vez de emitir uma asserção
- Marcação do KCodecs::uuencode como não fazendo nada
- nsEUCKRProber/nsGB18030Prober::HandleData não estoirar se o 'aLen' for igual a 0
- nsBig5Prober::HandleData: Não estoirar se o 'aLen' for igual a 0
- KCodecs::Codec::encode: Não emitir uma asserção/estoiro se o 'makeEncoder' devolver 'null'
- nsEUCJProber::HandleData: Não estoirar se o 'aLen' for igual a 0

### KConfig

- Escrita de caracteres UTF8 válidos sem os escapar (erro 403557)
- KConfig: tratamento de ligações simbólicas para pasta de forma correcta

### KConfigWidgets

- Ignorar o teste de performance se não forem encontrados ficheiros de esquemas
- Adição de uma nota para o KF6 usar a versão de base do KF5::Auth
- Colocação em 'cache' da configuração predefinida do KColorScheme

### KCoreAddons

- Criação de ligações 'tel:' para os números telefónicos

### KDeclarative

- uso do KPackage::fileUrl para suportar os pacotes KCM do 'rcc'
- [GridDelegate] Correcção de legendas longas que se fundem entre si (erro 404389)
- [GridViewKCM] melhoria do contraste e legibilidade para os botões à passagem incorporados dos delegados (erro 395510)
- Correcção da opção 'accept' do objecto 'event' do DragMove (erro 396011)
- Uso de um diferente ícone do item "Nenhum" nos KCM's em grelha de ícones

### KDESU

- kdesud: KAboutData::setupCommandLine() já define o 'help' &amp; e o 'version'

### KDocTools

- Migração do suporte multi-plataforma ao KF5_HOST_TOOLING
- Só declarar o DocBookXML como encontrado se for encontrado de facto
- Actualização das entidades em Espanhol

### KFileMetaData

- [Extracção] Adição de meta-dados às extracções (erro 404171)
- Adição de extracção para ficheiros AppImage
- Limpeza da extracção do 'ffmpeg'
- [ExternalExtractor] Apresentação de resultados mais úteis quando o módulo de extracção falhar
- Formatação dos dados do 'flash' da fotografia no EXIF (erro 343273)
- Evitar efeitos colaterais devido a um valor de 'errno' não tratado
- Uso do Kformat para a taxa de 'bits' e de amostragem
- Adição de unidades para a taxa de imagens e dados de GPS
- Adição de função de formatação de textos para a informação da propriedade
- Evitar a fuga de um QObject no ExternalExtractor
- Tratamento do &lt;a&gt; como elemento contentor no SVG
- Verificação do Exiv2::ValueType::typeId antes de o converter para um racional

### KImageFormats

- ras: Correcção de estoiro em ficheiros com problemas
- ras: protecção também do QVector da paleta
- ras: ajuste da verificação de máx. de ficheiros
- xcf: Correcção de memória não inicializada com documentos com problemas
- adição de 'const', ajuda a compreender melhor a função
- ras: ajuste de um tamanho máximo que "cabe" num QVector
- ras: não emitir uma asserção porque tentámos alocar um vector gigante
- ras: Protecção contra uma divisão por zero
- xcf: Não dividir por 0
- tga: falha graciosa em caso de erros no 'readRawData'
- ras: falha graciosa se a altura*altura*bpp &gt; tamanho

### KIO

- kioexec: KAboutData::setupCommandLine() já define o 'help' &amp; 'version'
- Correcção de um estoiro no Dolphin ao largar um ficheiro do lixo no lixo (erro 378051)
- Reticências a meio de nomes de ficheiros muito grandes nos textos dos erros (erro 404232)
- Adição de suporte para portais no KRun
- [KPropertiesDialog] Correcção da lista do grupo (erro 403074)
- Tentar adequadamente localizar o binário do 'kioslave' em $libexec E em $libexec/kf5
- Uso do AuthCore em vez do Auth
- Adição um nome de ícone aos fornecedores de serviços no ficheiro .desktop
- Leitura do ícone do fornecedor de pesquisa do IKWS a partir do ficheiro .desktop
- [PreviewJob] Também passar a informação de que somos o gerador de miniaturas ao invocar um 'stat' sobre o ficheiro (erro 234754)

### Kirigami

- remoção da modificação defeituosa do 'contentY' no 'refreshabeScrollView'
- adição de OverlayDrawer à documentação de coisas pelo Doxygen
- associação do 'currentItem' à janela
- cor adequada para o ícone da seta para baixo
- SwipeListItem: criar espaço para as acções se '!supportsMouseEvents' (erro 404755)
- ColumnView e remodelação parcial em C++ do PageRow
- agora podemos usar no máximo os Controls 2.3 com o Qt 5.10
- correcção da altura das áreas horizontais
- Melhoria do ToolTip no componente ActionTextField
- Adição de um componente ActionTextField
- correcção do espaço entre botões (erro 404716)
- correcção do tamanho dos botões (erro 404715)
- GlobalDrawerActionItem: referência correcta do ícone, usando a propriedade 'group'
- mostrar o separador se a barra do cabeçalho for invisível
- adição de um fundo de página predefinido
- DelegateRecycler: Correcção da tradução que usava o domínio errado
- Correcção de aviso ao usar o QQuickAction
- Remoção de algumas construções de QString desnecessárias
- Não mostrar a dica quando a lista está aberta (erro 404371)
- esconder as sombras quando fechar
- adição das propriedades necessárias para a cor alternada
- reverter a maior parte da mudança nas heurísticas na coloração dos ícones
- gestão adequada das propriedades em grupo
- [PassiveNotification] Não iniciar o temporizador até que a janela fique em primeiro plano (erro 403809)
- [SwipeListItem] Uso de um botão de ferramentas verdadeiro para melhorar a usabilidade (erro 403641)
- suporte para fundos alternados opcionais (erro 395607)
- mostrar apenas as pegas quando existirem acções visíveis
- suporte para ícone coloridos nos botões de acções
- mostrar sempre o botão 'recuar' nas camadas
- Actualização da documentação do SwipeListItem para o QQC2
- correcção da lógica do 'updateVisiblePages'
- exposição das páginas visíveis no 'pagerow'
- esconder a navegação quando a página actual tiver uma barra de ferramentas
- suporte do substituto da página do estilo da barra
- nova propriedade na página: titleDelegate para substituir o texto nas barras de ferramentas

### KItemModels

- KRearrangeColumnsProxyModel: passagem dos métodos de mapeamento em duas colunas para públicos

### KNewStuff

- Filtragem do conteúdo inválido nas listas
- Correcção de fuga de memória encontrada pelo 'asan'

### KNotification

- migração do 'findcanberra' a partir do ECM
- Listagem do Android como suportado oficialmente

### Plataforma KPackage

- Remoção do aviso de descontinuação do 'kpackage_install_package'

### KParts

- modelos: KAboutData::setupCommandLine() já define o 'help' &amp; o 'version'

### Kross

- Instalação dos módulos do Kross em ${KDE_INSTALL_QTPLUGINDIR}

### KService

- kbuildsycoca5: não é necessário repetir o trabalho do KAboutData::setupCommandLine()

### KTextEditor

- tentar melhorar a altura da pintura para as linhas de texto - erro 403868 - tentar cortar o _ e outras partes ainda com problemas: duplicação da altura de linhas com texto do tipo inglês/árabe misturado; veja o erro 404713
- Uso do QTextFormat::TextUnderlineStyle em vez do QTextFormat::FontUnderline (erro 399278)
- Possibilidade de mostrar todos os espaços no documento (erro 342811)
- Não imprimir as linhas indentadas
- KateSearchBar: Mostrar também a sugestão de que a pesquisa deu a volta no 'nextMatchForSelection()' (Ctrl-H)
- katetextbuffer: remodelação do TextBuffer::save() para separar melhor os caminhos de código
- Uso do AuthCore em vez do Auth
- Remodelação do KateViewInternal::mouseDoubleClickEvent(QMouseEvent *e)
- Melhorias na completação
- Configuração do esquema de cores para Impressão na Antevisão da Impressão (erro 391678)

### KWayland

- Só enviar o XdgOutput::done se tiver alterado (erro 400987)
- FakeInput: adição do suporte para o movimento do cursor com coordenadas absolutas
- Adição do XdgShellPopup::ackConfigure em falta
- [servidor] Adição do mecanismo de 'proxy' de dados da superfície
- [servidor] Adição do sinal 'selectionChanged'

### KWidgetsAddons

- Uso de um ícone correcto "não" para o KStandardGuiItem

### Plataforma do Plasma

- [Item de Ícone] Bloquear a próxima animação também com base na visibilidade da janela
- Mostrar um aviso se um 'plugin' precisar de uma versão mais recente
- Mudança das versões dos temas, dada a alteração dos ícones, para invalidar as 'caches' antigas
- [ícones-brisa] Remodelação do system.svgz
- Tornar consistentes os textos das dicas do tema Brisa
- Mudança do glowbar.svgz para um estilo mais suave (erro 391343)
- Criar uma contingência no contraste do fundo na altura da execução (erro 401142)
- [tema/janelas do ecrã brisa] Adição de cantos arredondados às janelas

### Purpose

- pastebin: não mostrar as notificações de progresso (erro 404253)
- sharetool: Mostrar o URL partilhado no topo
- Correcção da partilha de ficheiros com espaços nos nomes através do Telegram
- Fazer com que o ShareFileItemAction devolva algum resultado ou um erro se forem fornecidos (erro 397567)
- Activação da partilha de URL's por e-mail

### QQC2StyleBridge

- Uso do PointingHand ao passar o cursor sobre ligações numa Label
- Respeito da propriedade 'display' dos botões
- se carregar em áreas vazias, comportar-se-á como um pgup/pgdown (erro 402578)
- Suporte para ícones no ComboBox
- suporte para a API de posicionamento de texto
- Suporte de ícones de ficheiros locais nos botões
- Uso do cursor correcto ao passar o cursor sobre a parte editável de um campo incremental

### Solid

- Migração do FindUDev.cmake para as normas do ECM

### Sonnet

- Tratamento do caso em que o 'createSpeller' recebe uma língua indisponível

### Realce de Sintaxe

- Correcção do aviso de de remoção do repositório
- MustacheJS: realçar também os ficheiros do modelo, correcção da sintaxe e melhoria do suporte para o Handlebars
- tornar os contextos não-usados fatais na indexação
- Actualização do example.rmd.fold e do test.markdown.fold com números novos
- Instalação do cabeçalho do DefinitionDownloader
- Actualização do octave.xml para o Octave 4.2.0
- Melhoria do realce do TypeScript (e do React) e adição de mais testes para o PHP
- Adição de mais realce para as linguagens encadeadas no Markdown
- Devolução de definições ordenadas para os nomes dos ficheiros e tipos MIME
- Adição de actualização de referência em falta
- BrightScript: Números unários e hexadecimais, @attribute
- Evitar a duplicação de ficheiros *-php.xml noo "data/CMakeLists.txt"
- Adição de funções que devolvem todas as definições de um tipo MIME ou nome de ficheiro
- actualização do tipo MIME 'literate' do Haskell
- evitar uma asserção no carregamento da expressão regular
- cmake.xml: Actualizações para a versão 3.14
- CubeScript: correcções do escape da continuação de linhas nos textos
- adição de um HOWTO mínimo para a adição de testes
- Markdown do R: melhoria na dobragem de blocos
- HTML: realce de código em JSX, TypeScript &amp; MustacheJS na marca &lt;script&gt; (erro 369562)
- AsciiDoc: Adição da dobragem de secções
- Realce de sintaxe do esquema do FlatBuffers
- Adição de algumas constantes e funções do Maxima

### Informação de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
