---
aliases:
- ../../kde-frameworks-5.65.0
date: 2019-12-14
layout: framework
libCount: 70
qtversion: 5.12
---
New module: KQuickCharts -- a QtQuick module providing high-performance charts.

### Icônes « Breeze »

- Alignement des pixels dans le sélecteur de couleurs
- Ajouter de nouvelles icônes à « baloo »
- Ajout de nouvelles icônes pour les préférences de recherche
- Use an eyedropper for color-picker icons (bug 403924)
- Ajout d'une icône de catégorie « Toutes les applications »

### Modules additionnels « CMake »

- EBN extra-cmake-modules transport cleanup
- ECMGenerateExportHeader: add NO_BUILD_SET_DEPRECATED_WARNINGS_SINCE flag
- Explicitly use lib for systemd directories
- Ajout d'un dossier d'installation pour les unités de « systemd »
- KDEFrameworkCompilerSettings: enable all Qt &amp; KF deprecation warnings

### Intégration avec l'environnement de développement

- Conditionally set SH_ScrollBar_LeftClickAbsolutePosition based on kdeglobals setting (bug 379498)
- Set application name and version on the knshandler tool

### Outils KDE avec « DOxygen »

- Correction des importations pour les modules avec Python 3

### KAuth

- Installation du fichier « .pri » pour « KAuthCore »

### KBookmarks

- Deprecate KonqBookmarkMenu and KonqBookmarkContextMenu
- Move classes only used by KonqBookmarkMenu together with it

### KCalendarCore

- Fallback to system time zone on calendar creation with an invalid one
- Memory Calendar: avoid code duplication
- Use QDate as key in mIncidencesForDate in MemoryCalendar
- Handle incidences in different time zones in MemoryCalendar

### KCMUtils

- [KCMultiDialog] Remove most special margins handling; it's done in KPageDialog now
- KPluginSelector: use new KAboutPluginDialog
- Add guard for missing kirigami (bug 405023)
- Disable the restore defaults button if the KCModule says so
- Have KCModuleProxy take care of the defaulted state
- Make KCModuleQml conform to the defaulted() signal

### KCompletion

- Remaniement de « KHistoryComboBox::insertItems »

### KConfig

- Paramètres pour le notificateur de documents
- Only create a session config when actually restoring a session
- kwriteconfig : ajout d'une option de suppression
- Ajout de « KPropertySkeletonItem »
- Prepare KConfigSkeletonItem to allow inheriting its private class

### KConfigWidgets

- [KColorScheme] Make order of decoration colors match DecorationRole enum
- [KColorScheme] Fix mistake in NShadeRoles comment
- [KColorScheme/KStatefulBrush] Switch hardcoded numbers for enum items
- [KColorScheme] Add items to ColorSet and Role enums for the total number of items
- Register KKeySequenceWidget to KConfigDialogManager
- Adjust KCModule to also channel information about defaults

### KCoreAddons

- Deprecate KAboutData::fromPluginMetaData, now there is KAboutPluginDialog
- Ajout d'une alarme descriptive lorsque « inotify_add_watch » renvoie « ENOSPC » (bogue 387663)
- Add test for bug "bug-414360" it's not a ktexttohtml bug (bug 414360)

### KDBusAddons

- Include API to generically implement --replace arguments

### KDeclarative

- EBN kdeclarative transfer protocol cleanup
- S'adapte aux modifications dans « KConfigCompiler »
- make header and footer visible when they get content
- Prise en charge de « qqmlfileselectors »
- Allow to disable autosave behavior in ConfigPropertyMap

### KDED

- Suppression de la dépendance de « kdeinit » envers « kded »

### Prise en charge de « KDELibs 4 »

- supprime les éléments « kgesturemap » inutilisés de « kaction »

### KDocTools

- Tâches pour le Catalan : ajout des entrées manquantes

### KI18n

- Obsolescence de « I18N_NOOP2 »

### KIconThemes

- Deprecate top-level UserIcon method, no longer used

### KIO

- Ajout d'un nouveau protocole pour les archives « 7z »
- [CopyJob] When linking also consider https for text-html icon
- [KFileWidget] Avoid calling slotOk right after the url changed (bug 412737)
- [kfilewidget] Chargement des icônes par nom
- KRun: don't override user preferred app when opening local *.*html and co. files (bug 399020)
- Repair FTP/HTTP proxy querying for the case of no proxy
- Ftp ioslave: Fix ProxyUrls parameter passing
- [KPropertiesDialog] provide a way of showing the target of a symlink (bug 413002)
- [Remote ioslave] Add Display Name to remote:/ (bug 414345)
- Correction des paramètres de serveur mandataire « HTTP » (bogue 414346)
- [KDirOperator] Add Backspace shortcut to back action
- When kioslave5 couldn't be found in libexec-ish locations try $PATH
- [Samba] Amélioration du message d'alerte concernant le nom « netbios »
- [DeleteJob] Utilisation d'un processus de travail séparé pour lancer l'opération courante d'entrées / sorties (bogue 390748)
- [KPropertiesDialog] rendre la chaîne de création de date sélectionnable aussi par la souris (bogue 413902)

Obsolescences :

- Deprecated KTcpSocket and KSsl* classes
- Remove the last traces of KSslError from TCPSlaveBase
- Port ssl_cert_errors meta data from KSslError to QSslError
- Deprecate KTcpSocket overload of KSslErrorUiData ctor
- [http kio slave] use QSslSocket instead of KTcpSocket (deprecated)
- [TcpSlaveBase] port from KTcpSocket (deprecated) to QSslSocket

### Kirigami

- Correction de l'en-tête de la barre d'outils
- Ne pas se planter quand la source de l'icône est vide
- MenuIcon : corrections des alarmes quand l'ascenseur n'est pas initialisé
- Compte pour une étiquette mnémotechnique pour revenir à «  »
- Correction des actions « InlineMessage » pour être toujours placées dans le menu de débordement
- Correction de l'arrière-plan de cartes par défaut (bogue 414329)
- Icône : résolution du problème de traitement lorsque la source est de type « http »
- Corrections de la navigation par le clavier
- i18n : extraction additionnelle des messages à partir des sources « C++ »
- Correction de la position de la commande du projet « cmake »
- Ne proposer qu'une seule instance de « QmlComponentsPool » par moteur (bogue 414003)
- Basculer « ToolBarPageHeader » pour utiliser un comportement de fermeture de l'icône à partir de « ActionToolBar »
- ActionToolBar : modification automatique en mode « Icône uniquement » pour les actions marquées « KeepVisible »
- Ajouter une propriété « displayHint » pour les actions
- Ajout de l'interface « D-Bus » dans la version statique
- Revert "take into account dragging speed when a flick ends"
- FormLayout : correction de la hauteur de l'étiquette si le mode est différent de « Faux »
- ne pas afficher la poignée par défaut quand non modal
- Revert "Ensure that GlobalDrawer topContent always stays on top"
- Use a RowLayout for laying out ToolBarPageHeader
- Centrer verticalement et à gauche les actions dans « ActionTextField » (bogue 413769)
- irestore dynamic watch of tablet mode
- Remplacement de « SwipeListItem »
- prise en charge de la propriété « actionsVisible »
- Démarrer le portage de « SwipeListItem » vers « SwipeDelegate »

### KItemModels

- Déconseillé « KRecursiveFilterProxyModel »
- KNumberModel : gestion élégante d'un pas à 0
- Exposition de « KNumberModel » à « QML »
- Ajout d'une nouvelle classe « KNumberModel » comme modèle des nombres entre deux valeurs
- Exposition de « KDescendantsProxyModel » à « QML »
- Ajout de l'importation « qml » pour « KItemModels »

### KNewStuff

- Ajout quelques liens sympathiques « signaler des bugs ici »
- Correction de la syntaxe « i18n » pour éviter des erreurs d'exécution (bogue 414498)
- Transformer « KNewStuffQuick::CommentsModel » en un « SortFilterProxy » pour les relectures
- Paramétrage correct des arguments « i18n » en une passe (bogue 414060)
- Ces fonctions sont là depuis la version 5.65 et non 5.64
- Add OBS to screenrecorders (bug 412320)
- Fix a couple of broken links, update links to https://kde.org/applications/
- Correction de la traduction de « $GenericName »
- Show a "Loading more..." busy indicator when loading view data
- Give some more pretty feedback in NewStuff::Page while the Engine is loading (bug 413439)
- Add an overlay component for item activity feedback (bug 413441)
- Only show DownloadItemsSheet if there's more than one download item (bug 413437)
- Use the pointing hand cursor for the single-clickable delegates (bug 413435)
- Fix the header layouts for EntryDetails and Page components (bug 413440)

### KNotification

- Make the docs reflect that setIconName should be preferred over setPixmap when possible
- Emplacement du fichier de configuration de documents sous Android

### KParts

- Mark BrowserRun::simpleSave properly as deprecated
- BrowserOpenOrSaveQuestion: move AskEmbedOrSaveFlags enum from BrowserRun

### KPeople

- Autorisation de déclencher un tri à partir de « QML »

### kquickcharts

Nouveau module. Le module « Quick Charts » fournit un ensemble de graphiques pouvant être utilisés à partir d'applications « QtQuick ». Ils sont destinés à être utilisés à la fois pour l'affichage simple de données, mais aussi pour l'affichage continu de gros volume de données (souvent appelés traceurs). Les graphiques utilisent un système appelé « champs de distance » pour leur rendu accéléré, qui fournit des moyens d'utiliser le processeur graphique pour le rendu de formes 2D sans perte de qualité.

### KTextEditor

- KateModeManager::updateFileType(): validate modes and reload menu of the status bar
- Vérifier les modes du fichier de configuration de session
- Vérification correcte de la licence LGPLv2+ par Svyatoslav Kuzmich
- restaurer le préformat de fichiers

### KTextWidgets

- Obsolescence de « kregexpeditorinterface »
- [kfinddialog] Remove usage of kregexpeditor plugin system

### KWayland

- [serveur] Ne pas se rendre propriétaire de l'implémentation de « dmabuf »
- [server] Make double-buffered properties in xdg-shell double-buffered

### KWidgetsAddons

- [KSqueezedTextLabel] Add icon for "Copy entire text" action
- Unify KPageDialog margin handling into KPageDialog itself (bug 413181)

### KWindowSystem

- Adjust count after _GTK_FRAME_EXTENTS addition
- Ajout de la prise en charge de « _GTK_FRAME_EXTENTS »

### KXMLGUI

- Abandon de la prise en charge rompue et non utilisée de « KGesture »
- Add KAboutPluginDialog, to be used with KPluginMetaData
- Also allow invoking session restoration logic when apps are manually launched (bug 413564)
- Add missing property to KKeySequenceWidget

### Icônes « Oxygen »

- Symlink microphone to audio-input-microphone on all sizes (bug 398160)

### Environnement de développement de Plasma

- move backgroundhints managment in Applet
- Utilisation du sélecteur de fichiers dans l'interception
- Plus d'utilisation pour « ColorScope »
- surveille aussi les modifications de fenêtre
- support for user removing background and automatic shadow
- prend en charge le sélecteurs de fichiers
- Prise en charge des sélecteurs de fichiers « qml »
- Suppression des éléments égarés de « qgraphicsview »
- don't delete and recreate wallpaperinterface if not needed
- MobileTextActionsToolBar check if controlRoot is undefined before using it
- Add hideOnWindowDeactivate to PlasmaComponents.Dialog

### Motif

- include the cmake command we are about to use

### QQC2StyleBridge

- [TabBar] Use window color instead of button color (bug 413311)
- associer les propriétés activées à l'affichage activé
- [ToolTip] Base timeout on text length
- [ComboBox] blocage des dimensionnements des menus contextuels
- [ComboBox] Don't indicate focus when popup is open
- [Boîte à liste déroulante] Suivi de « focusPolicy »

### Opaque

- [udisks2] fix media change detection for external optical drives (bug 394348)

### Sonnet

- Désactivation du moteur « ispell » avec « mingw »
- Implement ISpellChecker backend for Windows &gt;= 8
- Basic cross-compiling support for parsetrigrams
- Intégration de « trigrams.map » dans la bibliothèque partagée

### Syndication

- Fix Bug 383381 - Getting the feed URL from a youtube channel no longer works (bug 383381)
- Extract code so we can fix parsing code (bug 383381)
- atom has icon support (So we can use specific icon in akregator)
- Conversion de « qtest » en application réelle

### Coloration syntaxique

- Mises à jour à partir de la version finale « CMake » 3.16
- reStructuredText: Fix inline literals highlighting preceding characters
- « rst » : ajoute la prise en charge pour les hyper-liens indépendantes
- JavaScript: move keywords from TypeScript and other improvements
- Réaction à « JavaScript / TypeScript » : renommage des définitions de syntaxe
- LaTeX: fix backslash delimiter in some keywords (bug 413493)

### ThreadWeaver

- Utilisation d'une « URL » avec un chiffrement du trafic

### Informations sur la sécurité

Le code publié a été signé en « GPG » avec la clé suivante  pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Empreinte de la clé primaire : 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB
