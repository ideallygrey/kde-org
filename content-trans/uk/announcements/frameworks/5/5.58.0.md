---
aliases:
- ../../kde-frameworks-5.58.0
date: 2019-05-13
layout: framework
libCount: 70
---
### Baloo

- [baloo_file] Реалізовано очікування на запуск процесу видобування даних
- [balooctl] Додано команду для показу файлів, які не вдалося індексувати (виправлено ваду 406116)
- Додано QML до списку типів початкових кодів
- [balooctl] Реалізовано захоплення сталого totalsize у лямбді
- [balooctl] Реалізовано перемикання багаторядкового виведення до нового допоміжного засобу
- [balooctl] Реалізовано використання нового допоміжного засобу у виведенні json
- [balooctl] Реалізовано використання нового допоміжного засобу для виведення у простому форматі
- [balooctl] Збірку станів індексування файлів виведено зі списку виведення
- Реалізовано зберігання порожніх документів метаданих Json поза базою даних DocumentData
- [balooshow] Уможливлено посилання на файли за адресою з жорсткого посилання
- [balooshow] Придушено попередження, якщо адреса посилається на неіндексований файл
- [MTimeDB] Уможливлено використання часової позначки для дати, яка є новішою за дату найновішого документа у діапазоні відповідності
- [MTimeDB] Реалізовано використання точної відповідності, якщо надходить запит щодо точної відповідності
- [balooctl] Спрощено обробку різних позиційних аргументів
- [balooctl] Розширено текстову довідку, поліпшено перевірку помилок
- [balooctl] Використано зрозуміліші назви для розміру у виведених даних щодо стану
- [balooctl] Команда clear: вилучено зайву перевірку documentData, спрощено
- [kio_search] Усунено попередження, додано UDSEntry для «.» у listDir
- Використано шістнадцятковий запис для переліку прапорців DocumentOperation
- Уточнено процедуру обчислення розміру бази даних
- Реалізовано відкладення обробки термінів до потреби, усунено одночасне визначення терміну і рядка для пошуку
- Усунено додавання фільтрів дати із типовими значення до json
- Реалізовано використання компактного формату Json при перетворенні адрес запитів
- [balooshow] Усунено виведення зайвого попередження для неіндексованого файла

### Піктограми Breeze

- Додано звичайні версії 16-піксельних піктограм find-location та mark-location
- Створено символічне посилання preferences-system-windows-effect-flipswitch на preferences-system-tabbox
- Додано символічне посилання edit-delete-remove» та 22-піксельну версію піктограм paint-none та edit-none
- Поліпшено сумісність із темою типової піктограми користувача Kickoff
- Додано піктограму для модуля керування Thunderbolt
- Збільшено різкість літер Z у піктограмах system-suspend*
- Поліпшено піктограму widget-alternatives
- Додано піктограми go-up/down/next/previous-skip
- Оновлено логотип KDE так, щоб він був ближчим до оригінального
- Додано піктограму альтернатив

### Додаткові модулі CMake

- Виправлено ваду: реалізовано пошук STL C++ за допомогою формального виразу
- Безумовно, а не лише у режимі діагностики, увімкнено -DQT_STRICT_ITERATORS

### KArchive

- KTar: реалізовано захист від від'ємних розмірів довгих посилань
- Усунено некоректний запис до пам'яті для файлів tar із помилковим форматуванням
- Усунено витік пам'яті при читанні деяких файлів tar
- Ініціалізовано область пам'яті при читанні помилково форматованих файлів tar
- Усунено переповнення буфера стека при читанні помилково форматованих файлів
- Усунено нуль-розіменування для помилково форматованих файлів tar
- Реалізовано встановлення заголовка krcc.h
- Усунено подвійне вилучення для пошкоджених файлів
- Заборонено копіювання KArchiveDirectoryPrivate і KArchivePrivate
- Усунено переповнення стека у KArchive::findOrCreate для дуже довгих шляхів
- Впроваджено і використано KArchiveDirectory::addEntryV2
- Реалізовано обробку невдалого завершення роботи removeEntry
- KZip: усунено використання купи після вивільнення для пошкоджених файлів

### KAuth

- Реалізовано примусову підтримку UTF-8 у KAuth (виправлено ваду 384294)

### KBookmarks

- Додано підтримку KBookmarkOwner для обміну даними, якщо відкрито вкладки

### KCMUtils

- Реалізовано використання підказок щодо розміру з самого запису ApplicationItem
- Виправлено градієнт тла Oxygen для модулів QML

### KConfig

- Додано можливість сповіщення до KConfigXT

### KCoreAddons

- Усунено помилкові попередження щодо неможливості знайти тип служби
- Створено клас KOSRelease для обробки файлів os-release

### KDeclarative

- [KeySequenceItem] Висоту кнопки спорожнення зрівняно із висотою кнопки скорочення
- Засіб малювання: область визначення програми GL зрівняно із терміном існування вузла графу сцени (виправлено ваду 403453)
- KeySequenceHelperPrivate::updateShortcutDisplay: усунено показ тексту англійською
- [ConfigModule] Реалізовано передавання початкових властивостей у push()
- Увімкнено типову підтримку glGetGraphicsResetStatus у Qt &gt;= 5.13 (виправлено ваду 364766)

### KDED

- Реалізовано встановлення файла .desktop для kded5 (виправлено ваду 387556)

### KFileMetaData

- [TagLibExtractor] Усунено аварійне завершення роботи під час обробки некоректних файлів Speex (виправлено ваду 403902)
- Усунено аварійне завершення роботи exivextractor під час обробки файлів із помилковим форматуванням (виправлено ваду 405210)
- Реалізовано оголошення властивостей як метатипу
- Для сумісності змінено атрибути властивостей
- Реалізовано обробку списку варіантів у форматованих функціях
- Виправлено тип LARGE_INTEGER для Windows
- Усунено помилки компіляції для реалізації UserMetaData у Windows
- Додано пропущений тип MIME до засобу записування taglib
- [UserMetaData] Реалізовано належну обробку розміру даних атрибута
- [UserMetaData] Розплутано код для Windows, Linux/BSD/Mac та фіктивний код

### KGlobalAccel

- Реалізовано копіювання контейнера у Component::cleanUp до виконання ітерації
- Усунено використання qAsConst для тимчасової змінної (виправлено ваду 406426)

### KHolidays

- holidays/plan2/holiday_zm_en-gb — додано замбійські свята
- holidays/plan2/holiday_lv_lv — виправлено дату для середини літа
- holiday_mu_en — додано свята 2019 року на Маврикії
- holiday_th_en-gb — оновлено до стану на 2019 рік (виправлено ваду 402277)
- Оновлено японські свята
- Додано громадські свята для Нижньої Саксонії (Німеччина)

### KImageFormats

- tga: усунено спроби читання понад max_palette_size до палітри
- tga: реалізовано memset dst, якщо не вдається прочитати дані
- tga: реалізовано memset для усього масиву палітри, а не лише palette_size
- Реалізовано ініціалізацію непрочитаних бітів _starttab
- xcf: виправлено використання неініціалізованої пам'яті під час обробки пошкоджених документів
- ras: усунено можливість понаднормового читання вхідних даних для помилково форматованих файлів
- xcf: шар є сталим при копіюванні та об'єднанні, тепер його позначено як сталий

### KIO

- [FileWidget] Замінено «Filter:» на «File type:» при зберіганні із обмеженим списком типів MIME (виправлено ваду 79903)
- Реалізовано використання загальної піктограми для створених файлів посилань на програму
- [Вікно властивостей] Реалізовано використання рядка «Free space» замість рядка «Disk usage» (виправлено ваду 406630)
- Реалізовано заповнення UDSEntry::UDS_CREATION_TIME у linux, якщо glibc &gt;= 2.28
- [KUrlNavigator] Виправлено навігацію адресами при виході з архіву, відкритого за допомогою krarc і Dolphin (виправлено ваду 386448)
- [KDynamicJobTracker] Якщо kuiserver є недоступним, реалізовано повернення до діалогового вікна віджета (виправлено ваду 379887)

### Kirigami

- [aboutpage] Реалізовано приховування заголовка авторів, якщо у списку авторів порожньо
- Оновлено qrc.in для відповідності .qrc (було пропущено ActionMenuItem)
- Забезпечено наявність у вікні ActionButton (виправлено ваду 406678)
- Сторінки: реалізовано експортування правильних розмірів contentHeight та неявних розмірів
- [ColumnView] Реалізовано перевірку індексу у дочірньому фільтрі.
- [ColumnView] Заборонено перехід за першу сторінку за допомогою кнопки «Назад» миші
- Реалізовано негайне використання належного розміру для заголовка

### KJobWidgets

- [KUiServerJobTracker] Реалізовано стеження за терміном роботи служби kuiserver і повторну реєстрацію завдань, якщо це потрібно

### KNewStuff

- Вилучено піксельовану рамку (виправлено ваду 391108)

### KNotification

- [Сповіщення від порталу] Реалізовано підтримку підказок щодо типової дії та пріоритетності
- [KNotification] Додано HighUrgency
- [KNotifications] Реалізовано оновлення при зміні прапорців, адрес або нагальності
- Уможливлено визначення рівня критичності для сповіщень

### Набір бібліотек KPackage

- Додано пропущені властивості у kpackage-generic.desktop
- kpackagetool: реалізовано читання kpackage-generic.desktop з qrc
- Створення даних AppStream: забезпечено пошук у структурі пакунків тих пакунків, які містять і metadata.desktop/json

### KTextEditor

- Переглянуто сторінки налаштувань kate з метою полегшення подальшого супроводу
- Уможливлено зміну режиму після зміни підсвічування
- ViewConfig: реалізовано використання нового загального інтерфейсу налаштовування
- Виправлено малювання растрового зображення закладки на панелі піктограм
- Забезпечено відповідність змінам кількості цифр у числі лівої межі
- Виправлено показ попереднього перегляду згорнутих даних при пересуванні вказівника миші знизу вгору
- Переглянуто IconBorder
- До кнопки смужки стану для способів введення додано пункти способів введення
- Реалізовано малювання позначки згортання належним кольором і забезпечено її кращу видимість
- Вилучено типове скорочення F6 для показу бічної смужки з піктограмами
- Додано пункт дії для перемикання згортання дочірніх діапазонів (виправлено ваду 344414)
- Змінено напис на кнопці «Close» на «Close file», якщо файл було вилучено з диска (виправлено ваду 406305)
- Піднято зауваження щодо авторських прав
- Усунено конфлікт клавіатурних скорочень для перемикання вкладок
- KateIconBorder: виправлено ширину та висоту контекстної панелі згортання
- Усунено перестрибування перегляду вниз при зміні згортання
- DocumentPrivate: реалізовано врахування режиму відступів при позначенні блоку (виправлено ваду 395430)
- ViewInternal: виправлено makeVisible(..) (виправлено ваду 306745)
- DocumentPrivate: обробку дужок зроблено кмітливішою (виправлено ваду 368580)
- ViewInternal: переглянуто подію скидання
- Уможливлено закриття документа, чий файл було вилучено з диска
- KateIconBorder: реалізовано використання символу UTF-8 замість спеціального растрового зображення для позначення динамічного перенесення рядків
- KateIconBorder: забезпечено показ позначки динамічного перенесення рядків
- KateIconBorder: косметичні удосконалення коду
- DocumentPrivate: реалізовано підтримку автоматичного додавання дужок у режимі позначення блоку (виправлено ваду 382213)

### KUnitConversion

- Виправлено перетворення л/100 км на милі на галон (виправлено ваду 378967)

### Бібліотека KWallet

- Реалізовано встановлення належного kwalletd_bin_path
- Реалізовано експортування шляху до виконуваного файла kwalletd для kwallet_pam

### KWayland

- Додано тип вікон CriticalNotification до протоколу PlasmaShellSurface
- Реалізовано інтерфейс сервера wl_eglstream_controller

### KWidgetsAddons

- Оновлено дані kcharselect до Unicode 12.1
- Внутрішня модель KCharSelect: забезпечено рівність rowCount() 0 для коректних індексів

### KWindowSystem

- Впроваджено CriticalNotificationType
- Реалізовано підтримку NET_WM_STATE_FOCUSED
- Додано документацію щодо того, що modToStringUser і stringUserToMod працюють із рядками англійською мовою

### KXMLGUI

- KKeySequenceWidget: усунено показ користувачеві рядків англійською

### NetworkManagerQt

- WireGuard: усунено вимогу щодо того, що «private-key» має бути непорожнім для «private-key-flags»
- WireGuard: реалізовано обхід при помилковому типі прапорця реєстраційних даних
- WireGuard: уможливлено одночасний запит щодо private-key і preshared-keys

### Бібліотеки Plasma

- PlatformComponentsPlugin: виправлено ідентифікатор додатка для QQmlExtensionInterface
- IconItem: вилучено решту невикористаних випадків вживання властивості smooth
- [Вікно] Додано тип CriticalNotification
- Виправлено помилкові назви груп для 22-, 32-піксельних варіантів у audio.svg
- Реалізовано показ мобільної панелі інструментів для роботи з текстом лише при натисканні
- Реалізовано використання нового Kirigami.WheelHandler
- Додано нові розміри піктограм для audio, configure, distribute
- [FrameSvgItem] Оновлено фільтрування при зміні згладжування
- Тема Air/Oxygen: виправлено висоту смужки поступу при використанні hint-bar-size
- Виправлено підтримку stylesheet для audio-volume-medium
- Оновлено піктограми audio, drive, edit, go, list, media, plasmavault так, щоб вони відповідали іншим піктограмам breeze
- Літеру «z» у system.svg вирівняно за піксельною ґраткою
- Реалізовано використання mobiletextcursor з належного простору назв
- [FrameSvgItem] Реалізовано враховування властивості згладжування
- Тема Oxygen: додано фаску для стрілок — допомагає зберігати контур при обертанні
- SvgItem, IconItem: викинуто перевизначення властивості «smooth», реалізовано оновлення вузла при зміні
- Реалізовано підтримку використання gzip для створення svgz у Windows за допомогою 7z
- Тема Air/Oxygen: виправлено відступи стрілок за допомогою hint-*-rotation-center-offset
- Додано придатний до виклику відкритий програмний інтерфейс для надання сигналів contextualActionsAboutToShow
- Годинник у темі Breeze: реалізовано підтримку відступу для тіней від стрілок у Плазмі 5.16
- Реалізовано можливість зберігання файлів SVG без стискання у сховищі коду із наступним встановленням стиснених svgz
- Відокремлено засоби позначення тексту на мобільних пристроях для уникнення рекурсивного імпортування
- Реалізовано використання кращої піктограми та тексту для пункту «Alternatives»
- FrameSvgItem: додано властивість «mask»

### Prison

- Aztec: виправлено фаску, якщо останнє часткове слово коду є однобітовим

### QQC2StyleBridge

- Усунено можливість вкладення Controls до TextField (виправлено ваду 406851)
- Реалізовано показ мобільної панелі інструментів для роботи з текстом лише при натисканні
- [TabBar] Оновлено висоту, якщо TabButtons додаються динамічно
- Реалізовано використання нового Kirigami.WheelHandler
- Реалізовано підтримку нетипових розмірів піктограм для ToolButton
- Вилучено foreach, без якого все компілюється

### Solid

- [Fstab] Додано підтримку для файлових систем, які не є мережевими
- [FsTab] Додано кеш для типу файлової системи на пристрої
- [Fstab] Виконано роботу з приготування до вмикання файлових систем поза NFS/SMB
- Виправлено помилку «Немає учасника із назвою setTime_t у QDateTime» під час збирання (виправлено ваду 405554)

### Підсвічування синтаксису

- Додано підсвічування синтаксичних конструкцій для оболонки fish
- AppArmor: усунено підсвічування визначення змінних та визначено правила альтернативних назв у профілях

### Відомості щодо безпеки

Випущений код підписано за допомогою GPG з використанням такого ключа: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Відбиток основного ключа: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
