---
aliases:
- ../../kde-frameworks-5.71.0
date: 2020-06-06
layout: framework
libCount: 70
---
### Baloo

- Store filename terms just once

### Iconos Brisa

- Improve accuracy of battery percentage icons
- Se ha corregido el icono de tipo MIME para KML.
- Change mouse icon to have better dark theme contrast (bug 406453)
- Require in-source build (bug 421637)
- Se han añadido iconos de lugares de 48 píxeles (fallo 421144).
- Se han añadido los accesos rápidos de iconos de LibreOffice que faltaban.

### Módulos CMake adicionales

- [android] Use newer Qt version in example
- [android] Allow specifying APK install location
- ECMGenerateExportHeader: add generation of *_DEPRECATED_VERSION_BELATED()
- ECMGeneratePriFile: fix for ECM_MKSPECS_INSTALL_DIR being absolute
- ECMGeneratePriFile: make the pri files relocatable
- Suppress find_package_handle_standard_args package name mismatch warning

### Herramientas KDE Doxygen

- Use logo.png file as logo as default, if present
- Change public_example_dir to public_example_dirs, to enable multiple dirs
- Drop code for Python2 support
- Adapt links to repo to invent.kde.org
- Fix link to kde.org impressum
- History pages of KDE4 got merged into general history page
- Unbreak generation with dep diagrams with Python 3 (break Py2)
- Export metalist to json file

### KAuth

- Use ECMGenerateExportHeader to manage deprecated API better

### KBookmarks

- Use UI marker context in more tr() calls
- [KBookmarkMenu] Assign m_actionCollection early to prevent crash (bug 420820)

### KCMUtils

- Add X-KDE-KCM-Args as property, read property in module loader
- Se ha corregido un fallo de la aplicación al cargar aplicaciones KCM externas, como yast (error 421566).
- KSettings::Dialog: avoid duplicate entries due cascading $XDG_DATA_DIRS

### KCompletion

- Wrap also API dox with KCOMPLETION_ENABLE_DEPRECATED_SINCE; use per method
- Use UI marker context in more tr() calls

### KConfig

- Don't try to initialize deprecated SaveOptions enum value
- Add KStandardShortcut::findByName(const QString&amp;) and deprecate find(const char*)
- Fix KStandardShortcut::find(const char*)
- Adjust name of internally-exported method as suggested in D29347
- KAuthorized: export method to reload restrictions

### KConfigWidgets

- [KColorScheme] Remove duplicated code
- Make Header colors fallback to Window colors first
- Introduce the Header color set

### KCoreAddons

- Fix Bug 422291 - Preview of XMPP URI's in KMail (bug 422291)

### KCrash

- Don't invoke qstring localized stuff in critical section

### KDeclarative

- Create kcmshell.openSystemSettings() and kcmshell.openInfoCenter() functions
- Port KKeySequenceItem to QQC2
- Pixel align children of GridViewInternal

### KDED

- Fix blurry icons in titlebar appmenu by adding UseHighDpiPixmaps flag
- Add systemd user service file for kded

### KFileMetaData

- [TaglibExtractor] Add support for Audible "Enhanced Audio" audio books
- Respetar el indicador «extractMetaData».

### KGlobalAccel

- Fix bug with components containing special characters (bug 407139)

### KHolidays

- Se han actualizado las festividades taiwanesas.
- holidayregion.cpp - provide translatable strings for the German regions
- holidays_de-foo - set name field for all German holiday files
- holiday-de-&lt;foo&gt; - where foo is a region is Germany
- Add holiday file for DE-BE (Germany/Berlin)
- holidays/plan2/holiday_gb-sct_en-gb

### KImageFormats

- Se han añadido algunas comprobaciones de sensatez y de límites.

### KIO

- Introduce KIO::OpenUrlJob, a rewrite and replacement for KRun
- KRun: deprecate all static 'run*' methods, with full porting instructions
- KDesktopFileActions: deprecate run/runWithStartup, use OpenUrlJob instead
- Look for kded as runtime dependency
- [kio_http] Parse a FullyEncoded QUrl path with TolerantMode (bug 386406)
- [DelegateAnimationHanlder] Replace deprecated QLinkedList with QList
- RenameDialog: Warn when file sizes are not the same (bug 421557)
- [KSambaShare] Check that both smbd and testparm are available (bug 341263)
- Places: Use Solid::Device::DisplayName for DisplayRole (bug 415281)
- KDirModel: fix hasChildren() regression for trees with files shown (bug 419434)
- file_unix.cpp: when ::rename is used as condition compare its return to -1
- [KNewFileMenu] Permitir la creación de un directorio llamado «~» (error 377978).
- [HostInfo] Set QHostInfo::HostNotFound when a host isn't found in the DNS cache (bug 421878)
- [DeleteJob] Report final numbers after finishing (bug 421914)
- KFileItem: localize timeString (bug 405282)
- [StatJob] Make mostLocalUrl ignore remote (ftp, http...etc) URLs (bug 420985)
- [KUrlNavigatorPlacesSelector] only update once the menu on changes (bug 393977)
- [KProcessRunner] Use only executable name for scope
- [knewfilemenu] Show inline warning when creating items with leading or trailing spaces (bug 421075)
- [KNewFileMenu] Remove redundant slot parameter
- ApplicationLauncherJob: show the Open With dialog if no service was passed
- Make sure the program exists and is executable before we inject kioexec/kdesu/etc
- Fix URL being passed as argument when launching a .desktop file (bug 421364)
- [kio_file] Handle renaming file 'A' to 'a' on FAT32 filesystems
- [CopyJob] Check if destination dir is a symlink (bug 421213)
- Fix service file specifying 'Run in terminal' giving an error code 100 (bug 421374)
- kio_trash: add support for renaming directories in the cache
- Warn if info.keepPassword isn't set
- [CopyJob] Check free space for remote urls before copying and other improvements (bug 418443)
- [kcm trash] Change kcm trash size percent to 2 decimal places
- [CopyJob] Get rid of an old TODO and use QFile::rename()
- [LauncherJobs] Emit description

### Kirigami

- Add section headers in sidebar when all the actions are expandibles
- fix: Padding in overlay sheet header
- Usar un mejor color predeterminado para el cajón global cuando se usa como barra lateral.
- Fix inaccurate comment about how GridUnit is determined
- More reliable parent tree climbing for PageRouter::pushFromObject
- PlaceholderMessage depends on Qt 5.13 even if CMakeLists.txt requires 5.12
- introduce the Header group
- Don't play close animation on close() if sheet is already closed
- Se ha añadido el componente «SwipeNavigator».
- Se ha introducido el componente «Avatar».
- Fix the shaders resource initialisation for static builds
- AboutPage: Add tooltips
- Ensure closestToWhite and closestToBlack
- AboutPage: Add buttons for email, webAddress
- Always use Window colorset for AbstractApplicationHeader (bug 421573)
- Introducción de ImageColors.
- Recommend better width calculation for PlaceholderMessage
- Improve PageRouter API
- Use small font for BasicListItem subtitle
- Se ha añadido el uso de capas en «PagePoolAction».
- Introduce RouterWindow control

### KItemViews

- Use UI marker context in more tr() calls

### KNewStuff

- Don't duplicate error messages in a passive notification (bug 421425)
- Fix incorrect colours in the KNS Quick messagebox (bug 421270)
- Do not mark entry as uninstalled if uninstallation script failed (bug 420312)
- KNS: Deprecate isRemote method and handle parse error properly
- KNS: Do not mark entry as installed if install script failed
- Fix showing updates when the option is selected (bug 416762)
- Se ha corregido la actualización de la selección automática (error 419959).
- Add KPackage support to KNewStuffCore (bug 418466)

### KNotification

- Implement lock-screen visibility control on Android
- Se ha implementado la agrupación de notificaciones en Android.
- Display rich text notification messages on Android
- Se ha implementado el uso de consejos en URL.
- Use UI marker context in more tr() calls
- Se ha implementado la urgencia en las notificaciones de Android.
- Remove checks for notification service and fallback to KPassivePopup

### KQuickCharts

- Simpler background rounding check
- PieChart: Render a torus segment as background when it isn't a full circle
- PieChart: Add a helper function that takes care of rounding torus segments
- PieChart: Expose fromAngle/toAngle to shader
- Always render the background of the pie chart
- Always set background color, even when toAngle != 360
- Allow start/end to be at least 2pi away from each other
- Se ha revisado la implementación de PieChart.
- Fixup and comment sdf_torus_segment sdf function
- Don't explicitly specify smoothing amount when rendering sdfs
- Workaround lack of array function parameters in GLES2 shaders
- Handle differences between core and legacy profiles in line/pie shaders
- Fix a few validation errors
- Update the shader validation script with the new structure
- Add a separate header for GLES3 shaders
- Remove duplicate shaders for core profile, instead use preprocessor
- expose both name and shortName
- support different long/short labels (bug 421578)
- guard model behind a QPointer
- Add MapProxySource

### KRunner

- Do not persist runner whitelist to config
- KRunner fix prepare/teardown signals (bug 420311)
- Detect local files and folders starting with ~

### KService

- Add X-KDE-DBUS-Restricted-Interfaces to Application desktop entry fields
- Add missing compiler deprecation tag for 5-args KServiceAction constructor

### KTextEditor

- Add .diff to the file-changed-diff to enable mime detection on windows
- scrollbar minimap: performance: delay update for inactive documents
- Make text always align with font base line
- Revert "Store and fetch complete view config in and from session config"
- Fix modified line marker in kate minimap

### KWidgetsAddons

- Be noisy about deprecated KPageWidgetItem::setHeader(empty-non-null string)

### KXMLGUI

- [KMainWindow] Invoke QIcon::setFallbackThemeName (later) (bug 402172)

### KXmlRpcClient

- Mark KXmlRpcClient as porting aid

### Framework de Plasma

- PC3: Add separator to toolbar
- Permitir el uso del grupo de colores para cabeceras.
- Implement scroll and drag adjustment of values for SpinBox control
- Use font: instead of font.pointSize: where possible
- kirigamiplasmastyle: Add AbstractApplicationHeader implementation
- Avoid potential disconnect of all signals in IconItem (bug 421170)
- Use small font for ExpandableListItem subtitle
- Add smallFont to Kirigami plasma style
- [Plasmoid Heading] Draw the heading only when there is an SVG in the theme

### QQC2StyleBridge

- Add ToolSeparator styling
- Remove "pressed" from CheckIndicator "on" state (bug 421695)
- Support the Header group
- Implement smallFont in Kirigami plugin

### Solid

- Add a QString Solid::Device::displayName, used in Fstab Device for network mounts (bug 415281)

### Sonnet

- Allow overriding to disable auto language detection (bug 394347)

### Resaltado de sintaxis

- Update Raku extensions in Markdown blocks
- Raku: fix fenced code blocks in Markdown
- Assign "Identifier" attribute to opening double quote instead of "Comment" (bug 421445)
- Bash: fix comments after escapes (bug 418876)
- LaTeX: fix folding in end{...} and in regions markers BEGIN-END (bug 419125)

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.
