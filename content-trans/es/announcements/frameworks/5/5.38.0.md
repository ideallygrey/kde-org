---
aliases:
- ../../kde-frameworks-5.38.0
date: 2017-09-09
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
Ausente en el anuncio del mes pasado: KF5 incluye un nuevo framework, Kirigami, que es un conjunto de componentes QtQuick para generar interfaces de usuario basadas en las directrices UX de KDE.

### Baloo

- Se ha corregido la búsqueda basada en directorios.

### Módulos CMake adicionales

- Definir «CMAKE_*_OUTPUT_5.38» para ejecutar pruebas sin instalación.
- Incluir un módulo para encontrar las importaciones de «qml» como dependencias en tiempo de ejecución.

### Integración con Frameworks

- Devolver el icono de alta resolución para borrar la edición de líneas.
- Se ha corregido la aceptación de diálogos con Ctrl+Intro cuando se ha cambiado el nombre de los botones.

### KActivitiesStats

- Refactorización de la consulta, que combina recursos enlazados y usados.
- Volver a cargar el modelo cuando el recurso se desenlaza.
- Se ha corregido la consulta cuando se fusionan recursos enlazados y usados.

### KConfig

- Se han corregido las etiquetas de las acciones «DeleteFile/RenameFile» (error 382450).
- kconfigini: Eliminar los espacios iniciales cuando se leen valores de entradas (error 310674).

### KConfigWidgets

- Marcar como obsoletas «KStandardAction::Help» y «KStandardAction::SaveOptions».
- Se han corregido las etiquetas de las acciones «DeleteFile/RenameFile» (error 382450).
- Usar «document-close» como icono para «KStandardAction::close».

### KCoreAddons

- DesktopFileParser: Añadir una búsqueda a la que recurrir en «:/kservicetypes5/*».
- Permitir complementos no instalados en kcoreaddons_add_plugin.
- desktopfileparser: Se ha corregido el análisis de clave/valor no compatible (error 310674).

### KDED

- Permitir el uso de «X-KDE-OnlyShowOnQtPlatforms».

### KDocTools

- CMake: Se ha corregido el acortamiento del nombre de destino cuando el directorio «build» contiene caracteres especiales (error 377573).
- Añadir «CC BY-SA 4.0 International» y definirla como predeterminada.

### KGlobalAccel

- KGlobalAccel: Se ha portado al nuevo método «symXModXToKeyQt» de «KKeyServer» para corregir las teclas del bloque numérico (error 183458).

### KInit

- klauncher: Se ha corregido la coincidencia de «appId» para aplicaciones flatpak.

### KIO

- Se ha portado el módulo de accesos rápidos de la web de KServiceTypeTrader a KPluginLoader::findPlugins.
- [KFilePropsPlugin] Formato local de «totalSize» durante el cálculo.
- KIO: Se ha corregido una fuga de memoria muy antigua al salir.
- Añadir la capacidad de filtrar por tipo MIME a «KUrlCompletion».
- KIO: Portar los complementos de filtrado de URI de «KServiceTypeTrader» a «json+KPluginMetaData».
- [KUrlNavigator] Emitir «tabRequested» cuando se hace un clic con el botón central en un lugar del menú (error 304589).
- [KUrlNavigator] Emitir «tabRequested» cuando se hace un clic con el botón central en el selector de lugares (error 304589).
- [KACLEditWidget] Permitir doble clic para editar la entrada.
- [kiocore] Corregir el error de lógica en el «commit» anterior.
- [kiocore] Comprobar si «klauncher» está funcionando o no.
- Mensajes de límite de velocidad real INF_PROCESSED_SIZE (error 383843).
- No borrar el almacén de certificados de la AC SSL de Qt.
- [KDesktopPropsPlugin] Crear el directorio de destino si no existe.
- [Esclavo KIO de archivos] Corregir la aplicación de atributos de archivo especiales (fallo 365795).
- Eliminar la comprobación de bucle ocupado en «TransferJobPrivate::slotDataReqFromDevice».
- Hacer que «kiod5» sea un «agente» en Mac.
- Corregir la carga de proxys manual en el KCM de proxy.

### Kirigami

- Ocultar las barras de desplazamiento cuando no sean de utilidad.
- Añadir ejemplo básico para ajustar el asa arrastrable da anchura de la columna.
- Posicionamiento de capas «ider» en asas.
- Corregir la colocación del asa cuando se solapa con la última página.
- No mostrar un asa falsa en la última columna.
- No almacenar cosas en los delegados (error 383741).
- Como ya hemos definido «keyNavigationEnabled», definimos «wraps» también.
- Mejor alineación a la izquierda para el botón de retroceso (fallo 383751).
- No tener en cuenta la cabecera dos veces al desplazar (fallo 383725).
- No envolver nunca las etiquetas de las cabeceras.
- Solucionar FIXME: eliminar «resetTimer» (fallo 383772).
- No desplazar «applicationheader» si no estamos en móvil.
- Añadir una propiedad para ocultar el separador «PageRow» que coincide con «AbstractListItem».
- Corregir el desplazamiento con «originY» y flujo «bottomtotop».
- Deshacerse de las advertencias sobre la definición de los tamaños del píxel y de punto a la vez.
- No disparar el modo alcanzable en las vistas invertidas.
- Tener en cuenta el pie de la página.
- Añadir un ejemplo algo más complejo de una aplicación de chat.
- Más a prueba de fallos para encontrar el pie de página correcto.
- Verificar la validez del elemento antes de usarlo.
- Respetar la posición de la capa para «isCurrentPage».
- Usar una animación en lugar de un animador (error 383761).
- Dejar el espacio necesario para el pie de página, si es posible.
- Mejor atenuador para cajones de elementos de la aplicación.
- Oscurecimiento de fondo para «applicationitem».
- Corregir correctamente los márgenes del botón de retroceso.
- Márgenes correctos para el botón de retroceso.
- Menos advertencias en «ApplicationHeader».
- No usar el escalado de Plasma para los tamaños de los iconos.
- Nuevo aspecto para las asas.

### KItemViews

### KConfigWidgets

- Inicializar el estado del botón «Pausa» en el rastreador de widgets.

### KNotification

- No bloquear el inicio del servicio de notificaciones (fallo 382444).

### Framework KPackage

- Refactorizar «kpackagetool» eliminando las opciones de «string».

### KRunner

- Borrar las acciones anteriores al actualizar.
- Añadir lanzadores remotos sobre DBus.

### KTextEditor

- Adaptar la API de guiones documento/vista a la solución basada en QJSValue.
- Mostrar los iconos en el borde de iconos del menú de contexto.
- Sustituir «KStandardAction::PasteText» con «KStandardAction::Paste».
- Permitir el escalado fraccionario al generar la vista previa de la barra lateral.
- Cambiar de QtScript a QtQml.

### KWayland

- Tratar los búferes RGB de entrada como premultiplicados.
- Actualizar las salidas de «SurfaceInterface» cuando se destruye una salida global.
- Destrucción de salida de pista de «KWayland::Client::Surface».
- Evitar el envío de ofertas de datos desde una fuente no válida (error 383054).

### KWidgetsAddons

- Simplificar «setContents» dejando que Qt haga la mayor parte del trabajo.
- KSqueezedTextLabel: Se ha añadido «isSqueezed()» por conveniencia.
- KSqueezedTextLabel: Pequeñas mejoras en la documentación de la API.
- [KPasswordLineEdit] Establecer el proxy de enfoque en la edición de línea (error 383653).
- [KPasswordDialog] Reiniciar la propiedad de la geometría.

### KWindowSystem

- KKeyServer: Corregir el manejo de «KeypadModifier» (fallo 183458).

### KXMLGUI

- Ahorrar un buen número de llamadas a «stat()» durante el inicio de la aplicación.
- Se ha corregido la posición de KHelpMenu en Wayland (error 384193).
- Descartar el manejo del clic con el botón central que no funcionaba (fallo 383162).
- KUndoActions: Usar «actionCollection» para asignar el acceso rápido.

### Framework de Plasma

- [ConfigModel] Prevenir la adición de «ConfigCategory» nulo.
- [ConfigModel] Permitir añadir y eliminar «ConfigCategory» en el código fuente (fallo 372090).
- [EventPluginsManager] Exponer «pluginPath» en el modelo.
- [Elemento icono] No desconfigurar «imagePath» innecesariamente.
- [FrameSvg] Usar «QPixmap::mask()» en lugar de la enrevesada forma obsoleta a través de «alphaChannel()».
- [FrameSvgItem] Crear el objeto «margins/fixedMargins» bajo demanda.
- Se ha corregido el estado de marcado en los elementos del menú.
- Forzar el estilo de Plasma para QQC2 en las miniaplicaciones.
- Instalar la carpeta «PlasmaComponents.3/private».
- Descartar los restos de los temas «locolor».
- [Tema] Usar «SimpleConfig» de «KConfig».
- Evitar algunas búsquedas innecesarias del contenido del tema.
- Ignorar eventos de cambio de tamaño falsos a tamaños vacíos (error 382340).

### Resaltado de sintaxis

- Añadir la definición de sintaxis para las listas de filtrado de «Adblock Plus».
- Se ha reescrito la definición de la sintaxis de Sieve.
- Se ha añadido resaltado de sintaxis para los archivos de configuración de QDoc.
- Se ha añadido resaltado de sintaxis para Tiger.
- Usar secuencias de escape con los guiones en las expresiones regulares de «rest.xml» (error 383632).
- Corrección: El texto sin formato se resalta como «powershell».
- Se ha añadido resaltado de sintaxis para Metamath.
- Resaltado de sintaxis de «Rebased Less» en «SCSS one» (error 369277).
- Se ha añadido el resaltado de sintaxis para Pony.
- Se ha reescrito la definición de la sintaxis de correo electrónico.

### Información de seguridad

El código publicado se ha firmado con GPG usando la siguiente clave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt;. Huella digital de la clave primaria: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB.

Puede comentar esta versión y compartir sus ideas en la sección de comentarios de <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>el artículo de dot</a>.
