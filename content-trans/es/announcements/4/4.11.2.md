---
aliases:
- ../announce-4.11.2
date: 2013-10-01
description: KDE lanza los Espacios de trabajo Plasma, las Aplicaciones y la Plataforma
  4.11.2.
title: KDE lanza las actualizaciones de octubre para los Espacios de trabajo de Plasma,
  las Aplicaciones y la Plataforma
---
1 de octubre de 2013. KDE ha lanzado hoy actualizaciones para sus Espacios de trabajo, Aplicaciones y Plataforma de desarrollo. Esta actualización es la primera de una serie de actualizaciones de estabilización mensuales para la serie 4.11. Tal como se había anunciado en la versión, se continuará actualizando los Espacios de trabajo durante los próximos dos años. Esta versión solo contiene soluciones de errores y será una actualización agradable y segura para todo el mundo.

Más de 70 soluciones de errores registrados, entre ellas, mejoras para el gestor de ventanas Kwin, para el administrador de archivos Dolphin y para la suite de gestión de información personal Kontact. También hay muchas mejoras en cuanto a la estabilidad y la habitual incorporación de nuevas traducciones.

Se puede encontrar una <a href='https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=RESOLVED&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=2013-06-01&amp;chfieldto=Now&amp;chfield=cf_versionfixedin&amp;chfieldvalue=4.11.2&amp;cmdtype=doit&amp;order=Bug+Number&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0='>lista</a> más completa de los cambios en el registro de problemas de KDE y más detalles sobre la lista de cambios de la versión 4.11.2 en los registros de Git.

Para descargar el código fuente de los paquetes que se deben instalar, visite la <a href='/info/4/4.11.2'>Página de información sobre 4.11.2</a>. Si desea encontrar más información sobre la versión 4.11 de los Espacios de trabajo, las Aplicaciones y la Plataforma de Desarrollo de KDE, consulte las <a href='/announcements/4.11/'>notas del lanzamiento de 4.11</a>.

{{< figure class="text-center img-size-medium" src="/announcements/4/4.11.0/screenshots/send-later.png" caption=`El nuevo flujo de trabajo «enviar más tarde» de Kontact` width="600px">}}

El software de KDE, que incluye todas las bibliotecas y aplicaciones, está disponible libremente bajo licencias de Código Abierto. Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.11.2'>download.kde.org</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> actuales.
