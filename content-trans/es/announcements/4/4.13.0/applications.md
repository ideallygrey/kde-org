---
date: '2014-04-16'
description: KDE lanza las Aplicaciones y la Plataforma 4.13
hidden: true
title: Las Aplicaciones de KDE 4.13 se benefician de la nueva búsqueda semántica e
  introducen nuevas funcionalidades
---
La comunidad de KDE se enorgullece de anunciar la última de las actualizaciones mayores de las Aplicaciones de KDE, que aportan nuevas funcionalidades y soluciones de errores. Kontact (el gestor de información personal) ha sido objeto de una intensa actividad, que se beneficia de las mejoras de la tecnología de búsqueda semántica de KDE y proporciona nuevas características. El visor de documentos Okular y el editor de texto avanzado Kate contienen mejoras relacionadas con la interfaz y con funcionalidades. En las áreas de educación y de juego se introduce el nuevo entrenador de habla extranjera Artikulate; Marble (el mapamundi del escritorio) implementa el Sol, la Luna, los planetas, rutas en bicicleta y el uso de millas náuticas. Palapeli (la aplicación de rompecabezas tipo puzle) ha avanzado a capacidades y dimensiones sin precedentes.

{{<figure src="/announcements/4/4.13.0/screenshots/applications.png" class="text-center" width="600px">}}

## KDE Kontact introduce nuevas funcionalidades y mayor velocidad

La suite Kontact de KDE introduce una serie de funcionalidades en sus distintos componentes. KMail permite usar almacenamiento en la nube y mejora el uso de filtros Sieve en el lado del servidor; KNotes puede generar alarmas e incorpora funcionalidades de búsqueda; también se han implementado muchas mejoras en la capa de caché de datos de Kontact, acelerando casi todas las operaciones.

### Uso de almacenamiento en la nube

KMail introduce el uso de servicios de almacenamiento en la nube, mediante el que se pueden almacenar en servicios de nube grandes archivos adjuntos para incluirlos como enlaces en mensajes de correo electrónico. Los servicios permitidos incluyen Dropbox, Box, KolabServer, YouSendIt, UbuntuOne, Hubic y una opción genérica para WebDav. Una herramienta de <em>gestión de servicios de almacenamiento</em> le ayuda a gestionar archivos en estos servicios.

{{<figure src="/announcements/4/4.13.0/screenshots/CloudStorageSupport.png" class="text-center" width="600px">}}

### Uso muy mejorado de filtros Sieve

Los filtros Sieve, una tecnología que permite a KMail el uso de filtros en los servidores, pueden gestionar respuestas de vacaciones en diversos servidores. La herramienta KSieveEditor le permite editar filtros Sieve sin necesidad de añadir el servidor a Kontact.

### Otros cambios

La barra de filtrado rápido contiene una pequeña mejora en la interfaz de usuario y se beneficia en gran medida de las mejoras de la función de búsqueda introducida con el lanzamiento de la Plataforma de Desarrollo de KDE 4.13. La búsqueda se realiza mucho más rápidamente y es más fiable. El compositor introduce un acortador de URL, incrementando las herramientas de traducción y de fragmentos de texto existentes.

Las etiquetas y anotaciones de datos PIM se guardan ahora en Akonadi. En futuras versiones también se almacenarán en los servidores (IMAP o Kolab), permitiendo compartir etiquetas entre distintos equipos. En Akonadi se ha añadido la implementación de la API de Google Drive. También se permite el uso de complementos de búsqueda proporcionados por terceras partes (lo que significa que el resultado se puede obtener muy rápidamente) y se puede buscar en el servidor (la búsqueda de elementos no indexados por un servicio de indexación local).

### KNotes, KAddressbook

Se ha realizado un intenso trabajo en KNotes, corrigiendo múltiples errores y algún comportamiento molesto. Es nueva la posibilidad de fijar alarmas en las notas, así como la búsqueda en las notas (<a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a> tiene más información). KAddressbook permite ahora imprimir contactos (más detalles <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>).

### Mejoras de rendimiento

El rendimiento de Kontact se ha mejorado notablemente en esta versión. Algunas mejoras se deben a la integración de la nueva infraestructura de <a href='http://dot.kde.org/2014/02/24/kdes-next-generation-semantic-search'>búsqueda semántica</a> de KDE, así como al intenso trabajo realizado en la capa de caché de datos y en la carga de datos por parte de KMail. Se ha llevado a cabo un notable trabajo para mejorar el uso de bases de datos PostgreSQL. En los siguientes enlaces puede encontrar más información y detalles sobre los cambios relacionados con el rendimiento:

- Mejoras de almacenamiento: <a href='http://www.progdan.cz/2013/11/kde-pim-sprint-report/'>informe del esprint</a>
- Velocidad y reducción del tamaño de la base de datos: <a href='http://lists.kde.org/?l=kde-pim&amp;m=138496023016075&amp;w=2'>lista de distribución</a>
- Optimización del acceso a carpetas: <a href='https://git.reviewboard.kde.org/r/113918/'>comisión de revisión</a>

### KNotes, KAddressbook

Se ha realizado un intenso trabajo en KNotes, corrigiendo múltiples errores y algún comportamiento molesto. Es nueva la posibilidad de fijar alarmas en las notas, así como la búsqueda en las notas (<a href='http://www.aegiap.eu/kdeblog/2014/03/whats-new-in-kdepim-4-13-knotes/'>aquí</a> tiene más información). KAddressbook permite ahora imprimir contactos (más detalles <a href='http://www.aegiap.eu/kdeblog/2013/11/new-in-kdepim-4-12-kaddressbook/'>aquí</a>).

## Okular refina la interfaz de usuario

Este lanzamiento del lector de documentos Okular trae cierto número de mejoras. Ahora es posible abrir múltiples archivos PDF en una única ejecución de Okular gracias al uso de pestañas. Existe un nuevo modo de ampliación con el ratón y se usa la información de la resolución de la pantalla actual para mostrar documentos PDF, mejorando el aspecto de los mismos. En el modo de presentaciones se ha incluido un nuevo botón para reproducirlas. También hay mejoras en las acciones de deshacer y rehacer.

{{<figure src="/announcements/4/4.13.0/screenshots/okular.png" class="text-center" width="600px">}}

## Kate introduce una barra de estado mejorada, el emparejamiento animado de paréntesis y complementos avanzados

La última versión del editor de texto avanzado introduce <a href='http://kate-editor.org/2013/11/06/animated-bracket-matching-in-kate-part/'>emparejamiento animado de paréntesis</a>, cambios para permitir el <a href='http://dot.kde.org/2014/01/20/kde-commit-digest-5th-january-2014'>uso de teclados con AltGr activado en el modo «vim»</a> y diversas mejoras en los complementos de Kate, especialmente en el área de la implementación de Python y en el <a href='http://kate-editor.org/2014/03/16/coming-in-4-13-improvements-in-the-build-plugin/'>complemento para compilar</a>. Se añaden una nueva <a href='http://kate-editor.org/2014/01/23/katekdevelop-sprint-status-bar-take-2/'>barra de estado mejorada</a> que permite realizar acciones directas, como cambiar los ajustes de sangrado, codificación y resaltado, una nueva barra de pestañas en cada vista, el uso de terminación automática de código para <a href='http://kate-editor.org/2014/02/20/lumen-a-code-completion-plugin-for-the-d-programming-language/'>el lenguaje de programación D</a> y <a href='http://kate-editor.org/2014/02/02/katekdevelop-sprint-wrap-up/'>más cosas</a>. El equipo ha <a href='http://kate-editor.org/2014/03/18/kate-whats-cool-and-what-should-be-improved/'>solicitado la colaboración de los usuarios para mejorar Kate</a> y ha desplazado parte de su atención a la adaptación a Frameworks 5.

## Diversas funcionalidades por todas partes

Konsole proporciona cierta flexibilidad adicional al permitir hojas de estilo personalizadas para controlar las barras de pestañas. Los perfiles pueden guardar ahora el tamaño deseado de filas y columnas. Vea más detalles <a href='http://blogs.kde.org/2014/03/16/konsole-new-features-213'>aquí</a>.

Umbrello permite duplicar diagramas e introduce menús contextuales inteligentes que ajustan su contenido según el elemento gráfico seleccionado. También se ha mejorado la implementación para deshacer acciones y el uso de propiedades visuales. Gwenview <a href='http://agateau.com/2013/12/12/whats-new-in-gwenview-4.12/'>incorpora la vista previa de imágenes en formato RAW</a>.

{{<figure src="/announcements/4/4.13.0/screenshots/marble.png" class="text-center" width="600px">}}

El mezclador de sonido KMix permite control remoto usando el protocolo de comunicación entre procesos DBUS (<a href='http://kmix5.wordpress.com/2013/12/28/kmix-dbus-remote-control/'>más detalles</a>), incluye más acciones en el menú de sonido y un nuevo diálogo de configuración (<a href='http://kmix5.wordpress.com/2013/12/23/352/'>más detalles</a>), así como diversas correcciones de errores y pequeñas mejoras.

La interfaz de búsqueda de Dolphin se ha modificado para aprovechar la nueva infraestructura de búsqueda y contiene más mejoras de rendimiento. Para más detalles, consulte este <a href='http://freininghaus.wordpress.com/2013/12/12/a-brief-history-of-dolphins-performance-and-memory-usage'>resumen del trabajo de optimización llevado a cabo el pasado año</a>.

KHelpcenter añade la ordenación alfabética de los módulos y una reorganización de categorías para facilitar su uso.

## Juegos y aplicaciones educativas

Las aplicaciones educativas y los juegos de KDE han recibido muchas actualizaciones en este lanzamiento. La aplicación de rompecabezas de tipo puzle de KDE, Palapeli, contiene <a href='http://techbase.kde.org/Schedules/KDE4/4.13_Feature_Plan#kdegames'>nuevas y excelentes características</a> que facilitan la resolución de grandes rompecabezas (de hasta 10.000 piezas) para quienes se atrevan a aceptar el reto. KNavalBattle muestra las posiciones del los barcos enemigos cuando se acaba la partida para que pueda ver dónde se ha equivocado.

{{<figure src="/announcements/4/4.13.0/screenshots/palapeli.png" class="text-center" width="600px">}}

Las aplicaciones educativas de KDE contienen nuevas funcionalidades. KStars incorpora una interfaz de guiones mediante D-BUS y puede usar la API del servicio web astrometry.net para optimizar el uso de memoria. Cantor permite el uso de resaltado de sintaxis en el editor de guiones y puede usar en el editor los motores Scilab y Python 2. La herramienta educativa de mapas y navegación Marble incluye ahora las posiciones del <a href='http://kovalevskyy.tumblr.com/post/71835769570/news-from-marble-introducing-sun-and-the-moon'>Sol, la Luna</a> y los <a href='http://kovalevskyy.tumblr.com/post/72073986685/news-from-marble-planets'>planetas</a>, y permite la <a href='http://ematirov.blogspot.ch/2014/01/tours-and-movie-capture-in-marble.html'>captura de películas durante las visitas virtuales</a>. Las rutas en bicicleta se han mejorado al añadir el uso de cyclestreet.net. También se pueden usar millas náuticas y podrá ahora abrir Marble al pulsar un <a href='http://en.wikipedia.org/wiki/Geo_URI'>URI geográfico</a>.

#### Instalación de las aplicaciones de KDE

El software de KDE, incluidas todas sus bibliotecas y aplicaciones, está libremente disponible bajo licencias de Código Abierto. El software de KDE funciona sobre diversas configuraciones de hardware y arquitecturas de CPU, como ARM y x86, sistemas operativos y funciona con cualquier tipo de gestor de ventanas o entorno de escritorio. Además de Linux y otros sistemas operativos basados en UNIX, puede encontrar versiones para Microsoft Windows de la mayoría de las aplicaciones de KDE en el sitio web <a href='http://windows.kde.org'>KDE software on Windows</a> y versiones para Apple Mac OS X en el sitio web <a href='http://mac.kde.org/'>KDE software on Mac</a>. En la web puede encontrar compilaciones experimentales de aplicaciones de KDE para diversas plataformas móviles como MeeGo, MS Windows Mobile y Symbian, aunque en la actualidad no se utilizan. <a href='http://plasma-active.org'>Plasma Active</a> es una experiencia de usuario para una amplia variedad de dispositivos, como tabletas y otro tipo de hardware móvil.

Puede obtener el software de KDE en forma de código fuente y distintos formatos binarios en <a href='http://download.kde.org/stable/4.13.0'>download.kde.org</a>, y también en <a href='/download'>CD-ROM</a> o con cualquiera de los <a href='/distributions'>principales sistemas GNU/Linux y UNIX</a> de la actualidad.

##### Paquetes

Algunos distribuidores del SO Linux/UNIX tienen la gentileza de proporcionar paquetes binarios de 4.13.0 para algunas versiones de sus distribuciones, y en otros casos han sido voluntarios de la comunidad los que lo han hecho posible.

##### Ubicación de los paquetes

Para una lista actual de los paquetes binarios disponibles de los que el Equipo de Lanzamiento de KDE ha sido notificado, visite la <a href='http://community.kde.org/KDE_SC/Binary_Packages#KDE_SC_4.13.0'>Wiki de la Comunidad</a>.

La totalidad del código fuente de %[1] se puede <a href='/info/4/4.13.0'>descargar libremente</a>. Dispone de instrucciones sobre cómo compilar e instalar el software de KDE 4.13.0 en la <a href='/info/4/4.13.0#binary'>Página de información sobre 4.13.0</a>.

#### Requisitos del sistema

Para obtener lo máximo de estos lanzamientos, le recomendamos que use una versión reciente de Qt, como la 4.8.4. Esto es necesario para asegurar una experiencia estable y con rendimiento, ya que algunas de las mejoras realizadas en el software de KDE están hechas realmente en la infraestructura subyacente Qt.

Para hacer un uso completo de las funcionalidades del software de KDE, también le recomendamos que use en su sistema los últimos controladores gráficos, ya que pueden mejorar sustancialmente la experiencia del usuario, tanto en las funcionalidades opcionales como en el rendimiento y la estabilidad general.
