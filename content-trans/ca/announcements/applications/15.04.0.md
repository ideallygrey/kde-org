---
aliases:
- ../announce-applications-15.04.0
changelog: true
date: '2015-04-15'
description: Es distribueixen les aplicacions 15.04 del KDE.
layout: application
title: KDE distribueix les aplicacions 15.04.0 del KDE
version: 15.04.0
---
15 d'abril de 2015. Avui KDE distribueix la versió 15.04 de les aplicacions del KDE. Amb aquest llançament hi ha un total de 72 aplicacions adaptades als <a href='https://dot.kde.org/2013/09/25/frameworks-5'>Frameworks 5 del KDE</a>. L'equip està tractant de donar la millor qualitat a l'escriptori i a aquestes aplicacions. Així que esperem comptar amb vós per enviar els vostres comentaris.

Amb aquest llançament hi ha diverses incorporacions noves a la llista d'aplicacions basades en els Frameworks 5, incloent-hi <a href='https://www.kde.org/applications/education/khangman/'>KHangMan</a>, <a href='https://www.kde.org/applications/education/rocs/'>Rocs</a>, <a href='https://www.kde.org/applications/education/cantor/'>Cantor</a>, <a href='https://www.kde.org/applications/development/kompare'>Kompare</a>, <a href='https://kdenlive.org/'>Kdenlive</a>, <a href='https://userbase.kde.org/Telepathy'>KDE Telepathy</a> i <a href='https://games.kde.org/'>alguns jocs del KDE</a>.

El Kdenlive és un dels millors programes d'edició de vídeo no lineal disponible. Recentment ha finalitzat el seu <a href='https://community.kde.org/Incubator'>procés d'incubació</a> per a esdevenir un projecte oficial del KDE i s'ha adaptat als Frameworks 5 del KDE. L'equip darrere aquesta obra mestra ha decidit que el Kdenlive s'hauria de publicar junt amb les aplicacions KDE. Algunes funcionalitats noves són la funció de desament automàtic dels projectes nous i una esmena a l'estabilització dels clips.

El Telepathy del KDE és l'eina per a la missatgeria instantània. S'ha adaptat als Frameworks 5 del KDE i a les Qt5 i és un membre nou dels llançaments de les aplicacions del KDE. És gairebé complet, excepte encara manca la interfície d'usuari de trucades d'àudio i vídeo.

Quan sigui possible, KDE usa la tecnologia existent com ja s'ha fet amb el KAccounts nou que també s'utilitza en el SailfishOS i l'Unity de Canonical. Amb les aplicacions del KDE, actualment només s'utilitza en el Telepathy del KDE. Però en el futur hi haurà un ús més ampli en les aplicacions com el Kontact i l'Akonadi.

En el <a href='https://edu.kde.org/'>mòdul educatiu del KDE</a>, el Cantor ha rebut algunes funcionalitats noves en la seva implementació del Python: un dorsal nou del Python 3 i categories noves de «Obtén les novetats». El Rocs s'ha capgirat: el nucli de la teoria de grafs s'ha reescrit, s'ha eliminat la separació d'estructures de dades i s'ha introduït un document de graf general com una entitat de graf central, així com una revisió gran de l'API per crear scripts per algorismes de grafs que ara proporciona una sola API unificada. El KHangMan s'ha adaptat al QtQuick i se li ha donat una capa fresca de pintura en el procés. I el Kanagram ha rebut un mode nou de 2 jugadors i les lletres ara són botons que es poden clicar i es poden teclejar com abans.

A part de les esmenes d'errors normals, aquesta vegada l'<a href='https://www.kde.org/applications/development/umbrello/'>Umbrello</a> ha rebut millores d'usabilitat i estabilitat. A més, la funció de cerca ara es pot limitar per categoria: classe, interfície, paquet, operacions, o atributs.
