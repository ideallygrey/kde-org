---
aliases:
- ../../kde-frameworks-5.15.0
date: 2015-10-10
layout: framework
libCount: 60
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Esmenar la gestió de límit/desplaçament a SearchStore::exec
- Tornar a crear l'índex del Baloo
- balooctl config: Afegir opcions per definir/veure «onlyBasicIndexing»
- Adaptar la verificació del «balooctl» al funcionament amb la nova arquitectura (error 353011)
- FileContentIndexer: Esmenar l'emissió duplicada de «filePath»
- UnindexedFileIterator: «mtime» és «quint32» no pas «quint64»
- Transaction: Esmenar un altre error del Dbi
- Transaction: Esmenar documentMTime() i documentCTime() usant Dbi erronis.
- Transaction::checkPostingDbInTermsDb: Optimització de codi
- Esmenar els avisos de D-Bus
- Balooctl: Afegir l'ordre checkDb
- balooctl config: Afegir «exclude filter»
- KF5Baloo: Assegura que les interfícies de D-Bus s'han generat abans que s'usin. (error 353308)
- Evitar l'ús de QByteArray::fromRawData
- Eliminar «baloo-monitor» del «baloo»
- TagListJob: Emetre un error quan falla en obrir la base de dades
- No ignorar els subtermes si no es troben
- Codi més net per fallades del Baloo::File::load() en fallar l'obertura de la BBDD.
- Fer que el «balooctl» usi IndexerConfig en lloc de manipular directament el «baloofilerc»
- Millorar la «i18n» per al «balooshow»
- Fer que el «balooshow» falli adequadament si no es pot obrir la base de dades.
- Fer que falli Baloo::File::load() si la base de dades no està oberta. (error 353049)
- IndexerConfig: Afegir el mètode refresh()
- inotify: No simular un esdeveniment «closedWrite» després de moure sense cap galeta
- ExtractorProcess: Eliminar la n extra al final del «filePath»
- baloo_file_extractor: Invocar QProcess::close abans de destruir el QProcess
- baloomonitorplugin/balooctl: Internacionalitzar l'estat de l'indexador.
- BalooCtl: Afegir una opció «config»
- Fer més presentable «baloosearch»
- Eliminar fitxers EventMonitor buits
- BalooShow: Mostrar més informació quan els «ids» no coincideixen
- BalooShow: En cridar amb una «id», verificar si la «id» és correcte
- Afegir una classe FileInfo
- Afegir comprovació d'errors en diversos punts de manera que el Baloo no falli quan es desactiva. (error 352454)
- Esmenar el Baloo quan no respecta l'opció de configuració «només indexació bàsica»
- Monitor: Recuperar el temps restant en iniciar-se
- Usar les crides al mètode real en el MainAdaptor en lloc del QMetaObject::invokeMethod
- Afegir la interfície org.kde.baloo a l'objecte arrel per compatibilitat inversa
- Esmenar la cadena de data mostrada en la barra d'adreces per l'adaptació a QDate
- Afegir un retard després de cada fitxer en lloc de cada lot
- Eliminar la dependència de Qt::Widgets del baloo_file
- Eliminar codi sense ús del baloo_file_extractor
- Afegir el monitor del Baloo o el connector experimental en QML
- Fer segur el fil «consulta del temps restant»
- kioslaves: Afegir una sobreescriptura que manca per a funcions virtuals
- Extractor: Definir l'«applicationData» després de construir l'aplicació
- Query: Implementar el «offset»
- Balooctl: Afegir --version i --help (error 351645)
- Eliminar el funcionament del KAuth per augmentar el monitoratge màxim dels «inotify» si el comptador és massa baix (error 351602)

### BluezQt

- Esmenar una fallada al «fakebluez» en el «obexmanagertest» amb ASAN
- Declaració prèvia de totes les classes exportades a types.h
- ObexTransfer: Donar error quan s'elimina la sessió de transferència
- Utils: Mantenir els apuntadors a les instàncies dels gestors
- ObexTransfer: Definir error quan org.bluez.obex falla

### Mòduls extres del CMake

- Actualitzar la memòria cau d'icones GTK en instal·lar icones.
- Eliminar una solució temporal per retardar l'execució a l'Android
- ECMEnableSanitizers: El sanejador no definit està implementat en el gcc 4.9
- Desactivar la detecció de X11, XCB, etc. en l'OS X
- Cercar els fitxers en el prefix instal·lat en lloc del camí del prefix
- Usar les Qt5 per a especificar quin és el prefix d'instal·lació de les Qt5
- Afegir la definició ANDROID tal com es necessita a qsystemdetection.h.

### Integració del marc de treball

- Esmenar un problema aleatori en diàleg de fitxer que no es mostra. (error 350758)

### KActivities

- Usar una funció de coincidència personalitzada en lloc del «glob» del SQLite. (error 352574)
- Esmenat un problema amb afegir un recurs nou al model

### KCodecs

- Esmenar una fallada en l'UnicodeGroupProber::HandleData amb cadenes curtes

### KConfig

- Marcar el kconfig-compiler com a una eina sense IGU

### KCoreAddons

- KShell::splitArgs: Només l'espai ASCII és un separador, no l'espai Unicode U+3000 (error 345140)
- KDirWatch: Esmenar una fallada quan un destructor estàtic global usa KDirWatch::self() (error 353080)
- Esmenar una fallada quan el KDirWatch usa en Q_GLOBAL_STATIC.
- KDirWatch: Esmena de seguretat en el fil
- Clarificar com definir els arguments del constructor KAboutData.

### KCrash

- KCrash: Passar «cwd» al «kdeinit» en reiniciar automàticament l'aplicació via el «kdeinit». (error 337760)
- Afegir KCrash::initialize() de manera que les aplicacions i el connector de la plataforma poden activar explícitament el KCrash.
- Desactivar l'ASAN si és actiu

### KDeclarative

- Petites millores a ColumnProxyModel
- Fer possible que les aplicacions coneguin el camí a «homeDir»
- Moure EventForge des del contenidor d'escriptori
- Proporcionar una propietat activada per a «QIconItem».

### KDED

- kded: Simplificar la lògica del «sycoca»; invocar «ensureCacheValid».

### Compatibilitat amb les KDELibs 4

- Cridar «newInstance» des del fill en la primera invocació
- Usar les definicions del «kdewin».
- No intentar cercar X11 en WIN32
- cmake: Esmenar la verificació de la versió de «taglib» a FindTaglib.cmake.

### KDesignerPlugin

- El «moc» de les Qt no pot gestionar macros (QT_VERSION_CHECK)

### KDESU

- kWarning -&gt; qWarning

### KFileMetaData

- Implementar les metadades d'usuari del Windows

### Complements de la IGU del KDE

- No té sentit cercar X11/XCB en WIN32

### KHTML

- Substituir std::auto_ptr per std::unique_ptr
- khtml-filter: Descartar regles que contenen funcionalitats especials de bloqueig publicitari que encara no es poden gestionar.
- khtml-filter: Reordenar el codi, sense canvis funcionals.
- khtml-filter: Ignorar expressions regulars amb opcions que encara no estan implementades.
- khtml-filter: Esmenar la detecció del delimitador d'opcions de bloqueig publicitari.
- khtml-filter: Neteja els espais en blanc finals.
- khtml-filter: No descartar línies que comencen per «&amp;», ja que no és cap caràcter especial de bloqueig publicitari.

### KI18n

- Eliminar iteradors estrictes per al MSVC, per a fer que es construeixi el «ki18n»

### KIO

- KFileWidget: L'argument pare hauria de ser per defecte 0 com en tots els ginys.
- Assegura que la mida de la matriu de bytes que s'ha bolcat a l'estructura és suficientment gran abans de calcular el «targetInfo», en cas contrari s'accedeix a memòria que no correspon
- Esmenar l'ús de Qurl en cridar QFileDialog::getExistingDirectory()
- Actualitzar la llista de dispositius del Solid abans de cercar a kio_trash
- Permetre trash: addicionalment a trash:/ com a URL per a «listDir» (crida «listRoot») (error 353181)
- KProtocolManager: Esmenar un interbloqueig en utilitzar l'EnvVarProxy. (error 350890)
- No intentar cercar X11 en WIN32
- KBuildSycocaProgressDialog: Usar l'indicador d'ocupació incorporat a les Qt. (error 158672)
- KBuildSycocaProgressDialog: Executar kbuildsycoca5 amb QProcess.
- KPropertiesDialog: Esmena per ~/.local quan és un enllaç simbòlic, comparació dels camins canònics
- Afegir la implementació per les comparticions de xarxa a kio_trash (error 177023)
- Connectar als senyals del QDialogButtonBox, no del QDialog (error 352770)
- KCM de galetes: Actualitzar els noms de D-Bus per al kded5
- Usar fitxers JSON directament en lloc de kcoreaddons_desktop_to_json()

### KNotification

- No enviar dues vegades el senyal d'actualització de notificació
- Tornar a analitzar el fitxer de configuració de notificació només quan canviï
- No intentar cercar X11 en WIN32

### KNotifyConfig

- Canviar el mètode per a carregar els valors per defecte
- Enviar el nom d'aplicació de la qual s'ha actualitzat la configuració, conjuntament amb el senyal de D-Bus
- Afegir un mètode per tornar «kconfigwidget» als valors per defecte
- No sincronitzar la configuració n vegades en desar

### KService

- Usar un segell de temps més gran en els subdirectoris com a segell de temps de recurs de directori.
- KSycoca: Emmagatzemar el «mtime» per a cada directori origen, per a detectar canvis. (error 353036)
- KServiceTypeProfile: Eliminar la creació de factories no necessàries. (error 353360)
- Simplificar i accelerar KServiceTest::initTestCase.
- make install: Fer com a nom del fitxer «applications.menu» una variable CMake de la memòria cau
- KSycoca: ensureCacheValid() hauria de crear la BBDD si no existeix
- KSycoca: Fer que la base de dades global funcioni després de la recent verificació de codi del segell de temps
- KSycoca: Canviar el nom de fitxer de la BBDD per a incloure el llenguatge i el SHA1 dels directoris dels quals està construïda.
- KSycoca: Fer ensureCacheValid() part de l'API pública.
- KSycoca: Afegir un apuntador q per eliminar l'ús de més «singleton»
- KSycoca: Eliminar tots els mètodes self() per a factories, i en el seu lloc, emmagatzemar-les al KSycoca.
- KBuildSycoca: Eliminar l'escriptura del fitxer ksycoca5stamp.
- KBuildSycoca: Usar qCWarning en lloc de fprintf(stderr,...) o qWarning
- KSycoca: Reconstruir el «ksycoca» en el procés en lloc d'executar kbuildsycoca5
- KSycoca: Moure tot el codi del kbuildsycoca a la biblioteca, excepte main().
- Optimització del KSycoca: Vigilar només el fitxer si l'aplicació es connecta a databaseChanged()
- Esmenar pèrdues de memòria a la classe KBuildSycoca
- KSycoca: Substitució de la notificació de D-Bus per la vigilància del fitxer usant el «KDirWatch».
- kbuildsycoca: Opció obsoleta --nosignal.
- KBuildSycoca: Substituir el bloqueig basat en el D-Bus per un fitxer de bloqueig.
- No fallar quan es troba informació no vàlida de connectors.
- Reanomenar les capçaleres a _p.h com a preparació per moure a la biblioteca «kservice».
- Moure checkGlobalHeader() amb KBuildSycoca::recreate().
- Eliminar el codi per --checkstamps i --nocheckfiles.

### KTextEditor

- Validar més expressions regulars
- Esmenar expressions regulars en els fitxers HL (error 352662)
- Sincronitzar el ressaltat de sintaxi de l'OCam amb l'estat de https://code.google.com/p/vincent-hugot-projects/ abans que desaparegui el codi de Google. Diverses esmenes petites d'errors
- Afegir la separació de paraules (error 352258)
- Validar la línia abans de cridar el codi de plegat (error 339894)
- Esmenar el comptador de paraules del Kate accedint a DocumentPrivate en comptes de Document (error 353258)
- Actualitzar el ressaltat de sintaxi del Kconfig: Afegir operadors nous del Linux 4.2
- Sincronitzat amb la branca KDE/4.14 del Kate
- minimap: Esmena les nanses de les barres de desplaçament que no es dibuixen quan les marques de desplaçament estan inactives. (error 352641)
- syntax: Afegir l'opció git-user per kdesrc-buildrc

### Framework del KWallet

- No tancar automàticament en el darrer ús

### KWidgetsAddons

- Esmenar l'avís C4138 (MSVC): S'ha trobat «*/» fora del comentari

### KWindowSystem

- Portar a terme una còpia completa de QByteArray get_stringlist_reply
- Permetre interactuar amb diversos servidors X a les classes NETWM.
- [xcb] Considera les modificacions en el KKeyServer com a inicialitzades en plataformes != x11
- Canviar el KKeyserver (x11) a enregistrament categoritzat

### KXMLGUI

- Fer possible importar/exportar esquemes de dreceres simètricament

### NetworkManagerQt

- Esmenar introspeccions, «LastSeen» hauria d'estar a AccessPoint i no a ActiveConnection

### Frameworks del Plasma

- Ocultar el diàleg de consell d'eina quan el cursor entra en una ToolTipArea inactiva
- Si el fitxer «desktop» té Icon=/foo.svgz, usar aquest fitxer del paquet
- Afegir un tipus de fitxer «screenshot» en els paquets
- Tenir en compte «devicepixelration» en les barres de desplaçament autònomes
- Sense efecte de passar per sobre en pantalla tàctil i mòbil
- Usar els marges SVG de la línia d'edició en el càlcul del «sizeHint»
- No esvair les icones animades en els consells d'eina del Plasma
- Esmenar un text de botó omès
- Els menús contextuals de les miniaplicacions en un plafó ja no se superposaran a la miniaplicació
- Simplificar l'obtenció de la llista d'aplicacions associades a AssociatedApplicationManager

### Sonnet

- Esmenar l'ID del connector de l'Hunspell per a una càrrega correcta
- Implementar la compilació estàtica al Windows, afegir el camí del diccionari Hunspell del Libreoffice
- No assumir que els diccionaris de l'Hunspell estan codificats en UTF-8. (error 353133)
- Esmenar «Highlighter::setCurrentLanguage()» per al cas quan el llenguatge anterior no era vàlid (error 349151)
- Implementar /usr/share/hunspell com a localització del diccionari
- Connector basat en el NSSpellChecker

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
