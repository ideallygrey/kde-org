---
aliases:
- ../../kde-frameworks-5.19.0
date: 2016-02-13
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Attica

- Simplificació de la cerca i inicialització de connectors per l'Attica

### Icones Brisa

- Moltes icones noves
- Afegir icones de tipus MIME des del conjunt d'icones de l'Oxygen

### Mòduls extres del CMake

- ECMAddAppIcon: Usar un camí absolut en operar amb icones
- Assegura que se cerca el prefix a l'Android
- Afegir un mòdul del FindPoppler
- Usar PATH_SUFFIXES a «ecm_find_package_handle_library_components()»

### KActivities

- No invocar «exec()» des del QML (error 357435)
- La biblioteca KActivitiesStats ara és en un repositori separat

### KAuth

- Executar «preAuthAction» també per als dorsals amb «AuthorizeFromHelperCapability»
- Esmenar el nom del servei de D-Bus de l'agent del «polkit»

### KCMUtils

- Esmenar problema d'alta densitat de PPP en el KCMUtils

### KCompletion

- El mètode KLineEdit::setUrlDropsEnabled no es pot marcar com a obsolet

### KConfigWidgets

- Afegir un esquema de color «Complementari» a «kcolorscheme»

### KCrash

- Actualitzar la documentació per KCrash::initialize. S'anima als desenvolupadors d'aplicacions que l'invoquin explícitament.

### KDeclarative

- Netejar les dependències per KDeclarative/QuickAddons
- [KWindowSystemProxy] Afegir «setter» a «showingDesktop»
- DropArea: Esmena per ignorar correctament un esdeveniment «dragEnter» amb «preventStealing»
- DragArea: Implementar un element delegat de captura
- DragDropEvent: Afegir una funció «ignore()»

### KDED

- Desfer el pedaç «BlockingQueuedConnection», les Qt 5.6 tenen una esmena millor
- Fer que el registre del kded funcioni amb els àlies especificats pels mòduls del kded

### Compatibilitat amb les KDELibs 4

- kdelibs4support requereix kded (per al kdedmodule.desktop)

### KFileMetaData

- Permetre consultes quant a l'URL origen d'un fitxer

### KGlobalAccel

- Evitar fallades en cas que el D-Bus no estigui disponible

### Complements de la IGU del KDE

- Esmenar el llistat de les paletes disponibles al diàleg dels colors

### KHTML

- Esmenar la detecció de tipus d'enllaç de la icona (àlies «icona de web»)

### KI18n

- Reduir l'ús de l'API del «gettext»

### KImageFormats

- Afegir els connectors «kra» i «ora» del «imageio» (només lectura)

### KInit

- Ignora l'àrea de visualització de l'escriptori actual en la informació d'inic d'engegada
- Adaptar el «klauncher» al «xcb»
- Usar un «xcb» fer a la interacció amb KStartupInfo

### KIO

- Classe FavIconRequestJob nova a la biblioteca KIOGui nova, per a recuperació d'icones de web
- Esmenar fallada a KDirListerCache amb dos visualitzadors incrementals («listers») en un directori buit en la memòria cau (error 278431)
- Fer la implementació al Windows de «KIO::stat» per a l'error en el protocol file:/ si el fitxer no existeix
- No assumir que els fitxers en un directori de només lectura no es poden suprimir al Windows
- Esmenar el fitxer .pri per al «KIOWidgets»: depèn del «KIOCore», no d'ell mateix
- Reparar la càrrega automàtica de «kcookiejar», els valors es van intercanviar en el 6db255388532a4
- Fer accessible el «kcookiejar» amb el nom del servei de D-Bus a org.kde.kcookiejar5
- kssld: instal·lar el fitxer del servei D-Bus per a org.kde.kssld5
- Proporcionar un fitxer del servei D-Bus per a org.kde.kpasswdserver
- [kio_ftp] Esmenar la visualització de la data/hora de modificació del fitxer/directori (error 354597)
- [kio_help] Esmenar la brossa enviada en servir fitxers estàtics
- [kio_http] Provar l'autenticació NTLMv2 si el servidor denega NTLMv1
- [kio_http] Esmenar l'adaptació d'error que trenquen la memòria cau temporal
- [kio_http] Esmenar la creació de la resposta NTLMv2 etapa 3
- [kio_http] Esmenar l'espera fins que el netejador de la memòria cau escolta el sòcol
- kio_http_cache_cleaner: no sortir en engegar si encara no existeix el directori de la memòria cau
- Canviar el nom de D-Bus del «kio_http_cache_cleaner» de manera que no surti si s'executa el del KDE 4

### KItemModels

- KRecursiveFilterProxyModel::match: Esmenar una fallada

### KJobWidgets

- Esmenar una fallada en els diàlegs del KJob (error 346215)

### Paquets dels Frameworks

- Evitar trobar el mateix paquet diverses vegades des de camins diferents

### KParts

- PartManager: aturar el seguiment d'un giny inclús si no és de nivell superior (error 355711)

### KTextEditor

- Comportament millorat per a la funcionalitat «autobrace» d'«inserir claus al voltant»
- Canviar la clau d'opció per reforçar el nou valor predeterminat. Línia nova al final de línia = cert
- Eliminar algunes invocacions setUpdatesEnabled sospitoses (error 353088)
- Retardar l'emissió de «verticalScrollPositionChanged» fins que tot sigui coherent per al plegat (error 342512)
- Pedaç per actualitzar la substitució d'etiqueta (error 330634)
- Actualitzar només una vegada la paleta per al canvi d'esdeveniment que pertany a «qApp» (error 358526)
- Afegir línies noves a l'EOF (final de fitxer) per defecte
- Afegir fitxer de ressaltat de sintaxi del NSIS

### Framework del KWallet

- Duplicar el descriptor de fitxer en obrir el fitxer per llegir l'entorn

### KWidgetsAddons

- Esmenar ginys dependents que funcionen amb el KFontRequester
- KNewPasswordDialog: usar el KMessageWidget
- Evitar una fallada en sortir a KSelectAction::~KSelectAction

### KWindowSystem

- Canviar la capçalera de llicència des de «Biblioteca GPL 2 o posterior» a «Reduïda GPL 2.1 o posterior»
- Esmenar fallada si KWindowSystem::mapViewport és invocada sense una QCoreApplication
- Memòria cau de QX11Info::appRootWindow a «eventFilter» (error 356479)
- Desfer-se d'una dependència de QApplication (error 354811)

### KXMLGUI

- Afegir una opció per desactivar KGlobalAccel en temps de compilació
- Reparar el camí a l'esquema de dreceres d'aplicació
- Esmenar el llistat de fitxers de drecera (ús incorrecte del QDir)

### NetworkManagerQt

- Tornar a comprovar l'estat de la connexió i altres propietats per assegurar que són reals (versió 2) (error 352326)

### Icones de l'Oxygen

- Eliminar fitxers enllaçats trencats
- Afegir icones d'aplicació des de les aplicacions KDE
- Afegir icones de lloc del Brisa a l'Oxygen
- Sincronitzar les icones de tipus MIME de l'Oxygen amb les icones de tipus MIME del Brisa

### Frameworks del Plasma

- Afegir una propietat «separatorVisible»
- Eliminació més explícita des de m_appletInterfaces (error 358551)
- Usar «complementaryColorScheme» des del KColorScheme
- AppletQuickItem: no intentar definir la mida inicial més gran que la mida del pare (error 358200)
- IconItem: Afegir la propietat «usesPlasmaTheme»
- No carregar el quadre d'eines en tipus diferents de «desktop» o «panel»
- IconItem: Intentar carregar les icones de QIcon::fromTheme com a SVG (error 353358)
- Ignorar la verificació si només una part de la mida és zero a «compactRepresentationCheck» (error 358039)
- [Unitats] Retornar com a mínim 1 ms per a les durades (error 357532)
- Afegir «clearActions()» per a eliminar cada acció d'interfície de miniaplicació
- [plasmaquick/dialog] No usar «KWindowEffects» per als tipus de finestra «Notification»
- Fer obsolet Applet::loadPlasmoid()
- [PlasmaCore DataModel] No reiniciar el model quan s'elimina un origen
- Esmenar els consells de marges en fons SVG de plafó opacs
- IconItem: Afegir una propietat d'animació
- [Unity] Escalar la mida d'icona d'escriptori
- El botó és compost sobre les vores
- «paintedWidth/paintedheight» per IconItem

Podeu debatre i compartir idees quant a aquest llançament en la secció de comentaris en <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>l'article del Dot</a>.
