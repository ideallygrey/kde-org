---
aliases:
- ../announce-applications-18.04.2
changelog: true
date: 2018-06-07
description: KDE veröffentlicht die KDE-Anwendungen 18.04.2
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 18.04.2
version: 18.04.2
---
7. Juni 2018. Heute veröffentlicht KDE die zweite Aktualisierung der <a href='../18.04.0'>KDE-Anwendungen 18.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für Kontact, Cantor, Dolphin, Gwenview, KGpg, Kig, Konsole, Lokalize und Okular.

Verbesserungen umfassen:

- Image operations in Gwenview can now be redone after undoing them
- KGpg no longer fails to decrypt messages without a version header
- Der Fehler beim Export von Cantor-Arbeitsblättern zu LaTeX wurde für Maxima-Matrizen behoben
