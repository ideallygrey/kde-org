---
aliases:
- ../announce-applications-16.04.1
changelog: true
date: 2016-05-10
description: KDE veröffentlicht die KDE-Anwendungen 16.04.1
layout: application
title: KDE veröffentlicht die KDE-Anwendungen 16.04.1
version: 16.04.1
---
10. Mai 2016. Heute veröffentlicht KDE die erste Aktualisierung der <a href='../16.04.0'>KDE-Anwendungen 16.04</a>. Diese Veröffentlichung enthält nur Fehlerkorrekturen und aktualisierte Übersetzungen und ist daher für alle Benutzer eine sichere und problemlose Aktualisierung.

Mehr als 25 aufgezeichnete Fehlerkorrekturen enthalten unter anderem Verbesserungen für kdepim, Ark, Kate, Dolphin, Kdenlive, Lokalize, und Spectacle.

Diese Veröffentlichung enthält die Version der KDE Development Platform %1 mit langfristige Unterstützung.
