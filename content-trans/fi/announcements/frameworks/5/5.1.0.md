---
aliases:
- ../../kde-frameworks-5.1
- ./5.1
customIntro: true
date: '2014-08-08'
description: KDE Ships Second Release of Frameworks 5.
layout: framework
qtversion: 5.2
title: Frameworks 5:n toinen julkaisu
---
August 7th, 2014. KDE today announces the second release of KDE Frameworks 5. In line with the planned release policy for KDE Frameworks this release comes one month after the initial version and has both bugfixes and new features.

{{% i18n "annc-frameworks-intro" "60" "/announcements/frameworks/5/5.0" %}}

## {{< i18n "annc-frameworks-new" >}}

This release, versioned 5.1, comes with a number of bugfixes and new features including:

- KTextEditor: Major refactorings and improvements of the vi-mode
- KAuth: Now based on PolkitQt5-1
- New migration agent for KWallet
- Windows compilation fixes
- Translation fixes
- New install dir for KXmlGui files and for AppStream metainfo
- <a href='http://www.proli.net/2014/08/04/taking-advantage-of-opengl-from-plasma/'>Plasma Taking advantage of OpenGL</a>
