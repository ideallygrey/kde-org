---
aliases:
- ../announce-applications-18.12.1
changelog: true
date: 2019-01-10
description: KDE Uygulamalar 18.12.1.'i Gönderdi
layout: application
major_version: '18.12'
title: KDE, KDE Uygulamalar 18.12.1'i Gönderdi
version: 18.12.1
---
{{% i18n_date %}}

Bugün KDE <a href='../18.12.0'>KDE Uygulamaları 18.12</a> için ilk kararlılık güncellemesini yayınladı. Bu sürüm yalnızca hata düzeltmeleri ve çeviri güncellemelerini içerir, herkes için güvenli ve hoş bir güncelleme sağlar.

Kaydedilen yaklaşık 20 hata düzeltmesi, diğerleri arasında Kontak, Cantor, Dolphin, JuK, Kdenlive, Konsole, Okular için iyileştirmeler içerir.

İyileştirmeler şunları içerir:

- Akregatör artık Qt 5.11 veya daha yeni WebEngine ile çalışıyor
- JuK müzik çalarda sıralama sütunları düzeltildi
- Konsole renders box-drawing characters correctly again
