---
aliases:
- ../announce-applications-17.12.2
changelog: true
date: 2018-02-08
description: KDE wydało Aplikacje KDE 17.12.2
layout: application
title: KDE wydało Aplikacje KDE 17.12.2
version: 17.12.2
---
8 lutego 2018. Dzisiaj KDE wydało drugie uaktualnienie stabilizujące <a href='../17.12.0'>Aplikacji KDE 17.12</a>. To wydanie zawiera tylko poprawki błędów i uaktualnienia do tłumaczeń; jest to bezpieczne i przyjemne uaktualnienie dla każdego.

About 20 recorded bugfixes include improvements to Kontact, Dolphin, Gwenview, KGet, Okular, among others.
