---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- DjVu-faile peetakse dokumentideks (veateade 369195)
- Õigekirja parandus, et WPS Office'i esitlusi korralikult ära tuntals

### Breeze'i ikoonid

- folder-stash'i lisamine Dolphini tööriistariba faililao ikooni jaoks

### KArchive

- Võimaliku mälulekke parandus. Loogika parandus

### KCMUtils

- qml-moodulid ilma veeristeta qwidget'i poolel
- muutujate initsialiseerimine (coverity leitud)

### KConfigWidgets

- KStandardAction::MoveToTrash'i ikooni parandus

### KCoreAddons

- URL-i tuvastamise parandus topelt-URL-ide korral, näiteks "http://www.foo.bar&lt;http://foo.bar/&gt;"
- KDE url-idel https'i kasutamine

### KDELibs 4 toetus

- disableSessionManagement() asendamise täielik dokumentatsioon
- kssl muudetakse kompileeritavaks OpenSSL 1.1.0 peal (veateade 370223)

### KFileMetaData

- Omaduse Generator näidatava nime parandus

### KGlobalAccel

- KGlobalAccel: numbriklahvistiku toetuse parandus (jälle)

### KInit

- start_kdeinit'i korralik paigaldus, kui kasutatakse korraga nii DESTDIR'i kui ka libcap'i

### KIO

- remote:/ näitamise parandus qfiledialog'is
- Kategooriate toetuse teostus KfilesPlacesView's
- HTTP: tõrkestringi parandus "207 Multi-Status" korral
- KNewFileMenu: surnud koodi puhastus (leidis Coverity)
- IKWS: võimaliku lõputu silmuse parandus (leidis Coverity)
- Funktsioon KIO::PreviewJob::defaultPlugins()

### Kirigami

- süntaks töötab vanema Qt 5.7 peal (veateade 385785)
- overlaysheet'i virnastamine teistmoodi (veateade 386470)
- Ka delegaadi esiletõstmise omaduse näitamine, kui fookus puudub
- eraldaja eelistatud suuruse vihjed
- Settings.isMobile'i korrektne kasutus
- Rakendustel lubatakse töölauasüsteemis mõnevõrra koonduda
- Tagamine, et SwipeListItem'i sisu ei kattu pidemega (veateade 385974)
- Overlaysheet'i kerimisvaade on alati interaktiivne
- Kategooriate lisamine galerii töölauafaili (veateade 385430)
- Kirigami .pri-faili uuendamine
- paigaldamata plugina kasutamine testimiseks
- Kirigami.Label'i märkimine iganenuks
- Galerii Label'i kasutuse näite portimine ühtlustamiseks QQC2-ga
- Kirigami Label'i Kirigami Controls'i kasutuse portimine
- kerimisala muutmine interaktiivseks puutesündmuste korral
- git'i find_package väljakutse liigutamine sinna, kus seda kasutatakse
- vaikimisi läbipaistvad loendivaate elemendid

### KNewStuff

- PreferCache'i eemaldamine võrgunõuetest
- privaatandmete jagatud viitu ei haagita eelvaatluse määramisel lahti
- KMoreTools: töölauafailide uuendus ja parandus (veateade 369646)

### KNotification

- SNI masinate kontrolli eemaldamine valimisel, kas kasutada pärandrežiimi või mitte (veateade 385867)
- Süsteemisalve pärandikoonide kontroll ainult siis, kui meil on kavas selline luua (veateade 385371)

### KPackage raamistik

- paigaldamata teenusefailide kasutamine

### KService

- väärtuste initsialiseerimine
- mõne viida initsialiseerimine

### KTextEditor

- API dox: fix wrong names of methods and args, add missing \\since
- (Teatavate) krahhide vältimine QML-i skriptide täitmisel (veateade 385413)
- QML-i krahhi vältimine, mille põhjustasid C-stiilis treppimisskriptid
- Lõpetava tühimärgi suuruse suurendamine
- Parandus: mõni treppija kippus treppima juhusliku märgi juurest
- Iganemishoiatuse parandus

### KTextWidgets

- Väärtuse initsialiseerimine

### KWayland

- [klient] kontrollist loobumine, kas platformName on "wayland"
- Ühendust wl_display_flush'iga ei looda kaks korda
- Waylandi võõrprotokoll

### KWidgetsAddons

- createKMessageBox'i fookusvidina ebaühtluse parandus
- kompaktsem paroolidialoog (veateade 381231)
- KPageListView laiuse kohane määramine

### KWindowSystem

- KKeyServer: Meta+tõstuklahv+Print, Alt+tõstuklahv+nooleklahv jne käitlemise parandus
- flatpak'i platvormi toetus
- KWindowSystemi enda platvormi tuvastamise API kasutamine topeltkoodi asemel

### KXMLGUI

- KDE url-idel https'i kasutamine

### NetworkManagerQt

- 8021xSetting: domain-suffix-match on defineeritud NM 1.2.0 ja uuemates
- "domain-suffix-match" toetus Security8021xSetting'is

### Plasma raamistik

- ringi kaare käsitsi joonistamine
- [PlasmaComponents Menu] ungrabMouseHack'i lisamine
- [FrameSvg] updateSizes'i optimeerimine
- Dialog'i ei asetata, kui selle tüüp on OSD

### QQC2StyleBridge

- Kompileerimise täiustamine staatilise pluginana
- raadionupu muutmine raadionupuks
- qstyle'i kasutamine Dial'i joonistamiseks
- ColumnLayout'i kasutamine menüüs
- Dialog'i parandus
- vigase omaduse group eemaldamine
- md-faili vorminduse parandus, et see vastaks teistele moodulitele
- liitkasti käitumine sarnasemaks qqc1-ga
- QQuickWidgets'i hädalahendus

### Sonnet

- Meetodi assignByDictionnary lisamine
- Signaali väljastamine, kui me suudame sõnastiku omistada

### Süntaksi esiletõstmine

- Makefile: regulaaravaldise vastendamise parandus "CXXFLAGS+"-is

### ThreadWeaver

- CMake'i puhastus: -std=c++0x ei kirjutata koodi sisse

### Turbeteave

The released code has been GPG-signed using the following key: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Primary key fingerprint: 53E6 B47B 45CE A3E0 D5B7 4577 58D0 EE64 8A48 B3BB

Väljalaske üle arutada ja mõtteid jagada saab <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>meie uudistelehekülje artikli</a> kommentaarides.
