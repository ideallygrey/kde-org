---
aliases:
- ../announce-applications-19.08.1
changelog: true
date: 2019-09-05
description: KDE Ships Applications 19.08.1.
layout: application
major_version: '19.08'
release: applications-19.08.1
title: KDE Ships Applications 19.08.1
version: 19.08.1
---
{{% i18n_date %}}

Today KDE released the first stability update for <a href='../19.08.0'>KDE Applications 19.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 20 teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Dolphini, Kdenlive'i, Konsooli ja Stepi täiustused.

Täiustused sisaldavad muu hulgas:

- Parandati mitmed tagasilangused Konsooli kaartide haldamisel
- Dolphin käivitub taas korrektselt ka poolitatud akende režiimis
- Elastse keha kustutamine füüsikasimulaatoris Step ei põhjusta enam krahhi
