---
aliases:
- ../announce-applications-15.08.2
changelog: true
date: 2015-10-13
description: KDE toob välja KDE rakendused 15.08.2
layout: application
title: KDE toob välja KDE rakendused 15.08.2
version: 15.08.2
---
October 13, 2015. Today KDE released the second stability update for <a href='../15.08.0'>KDE Applications 15.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Rohkem kui 30 teadaoleva veaparanduse hulka kuuluvad Arki, Gwenview, Kate, Kbruchi, Kdelibsi, Kdepimi, Lokalize ja Umbrello täiustused.

This release also includes Long Term Support version of KDE Development Platform 4.14.13.
