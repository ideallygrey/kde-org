---
aliases:
- ../announce-applications-18.08.2
changelog: true
date: 2018-10-11
description: KDE toob välja KDE rakendused 18.08.2
layout: application
title: KDE toob välja KDE rakendused 18.08.2
version: 18.08.2
---
October 11, 2018. Today KDE released the second stability update for <a href='../18.08.0'>KDE Applications 18.08</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

Üle tosina teadaoleva veaparanduse hulka kuuluvad muu hulgas Kontacti, Dolphini, Gwenview, KCalci ja Umbrello täiustused.

Täiustused sisaldavad muu hulgas:

- Faili lohistamine Dolphinis ei too enam juhuslikult kaasa kohapeal nime muutmist
- KCalc lubab taas kümnendkoha eraldajana nii punkti kui ka koma
- KDE kaadimängudes parandati ära Pariisi kaardipaki visuaalne ebakõla
