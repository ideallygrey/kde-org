---
aliases:
- ../../kde-frameworks-5.70.0
date: 2020-05-02
layout: framework
libCount: 70
---
### Baloo

- [FileWatch] Remoção da rotina redundante watchIndexedFolders()
- [ModifiedFileIndexer] Esclarecimento de um comentário
- [FileWatch] Correcção das actualizações das monitorização em caso de mudanças da na configuração
- [KInotify] Correcção da correspondência de localizações ao remover as monitorizações
- [Extractor] Uso de registos com categorias
- Uso do KFileMetaData para o suporte para o XAttr em vez de uma reimplementação privada
- Reversão do "Adição de sinais DBus do Baloo para os ficheiros movidos ou removidos"
- [Monitor de QML] Apresentação do tempo restante o mais cedo possível
- [FileContentIndexer] Correcção da actualização do estado e ordem dos sinais
- [Monitor] Correcção do estado do monitor e ordenação dos sinais
- [Extractor] Correcção da comunicação do progresso
- [Código] Evitar destacamentos recorrentes e verificações de tamanho
- [baloo_file] Remoção do KAboutData do 'baloo_file'
- [Searchstore] Reserva de espaço para os termos da pesquisa por frases
- [SearchStore] Possibilidade de pesquisa de ocorrências exactas para não-propriedades
- [PhraseAndIterator] Remoção de listas temporárias ao verificar as ocorrências
- [Extractor] Melhor balanço entre o modo inactivo e ocupado
- [Extractor] Correcção da monitorização inactiva
- [Extractor] Remoção da classe de interface do IdleStateMonitor
- [OrpostingIterator] Permitir ignorar elementos; implementação do 'skipTo'
- [PhraseAndIterator] Substituição da implementação recursiva do next()
- [AndPostingIterator] Substituição da implementação recursiva do next()
- [PostingIterator] Verificação que o 'skipTo' também funciona para o primeiro elemento
- mudança de nome e exportação do sinal 'newBatchTime' no 'filecontentindexer'
- [SearchStore] Tratamento de valores de vírgula flutuante de precisão dupla nas pesquisas por propriedades
- [AdvancedQueryParser] Passagem do tratamento semântico dos códigos no SearchStore
- [Inotify] Remoção do nem-por-isso-OptimizedByteArray
- [Inotify] Remoção de código inacessível/duplicado
- [QueryParser] Substituição do utilitário de uso único com o 'std::none_of'

### Ícones Breeze

- Mover a dobragem do canto do documento para o superior-direito em dois ícones
- Adição de ícones do Konversation a 16px
- correcção do nome do ícone do 'vscode'
- Adição de ícone do Vvave a 16px
- adição de ícone do Alligator
- Adição dos ícones 'preferences-desktop-tablet' e 'preferences-desktop-touchpad'
- Atualização dos links no README.md
- compilação: delimitação por aspas da pasta de origem
- Possibilidade de compilação a partir de uma localização de código apenas para leitura
- Adição de ícones para expandir/recolher que acompanham os ícones existentes para expandir-tudo/recolher-tudo
- Adição do 'auth-sim-locked' e 'auth-sim-missing'
- Adição de ícones de dispositivos de cartões SIM
- Adição de ícones de rotação
- Adição do ícone a 16px da Configuração do Sistema
- Mudança do ButtonFocus para o Highlight
- Melhoria da aparência do KCachegrind
- Remoção do contorno dos ícones 'format-border-set-*'

### Módulos extra do CMake

- android: inclusão da arquitectura do nome do APK
- ECMAddQch: correcção do uso de aspas com PREDEFINED na configuração do doxygen
- Adaptação do FindKF5 para verificações mais restritas no recente 'find_package_handle_standard_args'
- ECMAddQch: ajudar o 'doxygen' a lidar com o Q_DECLARE_FLAGS, para que esses tipos tenham documentação
- Correcção dos avisos de digitalização do Wayland
- ECM: tentativa de correcção do KDEInstallDirsTest.relative_or_absolute no Windows

### Ferramentas Doxygen do KDE

- Correcção do espaço em falta a seguir às "Plataforma(s):" nas páginas iniciais
- Correcção do uso de aspas nos elementos PREDEFINED no Doxygile.global
- Adaptação do doxygen ao Q_DECL_EQ_DEFAULT &amp; Q_DECL_EQ_DELETE
- Adição de área de arrumação na versão móvel e limpeza de código
- Adaptação do doxygen ao Q_DECLARE_FLAGS, para que esses tipos possam ser documentados
- Migração para o Aether Bootstrap 4
- Remodelação do api.kde.org para se parecer com o Aether

### KBookmarks

- Criar sempre uma 'actioncollection'
- [KBookMarksMenu] Definição do 'objectName' para o 'newBookmarkFolderAction'

### KCMUtils

- KSettings::Dialog: adição de suporte para KPluginInfo's sem um KService
- Pequena optimização: invocação do kcmServices() apenas uma vez
- Despromoção de aviso sobre o KCM obsoleto para o qDebug até o KF6
- Uso do ecm_setup_qtplugin_macro_names

### KConfig

- kconfig_compiler : geração da configuração do KConfig com sub-grupos
- Correcção de alguns avisos de compilação
- Adição de comportamento para forçar a gravação no KEntryMap
- Adição de um atalho-padrão para "Mostrar/Esconder os Ficheiros Escondidos" (erro 262551)

### KContacts

- Alinhamento da descrição no metainfo.yaml com a do README.md

### KCoreAddons

- Documentação da API: uso do 'typedef ulong' com o Q_PROPERTY(percent) para evitar um erro do Doxygen
- Documentação da API: documentação das opções baseadas nas Q_DECLARE_FLAGS
- Marcação do 'typedef' antigo da KLibFactory como obsoleto
- [KJobUiDelegate] Adição da opção AutoHandlingEnabled

### KCrash

- Eliminação da utilização do 'klauncher' no KCrash

### KDeclarative

- Dar um nome adequado ao conteúdo do projecto 'kcmcontrols'
- Ajustes na documentação dos 'kcmcontrols'
- Adição do método 'startCapture'
- [KeySequenceHelper] Tratamento alternativo do comportamento da tecla Meta
- Eliminação também da janela no destrutor

### KDED

- Migração do KToolInvocation::kdeinitExecWait para o QProcess
- Eliminação da segunda fase do 'delayed'

### KHolidays

- Feriados da Nicarágua
- Feriados de Taiwan
- Actualização dos feriados Romenos

### KI18n

- Macro KI18N_WRAP_UI: definição da propriedade SKIP_AUTOUIC no ficheiro UI e geração do cabeçalho

### KIconThemes

- Adição de nota sobre a migração do 'loadMimeTypeIcon'

### KImageFormats

- Adição do suporte para os ficheiros XCF/de imagens modernos do Gimp

### KIO

- [RenameDialog] Adição de uma seta que indica a direcção da origem para o destino (erro 268600)
- KIO_SILENT Ajuste da documentação da API para corresponder à realidade
- Passagem do tratamento dos programas não-fidedignos para o ApplicationLauncherJob
- Passagem da verificação de serviços inválidos do KDesktopFileActions para o ApplicationLauncherJob
- Detecção de executáveis sem permissão +x na $PATH para melhorar a mensagem de erro (erro 415567)
- Tornar o modelo de ficheiros HTML mais útil (erro 419935)
- Adição do construtor JobUiDelegate com a opção AutoErrorHandling e a janela
- Correcção do cálculo da pasta de 'cache' ao adicionar ao lixo
- Protocolo de ficheiros: garantia que o KIO::StatAcl funciona sem depender de forma implícita do KIO::StatBasic
- Adição do valor do detalhe KIO::StatRecursiveSize, para que o 'kio_trash' só o faça a pedido
- CopyJob: ao executar o 'stat' do destino, usar o StatBasic
- [KFileBookMarkHandler] Migração para o novo KBookmarkMenu-5.69
- Marcação da KStatusBarOfflineIndicator como obsoleta
- Substituição do KLocalSocket pelo QLocalSocket
- Evitar um estoiro na versão final a seguir ao aviso de itens-filhos inesperados (erro 390288)
- Docu: remoção de menção a um sinal inexistente
- [renamedialog] Substituição da utilização do KIconLoader por um QIcon::fromTheme
- kio_trash: Adição do tamanho e datas de acesso, modificação e criação no trash:/ (erro 413091)
- [KDirOperator] Uso do novo atalho-padrão para "Mostrar/Esconder os Ficheiros Escondidos" (erro 262551)
- Mostrar antevisões nos sistemas de ficheiros encriptados (erro 411919)
- [KPropertiesDialog] Desactivação da mudança das pastas remotas (erro 205954)
- [KPropertiesDialog] Correcção do aviso do QLayout
- Documentação da API: documentação de mais valores de propriedades predefinidas do KUrlRequester
- Correcção do DirectorySizeJob para que não dependa da ordem da listagem
- KRun: correcção de verificação ao falhar o início de uma aplicação

### Kirigami

- Introdução do Theme::smallFont
- Tornar o BasicListItem mais útil, atribuindo-lhe uma propriedade 'subtitle'
- PageRouterAttached com menos estoiros de memória
- PageRouter: melhor pesquisa dos itens-pais
- Remoção do QtConcurrent não usada do 'colorutils'
- PlaceholderMessage: Remoção da utilização de unidades do Plasma
- Possibilidade de o PlaceholderMessage não ter texto
- centrar na vertical as folhas se não tiverem uma barra de deslocamento (erro 419804)
- Ter em conta a margem superior e inferior na altura predefinida dos cartões
- Diversas correcções nos novos Card's (erro 420406)
- Ícone: melhoria do desenho dos ícones em configurações multi-ecrã e multi-resolução
- Correcção de erro no PlaceholderMessage: as acções estão desactivadas, não escondidas
- Introdução do componente PlaceholderMessage
- Correcção urgente: correcção de tipos inválidos nas funções com listas de FormLayout
- Correcção urgente do SwipeListItem: uso do Array.prototype.*.call
- Correcção urgente: uso do Array.prototype.some.call no ContextDrawer
- Correcção urgente do D28666: uso do Array.prototype.*.call em vez de invocar funções em objectos 'list'
- Adição da variável 'm_sourceChanged'
- Uso do ShadowedRectangle para os fundos dos Card's (erro 415526)
- Actualização da verificação da visibilidade para o ActionToolbar, verificando a largura com o menor-"igual"
- Um conjunto de correcções 'triviais' para código inválido
- nunca fechar quando o 'click' ocorre dentro do conteúdo da folha (erro 419691)
- a folha deve estar debaixo de outras áreas instantâneas (erro 419930)
- Adição do componente PageRouter
- Adição do ColorUtils
- Permitir a definição de raios separados dos cantos para o ShadowedRectangle
- Remoção da opção STATIC_LIBRARY para corrigir compilações estáticas

### KJobWidgets

- Adição do construtor KDialogJobUiDelegate(KJobUiDelegate::Flags)

### KJS

- Implementação do operator= do UString para deixar o 'gcc' satisfeito
- Silenciamento de aviso do compilador sobre a cópia de dados não-triviais

### KNewStuff

- KNewStuff: Correcção da localização do ficheiro e invocação do processo (erro 420312)
- KNewStuff: migração do KRun::runApplication para o KIO::ApplicationLauncherJob
- Substituição do Vokoscreen pelo VokoscreenNG (erro 416460)
- Introdução de um erro mais visível para o utilizador referente às instalações (erro 418466)

### KNotification

- Implementação da actualização das notificações no Android
- Tratamento de notificações multi-linhas e com texto formatado no Android
- Adição do construtor KNotificationJobUiDelegate(KJobUiDelegate::Flags)
- [KNotificationJobUiDelegate] Adição do "Failed" para as mensagens de erro

### KNotifyConfig

- Uso consistente do knotify-config.h para passar opções sobre o Canberra/Phonon

### KParts

- Adição do construtor substituto StatusBarExtension(KParts::Part *)

### KPlotting

- Migração do 'foreach' (obsoleto) para um 'for' com intervalos

### KRunner

- Execução do DBus: Adição de propriedade 'service' para pedir as acções uma vez (erro 420311)
- Impressão de um aviso se o módulo de execução é incompatível com o KRunner

### KService

- Descontinuação do KPluginInfo::service(), dado que o construtor com um KService é obsoleto

### KTextEditor

- correcção do arrastamento e largada do item do contorno do lado esquerdo (erro 420048)
- Configuração das vistas de gravação e carregamento completos com base na configuração da sessão
- Reversão da migração prematura para o Qt 5.15 - ainda por lançar - que foi alterado entretanto

### KTextWidgets

- [NestedListHelper] Correcção da indentação da selecção, adição de testes
- [NestedListHelper] Melhoria no código de indentação
- [KRichTextEdit] Verificação que os cabeçalhos não interferem com a pilha de anulações
- [KRichTextEdit] Correcção do salto com deslocamento quando está adicionada uma régua horizontal (erro 195828)
- [KRichTextWidget] Remoção de truque antigo e correcção de regressão (versão '1d1eb6f')
- [KRichTextWidget] Adição de suporte para cabeçalhos
- [KRichTextEdit] Tratamento de cada tecla sempre como uma única modificação na pilha de anulações (erro 256001)
- [pesquisa-substituição] Tratamento da pesquisa com WholeWordsOnly no modo Regex (expressões regulares)

### KUnitConversion

- Adição de galões imperiais e 'pints' (copos) dos EUA (erro 341072)
- Adição da Coroa Islandesa às moedas

### KWayland

- [Wayland] Adição ao protocolo PlasmaWindowManagement da ordem de empilhamento das janelas
- [servidor] Adição de alguns sinais de ciclo de vida das sub-superfícies

### KWidgetsAddons

- [KFontChooser] Remoção da DisplayFlag do NoFixedCheckBox por redundância
- [KFontChooser] Adição de uma nova DisplayFlag; modificação do uso das opções
- [KFontChooser] Tornar o styleIdentifier() mais exacto ao adicionar o 'styleName' do tipo de letra (erro 420287)
- [KFontRequester] Migração do QFontDialog para o KFontChooserDialog
- [KMimeTypeChooser] Adição da capacidade de filtrar a árvore com um QSFPM (erro 245637)
- [KFontChooser] Tornar o código um pouco mais legível
- [KFontChooser] Adição de uma opção para comutar a apresentação apenas de tipos de letra monoespaçados
- Remoção de inclusões desnecessárias

### KWindowSystem

- Impressão de um aviso compreensível quando não existe uma QGuiApplication

### KXMLGUI

- [KRichTextEditor] Adição do suporte para cabeçalhos
- [KKeySequenceWidget] Truque no comportamento da tecla modificadora Meta

### NetworkManagerQt

- Substituição do 'foreach' por 'for' com intervalos

### Plasma Framework

- [PlasmaCore.IconItem] Regressão: correcção de estoiro na mudança da origem (erro 420801)
- [PlasmaCore.IconItem] Remodelação do tratamento da origem para diferentes tipos
- Tornar consistentes os espaços dos textos das dicas
- [ExpandableListItem] tornar o componente amigável em ecrãs tácteis
- [ExpandableListItem] Uso de ícones de expansão e fecho semanticamente mais correctos
- Correcção do ciclo de associação do BusyIndicator no PC3
- [ExpandableListItem] Adição da nova opção 'showDefaultActionButtonWhenBusy'
- Remoção de contornos arredondados no 'plasmoidHeading'
- [ExpandableListItem] Adição do sinal 'itemCollapsed' e não emitir o 'itemExpanded' quando o item está recolhido
- Adição de ficheiros README que esclarecem o estado das versões dos componentes do Plasma
- [configview] Simplificação de código / truque para estoiro no Qt5.15
- Criação do ExpandableListItem
- Tornar as durações das animações consistentes com os valores do Kirigami

### QQC2StyleBridge

- Detecção da versão do QQC2 na altura da compilação com uma detecção efectiva
- [ComboBox] Uso de um escurecimento transparente

### Solid

- [Solid] Substituição do 'foreach' por 'for' baseado em intervalos
- [FakeCdrom] Adição de um novo enumerador UnknownMediumType para o MediumType
- [FstabWatcher] Correcção de problemas na monitorização do 'fstab'
- [Fstab] Não emitir um 'deviceAdded' duas vezes em caso de mudança no fstab/mtab

### Realce de sintaxe

- debchangelog: adição do Gorilla do Groovy
- Actualização do suporte da sintaxe da linguagem Logtalk
- TypeScript: adição do operador de tipos "awaited"

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB
