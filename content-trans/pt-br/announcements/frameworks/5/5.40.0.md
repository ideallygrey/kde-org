---
aliases:
- ../../kde-frameworks-5.40.0
date: 2017-11-11
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Baloo

- Classificação dos ficheiros DjVu como Documentos (erro 369195)
- Correcção ortográfica para que as apresentações do WPS Office presentations sejam reconhecidas correctamente

### Ícones Breeze

- adição do 'folder-stash' como ícone da barra de ferramentas do armazenamento temporário do Dolphin

### KArchive

- Correcção de potencial fuga de memória. Correcção da lógica

### KCMUtils

- sem margens nos módulos QML do lado do 'qwidget'
- Inicialização de variáveis (descoberto pelo Coverity)

### KConfigWidgets

- Correcção do ícone do KStandardAction::MoveToTrash

### KCoreAddons

- correcção da detecção do URL com URL's duplos do tipo "http://www.xpto.com&lt;http://xpto.com/&gt;"
- Uso de 'https' para os URL's do KDE

### KDELibs 4 Support

- documentação completa do substituto do 'disableSessionManagement()'
- Fazer com que o 'kssl' compile com o OpenSSL 1.1.0 (erro 370223)

### KFileMetaData

- Correcção do nome visível do Generator de forma adequada

### KGlobalAccel

- KGlobalAccel: correcção do suporte das teclas do teclado numérico (de novo)

### KInit

- Correcção da instalação do 'start_kdeinit' quando o DESTDIR e a 'libcap' são usadas em conjunto

### KIO

- Correcção da apresentação do 'remote:/' no 'qfiledialog'
- Implementação do suporte para categorias no KFilesPlacesView
- HTTP: correcção do texto de erro do caso '207 Multi-Status'
- KNewFileMenu: limpeza de código inactivo, detectada pelo Coverity
- IKWS: Correcção de um possível ciclo infinito, detectado pelo Coverity
- função KIO::PreviewJob::defaultPlugins()

### Kirigami

- sintaxe a funcionar no Qt 5.7, mais antigo (erro 385785)
- empilhamento do 'overlaysheet' de forma diferente (erro 386470)
- Mostrar a propriedade delegada 'highlighted' também quando não existe foco
- sugestões do tamanho preferido para o separador
- correcção do uso do Settings.isMobile
- Permitir às aplicações ser convergentes de certa forma num sistema de um computador
- Validação de que o conteúdo do SwipeListItem não se sobrepõe à pega (erro 385974)
- A área de deslocamento do Overlaysheet é sempre 'ointeractive'
- Adição das categorias no ficheiro 'desktop' da galeria (erro 385430)
- Actualização do ficheiro 'kirigami.pri'
- uso de um 'plugin' não instalado para efectuar os testes
- Descontinuação do Kirigami.Label
- Migração do uso de exemplo da galeria do Labels para ser QQC2 de forma consistente
- Migração das utilizações do Kirigami.Label no Kirigami.Controls
- torcar a área de deslocamento interactiva aos eventos de toque
- Migração da chamada 'find_package' do Git para onde é usada
- uso por omissão de itens da lista transparentes

### KNewStuff

- Remoção do PreferCache dos pedidos de rede
- Não dissociar ponteiros partilhados para dados privados quando definir antevisões
- KMoreTools: Actualização e correcção dos ficheiros 'desktop' (erro 369646)

### KNotification

- Remoção da verificação dos servidores SNI quando optar por usar o modo antigo (erro 385867)
- Só verificar os ícones da bandeja antigos se for para criar um (erro 385371)

### Plataforma KPackage

- uso de ficheiros de serviços não instalados

### KService

- Inicialização de valores
- Inicialização de alguns ponteiros

### KTextEditor

- Documentação da API: correcção de nomes errados dos métodos e argumentos, adição do '\\since' em falta
- Protecção contra (determinados) estoiros na execução dos programas QML (erro 385413)
- Protecção contra um estoiro do QML despoletado pelos programas de indentação do tipo do C
- Aumento do tamanho da marca de caracteres finais
- correcção de alguns módulos de indentação na indexação de caracteres aleatórios
- Correcção do aviso de descontinuação

### KTextWidgets

- Inicialização de valor

### KWayland

- [cliente] Eliminação das validações do 'platformName' como sendo "wayland"
- Não duplicar a ligação ao 'wl_display_flush'
- Protocolo externo do Wayland

### KWidgetsAddons

- Correcção de inconsistência no elemento em foco do 'createKMessageBox'
- janela de senhas mais compacta (erro 381231)
- Definição adequada da largura da KPageListView

### KWindowSystem

- KKeyServer: correcção do tratamento do Meta+Shift+Print, Alt+Shift+cursor, etc
- Suporte para a plataforma Flatpak
- Uso da própria API de detecção da plataforma KWindowSystem em vez de duplicar o código

### KXMLGUI

- Uso de 'https' para os URL's do KDE

### NetworkManagerQt

- 8021xSetting: o 'domain-suffix-match' está definido no NM 1.2.0 e posteriores
- Suporte do "domain-suffix-match" no Security8021xSetting

### Plasma Framework

- desenho manual do arco do círculo
- [Menu do PlasmaComponents] Adição do 'ungrabMouseHack'
- [FrameSvg] Optimização do 'updateSizes'
- Não posicionar uma Dialog se for do tipo OSD

### QQC2StyleBridge

- Melhoria da compilação como um módulo estático
- colocação do 'radiobutton' como uma opção exclusiva de facto
- Uso do 'qstyle' para pintar o Dial
- Uso de um ColumnLayout para os menus
- correcção do Dialog
- remoção da propriedade inválida 'group'
- Correcção da formatação do ficheiro 'md' se corresponder aos outros módulos
- comportamento do 'combobox' mais próximo do 'qqc1'
- implementação alternativa para o QQuickWidgets

### Sonnet

- Adição de um método 'assignByDictionnary'
- Assinalar se for possível atribuir um dicionário

### Realce de sintaxe

- Makefile: correcção da correspondência de expressões regulares no "CXXFLAGS+"

### ThreadWeaver

- Limpeza do CMake: Não aplicar de forma forçada o '-std=c++0x'

### Informações de segurança

O código lançado foi assinado com GPG, usando a seguinte chave: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impressão digital da chave primária: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Você pode discutir e compartilhar ideias sobre esta versão na seção de comentários do <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>artigo do Dot</a>.
