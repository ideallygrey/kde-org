---
aliases:
- ../announce-applications-17.04.1
changelog: true
date: 2017-05-11
description: KDE vydává Aplikace KDE 17.04.1
layout: application
title: KDE vydává Aplikace KDE 17.04.1
version: 17.04.1
---
May 11, 2017. Today KDE released the first stability update for <a href='../17.04.0'>KDE Applications 17.04</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to kdepim, dolphin, gwenview, kate, kdenlive, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.32.
