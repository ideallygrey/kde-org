---
aliases:
- ../announce-applications-16.12.3
changelog: true
date: 2017-03-09
description: KDEk, KDE Aplikazioak 16.12.3 kaleratzen du
layout: application
title: KDEk, KDE Aplikazioak 16.12.3 kaleratzen du
version: 16.12.3
---
March 9, 2017. Today KDE released the third stability update for <a href='../16.12.0'>KDE Applications 16.12</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone.

More than 20 recorded bugfixes include improvements to kdepim, ark, filelight, gwenview, kate, kdenlive, okular, among others.

This release also includes Long Term Support version of KDE Development Platform 4.14.30.
