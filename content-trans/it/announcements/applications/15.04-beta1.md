---
aliases:
- ../announce-applications-15.04-beta1
date: '2015-03-06'
description: KDE rilascia Applications 15.04 Beta 1.
layout: application
title: KDE rilascia la prima beta di KDE Applications 15.04
---
6 marzo 2015. Oggi KDE ha rilasciato la beta della nuova versione di KDE Applications. Con il &quot;congelamento&quot; di dipendenze e funzionalità, l'attenzione degli sviluppatori KDE è adesso concentrata sulla correzione degli errori e sull'ulteriore rifinitura del sistema.

Dato che molte applicazioni sono ora basate su KDE Frameworks 5, i rilasci di KDE Applications 15.04 hanno bisogno di una verifica accurata per poter mantenere e migliorare la qualità e l'esperienza utente. Gli utenti &quot;reali&quot; sono fondamentali per mantenere la qualità di KDE, perché gli sviluppatori non possono testare completamente ogni possibile configurazione. Contiamo su di voi per aiutarci a trovare i bug al più presto possibile perché possano essere eliminati prima della versione finale. Valutate la possibilità di contribuire al gruppo di sviluppo installando la versione &quot;beta&quot; <a href='https://bugs.kde.org/'>e segnalando ogni problema</a>.
