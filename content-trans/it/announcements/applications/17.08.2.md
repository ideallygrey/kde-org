---
aliases:
- ../announce-applications-17.08.2
changelog: true
date: 2017-10-12
description: KDE rilascia KDE Applications 17.08.2
layout: application
title: KDE rilascia KDE Applications 17.08.2
version: 17.08.2
---
12 ottobre 2017. Oggi KDE ha rilasciato il primo aggiornamento di stabilizzazione per <a href='../17.08.0'>KDE Applications 17.08</a>. Questo rilascio contiene solo correzioni di errori e aggiornamenti delle traduzioni, e costituisce un aggiornamento sicuro e gradevole per tutti.

Gli oltre 25 errori corretti includono, tra gli altri, miglioramenti a Kontact, Dolphin, Gwenview, Kdenlive, Marble e Okular.

Questo rilascio include inoltre le versioni con supporto a lungo termine di KDE Development Platform 4.14.37.

I miglioramenti includono:

- È stata corretta una perdita di memoria e un arresto anomalo nella configurazione dell'estensione degli eventi di Plasma
- I messaggi letti non vengono più rimossi immediatamente dal filtro Non letto in Akregator
- Lo strumento di importazione di Gwenview ora utilizza la data/ora EXIF
