---
aliases:
- ../../kde-frameworks-5.29.0
date: 2016-12-12
layout: framework
libCount: 70
src: /announcements/frameworks/5-tp/KDE_QT.jpg
---
### Nuovo framework

Questo rilascio include Prison, un nuovo framework per la generazione dei codici a barre (incluso i codici QR).

### Generale

FreeBSD was added to metainfo.yaml in all frameworks tested to work on FreeBSD.

### Baloo

- Performance improvements when writing (4 * speedup for writing out data)

### Icone Brezza

- Make BINARY_ICONS_RESOURCE ON by default
- add vnd.rar mime for shared-mime-info 1.7 (bug 372461)
- add claws icon (bug 371914)
- add gdrive icon instead of a generic cloud icon (bug 372111)
- fix bug "list-remove-symbolic use wrong image" (bug 372119)
- other additions and improvements

### Moduli CMake aggiuntivi

- Skip Python bindings test if PyQt isn't installed
- Only add the test if python is found
- Reduce the CMake minimum required
- Add ecm_win_resolve_symlinks module

### Integrazione della struttura

- find QDBus, needed by appstream kpackage handler
- Let KPackage have dependencies from packagekit &amp; appstream

### KActivitiesStats

- Properly sending the resource linked event

### KDE Doxygen Tools

- Adapt to quickgit -&gt; cgit change
- Fix bug if group name is not defined. Can still break under bad conditions

### KArchive

- Add errorString() method to provide error info

### KAuth

- Add timeout property (bug 363200)

### KConfig

- kconfig_compiler - generate code with overrides
- Properly parse function keywords (bug 371562)

### KConfigWidgets

- Ensure menu actions get the intended MenuRole

### KCoreAddons

- KTextToHtml: fix bug "[1] added at the end of a hyperlink" (bug 343275)
- KUser: Only search for an avatar if loginName isn't empty

### KCrash

- Align with KInit and don't use DISPLAY on Mac
- Don't close all file descriptors on OS X

### KDesignerPlugin

- src/kgendesignerplugin.cpp - add overrides to generated code

### KDESU

- Unsets XDG_RUNTIME_DIR in processes run with kdesu

### KFileMetaData

- Actually find FFMpeg's libpostproc

### KHTML

- java: apply the names to the right buttons
- java: set names in permission dialog

### KI18n

- Check properly pointer inequality from dngettext (bug 372681)

### KIconThemes

- Allow showing icons from all categories (bug 216653)

### KInit

- Set environment variables from KLaunchRequest when starting new process

### KIO

- Ported to categorized logging
- Fix compilation against WinXP SDK
- Allow uppercase checksums matching in Checksums tab (bug 372518)
- Never stretch the last (=date) column in the file dialog (bug 312747)
- Import and update kcontrol docbooks for code in kio from kde-runtime master
- [OS X] make KDE's trash use the OS X trash
- SlaveBase: add documentation about event loops and notifications and kded modules

### KNewStuff

- Add new archive management option (subdir) to knsrc
- Consume the new error signals (set job errors)
- Handle oddity regarding files disappearing when just created
- Actually install the core headers, with CamelCases

### KNotification

- [KStatusNotifierItem] Save / restore widget position during hide / restore it window (bug 356523)
- [KNotification] Allow annotating notifications with URLs

### KPackage Framework

- keep installing metadata.desktop (bug 372594)
- manually load metadata if absolute path is passed
- Fix potential failure if package is not appstream compatible
- Let KPackage know about X-Plasma-RootPath
- Fix generating the metadata.json file

### KPty

- More utempter path searching (including /usr/lib/utempter/)
- Add library path so utempter binary is found in Ubuntu 16.10

### KTextEditor

- Prevent Qt warnings about an unsupported clipboard mode on Mac
- Use syntax definitions from KF5::SyntaxHighlighting

### KTextWidgets

- Don't replace window icons with the result of a failed lookup

### KWayland

- [client] Fix nullptr dereference in ConfinedPointer and LockedPointer
- [client] Install pointerconstraints.h
- [server] Fix regression in SeatInterface::end/cancelPointerPinchGesture
- Implementation of PointerConstraints protocol
- [server] Reduce overhead of pointersForSurface
- Return SurfaceInterface::size in global compositor space
- [tools/generator] Generate enum FooInterfaceVersion on server side
- [tools/generator] Wrap wl_fixed request args in wl_fixed_from_double
- [tools/generator] Generate implementation of client side requests
- [tools/generator] Generate client side resource factories
- [tools/generator] Generate callbacks and listener on client side
- [tools/generator] Pass this as q pointer to Client::Resource::Private
- [tools/generator] Generate Private::setup(wl_foo *arg) on client side
- Implementation of PointerGestures protocol

### KWidgetsAddons

- Prevent crashing on Mac
- Don't replace icons with the result of a failed lookup
- KMessageWidget: fix layout when wordWrap is enabled without actions
- KCollapsibleGroupBox: don't hide widgets, override focus policy instead

### KWindowSystem

- [KWindowInfo] Add pid() and desktopFileName()

### Icone Oxygen

- Add application-vnd.rar icon (bug 372461)

### Plasma Framework

- Check for metadata validity in settingsFileChanged (bug 372651)
- Don't flip tabbar layout if vertical
- Remove radialGradient4857 (bug 372383)
- [AppletInterface] Never pull focus away from fullRepresentation (bug 372476)
- Fix SVG icon ID prefix (bug 369622)

### Solid

- winutils_p.h: Restore compatibility with WinXP SDK

### Sonnet

- Also search for hunspell-1.5

### Evidenziazione della sintassi

- Normalize XML license attribute values
- Sync syntax definitions from ktexteditor
- Fix folding region merging

### Informazioni di sicurezza

Il codice rilasciato è stato firmato con GPG utilizzando la chiave seguente: pub rsa2048/58D0EE648A48B3BB 2016-09-05 David Faure &lt;faure@kde.org&gt; Impronta della chiave primaria: 53E6 B47B 45CE A3E0 D5B7  4577 58D0 EE64 8A48 B3BB

Puoi discutere e condividere idee su questo rilascio nella sezione dei commenti nell'<a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>articolo sul Dot</a>.
