<?php

require("www_config.php");

$year = (int)$_GET["year"];
if ($year < 2001 || $year > 2030) {
	echo "year check failed";
	exit(1);
}

// initialise
for ($i = 1; $i <= 12; $i++) {
	$i < 10 ? $i_s = "0".$i : $i_s = $i;
	$data[ $year."-".$i_s ] = 0;
}
// $dbConnection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );//Error Handling

$stmt = $dbConnection->prepare("SELECT SUM(amount) as don, DATE_FORMAT(date,\"%Y-%m\") as month from donations WHERE YEAR(date) = :year GROUP BY month ORDER BY month DESC");

$stmt->execute([
    'year' => $year,
]);

while ($row = $stmt->fetch()) {
	$data[ $row["month"] ] = $row["don"];
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link href="https://cdn.kde.org/aether-devel/bootstrap.css" rel="stylesheet">
</head>
<body>
<div class="container">
<canvas id="myChart" width="400" height="200"></canvas>
<script src="https://cdn.kde.org/aether-devel/charjs.js"></script>
<script>
let ctx = document.getElementById('myChart');
new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
        datasets: [{
            label: "Donation for the year <?= $year ?>",
            data: [<?php foreach ($data as $month) { echo($month . ","); } ?>],
            fill: false,
        }],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
</div>
</body>
</html>
